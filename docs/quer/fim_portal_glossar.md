---
sidebar_label: FIM-Portal Begriffserklärungen.
---

# FIM Portal Begriffserklärungen

Das neue FIM-Portal verwendet wenig überraschend viele Fachbegriffe aus der FIM-Methodik. Die meisten finden sich im Glossar, zusätzliche Datenfeldbezeichnungen oder Referenzen auf zugrundeliegende XÖV-Standards werden hier erläutert.

#### Datum der Erstellung {#datum_der_erstellung}
Bei Leistungs-Steckbriefen gibt es ein Datum der Erstellung, dieses ist das Datum der erstmaligen Erstellung des Datenbankeintrags im Leika und nicht notwendigerweise identisch mit dem Datum der fachlichen Erstellung des Steckbriefs.

### Neueste zuerst {#neueste_zuerst}
Bei der Sortierung von Suchergebnissen für Steckbriefe kann nach "Neueste zuerst" sortiert werden; gemeint ist damit eine Sortierung nach dem neuesten [Datum der Erstellung](#datum-der-erstellung).



## Weiterführende Informationen:
- [Glossar](../glossar.md)
- [neues FIM-Portal](https://schema.fim.fitko.net)