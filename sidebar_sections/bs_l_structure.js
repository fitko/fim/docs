// 
const lz_structure = [
	{
		'type': 'category',
		'label': 'Baustein Leistungen (new)',
		'items': [
			{
				'type': 'category',
				'label': 'Einleitung',
				'items': [
					// Einleitende Dokuemtne zum Baustein Leistungen
					'leistung/leistungqskriterien/Einleitung',	
				]
			},
			{
				'type': 'category',
				'label': 'Fachkonzept',
				'items': [
					// Die einzelnen Dokumente des Fachkonzepts BS Leistungen,
					{
						type: 'category',
						label: 'Einleitung',
						collapsed: true,
						items: [
						  'leistung/leikahandbuch/Einleitung/GrundsaetzlicheZielsetzung',
						  'leistung/leikahandbuch/Einleitung/Handlungsgrundlagen',
						  'leistung/leikahandbuch/Einleitung/ZieledesDokuments',
						],
					  },
					  {
						type: 'category',
						label: 'Anwendungsbeteiligte und Rollen',
						collapsed: true,
						items: [
						  'leistung/leikahandbuch/2 Anwendungsbeteiligte und Rollen/2.1 Geschäftsstelle des IT-Planungsrats GS-ITPLR',
						  'leistung/leikahandbuch/2 Anwendungsbeteiligte und Rollen/2.2 Fachgruppe LeiKa-plus',
						  'leistung/leikahandbuch/2 Anwendungsbeteiligte und Rollen/2.3 Geschäfts- und Koordinierungsstelle LeiKa GK LeiKa',
						  'leistung/leikahandbuch/2 Anwendungsbeteiligte und Rollen/2.4 Geschäfts- und Koordinierungsstelle 115',
						  'leistung/leikahandbuch/2 Anwendungsbeteiligte und Rollen/2.5 Bundesbehörden Soll-Stand',
							],
					  },
					  {
						type: 'category',
						label: 'Definition einer Leistung',
						collapsed: true,
						items: [
						  'leistung/leikahandbuch/3 Definition einer Leistung/3',
							],
					  },
					  {
						type: 'category',
						label: 'LeiKa-Struktur',
						collapsed: true,
						items: [
						  'leistung/leikahandbuch/4 LeiKa-Struktur/4',
							],
					  },
					  {
						type: 'category',
						label: 'Inhalt des LeiKa, Stammtext- und Ergänzungsmodell',
						collapsed: true,
						items: [
						  'leistung/leikahandbuch/5 Inhalt des LeiKa, Stammtext- und Ergänzungsmodell/5',
						  {
							type: 'category',
							label: 'Begriffsdefinitionen',
							collapsed: true,
							items: [
							  'leistung/leikahandbuch/5 Inhalt des LeiKa, Stammtext- und Ergänzungsmodell/5.1 Begriffsdefinitionen/5.1.1 Leistungsbericht',
							  'leistung/leikahandbuch/5 Inhalt des LeiKa, Stammtext- und Ergänzungsmodell/5.1 Begriffsdefinitionen/5.1.2 Modul',
							  'leistung/leikahandbuch/5 Inhalt des LeiKa, Stammtext- und Ergänzungsmodell/5.1 Begriffsdefinitionen/5.1.3 Befüllung von Modulen gemäß Typisierung der Leistungen',
								],
						  },
						  'leistung/leikahandbuch/5 Inhalt des LeiKa, Stammtext- und Ergänzungsmodell/5.2 Bereitstellung von Inhalten über den LeiKa',
						  'leistung/leikahandbuch/5 Inhalt des LeiKa, Stammtext- und Ergänzungsmodell/5.3 Qualitätskriterien',
							],
					  },
					  {
						type: 'category',
						label: 'Pflegeprozesse des LeiKa',
						collapsed: true,
						items: [
						  'leistung/leikahandbuch/6 Pflegeprozesse des LeiKa/6.1 Ersterstellung, Änderung, Außerkrafttreten, Monitoring',
						  'leistung/leikahandbuch/6 Pflegeprozesse des LeiKa/6.2 Freigabe',
						  'leistung/leikahandbuch/6 Pflegeprozesse des LeiKa/6.3 Benachrichtigung der Nutzer über erfolgte Änderungen',
						  'leistung/leikahandbuch/6 Pflegeprozesse des LeiKa/6.4 Versionierung',
							],
					  },
					  {
						type: 'category',
						label: 'Technische Bereitstellung und Visualisierung des LeiKa',
						collapsed: true,
						items: [
						  'leistung/leikahandbuch/7 Technische Bereitstellung und Visualisierung des LeiKa/7.1 Datenimport',
						  'leistung/leikahandbuch/7 Technische Bereitstellung und Visualisierung des LeiKa/7.2 Webservice',
						  'leistung/leikahandbuch/7 Technische Bereitstellung und Visualisierung des LeiKa/7.3 Leistungsviewer',
						  'leistung/leikahandbuch/7 Technische Bereitstellung und Visualisierung des LeiKa/7.4 LeiKa-plus Portal',
							],
					  }
					
				]
			},
			{
				'type': 'category',
				'label': 'Erstellungsprozess',
				'items': [
					{
						'type': 'category',
						'label': 'QS-Kriterien',
						'items': [
						{
						  type: 'category',
						  label: 'Überblick',
						  collapsed: true,
						  items: [
							// Die QS-Kriterien BS Leistungen
							'datenfelder/VB_Einleitung',
						  ],
						},
					]},

				]

			},
			{
				'type': 'category',
				'label': 'QS-Kriterien',
				'items': [
				{
				  type: 'category',
				  label: 'Überblick',
				  collapsed: true,
				  items: [
					// Die QS-Kriterien BS Leistungen
					'datenfelder/VB_Einleitung',
				  ],
				},
			]},
			{
				'type': 'category',
				'label': 'Organisation',
				'items': [
					{
						// Überblick zu organisatorischen Themen (Gremien, Austauschrunden, etc.)
						'type': 'category',
						'label': 'Gremien und Arbeitsgruppen',
						'items': [
							// Gremien und Arbeitsgruppen BS Leistungen (z.B. Redaktionszirkel, UAG Leistungen)
							'datenfelder/ME-Austausch/uebersicht',
						]
					},
			]},
			{
				'type': 'category',
				'label': 'Technik',
				'items': [
				{
				  type: 'category',
				  label: 'Überblick',
				  collapsed: true,
				  items: [
					// Einleitende Dokumente zu Schnittstellen und Technik BS Leistungen
					'datenfelder/VB_Einleitung',
				  ],
				},
				{
					'type': 'category',
					'label': 'Schnittstellen',
					'items': [
						{
							'type': 'category',
							'label': 'XZuFi-Schnittstelle',
							'items': [	
								'schema-repository/about-this-doc',
							]
						},
						{
							'type': 'category',
							'label': 'PVOG-Schnittstelle',
							'items': [	
								'schema-repository/about-this-doc',
							]
						},
						{
							'type': 'category',
							'label': 'Schnittstellen Landesebene',
							'items': [	
								'schema-repository/about-this-doc',
							]
						},
		
					]
				},
			]},

		]
		},
]

    // export the structure to enable use in other files
module.exports = lz_structure;