## Leistungsschlüssel
Inhalt: Ziffernfolge zur eindeutigen Identifikation der Leistung (Zu Aufbau des Leistungsschlüssels und Bedeutung seiner Bestandteile siehe Fachkonzept FIM Baustein Leistungen (noch Handbuch Leika-plus; [FIM-Dokumente](https://fimportal.de/fim-haus)))

Wird automatisch beim Anlegen des Leistungssteckbriefs vom Redaktionssystem des Bausteins Leistungen erzeugt und im Musterformular Leistungszuschnitt dokumentiert.

|                       |      |
| --------------------- | ---- |
| Leistungssteckbrief   | Ja   |
| Stammtext             | MUSS |
| Leistungsbeschreibung | MUSS |
| erforderlich für SDG  | Nein |
| Erforderlich für PVOG | Ja   |
#### Konstanter Leistungsschlüsse
Ein einmal zu einer Leistung vergebener Leistungsschlüssel soll über den Lebenszyklus des Leistungssteckbriefs konstant bleiben. (Beschluss der FG FIM)

Änderungsbedarfe an den Modulen \[Leistungsgruppierung\] und \[Verrichtungskennung\] würden zu einem veränderten Leistungsschlüssel führen.

Wenn Änderung an \[Leistungsgruppierung\] und \[Verrichtungskennung\] erforderlich sind, wird ein neuer Katalogeintrag (mit den geänderten Werten) mit einem neuen Leistungsschlüsseln erzeugt. D. h., semantische Änderungen an der Leistungsbezeichnung I sind nur über einen neuen Leistungszuschnitt möglich.

Neben dem vom Bausteins Leistungen erzeugten Leistungsschlüssel, der immer mit der Instanz „99“ beginnt, können in den dezentralen Redaktionssystemen (Landeserdaktionssystemen) redaktionsinterne (landesspezifische) IDs erzeugt werden.
## Leistungsbezeichnung (synonymhaft Leistungsbezeichnung I)
Inhalt: Bezeichnung der Leistung aus Sicht der Verwaltung.

Die Leistungsbezeichnung wird aus Leistungskennung, Verrichtungskennung und ggfs. vorhandenem Verrichtungsdetail gebildet.

Format: \[Leistungskennung\]\[Leerzeichen\]\[Verrichtungskennung\]\[Leerzeichen\]\[Verrichtungsdetail\]

\[Verrichtungskennung\] ist ein aus der Codeliste der Verrichtungskennungen auswählbarer Wert

\[Leistungskennung\] und \[Verrichtungsdetail\] können selbst vergeben werden. Nennen Sie Rechts-/Handlungsgrundlagen/Paragraphen nicht in der Leistungsbezeichnung.

|                       |      |
| --------------------- | ---- |
| Leistungssteckbrief   | Ja   |
| Stammtext             | MUSS |
| Leistungsbeschreibung | MUSS |
| erforderlich für SDG  | Nein |
| Erforderlich für PVOG | Ja   |
Beispiel: 
Leistungskennung: Personalausweis 
Verrichtungskennung: Ausstellung 
Verrichtungsdetail: neu wegen Ablauf der Gültigkeit 

„Personalausweis Ausstellung neu wegen Ablauf der Gültigkeit“

**\[Leistungskennung\]**
Die Leistungskennung ist das zentrale Merkmal der Leistung und stellt den Regelungsgegenstand der Leistung dar. 

Textlänge: unbegrenzt 

Beispiel: Personalausweis

**\[Verrichtungskennung\]**
Die Verrichtungskennung beschreibt das Ergebnis des Verwaltungshandelns in Bezug auf die Leistungskennung.

Verrichtungskennungen werden standardisiert in Form von Substantivierungen vorgegeben - Elemente einer Codeliste.

[Codeliste](https://www.xrepository.de/details/urn:de:fim:leika:verrichtung)

Die Kennungen bestehen aus einem 3-stelligen nummerischen Code sowie der Bezeichnung der Verrichtung selbst.

Beispiel: 012 – Ausstellung 

**\[Verrichtungsdetail\]**
Das Verrichtungsdetail spezifiziert die Verrichtungskennung insbesondere in Bezug auf verschiedene Verfahrensabläufe, Zielgruppen oder Ausnahmen innerhalb einer Leistung.

Verrichtungsdetails werden zu jeder Verrichtung eines Leistungsobjektes fortlaufend vergeben. 

Textlänge: unbegrenzt 

Beispiel: neu wegen Ablauf der Gültigkeit
## Leistungsbezeichnung II 

Inhalt: Bezeichnung der Leistung aus Sicht der Nutzenden (Bürgerinnen/Bürger/ Unternehmen) 

Textlänge: 150 Zeichen 

Nennen Sie Rechts-/Handlungsgrundlagen/Paragraphen nicht in der Leistungsbezeichnung II.

|                       |      |
| --------------------- | ---- |
| Leistungssteckbrief   | Ja   |
| Stammtext             | MUSS |
| Leistungsbeschreibung | MUSS |
| erforderlich für SDG  | Nein |
| Erforderlich für PVOG | Ja   |

Formulieren Sie die Leistungsbezeichnung Il prägnant und allgemeinverständlich. Bezeichnen Sie die Leistung nach Möglichkeit mit Begriffen aus der Alltagssprache. Sie muss aus Sicht der Zielgruppe formuliert werden - was muss diese tun?

\[Verrichtung\] \[Leistungskennung\] Verb und/oder \[Leistungskennung\] \[Verrichtung\] Verb

Beispiel: für die Leistungsbezeichnung „Hundesteuer Festsetzung" *„Hund anmelden"*

Beispiel: für die Leistungsbezeichnung „Führerschein Ausstellung Internationaler Führerschein" *"Internationalen Führerschein beantragen"*

Verwenden Sie nach Möglichkeit die Paarform (weibliche/ männliche Formulierung) geschlechterneutral. Wenn das nicht möglich oder sehr kompliziert ist, verwenden Sie nur die männliche Personenbezeichnung.

**Beispiel**: statt: *„Anerkennung als Rechtsanwalt"* besser: *„Anerkennung als Rechtsanwältin oder*
*Rechtsanwalt"*

Die Bezeichnung soll mit einem Substantiv beginnen

**Beispiel**: statt: *„den Hund anmelden"* besser: *„Hund anmelden"*

**Weitere Beispiele**: *„Alkoholsteuer bezahlen", „Bei der Bundesnetzagentur über Post- und Paketdienstleister beschweren", „Post- oder Kuriersendung bis EUR 150,00 beim Zoll anmelden"*
##### Mundart
regional geprägte Informationen können im Modul „Leistungsbezeichnung Il" erfasst werden.
## Leistungsadressat
#### Empfänger der Leistung
Inhalt: Kennzeichnung des Empfängers / der Empfänger der Leistung. Um eine eindeutige Unterscheidung der Empfänger vornehmen zu können, kann im Leistungssteckbrief das vorhandene Modul „Leistungsadressat“ genutzt werden.

„Bürger“, „Unternehmen“, „Verwaltung“, „Behördenintern“ 

|                       |      |
| --------------------- | ---- |
| Leistungssteckbrief   | Nein |
| Stammtext             | SOLL |
| Leistungsbeschreibung | SOLL |
| erforderlich für SDG  | Nein |
| Erforderlich für PVOG | Nein |
 ZU KLÄREN: FOLGENDE ZWEI FUßNOTEN:
 1 wenn Leistungsadressat Bürger bzw. Unternehmen 
 2 wenn Leistungsadressat Bürger bzw. Unternehmen 
 
 Mit dem Element Leistungsadressat können folgende Beziehungen – auch über eine Mehrfachauswahl – erfasst und ausgedrückt werden:
 
 G2B „Bürger“
 G2C „Unternehmen“
 G2G „Verwaltung“ (verwaltungsintern - behördenübergreifend)
 Bei einer G2G-Leistungen handelt es sich um eine verwaltungsinterne Leistung
 G „Behördenintern“ (verwaltungsintern - behördenintern)
 Bei einer G-Leistung handelt es sich um eine verwaltungsinterne Leistung Empfänger der Leistung
 
 Sie können durch die Benutzung des Elements Leistungsadressat die Leistungseinträge kennzeichnen, für die jeweils eine Leistungsbeschreibung erfasst wurde.
 
 Nachverarbeitende Systeme können so ermitteln, wo Leistungsbeschreibungen gefunden werden können.
 
 Für den Leistungsadressat ist eine Mehrfachauswahl möglich.

#### Begriffe im Kontext (Synonyme)
Inhalt: Synonyme und andere Begriffe, unter denen die Leistung typischerweise gesucht wird oder bei einer Stichwortsuche gefunden werden soll.

|                       |      |
| --------------------- | ---- |
| Leistungssteckbrief   | Ja   |
| Stammtext             | MUSS |
| Leistungsbeschreibung | MUSS |
| erforderlich für SDG  | Nein |
| Erforderlich für PVOG | Nein |
Tragen Sie mehrere, vor allem umgangssprachliche oder regionale Begriffe ein wie: 

„Fleppe“; „Perso“; „Behindertenausweis“; „Flaschner“; „Herdprämie“.
#### Redundanz
Erfassen Sie Synonyme nicht doppelt zu den Begriffen, die sich bereits in der Leistungsbezeichnung / Leistungsbezeichnung II finden
#### Singular/Plural
Formulieren Sie die Begriffe im Kontext je nach Bedarf im Singular oder im Plural.
#### Sonderzeichen
Verwenden Sie nur Buchstaben, Ziffern, und nach Möglichkeit keine Sonderzeichen
## Teaser
#### Anreißertext
Inhalt: kurze Beschreibung der Leistung Teaser sind Anreißertexte, die die Lesenden zum Weiterlesen verleiten sollen.

|                       |      |
| --------------------- | ---- |
| Leistungssteckbrief   | Nein |
| Stammtext             | MUSS |
| Leistungsbeschreibung | MUSS |
| erforderlich für SDG  | Ja   |
| Erforderlich für PVOG | Nein |
Teaser-Texte sollen aus max. 2 kurzen Sätzen bestehen.

Textlänge: 280 Zeichen

Teaser soll in knapper Form die wichtigsten Informationen enthalten damit die Leistungsadressaten entscheiden können, ob das die Leistung ist, die sie suchen.
## Volltext
Inhalt: ausführliche Beschreibung der Leistung

|                       |      |
| --------------------- | ---- |
| Leistungssteckbrief   | Nein |
| Stammtext             | MUSS |
| Leistungsbeschreibung | MUSS |
| erforderlich für SDG  | Ja   |
| Erforderlich für PVOG | Ja   |
Beantworten Sie folgende Fragen der Bürgerin oder des Bürgers: „Betrifft mich das?“ und „Was habe ich davon?“ Vermeiden Sie zusätzliche Information, die nichts mit dem Anliegen zu tun haben.

Erfassen Sie hier keine Informationen, für die es eigene Module gibt (zum Beispiel Gebühren, Links auf weiterführende Informationen oder vorzulegenden Unterlagen).
#### 5.000 Zeichen
höchstens 5.000 Zeichen (einschließlich Leerzeichen und Satzzeichen)
## Kurztext
Inhalt: Kurzbeschreibung der Leistung zur Beauskunftung durch die 115

Erfassen Sie hier die Informationen für telefonische Auskünfte durch die Agenten der 115.

Strukturierung stichpunktartig:
- 1. Spiegelstrich: ganzer Titel der Leistung, also Leistungsbezeichnung II
- Spiegelstrich 2-x: wesentliche Informationen aus der Leistungsbeschreibung nach Relevanz sortiert letzter Spiegelstrich: zuständige Behörde
#### 1.800 Zeichen
höchstens 1.800 Zeichen (einschließlich Leerzeichen, ohne Satzzeichen)

Der Inhalt des Moduls „Kurztext“ wird nicht in Verwaltungsportalen veröffentlicht. Dieser soll nur den Agenten der 115 zur Verfügung stehen.
## Handlungsgrundlage(n)
Inhalt: Handlungsgrundlagen, die für die Leistung relevant sind; möglichst verlinkt. Hierzu gehören auch Gebührenverordnungen, etc.

Erfassen Sie die Verlinkung als externen Link.

Die Übersicht der Handlungsgrundalge(n) finden Sie in der Codelisteneinträge Handlungsgrundlagenart zum Standard XProzess:[[https://www.xrepository.de/details/urn:xoev-de:mv:em:codeliste:xprozess:handlungsgrundlagenart]]

|                       |                               |
| --------------------- | ----------------------------- |
| Leistungssteckbrief   | Ja (Bei Typ 1 - 3 Leistungen) |
| Stammtext             | SOLL                          |
| Leistungsbeschreibung | SOLL                          |
| erforderlich für SDG  | Nein                          |
| Erforderlich für PVOG | Nein                          |

Nennen Sie die Anspruchs- oder Eingriffsnorm.

Verweisen Sie auf die konkreten Paragrafen, Artikel, Absätze, Sätze oder Nummern der Gesetze/Vorschriften/etc. Wenn mehrere Gesetze, Verordnungen usw. einschlägig sind, verwenden Sie pro Normenkörper einen Listenpunkt. Sortieren Sie die Punkte nach Wichtigkeit für die Verwaltungsleistung.

Verlinken Sie bei Bundesrecht auf eine Seite von [[https://www.gesetze-im-internet.de/ ]] oder die Webseite des jeweils Rechtsetzenden. Bei Unionsrecht auf EUR-LEX[EUR-LEX](https://eur-lex.europa.eu/homepage.html?locale=de) und bei Landesrecht bzw. kommunalem Satzungsrecht die jeweils offizielle Webseite.

Verlinken Sie auf das Inhaltsverzeichnis oder einen Abschnitt, wenn mehrere Paragrafen, Artikel, Absätze, Sätze oder Nummern einschlägig sind.

Verwenden Sie als Linktext den Titel der verlinkten Norm einschl. konkretem Paragrafen, Artikel, Absätze, Sätze oder Nummern

Vermeiden Sie, Links zu erfassen, die das Präfix „http“ tragen.

Formate:
- § 3 Absatz 1 vollständiger Titel (Kurztitel - Abkürzung)
- §§ 3, 6 vollständiger Titel (Kurztitel - Abkürzung)
- §§ 3 bis 6 vollständiger Titel (Kurztitel - Abkürzung)
- vollständiger Titel (Kurztitel - Abkürzung)

Verwenden Sie beim Zitieren mehrerer Normen des gleichen Gesetzes nacheinander das doppelte Paragraphenzeichen. Das gilt nicht, wenn neben den Paragrafen auch Absätze Nummern, etc. zitiert werden.

Geben Sie den vollständigen Titel an. Kurztitel und Abkürzung in Klammer angeben. Schließen Sie kleine Buchstaben direkt der Ziffer an.

In einigen Fällen ist der vollständige Titel nicht geeignet. Der Bürger kann mit dem Kurztitel dann meist mehr anfangen. Hier sollte darauf geachtet werden, dass der Linktext dann nicht zu lang wird.

Beispiel: statt: *Bundesgesetz über individuelle Förderung der Ausbildung (Bundesausbildungsförderungsgesetz - BAföG)* besser: *Bundesausbildungsförderungsgesetz*
## Erforderliche Unterlagen
Inhalt: Welche Unterlagen sind nötig, um das Verfahren durchzuführen.

|                       |      |
| --------------------- | ---- |
| Leistungssteckbrief   | Nein |
| Stammtext             | SOLL |
| Leistungsbeschreibung | MUSS |
| erforderlich für SDG  | Ja   |
| Erforderlich für PVOG | Nein |
Wenn bei der Erstellung der Leistungsbeschreibung klar ist, dass wirklich keine Unterlagen erforderlich sind, schreiben Sie z. B. schlicht „keine“ (z. B.: beim Melden von Schlaglöchern).

Wenn mehr als eine Unterlage erforderlich ist, dann muss eine stichpunktartige Aufzählung verwendet werden (siehe Punkt 1.7).

Soweit für erforderliche Unterlagen amtliche Vordrucke existieren, sollten diese unter dem Abschnitt 3 Formulare (offline) bereitgestellt werden

Machen Sie deutlich, wenn eine Unterlage nicht in jedem Fall erforderlich ist.

**Beispiele**:
- Personalausweis
- Bei Vertretung: schriftliche Vollmacht
## Voraussetzungen
Inhalt: Welche rechtlichen Voraussetzungen erfordert das Verfahren?

|                       |      |
| --------------------- | ---- |
| Leistungssteckbrief   | Nein |
| Stammtext             | SOLL |
| Leistungsbeschreibung | MUSS |
| erforderlich für SDG  | Ja   |
| Erforderlich für PVOG | Nein |
Wenn bei der Erstellung der Leistungsbeschreibung klar ist, dass wirklich keine Voraussetzungen zu erfüllen sind, schreiben Sie z. B. schlicht „keine“.

Nutzen Sie ggf. Aufzählungen, wenn mehr als eine Voraussetzung zu erfüllen ist (siehe Punkt 1.7).

Machen Sie deutlich, wenn eine Voraussetzung nicht in jedem Fall erfüllt sein muss.

**Beispiele**:
- Schwerbehinderung mit einem Grad von 70 oder mehr
- Merkzeichen „aG“ (außergewöhnliche Gehbehinderung)
- Bei Minderjährigen: Zustimmung der Eltern
## Kosten
Inhalt: Welche Kosten entstehen Bürgerinnen und Bürger oder Unternehmen für die Leistung? Welche Gebühren fallen an? 

Erfassen Sie die Daten für Kosten strukturiert.

|                       |      |
| --------------------- | ---- |
| Leistungssteckbrief   | Nein |
| Stammtext             | SOLL |
| Leistungsbeschreibung | MUSS |
| erforderlich für SDG  | Ja   |
| Erforderlich für PVOG | Nein |
Geben Sie die Kosten so konkret wie möglich an. Wenn Sie die Kosten nicht genau beziffern können, geben Sie einen Kostenrahmen an. 

In solchen Fällen kann konkret angegeben werden, von welchen Faktoren die Kosten abhängen. Welche Faktoren das sind, ist jeweils von der Leistung abhängig.

**Beispiel**: „Die Kosten richten sich nach der Größe des Grundstücks, das genutzt werden soll.
#### Nachkommastellen
Wenn der Betrag **kleiner als EUR 1.000** ist, geben Sie **immer Nachkommastellen** an. **Glatte Beträge ab EUR 1.000** werden **ohne Nachkommastelle** angegeben. Wenn Sie einen Kostenrahmen angeben möchten, müssen Sie beide Beträge mit oder beide ohne Nachkommastelle schreiben.
Beispiele: 1,00 EUR, 700,00 EUR, 2.000 EUR, 2.000,99 EUR, 2.000,00 EUR – 7.999,90 EUR

Schreiben Sie Euro, Cent und Prozent im Fließtext aus, wenn sie nicht im Zusammenhang mit einer Betrags- oder Prozentangabe in Ziffern stehen.

Schreiben Sie innerhalb eines Kostenrahmens beide Beträge mit Nachkommastellen.

Verwenden Sie „EUR“ als Währungsbezeichnung. Setzen Sie zwischen „EUR“ und Betrag ein geschütztes Leerzeichen. EUR muss nach dem Betrag stehen.

Formate: 

Fixe Kosten: \[Kostenart\]: \[Betrag\] EUR
Kostenrahmen: \[Kostenart\]: \[Betrag\] – \[Betrag\] EUR

**Beispiele**:
- Abgabe: 1.275,50 EUR
- Benutzungsgebühr: 1.200 – 1.750 EUR
## Verfahrensablauf
Inhalt: Was müssen Bürgerinnen und Bürger sowie Unternehmen tun, um das Verfahren anzustoßen? Wie endet das Verfahren?

|                       |      |
| --------------------- | ---- |
| Leistungssteckbrief   | Nein |
| Stammtext             | SOLL |
| Leistungsbeschreibung | MUSS |
| erforderlich für SDG  | Ja   |
| Erforderlich für PVOG | Nein |

Machen Sie deutlich, ob, wann und wie Bürgerinnen und Bürger oder Unternehmen handeln müssen.

Beschreiben Sie kurz wichtige Verfahrensschritte und Beteiligte.
- wichtige Verfahrensschritte sollen verständlich gemacht werden
- den Verfahrensablauf kurz mit einem Satz einleiten, dann Schritt für Schritt erklären, was zu tun ist
- hier nur schriftliches (analoges) Verfahren näher beschreiben
- Ablauf bei elektronischer Antragstellung bitte im Abschnitt „Online-Dienst“ im Feld „Hilfetext“ erläutern

Nutzen Sie Aufzählungen (siehe Punkt 2.1.8), wenn es mehr als einen Verfahrensschritt gibt.

Beispiel:

Kurzarbeitergeld können Sie schriftlich oder online beantragen. Das Verfahren ist zweistufig: Zuerst zeigen Sie den Arbeitsausfall an, dann stellen Sie den Antrag.
Wenn Sie das Kurzarbeitergeld schriftlich beantragen wollen:

- Sie kündigen Ihren Beschäftigten die Kurzarbeit an. Häufig wird hierzu eine Betriebsvereinbarung mit dem Betriebsrat geschlossen. Gibt es keinen Betriebsrat, müssen Sie von allen Beschäftigten, die von der Kurzarbeit betroffen sein werden, das Einverständnis einholen.
- Sie zeigen die Kurzarbeit schriftlich bei der zuständigen Dienstelle der Agentur für Arbeit (Anzeige über Arbeitsausfall) an.
- Die Agentur für Arbeit prüft die Anzeige und entscheidet, ob Kurzarbeitergeld bewilligt werden kann.
- Sie berechnen monatlich die Lohn- und Gehaltszahlungen sowie das Kurzarbeitergeld
- Sie zahlen das Arbeitsentgelt für geleistete Arbeitsstunden und das Kurzarbeitergeld für ausgefallene Stunden an Ihre Beschäftigten aus und entrichten die Sozialversicherungsbeiträge.
- Sie beantragen monatlich rückwirkend die Erstattung des Kurzarbeitergeldes bei der zuständigen Dienststelle der Agentur für Arbeit (Antrag auf Kurzarbeitergeld und Abrechnungsliste).
- Die Agentur für Arbeit prüft den Antrag und die Abrechnungsliste, bewilligtes Kurzarbeitergeld wird überwiesen.
- Nach Abschluss der Kurzarbeit prüft die Agentur für Arbeit die eingereichten Abrechnungen und korrigiert, falls erforderlich, das bewilligte Kurzarbeitergeld. 

Wenn Sie das Kurzarbeitergeld online beantragen wollen:

- Sie reichen die Anzeige über Arbeitsausfall, den Antrag auf Kurzarbeitergeld und gegebenenfalls weitere Dokumente über das Online-Portal „eServices“ der Bundesagentur für Arbeit ein.
- Die restlichen Verfahrensschritte entsprechen dem schriftlichen Verfahren.
## Bearbeitungsdauer
Inhalt: Wie lange dauert es, bis das Verfahren abgeschlossen ist? (auch durch Genehmigungsfiktionen)

|                       |      |
| --------------------- | ---- |
| Leistungssteckbrief   | Nein |
| Stammtext             | SOLL |
| Leistungsbeschreibung | MUSS |
| erforderlich für SDG  | Ja   |
| Erforderlich für PVOG | Nein |

Geben Sie die Bearbeitungsdauer an, wenn diese gesetzlich geregelt ist. Wenn sie nicht geregelt sind, geben Sie mindestens „keine“, o.ä. an.

Nutzen Sie Aufzählungen (siehe Punkt 2.1.8), wenn es mehrere Bearbeitungsdauern gibt.

In der Praxis hat es sich als hilfreich erwiesen, wenn den Fachbereich um zumindest eine grobe Einschätzung zu bitten und diese zu erfassen. Bewegt sich die Bearbeitungsdauer bspw. in einem Zeitraum von Wochen oder Monaten?

Bei vielen Leistungen ist aus Bürgersicht eher die Bearbeitungsdauer des Antrags bis zum nächsten wichtigen Verfahrensschritt relevant, während das dahinter liegende Verfahren sehr viel länger dauern kann.

Förderverfahren beispielsweise werden an sich erst nach vielen Jahren abgeschlossen; relevant ist aber oft, wie lange es z.B. von Antrag bis zum Bescheid bzw. der Ausreichung der Fördermittel braucht.

In der strukturierten Erfassung stehen Ihnen Intervalle oder die Angabe der Dauer mit Startdatum und Enddatum zur Verfügung.

**Beispiel**: Dauer: 5 Werktage
## Fristen
Inhalt: Welche Fristen müssen Bürgerinnen und Bürger sowie Unternehmen beachten?

Hinweis für Redakteurinnen/Redakteure: Ihnen stehen sowohl die strukturierte als auch die Erfassung als Freitext zur Verfügung. Erfassen Sie bitte strukturiert.

|                       |      |
| --------------------- | ---- |
| Leistungssteckbrief   | Nein |
| Stammtext             | SOLL |
| Leistungsbeschreibung | MUSS |
| erforderlich für SDG  | Ja   |
| Erforderlich für PVOG | Nein |
**Beispiel**: 
- Geltungsdauer: vom 01.01. – 15.01. monatlich

Geben Sie die Fristen an, wenn diese gesetzlich geregelt sind. Wenn keine Fristen geregelt sind, geben Sie mindestens „keine“, o.ä. an.

Sie können in der strukturierten Erfassung mehrere Fristen angeben, wenn dies erforderlich ist.
## Hinweise zu Formularen und Online-Diensten (Modul Formulare)
Inhalt: allgemeine Angaben zu Formularen und Online-Diensten

|                       |      |
| --------------------- | ---- |
| Leistungssteckbrief   | Nein |
| Stammtext             | SOLL |
| Leistungsbeschreibung | MUSS |
| erforderlich für SDG  | Ja   |
| Erforderlich für PVOG | Nein |
Es ist nur Text zulässig. Geben Sie an, ob
- der Antrag analog und/oder elektronisch eingereicht werden kann
- der Antrag formlos gestellt werden kann
- die Schriftform erforderlich ist
- persönliches Erscheinen nötig ist
Informationen und Links zu Formularen und Online-Diensten geben Sie bitte in den jeweils dafür vorgesehenen Objekten / Abschnitten an.

Wenn bei einer Leistung in diesen Feldern keine klare ja/nein- Antwort möglich ist, können stichpunktartige Hinweise gegeben werden: 

**Beispiel**: „Persönliches Erscheinen nötig:
- bei Antragstellung: nein
- bei eidesstattlicher Erklärung: ja


Beispiel: 
Formulare/Online-Dienste vorhanden: ja
Schriftform erforderlich: ja
Formlose Antragsstellung möglich: nein
Persönliches Erscheinen nötig: nein

zur Definition des Onlinedienstes finden Sie hier [2.2 Digitale Services im Sinne des OZG - OZG-Leitfaden](https://leitfaden.ozg-umsetzung.de/display/OZG/2.2+Digitale+Services+im+Sinne+des+OZG):

Als Online Service wird eine elektronisch angebotene bzw. digital verfügbare Verwaltungsleistung verstanden. Berücksichtigt werden in dieser Übersicht zukünftig geplante, aktuell in der Entwicklung befindliche oder bereits fertige Dienste. Diese Services entsprechen mindestens Reifegrad 2 der Online-Verfügbarkeit von Verwaltungsleistungen: Das heißt, eine Online-Beantragung ist grundsätzlich möglich. Online Services, die aus dem OZG-Programm heraus entwickelt wurden und für eine Nachnutzung durch andere Länder und Kommunen nach dem Modell „Einer für alle“ bereitstehen, können zusätzlich auf dem Marktplatz der Nachnutzung aufgeführt werden.
(Stand: 09.05.2022)
## Formulare (Objekt Formular)
Inhalt: Welche Formulare benötigen Bürgerinnen und Bürger oder Unternehmen, um die Leistung in Anspruch zu nehmen?

|                       |      |
| --------------------- | ---- |
| Leistungssteckbrief   | Nein |
| Stammtext             | SOLL |
| Leistungsbeschreibung | MUSS |
| erforderlich für SDG  | Ja   |
| Erforderlich für PVOG | Nein |

Verlinken Sie auf Antragsformulare (auch Antragsassistenten), Merkblätter, Fragebögen, vorformulierte Erklärungen. Nutzen Sie ggf. Aufzählungen (siehe Punkt 2.1.8).

Linktext: Verwenden Sie nach Möglichkeit den Titel, der auf dem Formular selbst steht. Damit der Titel aber nicht unnötig lang wird, vermeiden Sie im Titel zusätzliche Hinweise auf z. B. Rechtsgrundlagen.

Stehen für die Leistung keine Formulare zur Verfügung, erfassen Sie z. B. „Sie müssen keine Formulare ausfüllen.“

Beispiel: 
- Antrag auf Kindergeld (https://www.arbeitsagentur.de/datei/kg1-antrag-kindergeld_ba017202.pdf)
- Hinweise zum Antrag auf Kindergeld (https://www.arbeitsagentur.de/datei/kg2-merkblattkindergeld_ba015394.pdf)
## Online-Dienste (Objekt Online-Dienst)
Inhalt: Welchen Online-Dienst benötigen Bürgerinnen und Bürger oder Unternehmen, um die Leistung online in Anspruch zu nehmen?

|                       |      |
| --------------------- | ---- |
| Leistungssteckbrief   | Nein |
| Stammtext             | SOLL |
| Leistungsbeschreibung | MUSS |
| erforderlich für SDG  | Ja   |
| Erforderlich für PVOG | Ja   |
**Bezeichnung**
Erfassen Sie hier die Bezeichnung des Onlinedienstes, die als Linktext veröffentlicht werden soll
Erfassen Sie diesen als bürgerfreundlichen Titel. Nicht in der Formulierung eines Arbeits- oder internen Titels.

**Link**
- Wie lautet der Link zum Online-Dienst? 
- vollständige URL des Online-Dienstes oder der Landing- Page zu diesem Dienst 

**Beschreibung**
- allgemeine Beschreibung des Online-Dienstes, Teaser (kurzer „Anreißer“-Text)
- Pfad beschreiben, wenn kein Direktlink möglich
- nur Text, keine Links zulässig - maximal 280 Zeichen

**Hilfetext**
- Was sollten Nutzende vorab wissen?
- Welche wichtigen technischen Voraussetzungen müssen Nutzende erfüllen?
- In welchem Format können Anlagen übermittelt werden?
- Hinweise zur Nutzung des Online-Dienstes

**Zahlungsmethode**
- Wie können die Antragstellenden online bezahlen?
- muss verbindlich vorgegeben werden
- für online bereitgestellte Dienste muss mindestens eine EU-weit gängige Online-Zahlungsmethode angegeben werden (zum Beispiel „giropay“, „Kreditkarte“, „SEPA- Lastschrift“)
- wenn keine Kosten anfallen, dann erfassen Sie bei Bemerkung mind. „keine“

**Vertrauensniveau**
- Welches Vertrauensniveau setzt der Online-Dienst voraus?
- Das [Praxistool Vertrauensniveau](https://vn-check.ozg-umsetzung.de/index.php/12295) gibt einen Anhaltspunkt, welches für Online-Dienste notwendige Vertrauensniveau genutzt wird. Dieses gibt Aufschluss darüber, welche Identifizierungsmittel beispielsweise für die Antragstellung oder für die elektronische Kommunikation eingesetzt werden.

**Identifizierung**
- Welche Mittel der Authentifizierung, Identifizierung und Unterzeichnung sind für den Online-Dienst zulässig?
- Wahl des Identifizierungsmittels nach dem jeweils benötigten Vertrauensniveau der Verwaltungsdienstleistung

**Sprachen außer Deutsch**
- In welchen anderen Sprachen (neben Deutsch) ist der Online-Dienst nutzbar?
- wenn keine weitere Sprache, bitte einfügen: „keine“

**Gültigkeitsgebiet**
- Für welches verwaltungspolitische Gebiet gilt der Online-Dienst?
- Sie können eine Leistung mehreren Online-Diensten zuordnen
## Weiterführende Informationen
Inhalt: Links auf Internetseiten mit Informationen rund um die Verwaltungsleistung.
Die Erfassung / Verwendung von Freitext ist hier nicht zulässig.

|                       |      |
| --------------------- | ---- |
| Leistungssteckbrief   | Nein |
| Stammtext             | SOLL |
| Leistungsbeschreibung | SOLL |
| erforderlich für SDG  | Nein |
| Erforderlich für PVOG | Nein |
Verlinken Sie auf Fachportale, Internetseiten zur Anspruchsberatung oder Anspruchsberechnung, Informationsseiten der Fachverwaltung.

Erfassen Sie jeden Link als separaten Eintrag mit mindestens Linktext und URL.

Linktext: Verwenden Sie den Titel der Seite auf die Sie verweisen.

Beschreibung des Links: Neben dem Titel kann auch angegeben werden, welche Informationen auf der Seite zu finden sind.

Link muss zwingend angegeben. Bei Änderung des Links ist der Redaktion der neue Link mitzuteilen.
## Hinweise (Besonderheiten)
Inhalt: Hinweise, die im Zusammenhang mit der Leistung beachtet werden sollten.

|                       |      |
| --------------------- | ---- |
| Leistungssteckbrief   | Nein |
| Stammtext             | SOLL |
| Leistungsbeschreibung | MUSS |
| erforderlich für SDG  | Ja   |
| Erforderlich für PVOG | Nein |
Nennen Sie zusätzliche Informationen für spezielle Zielgruppen; Hinweise auf ähnliche Leistungen, die zusätzlich in Anspruch genommen werden können oder Alternativen zu dieser Leistung sind.

Falls Querbeziehungen zu anderen Themen bestehen, nennen Sie diese Themen nicht nur stichwortartig, sondern erläutern Sie die Querbeziehung kurz.

Wenn es keine Hinweise (Besonderheiten) zu beachten gilt, erfassen Sie mind. „keine“.
## Letzte Änderung
Inhalt: Datum der letzten Aktualisierung des Datensatzes

|                       |                                                                                                                                                                                                                               |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Leistungssteckbrief   | Ja                                                                                                                                                                                                                            |
| Stammtext             | SOLL                                                                                                                                                                                                                          |
| Leistungsbeschreibung | MUSS                                                                                                                                                                                                                          |
| erforderlich für SDG  | Ja (Für Typ 11 gilt, dass sie das Datum der letzten Aktualisierung der Informationen, falls vorhanden, oder wenn die Informationen nicht aktualisiert wurden, das Veröffentlichungsdatum der Informationen enthalten müssen.) |
| Erforderlich für PVOG | Nein                                                                                                                                                                                                                          |

Wird automatisch von Redaktionssystem als Meta-Information gespeichert und als Datum dargestellt.
## Urheber
Inhalt: Angaben zum Verfasser des Textes (Person/Organisation)

Viele Redaktionssysteme kennzeichnen den Urheber eines Textes automatisch im Rahmen der Versionierung des Datensatzes.

|                       |      |
| --------------------- | ---- |
| Leistungssteckbrief   | Nein |
| Stammtext             | SOLL |
| Leistungsbeschreibung | MUSS |
| erforderlich für SDG  | Nein |
| Erforderlich für PVOG | Nein |
## Zuständige Stelle
Inhalt: Angabe der örtlich und sachlich zuständigen Stelle

|                       |      |
| --------------------- | ---- |
| Leistungssteckbrief   | Nein |
| Stammtext             | MUSS |
| Leistungsbeschreibung | MUSS |
| erforderlich für SDG  | Nein |
| Erforderlich für PVOG | Ja   |
**Informationen zur zuständigen Stelle müssen strukturiert im Objekt Organisationseinheit (Zuständigkeit) erfasst werden.**

Wenn eine Vielzahl von Behörden zuständig sind, die nicht strukturiert erfasst werden sollen/können, dann nur die Behördengruppe oder die Bezeichnung der Stelle angeben.

Zusätzlich dazu muss ein Link angegeben werden, über den die zuständige Stelle ermittelt werden kann.

In diesem Fall kann auf die Erfassung einer Anschrift verzichtet werden.

Beispiel: Krankenkassen

Leistungen, die keiner Organisationseinheit zugeordnet sind, werden nicht vom Sammlerdienst des PVOG verarbeitet.
## Ansprechpunkt
Inhalt: Angabe des/der Ansprechpunkte(s) für Bürgerin/Bürger/Unternehmen. Dies kann ein Servicecenter oder ein Bürgerbüro sein, wo ein Antrag z. B. abgegeben werden kann.

**Informationen zum Ansprechpunkt müssen strukturiert im Objekt Organisationseinheit (Zuständigkeit) erfasst werden.**

Bitte denken Sie - auch mit Blick auf die und in Abgrenzung zur zuständige/n Stelle - daran, hier die Informationen zu erfassen, mit denen die Aufgaben, die der Ansprechpunkt leistet, beschrieben werden.

|                       |      |
| --------------------- | ---- |
| Leistungssteckbrief   | Nein |
| Stammtext             | MUSS |
| Leistungsbeschreibung | MUSS |
| erforderlich für SDG  | Nein |
| Erforderlich für PVOG | Ja   |
Die zuständige Stelle kann zugleich Ansprechpunkt sein. Mehrere Ansprechpunkte sind möglich. Diese können sich ggf. im Leistungsumfang (z. B. Antragsannahme, Beratung, Beschwerdestelle) unterscheiden.

Geben Sie Name, Anschrift, Kontaktmöglichkeiten und Öffnungszeiten bevorzugt strukturiert an.

Wenn eine nachgeordnete Organisationseinheit Ansprechpunkt ist, z. B. Referat x, Hotline x, dann geben Sie zuerst die Bezeichnung der Behörde/Stelle und, getrennt mit Bindestrich, die Bezeichnung der Organisationseinheit an.

#### DIN 5008 schreibt für Stunden-, Minuten- und ggf. Sekundenangaben die führende Null vor
**Beispiel**: 

Deutsche Rentenversicherung Bund - Servicetelefon

Kontaktmöglichkeiten: Tel.: +49 800 1000480070 (Hotline)

Anrufzeiten:
Montag 07:30 - 19:30 Uhr
Dienstag 07:30 - 19:30 Uhr
Mittwoch 07:30 - 19:30 Uhr
Donnerstag 07:30 - 19:30 Uhr
Freitag 07:30 - 15:30 Uhr

E-Mail: meine-frage@drv-bund.de
Internet: „Internetseite der Deutschen Rentenversicherung“ http://www.deutsche-rentenversicherung-bund.de
#### „Dummy OE“
Wenn eine Vielzahl von Behörden/Stellen Ansprechpunkte sind, die nicht strukturiert erfasst werden sollen/können, dann nur die Behördengruppe oder die Bezeichnung der Stellen angeben.

Zusätzlich dazu muss mindestens eine URL, über die eine externe Ressource zur OE-Ermittlung erreicht wird, angegeben werden.

Beispiel

Bezeichnung: Krankenkassen
Link: https://www.gkv-spitzenverband.de/service/krankenkassenliste/krankenkassen.jsp
## Rechtsbehelf
Inhalt: Die den Antragstellenden zustehenden Rechte.

|                       |      |
| --------------------- | ---- |
| Leistungssteckbrief   | Nein |
| Stammtext             | SOLL |
| Leistungsbeschreibung | MUSS |
| erforderlich für SDG  | Ja   |
| Erforderlich für PVOG | Nein |
Hier erfassen Sie, welche Möglichkeiten die/der Antragstellende hat, um gegen die Entscheidung der Behörde vorzugehen. - Erfassen Sir hier die Angabe der möglichen Rechtsbehelfe und den Hinweis auf Klagemöglichkeit im Fall rechtlich vorgesehener Genehmigungsfiktion

Beispiele:
- Einspruch
- Widerspruch
- Klage vor dem Verwaltungsgericht (dem Sozialgericht, etc.)
## Fachlich freigegeben durch
Inhalt: Die Stelle, die für die fachliche Richtigkeit der erfassten Daten/Informationen verantwortlich ist.

|                       |      |
| --------------------- | ---- |
| Leistungssteckbrief   | Ja   |
| Stammtext             | MUSS |
| Leistungsbeschreibung | MUSS |
| erforderlich für SDG  | Nein |
| Erforderlich für PVOG | Nein |
Format:
„fachlich freigegeben durch: \[vollständige amtliche Bezeichnung der Behörde/ Organisation\]“

Struktureinheiten einer Behörde (z. B. Referate, Abteilungen) dürfen hier nicht erfasst werden.

Für den Bund ist eine Übersicht im [Anschriftenverzeichnis des Bundes](https://www.govdata.de/daten/-/details/anschriftenverzeichnis-des-bundes) zu finden.

**Beispiel**: Bundesministerium der Verteidigung
## Fachlich freigegeben am
Inhalt: Datum der letzten fachlichen Freigabe

|                       |      |
| --------------------- | ---- |
| Leistungssteckbrief   | Ja   |
| Stammtext             | MUSS |
| Leistungsbeschreibung | MUSS |
| erforderlich für SDG  | Nein |
| Erforderlich für PVOG | Nein |
Format: „fachlich freigegeben am: TT.MM.JJJJ“

**Beispiel**: 01.06.2022
## Typisierung
Inhalt: Regelungs- und Vollzugskompetenz zur Leistung gemäß Codes für Typisierungen von Leistungen des Leistungskatalogs / FIM Baustein Leistung: [Codeliste](https://www.xrepository.de/details/urn:de:fim:leika:typisierung)

|                       |      |
| --------------------- | ---- |
| Leistungssteckbrief   | Ja   |
| Stammtext             | MUSS |
| Leistungsbeschreibung | MUSS |
| erforderlich für SDG  | Nein |
| Erforderlich für PVOG | Ja   |
Geben Sie die Typisierung an durch Ziffern und gegebenenfalls Buchstaben. Verwenden Sie keine Leerzeichen.

#### mehrwertige Typisierung
Sie können die Typisierung mehrwertig anhand der in der Codeliste enthaltenen Kombinationen erfassen. Mit der mehrwertigen Typisierung wird der Vollzug der Verwaltungsleistung auf mehr als einer Ebene gekennzeichnet.

Beispiel für Vollzug der Leistung sowohl auf Bundes- als auch auf Landesebene:
„1, 3“
## SDG Informationsbereich
Inhalt: SDG Informationsbereich gemäß Single-Digital-Gateway- Verordnung (SDG-VO), Annex I

|                       |      |
| --------------------- | ---- |
| Leistungssteckbrief   | Ja   |
| Stammtext             | SOLL |
| Leistungsbeschreibung | MUSS |
| erforderlich für SDG  | Ja   |
| Erforderlich für PVOG | Nein |
Codes der Informationsbereiche, die für Bürger und Unternehmen relevant sind, die ihre Binnenmarktrechte ausüben.

Siehe *VERORDNUNG (EU) 2018/1724 DES EUROPÄISCHEN PARLAMENTS UND DES RATES über die Einrichtung eines einheitlichen digitalen Zugangstors zu Informationen, Verfahren, Hilfs- und Problemlösungsdiensten und zur Änderung der Verordnung (EU) Nr. 1024/2012 (SDG)*

[Codeliste](https://www.xrepository.de/details/urn:xoev-de:fim:codeliste:sdginformationsbereich)

Die Pflege des Moduls erfolgt durch Bundesministerium des Innern und für Heimat (BMI) für Leistungsbeschreibungen der Typen 1 - 5.

Das beim BMI erhältliche Kurzgutachten „Kurzgutachten zum Anwendungsbereich der Single-Digital-Gateway-Verordnung (SDG-VO)“ liefert Hinweise dafür, wie bei der Ermittlung und Zuordnung von SDG- Informationsberiechen zu einer Leistung vorgegangen werden soll.

## Ursprungsportal
Inhalt: Rück-Link ins Originalportal.

Der Link sollte automatisch generiert werden.

|                       |      |
| --------------------- | ---- |
| Leistungssteckbrief   | Nein |
| Stammtext             | SOLL |
| Leistungsbeschreibung | MUSS |
| erforderlich für SDG  | Nein |
| Erforderlich für PVOG | Ja   |
## Lagen Portalverbund
Inhalt: Modell für Bürger und Unternehmen zur verbesserten Auffindbarkeit von Verwaltungsleistungen.

|                       |      |
| --------------------- | ---- |
| Leistungssteckbrief   | Ja   |
| Stammtext             | MUSS |
| Leistungsbeschreibung | MUSS |
| erforderlich für SDG  | Nein |
| Erforderlich für PVOG | Nein |
Die Zuordnung der Lagen Portalverbund zu einer Verwaltungsleistung erfolgt nicht automatisch, sondern muss durch den Redakteur / Redakteurin vorgenommen werden. Hierzu ist die, zum Zeitpunkt der Zuordnung, aktuelle Fassung der Lagen Portalverbund zu verwenden.

Die Codeliste für die Lagen Portalverbund kann unter folgendem Link gefunden werden: [[https://www.xrepository.de/details/urn:xoev-de:fim:codeliste:pvlagen]]

Perspektivisch werden auch Verwaltungslagen (Ebene 3 – Verwaltung) erarbeitet und aufgenommen.

Gegebenenfalls notwendige Änderungen und Anpassungen am Lagenmodell selbst werden einmal jährlich durch den Herausgeber vorgenommen. Die Veränderungen werden nach entsprechender Vorankündigung technisch im XRepository und im Redaktionssystem des Bausteins Leistungen umgesetzt. Hierbei soll die Grundstruktur und Logik des Modells unberührt bleiben, so dass weder eine Änderung am XZuFi-Standard, noch an bestehenden Zuordnungen des Lagenmodells zu Verwaltungsleistungen notwendig ist. Hiervon ausgenommen sind Leistungen, die veränderten bzw. neu hinzugefügten Lebens- oder Geschäftslagen zugeordnet wurden. Weiterhin hat eine bürgernahe Sprache bezüglich Titel und Beschreibung der Lage Priorität. Die Sicht des Nutzers (Bürger oder Unternehmen) steht im Fokus.

Stand; 02.06.2022; Version 1.00 Leistungen werden ausschließlich zu Sublagen (Lagen der 3. Ebene) zugeordnet.

**Bespiel**
1000000 Bürger
1090000 Fahrzeug
1090100 Führerschein

Leistungen sollten nicht direkt der 1. („Bürger“) oder 2. („Fahrzeug“) Ebene zugeordnet werden (auch wenn tlw. im Redaktionssystem technisch möglich). Die Zuordnung erfolgt hier zu „Führerschein“, „Kfz-Besitz“ oder ggf. beiden Sublagen.

Bei der Zuordnung sollte nicht nur die Bezeichnung der Sublage als Zuordnungshilfe genutzt werden, sondern auch der dazu gehörige Beschreibungstext und gegebenenfalls die entsprechende Hauptlage. Die Berücksichtigung der Beschreibung und auch der Hauptlagenzuordnung ist insofern sinnvoll, da das Auffinden der Verwaltungsleistung im Service- bzw. Verwaltungsportal auch über die Hauptlagennavigation bzw. über die vorherige Anzeige der Beschreibungstexte erfolgen kann.

Eine Leistung kann mehreren Sublagen zugeordnet werden. In bestimmten Fällen ist dies sinnvoll, z.B. bei Leistungen, die Bürger und Unternehmen gleichermaßen betreffen könnten. Gleichzeitig sollten Mehrfachzuordnungen vermieden werden, um Eindeutigkeit und Übersichtlichkeit des Modells zu gewährleisten. Es wird empfohlen nicht mehr als 3 Sublagen zuzuordnen, auch wenn eine höhere Zuordnungsanzahl technisch möglich ist.

Eine doppelte Zuordnung zu Sublagen der Ebenen Bürger UND Unternehmen sollte nur vorgenommen werden, wenn definitiv beide Gruppen von der Leistung Gebrauch machen können. Ob dies der Fall ist, muss in jedem Einzelfall geprüft werden.
## Kennzeichen einheitliche Stelle
Markieren Sie das Kennzeichen einheitliche Stelle, wenn durch eine Rechtsvorschrift angeordnet, dass das Verwaltungsverfahren für diese Leistung über eine einheitliche Stelle (eS) nach § 71a-e Verwaltungsverfahrensgesetz (VwVfG) abgewickelt werden kann.

|                       |      |
| --------------------- | ---- |
| Leistungssteckbrief   | Nein |
| Stammtext             | MUSS |
| Leistungsbeschreibung | MUSS |
| erforderlich für SDG  | Nein |
| Erforderlich für PVOG | Nein |

Die einheitliche Stelle (eS) nimmt Anzeigen, Anträge, Willenserklärungen und Unterlagen entgegen und leitet sie unverzüglich an die für die Erbringung der Leistung zuständigen Behörden weiter.
## Mustertext
Markieren Sie den Text, wenn es sich um einen Mustertext handelt.

Die Erläuterungen zum Mustertext finden Sie im Leitfaden Typisierung.

|                       |      |
| --------------------- | ---- |
| Leistungssteckbrief   | Nein |
| Stammtext             | SOLL |
| Leistungsbeschreibung | SOLL |
| erforderlich für SDG  | Nein |
| Erforderlich für PVOG | Nein |
