# QS-Kriterien pro Textmodul

Die untenstehenden Abschnitte sollen all jenen als Arbeitsgrundlage dienen, die mit der Erstellung oder der Qualitätssicherung von Stammtexten oder Leistungsbeschreibungen betraut sind. Sie geben keine Antworten darauf, wie erstellte Leistungsbeschreibungen z. B. in Verwaltungsportalen angezeigt werden sollten.

Im Folgenden werden pro Textmodul

1. die Soll- und Muss-Kriterien angegeben sowie
2. die Fragen beantwortet:
   - Ist das betreffende Modul ein Pflichtmodul (in Stammtexten/in Leistungsbeschreibungen) oder nicht?
   - Welche Kriterien werden von den PVOG-Validatoren wie geprüft?

## Leistungssteckbrief

### Leistungsschlüssel (LeiKa-Schlüssel)

Das Feld "Leistungsschlüssel" enthält den Leistungsschlüssel (ehemals LeiKa-Schlüssel), welcher eine Ziffernfolge zur eindeutigen Identifikation einer Leistung ist.

||Leistungsschlüssel (LeiKa-Schlüssel)|
|----|----|
|Soll|<ul><li>Keine</li></ul>|
|Muss|<ul><li>Der Leistungsschlüssel muss eine 14-stellige Zahlenfolge sein.</li><li>Der Leistungsschlüssel muss mit der Instanz „99“ beginnen.</li><li>Zum Leistungsschlüssel muss ein LeiKa-Eintrag existieren.</li><li>Achtung: Die QS-Kriterien besagen, dass ländereigene Leistungsschlüssel existieren können.</li></ul>|
|Pflichtmodul|<ul><li>Stammtext: Ja</li><li>Leistungsbeschreibung: Ja</li></ul>|
|Technische Prüfung|<ul><li>Self-Service: Ja (perspektivisch)</li><ul><li>Kriterium: Auf korrektes Format prüfen, mit Referenz (LeiKa) abgleichen</li></ul><li>PVOG-Validatoren: VAL0022, VAL0045, VAL0046, VAL0050, VAL0056, VAL0059</li></ul>|

### Leistungsbezeichnung I

Das Feld "Leitstungsbezeichnung I" enthält die Bezeichnung der Leistung aus Sicht der Verwaltung.

|                    | Leistungsbezeichnung I                                                                                                                                                                                                                                                      |
|--------------------|---------------------------------------------------------------------------------------------------------|
| Soll               | <ul><li>Keine</li></ul>                                                                                                                                                                                                                                                     |
| Muss               | <ul><li>Format: [Leistungskennung][Leerzeichen][Verrichtungskennung][Leerzeichen][Verrichtungsdetail]</li><li>Die [Verrichtungskennung] muss einem Wert aus der Codeliste der Verrichtungskennungen entsprechen.</li><li>Darf keine Handlungsgrundlage enthalten.</li></ul> |
| Pflichtmodul       | <ul><li>Stammtext: Ja</li><li>Leistungsbeschreibung: Ja</li></ul>                                                                                                                                                                                                           |
| Technische Prüfung | <ul><li>Self-Service: Ja (perspektivisch)</li><ul><li>Kriterium: Wertebereich aus XRepository prüfen, z. B. Auswertung der Häufigkeit</li></ul><li>PVOG-Validatoren: Nein</li></ul>|

### Typisierung

Das Feld "Typisierung" gibt Auskunft über die Regelungs- und Vollzugskompetenz zur Leistung.

||Typisierung|
|----|----|
|Soll|<ul><li>eher redaktionelle Hinweise</li></ul>|
|Muss|<ul><li>Darf keine Leerzeichen enthalten.</li><li>Die Typisierung muss einem Wert aus der Codeliste der Typisierungen entsprechen.</li></ul>|
|Pflichtmodul|<ul><li>Stammtext: Ja</li><li>Leistungsbeschreibung: Ja</li></ul>|
|Technische Prüfung|<ul><li>Self-Service: Ja (perspektivisch)</li><ul><li>Kriterium: Wertebereich aus XRepository prüfen, z. B. Auswertung der Häufigkeit</li></ul><li>PVOG-Validatoren: Nein</li></ul>|

### Mustertext

Das Feld "Mustertext" gibt Auskunft darüber, ob es sich um einen Mustertext handelt.

||Mustertext|
|----|----|
|Soll|<ul><li>Keine</li></ul>|
|Muss|<ul><li>Keine</li></ul>|
|Pflichtmodul|<ul><li>Stammtext: Nein</li><li>Leistungsbeschreibung: Nein</li></ul>|
|Technische Prüfung|<ul><li>Keine</li></ul>|

### Herausgebende Stelle (Urheber, Herausgeber)

Das Feld "Herausgebende Stelle" gibt Auskunft darüber, wer den Text erstellt hat.

||Herausgebende Stelle|
|----|----|
|Soll|<ul><li>Referat einer Bundes- oder Landesbehörde, Fachbehörde, Landkreis oder Gemeinde</li><li>Achtung: Im LeiKa Handbuch-*plus* steht „Autor der letzten Veröffentlichung“.</li></ul>|
|Muss|<ul><li>Keine</li></ul>|
|Pflichtmodul|<ul><li>Stammtext: Nein</li><li>Leistungsbeschreibung: Ja</li></ul>|
|Technische Prüfung|<ul><li>Keine</li></ul>|

### Lagen Portalverbund

Die im Feld "Lagen Portalverbund" enthaltenen Lebenslagen bieten ein Navigationsmodell für Bürger und Unternehmen zur besseren Auffindbarkeit von Verwaltungsleistungen.

||Lagen Portalverbund|
|----|----|
|Soll|<ul><li>Keine</li></ul>|
|Muss|<ul><li>Es dürfen maximal drei Lagen ausgewählt werden.</li><li>Die Lagen Portalverbund müssen einem Wert aus der Codeliste der Lagen Portalverbund entsprechen.</li></ul>|
|Pflichtmodul|<ul><li>Stammtext: Ja</li><li>Leistungsbeschreibung: Ja</li></ul>|
|Technische Prüfung|<ul><li>Self-Service: Nein</li><li>PVOG-Validatoren: VAL0051</li></ul>|

## Stammtext/Leistungsbeschreibung

- Self-Service: Ja (perspektivisch)
  - Kriterium: Vorhandensein OE (Ansprechpunkt, zuständige Stelle) prüfen
- PVOG-Validatoren: VAL0035

### Leistungsbezeichnung II

Das Feld "Leistungsbezeichnung II" beinhaltet die Bezeichnung der Leistung aus Sicht der Nutzenden.

||Leistungsbezeichnung II|
|----|----|
|Soll|<ul><li>Sollte mit einem Substantiv beginnen.</li></ul>|
|Muss|<ul><li>Format: [Verrichtung\] [Leistungskennung\] Verb und/oder [Leistungskennung\] [Verrichtung\] Verb</li><li>Darf keine Handlungsgrundlage enthalten.</li><li>Darf maximal 150 Zeichen (inklusive Leerzeichen) enthalten.</li></ul>|
|Pflichtmodul|<ul><li>Stammtext: Ja</li><li>Leistungsbeschreibung: Ja</li></ul>|
|Technische Prüfung|<ul><li>Self-Service: Ja (perspektivisch)</li><ul><li>Kriterium: Textlänge prüfen, ggf. Aufbau prüfen (geht aber in Richtung Textqualität)</li></ul><li>PVOG-Validatoren: Nein</li></ul>|

### Teaser

Das Feld "Teaser" enthält eine kurze Beschreibung der Leistung zur Darstellung auf SERPs (Search Engine Result Pages).

||Teaser|
|----|----|
|Soll|<ul><li>Sollte den Nutzenden persönlich ansprechen („Sie“).</li><li>Sollte keine Dopplungen zum Volltext enthalten.</li><li>Sollte maximal zwei Sätze enthalten.</li><li>Sollte maximal 50 Wörter enthalten.</li><li>Sollte keine Aufzählungen enthalten.</li></ul>|
|Muss|<ul><li>Darf maximal 280 Zeichen (inklusive Leerzeichen) enthalten.</li></ul>|
|Pflichtmodul|<ul><li>Stammtext: Ja</li><li>Leistungsbeschreibung: Ja</li></ul>|
|Technische Prüfung|<ul><li>Self-Service: Ja (perspektivisch)</li><ul><li>Kriterium: Textlänge prüfen, Anzahl an Sätzen prüfen</li></ul><li>PVOG-Validatoren: Nein</li></ul>|


### Volltext

Das Feld "Volltext" enthält eine ausführliche Beschreibung der Leistung.

|                    | Volltext                                                                                                                                                                                                                                                                                                                                       |
|--------------------|------------------------------------------------------------------------------------|
| Soll               | <ul><li>Sollte Art, Zweck und Ergebnis der Leistung bürgernah beschreiben.</li><li>Sollte den Nutzenden persönlich ansprechen („Sie“).</li><li>Sollte mit der wichtigsten Information beginnen.</li><li>Sollte keine Dopplungen zum Teaser enthalten.</li><li>Sollte keine Information enthalten, für die es ein eigenes Modul gibt.</li></ul> |
| Muss               | <ul><li>Darf maximal 5.000 Zeichen (inklusive Leerzeichen) enthalten.</li></ul>                                                                                                                                                                                                                                                                |
| Pflichtmodul       | <ul><li>Stammtext: Ja</li><li>Leistungsbeschreibung: Ja</li></ul>                                                                                                                                                                                                                                                                              |
| Technische Prüfung | <ul><li>Self-Service: Ja (perspektivisch)</li><ul><li>Kriterium: Textlänge prüfen</li></ul><li>PVOG-Validatoren: Nein</li></ul>|

### Handlungsgrundlagen

Das Feld "Handlungsgrundlagen" enthält Angaben der zugrundeliegenden Gesetze, Richtlinien, Vorschriften und Verordnungen mit entsprechender Verlinkung.

||Handlungsgrundlagen|
|----|----|
|Soll|<ul><li>Sollte aus der Kurzbezeichnung und der entsprechenden Abkürzung in Klammern bestehen.</li><li>Bei Typ-1- bis -3-Leistungen sollte der vollständige Titel sowie in Klammern der Kurztitel und die Abkürzung genannt werden.</li><li>Sollte die konkrete Fundstelle benennen (Paragraphen, Artikel, Absätze, Sätze oder Nummern) verlinken.</li><li>Buchstaben sollten direkt hinter der Ziffer stehen (kein Leerzeichen).</li><li>Wenn mehrere Fundstellen einschlägig sind, sollte pro Normenkörper ein Listenpunkt gesetzt werden, beginnend mit dem wichtigsten.</li><li>Beim Zitieren mehrerer Normen des gleichen Gesetzes nacheinander sollte das doppelte Paragraphenzeichen gesetzt werden (gilt nicht, wenn neben den Paragraphen auch Artikel, Absätze, Sätze, Nummern zitiert werden).</li><li>Wenn mehrere Paragraphen, Artikel, Absätze, Sätze oder Nummern einschlägig sind, sollte auf das Inhaltsverzeichnis verlinkt werden.</li><li>Bei Unionsrecht sollte auf eine Webseite von EUR-LEX verlinkt werden.</li><li>Bei Bundesrecht sollte auf eine Webseite von <https://www.gesetze-im-internet.de/> oder des Rechtsetzenden verlinkt werden.</li><li>Bei Landesrecht und kommunalem Satzungsrecht sollte auf eine offizielle Webseite des Rechtsetzenden verlinkt werden.</li><li>Als Linktext sollte der Titel der verlinkten Norm inklusive Paragraph, Artikel, Absatz, Satz oder Nummer gesetzt werden.</li></ul>|
|Muss|<ul><li>Darf keine Links mit dem Präfix „http“ enthalten.</li></ul>|
|Pflichtmodul|<ul><li>Stammtext: Nein</li><li>Leistungsbeschreibung: Nein</li></ul>|
|Technische Prüfung|<ul><li>Self-Service: Ja (perspektivisch)</li><ul><li>Kriterium: Link prüfen (3 Optionen: 1. Gesetze im Internet verlinkt, 2. Anderer korrekter Link, 3. Fehlermeldung)</li></ul><li>PVOG-Validatoren: Nein</li></ul>|

### Erforderliche Unterlagen

Das Feld "Erforderliche Unterlagen" enthält Angaben von Unterlagen, die zur Durchführung des Verfahrens notwendig sind.

|                    | Erforderliche Unterlagen                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
|--------------------|-----------------------------------------|
| Soll               | <ul><li>Sollte aus einer stichpunktartigen Auflistung der für die Durchführung des Verfahrens erforderlichen Unterlagen bestehen.</li><li>Sollte keine ganzen Sätze enthalten.</li><li>Sollte mit der wichtigsten Unterlage beginnen (zuerst Hauptantrag, dann Zusatzantrag, dann Nachweise).</li><li>Wenn keine Unterlagen erforderlich sind, sollte schlicht „Keine“ angegeben werden.</li><li>Achtung: Soweit für erforderliche Unterlagen amtliche Vordrucke existieren, sollten diese unter dem Abschnitt „Formulare (offline)“ bereitgestellt werden.</li></ul> |
| Muss               | <ul><li>Keine</li></ul>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| Pflichtmodul       | <ul><li>Stammtext: Nein</li><li>Leistungsbeschreibung: Ja</li></ul>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| Technische Prüfung | <ul><li>Keine</li></ul>|

### Voraussetzungen

Das Feld "Voraussetzungen" enthält Angaben der für die Durchführung des Verfahrens zu erfüllenden Voraussetzungen.

||Voraussetzungen|
|----|----|
|Soll|<ul><li>Sollte aus einer kurzen, prägnanten Aufzählung der Tatbestandsmerkmale der gesetzlichen Regelung des Leistungsobjekts bestehen.</li><li>Sollte den Nutzenden persönlich ansprechen („Sie“).</li><li>Wenn es keine Voraussetzungen gibt, die erfüllt werden müssen, sollte schlicht „Keine“ angegeben werden.</li></ul>|
|Muss|<ul><li>Keine</li></ul>|
|Pflichtmodul|<ul><li>Stammtext: Nein</li><li>Leistungsbeschreibung: Ja</li></ul>|
|Technische Prüfung|<ul><li>Keine</li></ul>|

### Kosten

Das Feld "Kosten" enthält Angaben zu den bei Inanspruchnahme der Leistung anfallenden Kosten oder Gebühren.

||Kosten|
|----|----|
|Soll|<ul><li>Kosten sollten strukturiert erfasst werden.</li><li>Es sollte der genaue Betrag in Euro (fix) oder der Kostenrahmen inklusive Mindestbetrag und Höchstbetrag (variabel) angegeben werden.</li><li>Wenn der Betrag kleiner als EUR 1.000 ist, sollten immer Nachkommastellen angegeben werden.</li><li>Glatte Beträge ab EUR 1.000 sollten ohne Nachkommastelle angegeben werden.</li><li>Innerhalb eines Kostenrahmens sollten beide Beträge mit Nachkommastellen angegeben werden.</li><li>Euro, Cent und Prozent sollten im Fließtext ausgeschrieben werden, wenn sie nicht im Zusammenhang mit einer Betrags- oder Prozentangabe in Ziffern stehen.</li><li>Formate: <ul><li>Fixe Kosten: [Kostenart] : [Betrag] EUR</li><li>Kostenrahmen: [Kostenart]: [Betrag] – [Betrag] EUR</li></ul></li><li>Wenn für Bezahlung nötig, dann sollte ein Kassenzeichen angeben werden.</li><li>Wenn Vorkasse verlangt wird, dann sollte dies angeben werden.</li><li>Wenn die Leistung kostenlos ist, sollte die Bemerkung „Es fallen keine Kosten an.“ eingefügt werden.</li><li>Ist ein Typ nicht vorhanden, kann ein eigener Code verwendet werden.</li><li>Es sollten keine pauschalen Angaben wie „Es fallen gegebenenfalls Kosten an.“ oder „Gebühren nach Landesrecht.“ gemacht werden.</li><li>Es sollte die in XZuFi hinterlegte Codeliste verwendet werden.</li><li>Ist ein Typ nicht vorhanden, kann ein eigener Code verwendet werden.</li></ul>|
|Muss|<ul><li>Das Format von URIs und URLs muss dem „RFC 2396“ entsprechen.</li><li>Zusätzlich müssen die Elemente „linkKostenbildung“ aus einem Kostenmodul und „link“ aus dem Online-Dienst syntaktisch korrekte URLs gemäß „RFC 2396“ enthalten.</li></ul>|
|Pflichtmodul|<ul><li>Stammtext: Nein</li><li>Leistungsbeschreibung: Ja</li></ul>|
|Technische Prüfung|<ul><li>Self-Service: Ja (perspektivisch)<ul><li>Kriterium: Vorhandensein prüfen, „Es fallen keine Kosten an.“ prüfen, auf formatkonform eingetragenen Betrag prüfen, auf formatkonform eingetragene Währungsbezeichnung prüfen, auf strukturierte Erfassung prüfen</li></ul></li><li>PVOG-Validatoren: VAL0033</li></ul>|

### Verfahrensablauf

Das Feld "Verfahrensablauf" enthält eine Aufzählung der wichtigsten Verfahrensschritte und der Beteiligten.

||Verfahrensablauf|
|----|----|
|Soll|<ul><li>Sollte aus einer Aufzählung bestehen, wenn es mehr als einen Verfahrensschritt gibt.</li></ul>|
|Muss|<ul><li>Keine</li></ul>|
|Pflichtmodul|<ul><li>Stammtext: Nein</li><li>Leistungsbeschreibung: Ja</li></ul>|
|Technische Prüfung|<ul><li>Keine</li></ul>|

### Bearbeitungsdauer

Das Feld "Bearbeitungsdauer" enthält Angaben zur Dauer der Bearbeitung eines Verfahrens.

||Bearbeitungsdauer|
|----|----|
|Soll|<ul><li>Die Bearbeitungsdauer sollte strukturiert erfasst werden.</li><li>Wenn es keine gesetzliche Regelung zur Bearbeitungsdauer gibt, sollte schlicht „Keine“ angegeben werden.</li><li>Wenn es mehrere Bearbeitungsdauern gibt, sollten dafür Aufzählungen verwendet werden.</li></ul>|
|Muss|<ul><li>Wenn die Bearbeitungsdauer strukturiert erfasst wird, werden die Datentypen für <b>Fristen</b> genutzt.</li></ul>|
|Pflichtmodul|<ul><li>Stammtext: Nein</li><li>Leistungsbeschreibung: Ja</li></ul>|
|Technische Prüfung|<ul><li>Self-Service: Ja (perspektivisch)<ul><li>Kriterium: Vorhandensein prüfen, „Keine“ prüfen (bei strenger Prüfung: auf Text prüfen ([Zahl] [Werktage/Tage/Wochen/Monate])), auf strukturierte Erfassung prüfen</li></ul></li><li>PVOG-Validatoren: Nein</li></ul>|

### Fristen

Das Feld "Fristen" enthält Angaben zu Fristen, die Antragstellende einhalten oder beachten müssen.

||Fristen|
|----|----|
|Soll|<ul><li>Fristen sollten strukturiert erfasst werden.</li><li>Wenn es keine gesetzliche Regelung zu Fristen gibt, sollte schlicht „Keine“ angegeben werden.</li><li>Wenn mehrere Fristen angegeben werden müssen, sollten die Felder kopiert werden.</li><li>Es sollte die in XZuFi hinterlegte Codeliste verwendet werden.</li></ul>|
|Muss|<ul><li>Dauer der Frist als Ganzzahl: Wenn „dauerBis“ gesetzt wird, muss dies der Von-Wert sein.</li></ul>|
|Pflichtmodul|<ul><li>Stammtext: Nein</li><li>Leistungsbeschreibung: Ja</li></ul>|
|Technische Prüfung|<ul><li>Self-Service: Ja (perspektivisch)<ul><li>Kriterium: Vorhandensein prüfen, „Keine“ prüfen (bei strenger Prüfung: auf Text prüfen ([Zahl] [Werktage/Tage/Wochen/Monate])), auf strukturierte Erfassung prüfen</li></ul></li><li>PVOG-Validatoren: Nein</li></ul>|

### Weiterführende Informationen

Das Feld "Weiterführende Informationenen" enthält Angaben von weiterführenden Informationen zur Leistung.

||Weiterführende Informationen|
|----|----|
|Soll|<ul><li>Sollten auf Fachportale, Themenportale, Broschüren verweisen.</li><li>Die Bezeichnung des Links sollte den Inhalts oder die Funktion der Seite aussagekräftig ankündigen.</li><li>Wenn mehrere Links angegeben werden müssen, sollten die Felder kopiert werden.</li></ul>|
|Muss|<ul><li>Die URL der Seite muss angeben werden.</li></ul>|
|Pflichtmodul|<ul><li>Stammtext: Nein</li><li>Leistungsbeschreibung: Nein</li></ul>|
|Technische Prüfung|<ul><li>Self-Service: Ja (perspektivisch)<ul><li>Kriterium: Link prüfen</li></ul></li><li>PVOG-Validatoren: Nein</li></ul>|

### Hinweise/Besonderheiten

Das Feld "Hinweise/Besonderheiten" enthält Angaben von zusätzlich relevanten Informationen.

||Hinweise/Besonderheiten|
|----|----|
|Soll|<ul><li>Es sollten ganze Sätze formuliert werden.</li><li>Es sollten nur Informationen angegeben werden, die nicht in anderen Modulen erwähnt werden.</li><li>Wenn es keine Hinweise/Besonderheiten gibt, sollte schlicht „Keine“ angegeben werden.</li></ul>|
|Muss|<ul><li>Es dürfen keine Links hinterlegt werden.</li></ul>|
|Pflichtmodul|<ul><li>Stammtext: Nein</li><li>Leistungsbeschreibung: Ja</li></ul>|
|Technische Prüfung|<ul><li>Self-Service: Ja (perspektivisch)<ul><li>Kriterium: Auf zusätzlichen Informationsgehalt prüfen</li></ul></li><li>PVOG-Validatoren: Nein</li></ul>|

### Fachlich freigegeben durch

||Fachlich freigegeben durch|
|----|----|
|Soll|<ul><li>Es sollte nur das für die Gesetzgebung zuständige Landes- oder Bundesministerium oder das für Satzungsrecht zuständige Amt einer Kommune genannt werden.</li><li>Die Bezeichnung der Behörde sollte vollständig ausgeschrieben werden.</li><li>Fachbereiche sollten nicht genannt werden.</li></ul>|
|Muss|<ul><li>Keine</li></ul>|
|Pflichtmodul|<ul><li>Stammtext: Ja</li><li>Leistungsbeschreibung: Ja</li></ul>|
|Technische Prüfung|<ul><li>Keine</li></ul>|

### Fachlich freigegeben am

Das Feld "Fachlich freigegeben am" enthält die Angabe des Datums der fachlichen Freigabe.

||Fachlich freigegeben am|
|----|----|
|Soll|<ul><li>Keine</li></ul>|
|Muss|<ul><li>Format: TT.MM.JJJJ</li></ul>|
|Pflichtmodul|<ul><li>Stammtext: Ja</li><li>Leistungsbeschreibung: Ja</li></ul>|
|Technische Prüfung|<ul><li>Self-Service: Ja (perspektivisch)<ul><li>Kriterium: Auf formatkonform eingetragenes Datum prüfen (DIN 5008)</li></ul></li><li>PVOG-Validatoren: Nein</li></ul>|

### Rechtsbehelf

Das Feld "Rechtsbehelf" enthält die Angabe der möglichen Rechtsbehelfe.

||Rechtsbehelf|
|----|----|
|Soll|<ul><li>Sollte auch auf Klagemöglichkeit im Fall einer rechtlich vorgesehenen Genehmigungsfiktion hinweisen.</li></ul>|
|Muss|<ul><li>Keine</li></ul>|
|Pflichtmodul|<ul><li>Stammtext: Nein</li><li>Leistungsbeschreibung: Ja</li></ul>|
|Technische Prüfung|<ul><li>Keine</li></ul>|

### Leistungsadressatinnen und Leistungsadressaten

Das Feld "Leistungsadressatinnen und Leistungsadressaten" gibt an, für wen die Leistung bestimmt ist bzw. wer sie in Anspruch nehmen kann.

|                    | Leistungsadressatinnen und Leistungsadressaten                        |
|--------------------|-----------------------------------------------------------------------|
| Soll               | <ul><li>Keine</li></ul>                                               |
| Muss               | <ul><li>Keine</li></ul>                                               |
| Pflichtmodul       | <ul><li>Stammtext: Nein</li><li>Leistungsbeschreibung: Nein</li></ul> |
| Technische Prüfung | <ul><li>Keine</li></ul>                                               |

### Status Bibliothekseintrag

Das Feld "Status Bibliothekseintrag" enthält die Angabe des Status des Stammtextes bzw. der Leistungsbeschreibung.

||Status Bibliothekseintrag|
|----|----|
|Soll|<ul><li>Für Typ-1- bis -3-Leistungen sollte im Leistungssteckbrief der jeweilige Status angegeben werden.</li><li>Es sollte die in XZuFi hinterlegte Codeliste verwendet werden.</li></ul>|
|Muss|<ul><li>Keine</li></ul>|
|Pflichtmodul|<ul><li>Stammtext: Ja</li><li>Leistungsbeschreibung: Ja</li></ul>|
|Technische Prüfung|<ul><li>Self-Service: Ja (perspektivisch)<ul><li>Kriterium: Wertebereich prüfen, Häufigkeit auswerten</li></ul></li><li>PVOG-Validatoren: Nein</li></ul>|

### Begriffe im Kontext (Synonyme, Suchbegriffe)

Das Feld "Begriffe im Kontext" beinhaltet eine Verschlagwortung, die der Auffindbarkeit über Suchmaschinen und in Verwaltungsportalen dient.

|                    | Begriffe im Kontext (Synonyme, Suchbegriffe)                                                                                                                                                                                                                                                                                                                                                                |
|--------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Soll               | <ul><li>Sollten nicht zusammen mit der Leistungsbeschreibung angezeigt werden.</li><li>Es sollte die in XZuFi hinterlegte Codeliste verwendet werden.</li><li>Begriffen, die sich bereits in der Leistungsbezeichnung I/Leistungsbezeichnung II finden, sollten nicht doppelt erfasst werden.</li><li>Es sollten nur Buchstaben, Ziffern und nach Möglichkeit keine Sonderzeichen erfasst werden.</li></ul> |
| Muss               | <ul><li>Bei Wortfolgen dürfen maximal sechs Wörter verwendet werden.</li><li>In der Aufzählung dürfen keine Spiegelstriche verwendet werden.</li><li>Begriffe müssen durch Absatzumbruch voneinander getrennt werden.</li></ul>                                                                                                                                                                             |
| Pflichtmodul       | <ul><li>Stammtext: Ja</li><li>Leistungsbeschreibung: Ja</li></ul>                                                                                                                                                                                                                                                                                                                                           |
| Technische Prüfung | <ul><li>Keine</li></ul>|

### Kennzeichen einheitliche Stelle

Das Feld "Kennzeichen einheitliche Stelle" gibt an, ob für die Leistung (auch) eine einheitliche Stelle (eS) zuständig ist, die Anzeigen, Anträge, Willenserklärungen und Unterlagen entgegennimmt und sie unverzüglich an die für die Erbringung der Leistung zuständigen Behörden weiterleitet.

||Kennzeichen einheitliche Stelle|
|----|----|
|Soll|<ul><li>Das Kennzeichen einheitliche Stelle sollte dann gesetzt werden, wenn durch eine Rechtsvorschrift angeordnet wird, dass das Verwaltungsverfahren für diese Leistung über eine einheitliche Stelle (eS) nach § 71a-e Verwaltungsverfahrensgesetz (VwVfG) abgewickelt werden kann.</li></ul>|
|Muss|<ul><li>Achtung: Bei Aktualisierung der Liste muss auch urn:xoev-de:fim:codeliste:xzufi.zustaendigkeitsrolle aktualisiert werden.</li></ul>|
|Pflichtmodul|<ul><li>Stammtext: Ja</li><li>Leistungsbeschreibung: Ja</li></ul>|
|Technische Prüfung|<ul><li>Keine</li></ul>|

### Kurztext (Information für die Behördennummer 115)

Das Feld "Kurztext" enthält Informationen für die telefonische Beauskunftung durch Hotline-Mitarbeitende.

||Kurztext (Information für die Behördennummer 115)|
|----|----|
|Soll|<ul><li>Sollte stichpunktartig strukturiert sein.</li><li>Die Spiegelstriche sollten alle Informationen aus dem Volltext (kurz und prägnant) enthalten, ohne Informationen, die auch in anderen Modulen enthalten sind (z. B. Kosten, zuständige Stelle, Verfahrensablauf usw.).</li><li>Die Leistungsbezeichnung I kann entfallen, da diese übersichtlich und leicht zugänglich an einer anderen Stelle der Leistungsbeschreibung enthalten ist.</li><li>Die Nennung der zuständigen Behörde kann entfallen, da diese übersichtlich und leicht zugänglich an einer anderen Stelle der Leistungsbeschreibung enthalten ist.</li><li>Achtung: In den QS-Kriterien steht: <ul><li>1. Spiegelstrich: ganzer Titel der Leistung, also Leistungsbezeichnung II</li><li>letzter Spiegelstrich: zuständige Behörde</li></ul></li></ul>|
|Muss|<ul><li>Darf höchstens 1.800 Zeichen (inklusive Leerzeichen, ohne Satzzeichen) enthalten.</li></ul>|
|Pflichtmodul|<ul><li>Stammtext: Ja</li><li>Leistungsbeschreibung: Ja</li></ul>|
|Technische Prüfung|<ul><li>Self-Service: Ja (perspektivisch)<ul><li>Kriterium: Textlänge prüfen</li></ul></li><li>PVOG-Validatoren: Nein</li></ul>|

## Organisationseinheit

PVOG-Validatoren: VAL0015

### Bezeichnung

Das Feld "Bezeichnung" enthält die Angabe des Namens bzw. der offiziellen Bezeichnung der Stelle/Organisationseinheit.

||Bezeichnung|
|----|----|
|Soll|<ul><li>Die Bezeichnung sollte strukturiert erfasst werden.</li><li>Eine Organisationseinheit kann sowohl zuständige Stelle als auch Ansprechpunkt sein:<ul><li>Zuständige Stellen sind i. d. R. die Organisationseinheiten, die einen Antrag bearbeiten und den Verwaltungsakt erlassen.</li><li>Ansprechpunkte sind i. d. R. die Organisationseinheiten, die einen Antrag entgegennehmen, diesen aber nicht bearbeiten und nicht den Verwaltungsakt erlassen (z. B. Servicecenter/Bürgerbüros).</li></ul></li><li>Im Kontext XZuFi sollte hier der vollständige Name der Organisationseinheit angegeben werden, d. h. die vollständige Hierarchie (z. B. „Stadtverwaltung Musterstadt – Dezernat II – Bürgerservice“).</li></ul>|
|Muss|<ul><li>Die Rolle einer Organisationseinheit muss gesetzt sein.</li></ul>|
|Pflichtmodul|<ul><li>Stammtext: Ja</li><li>Leistungsbeschreibung: Ja</li></ul>|
|Technische Prüfung|<ul><li>Self-Service: Nein</li><li>PVOG-Validatoren: VAL0044</li></ul>|

### Kontakt und Verkehr sowie zusätzliche Informationen zur physischen Erreichbarkeit

Das Feld "Kontakt und Verkehr" gibt an, wie die Organisationseinheit physisch erreichbar ist.

||Kontakt und Verkehr sowie zusätzliche Informationen zur physischen Erreichbarkeit|
|----|----|
|Soll|<ul><li>Die Adresse (Besucheradresse, Anschrift für die Zusendung einer Postsendung) sollte angegeben werden.</li><li>Kommunikationsinformationen (Telefonnummer der Hotline, der Zentrale, Faxnummer, E-Mail-Adresse, De-Mail, Website) sollten erfasst werden.</li><li>Wenn eine Organisationseinheit erstmals erfasst wird, dann sollten auch Angaben zu „Kontakt und Verkehr“ erfasst werden, sofern es sich nicht um eine externe Organisationseinheit handelt.</li><li>Eine Anschrift kann bei Bedarf mit zusätzlichen Informationen versehen werden, z. B.: <ul><li>über einen barrierefreien Zugang,</li><li>über Haltestellen von öffentlichen Verkehrsmitteln in der Nähe oder</li><li>über Parkmöglichkeiten in der Nähe.</li></ul></li><li>Für Anschriften im Ausland sollte der Standard des jeweiligen Staates verwendet werden.</li><li>Darunter sollte der Staat auf Deutsch stehen.</li><li>Wenn mehrere Telefonnummern mit unterschiedlichen Funktionen angeben, sollten deren Funktionen in Klammern hinter den Nummern stehen.</li><li>Lange Ziffernfolgen sollten mit Leerzeichen in Vierer-Blöcke gegliedert werden.</li><li>Bei der strukturierten Erfassung von Telefon- und Faxnummern sollten zusätzliche Informationen (z. B. zu Erreichbarkeiten oder Gebühren) erfasst werden.</li><li>E-Mail-Adressen sollten als Linktext geschrieben werden.</li></ul>|
|Muss|<ul><li>Format für Anschriften in Deutschland: Organisationsname (falls erforderlich: Abteilung/Ressort), Straße, Hausnummer (falls erforderlich: Adresszusatz), PLZ und Ort.</li><li>Format für Telefon- und Faxnummern: Länderkennung + Ortsnetzkennzahl + Leerzeichen + Teilnehmerrufnummer + Bindestich + Durchwahl</li></ul>|
|Pflichtmodul|<ul><li>Stammtext: Nein</li><li>Leistungsbeschreibung: Nein</li></ul>|
|Technische Prüfung|<ul><li>Self-Service: Ja (perspektivisch)<ul><li>Kriterium: Auf korrektes Anschriften- und Telefonnummernformat prüfen, bei Hotline-Angabe auf Vorhandensein von Wochentagen und Uhrzeiten prüfen (DIN 5008), bei E-Mail-Adressen Links prüfen</li></ul></li><li>PVOG-Validatoren: VAL0003</li></ul>|

### Öffnungszeiten

Das Feld "Öffnungszeiten" enthält die Angabe der Zeiten, zu denen die Organisationseinheit erreichbar ist.

||Öffnungszeiten|
|----|----|
|Soll|<ul><li>Die Öffnungszeiten sollten strukturiert erfasst werden.</li><li>Bei verschiedenen Ausprägungen (z. B. Besuchszeit, Terminvergabe, Hotline) sollten mehrere Öffnungszeiten angegeben werden.</li></ul>|
|Muss|<ul><li>Format: hh:mm Uhr</li><li>Das Element "infoOeffnungszeitenText" darf keine HTML-Tags enthalten (Absatz &lt;p&gt; und Liste &lt;ul&gt; &lt;li&gt; sind erlaubt).</li></ul>|
|Pflichtmodul|<ul><li>Stammtext: Nein</li><li>Leistungsbeschreibung: Nein</li></ul>|
|Technische Prüfung|<ul><li>Self-Service: Ja (perspektivisch)<ul><li>Kriterium: Auf Vorhandensein von Wochentagen und Uhrzeiten prüfen (DIN 5008)</li></ul></li><li>PVOG-Validatoren: VAL0053</li></ul>|

### Zahlungsinformationen

Das Feld "Zahlungsinformationen" enthält Angaben der von der Organisationseinheit unterstützten Zahlungsweisen vor Ort.

||Zahlungsinformationen|
|----|----|
|Soll|<ul><li>Die Zahlungsinformationen sollten strukturiert erfasst werden.</li><li>Bei Vorkasse sollte die Bankverbindung angegeben werden.</li></ul>|
|Muss|<ul><li>Keine</li></ul>|
|Pflichtmodul|<ul><li>Stammtext: Nein</li><li>Leistungsbeschreibung: Nein</li></ul>|
|Technische Prüfung|<ul><li>Keine</li></ul>|

### Zuständigkeit (Leistung und Gebiet)

Das Feld "Zuständigkeit (Leistung und Gebiet)" enthält die Angabe der Leistungen, die von einer Organisationseinheit in einem bestimmten Gebiet angeboten werden.

||Zuständigkeit (Leistung und Gebiet)|
|----|----|
|Soll|<ul><li>Die Zuständigkeiten von Organisationseinheiten für Verwaltungsleistungen sollten immer an ein Gebiet oder mehrere Gebiete gebunden sein, die durch Angabe der entsprechenden amtlichen Regionalschlüssel (ARS) festgelegt werden.</li></ul>|
|Muss|<ul><li>Informationen zur zuständigen Stelle und/oder zum Ansprechpunkt müssen strukturiert im Objekt Organisationseinheit (Zuständigkeit) erfasst werden.</li><li>Zuständigkeiten dürfen nur auf die im XZuFi-Dokument gelieferte Leistung verweisen.</li><li>Für eine Leistung muss mindestens eine Organisationseinheit oder ein Online-Dienst zuständig sein.</li><li>Zwei Zuständigkeiten einer Organisationseinheit oder eines Online-Diensts dürfen nicht identisch sein.</li><li>Der Online-Dienst muss mindestens eine Zuständigkeit aufweisen.</li><li>Das Gebiet an der Online-Dienst-Zuständigkeit muss ARS-Format haben.</li><li>Eine Organisationseinheit muss für mindestens eine Leistung zuständig sein.</li><li>Die Zuständigkeit darf in einer Transfernachricht nicht übertragen werden.</li><li>Die Zuständigkeit muss mindestens ein Gebiet haben.</li></ul>|
|Pflichtmodul|<ul><li>Stammtext: Nein</li><li>Leistungsbeschreibung: Ja</li></ul>|
|Technische Prüfung|<ul><li>PVOG-Validatoren: VAL0001, VAL0002, VAL0015, 0018, VAL0019, VAL0020, VAL0029, VAL0035, VAL0049</li></ul>|

### Formulare

Das Element Formulare soll mittelfristig vollständig durch das Element Online-Dienst ersetzt und daher nicht mehr verwendet werden.

## Online-Dienst

### Bezeichnung

Das Feld "Bezeichnung" enthält die Angabe des bürgerfreundlichen Titels eines Online-Diensts.

||Bezeichnung|
|----|----|
|Soll|<ul><li>Sollte kein Arbeitstitel oder interner Titel sein.</li></ul>|
|Muss|<ul><li>Keine</li></ul>|
|Pflichtmodul|<ul><li>Stammtext: Nein</li><li>Leistungsbeschreibung: Ja</li></ul>|
|Technische Prüfung|<ul><li>Keine</li></ul>|

### Link

Das Feld "Linke" enthält die Angabe der URL zum Online-Dienst inklusive aller Parameter.

||Link|
|----|----|
|Soll|<ul><li>Sollte die URI beschreiben (z. B. kurze Hinweise zur Zielseite).</li></ul>|
|Muss|<ul><li>Vorhandene dynamische Parameter müssen zusätzlich angehangen werden.</li><li>Das Format von URIs und URLs muss RFC 2396 entsprechen.</li></ul>|
|Pflichtmodul|<ul><li>Stammtext: Nein</li><li>Leistungsbeschreibung: Ja</li></ul>|
|Technische Prüfung|<ul><li>Self-Service: Ja (perspektivisch)<ul><li>Kriterium: Link prüfen</li></ul></li><li>PVOG-Validatoren: VAL0033</li></ul>|

### Beschreibung

Das Feld "Beschreibung" enthält eine kurze allgemeine Beschreibung des Online-Diensts.

|                    | Beschreibung                                                                                                                        |
|--------------------|-------------------------------------------------------------------------------------------------------------------------------------|
| Soll               | <ul><li>Wenn kein Direktlink zum Online-Dienst möglich ist, sollte hier der Pfad beschrieben werden.</li></ul>                      |
| Muss               | <ul><li>Darf keine Links enthalten.</li><li>Darf maximal 280 Zeichen (inklusive Leerzeichen) enthalten.</li></ul>                   |
| Pflichtmodul       | <ul><li>Stammtext: Nein</li><li>Leistungsbeschreibung: Ja</li></ul>                                                                 |
| Technische Prüfung | <ul><li>Self-Service: Ja (perspektivisch)<ul><li>Kriterium: Auf Textlänge prüfen</li></ul></li><li>PVOG-Validatoren: Nein</li></ul> |

### Hilfetext

Das Feld "Hilfetext" gibt an, was Nutzende zur Nutzung des Online-Dienstes vorab wissen sollten.

||Hilfetext|
|----|----|
|Soll|<ul><li>Keine</li></ul>|
|Muss|<ul><li>Keine</li></ul>|
|Pflichtmodul|<ul><li>Stammtext: Nein</li><li>Leistungsbeschreibung: Nein</li></ul>|
|Technische Prüfung|<ul><li>Keine</li></ul>|

### Zahlungsmethode

Das Feld "Zahlungsmethode" enthält die Angabe der Online-Zahlungsmethode(n), über die der Online-Dienst verfügt.

||Zahlungsmethode|
|----|----|
|Soll|<ul><li>Wenn keine Kosten anfallen, sollte in der Bemerkung schlicht „Keine“ angegeben werden.</li></ul>|
|Muss|<ul><li>Für Online-Dienste muss mindestens eine EU-weit gängige Online-Zahlungsmethode angegeben werden.</li></ul>|
|Pflichtmodul|<ul><li>Stammtext: Nein</li><li>Leistungsbeschreibung: Ja</li></ul>|
|Technische Prüfung|<ul><li>Self-Service: Ja (perspektivisch)<ul><li>Kriterium: Bei strenger Prüfung: auf Wortkatalog prüfen</li></ul></li><li>PVOG-Validatoren: Nein</li></ul>|

### Vertrauensniveau

Das Feld "Vertrauensniveau" enthält die Angabe des Vertrauensniveaus, das der Online-Dienst voraussetzt, und der Identifizierungsmittel für die Antragstellung oder die elektronische Kommunikation.

||Vertrauensniveau|
|----|----|
|Soll|<ul><li>Keine</li></ul>|
|Muss|<ul><li>Keine</li></ul>|
|Pflichtmodul|<ul><li>Stammtext: Nein</li><li>Leistungsbeschreibung: Nein</li></ul>|
|Technische Prüfung|<ul><li>Keine</li></ul>|

### Identifizierung

Das Feld "Identifizierung" enthält die Angabe des in Abhängigkeit vom Vertrauensniveau notwendigen Identifizierungsmittels.

||Identifizierung|
|----|----|
|Soll|<ul><li>Verwaltungsdienstleistungen mit hohem Vertrauensniveau sollten hohe Anforderungen an das Identifizierungsmittel stellen.</li><li>Verwaltungsdienstleistungen mit niedrigerem Vertrauensniveau sollten geringere Anforderungen an das Identifizierungsmittel stellen.</li></ul>|
|Muss|<ul><li>Keine</li></ul>|
|Pflichtmodul|<ul><li>Stammtext: Nein</li><li>Leistungsbeschreibung: Ja</li></ul>|
|Technische Prüfung|<ul><li>Keine</li></ul>|

### Sprachen außer Deutsch

Das Feld "Sprachen außer Deutsch" gibt an, welche weiteren Sprachen außer Deutsch übertragen werden.

||Sprachen außer Deutsch|
|----|----|
|Soll|<ul><li>Wenn keine weitere Sprache vorhanden ist, sollte hier schlicht „Keine“ angegeben werden.</li></ul>|
|Muss|<ul><li>Alle übertragenen Sprachen müssen durch die Sprachversion deklariert sein.</li><li>Alle Module, die mehrsprachig geliefert werden, müssen das Attribut „languageCode“ enthalten.</li></ul>|
|Pflichtmodul|<ul><li>Stammtext: Nein</li><li>Leistungsbeschreibung: Ja</li></ul>|
|Technische Prüfung|<ul><li>Self-Service: Ja (perspektivisch)<ul><li>Kriterium: Vorhandensein prüfen, „Keine“ prüfen</li></ul></li><li>PVOG-Validatoren: VAL0023, VAL0041, VAL0043</li></ul>|

### Gültigkeitsgebiet

Das Feld "Gültigkeitsgebiet" gibt an, für welches Gebiet der Online-Dienst zuständig gültig ist.

|                    | Gültigkeitsgebiet                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
|--------------------|----------------------------------------------------------------------------------------------------------|
| Soll               | <ul><li>Keine</li></ul>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| Muss               | <ul><li>Online-Dienste müssen eine ID sowie eine „schemeAgencyID“ in gültiger Form enthalten.</li><li>Das Gültigkeitsgebiet einer Leistung muss ARS-Format haben.</li><li>Die Leistung muss ein Gültigkeitsgebiet besitzen.</li><li>Zwei Zuständigkeiten am Online-Dienst dürfen nicht identisch sein.</li><li>Der Online-Dienst muss mindestens eine Zuständigkeit besitzen.</li><li>Eine im Online-Dienst referenzierte Leistung muss existieren.</li><li>Ein Redaktionssystem darf die Daten nur für erlaubte Gebiete liefern.</li><li>Die Angabe von „zustaendigkeitGebietID“ und „zustaendigkeitLeistungID“ wird logisch UND verknüpft, d. h. die OE muss für alle Leistungen für alle Gebiete zuständig sein.</li></ul> |
| Pflichtmodul       | <ul><li>Stammtext: Nein</li><li>Leistungsbeschreibung: Ja</li></ul>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| Technische Prüfung | <ul><li>Self-Service: Ja (perspektivisch)</li><ul><li>Kriterium: Auf Konsistenz prüfen</li></ul><li>PVOG-Validatoren: VAL0008, VAL0010, VAL0016, VAL0017, VAL0018, VAL0019, VAL0020, VAL0021, VAL0026, VAL0055</li></ul>|