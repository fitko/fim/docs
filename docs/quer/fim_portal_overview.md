---
sidebar_label: FIM-Portal Überblick
---

# FIM Portal Überblick

Das FIM Portal ist die Webseite für alle Themen rund um FIM, insbesondere eine kurze Erklärung für Einsteigende, vertiefende Dokumentation für (angehende) Experten/-innen und eine Suche über die FIM-Daten aus den drei Bausteinen. Es enthält Links zu Schulungsmaterialien, Kontaktinformationen und ermöglicht über RSS-Feeds, immer auf dem Laufenden gehalten zu werden. Arbeitsmittel für Dienstleistende und Redaktionen stehen hier zum Download zur Verfügung.

Das FIM-Portal gibt es im Oktober 2024 in zwei Ausprägungen:
- https://fimportal.de, die technisch nicht mehr weiterentwickelt wird,
- https://schema.fim.fitko.net, die gerade in Entwicklung befindliche Nachfolgelösung für https://fimportal.de.

Beide Plattformen werden von der FITKO betrieben. 


## Weiterführende Informationen:
- zu [Datenquellen](./fim_portal_sources.md) des neuen FIM-Portals
- zu detaillierten [Unterschiede](./fim_portal_differences.md) zwischen altem und neuem FIM-Portal