---
sidebar_label: Einsatzgebiete
---

# Einsatzgebiete von FIM

In der [Erläuterung](fim_einleitung) zu FIM haben wir gelernt, dass FIM Grundlagen für die Digitalisierung schafft. Im Folgenden schauen wir auf Anwendungsfälle, welche auf FIM aufbauen oder von FIM profitieren.


## FIM im Kontext von SDG

Das föderale Informationsmanagement (FIM) ist ein zentrales Instrument zur strukturierten und einheitlichen Beschreibung von Verwaltungsleistungen. Durch eine standardisierte Methodik fördert es die Zusammenarbeit zwischen Bund und Ländern und steigert die Effizienz von Verwaltungsabläufen. FIM unterstützt zudem die Barrierefreiheit der Verwaltungsprozesse.

Im Kontext der Europäischen Verordnung zum Single-Digital-Gateway (SDG-VO) spielen die FIM-Methodik und insbesondere die FIM-Leistungsbeschreibungen eine wesentliche Rolle. Sie ermöglicht, dass deutsche Verwaltungsleistungen über nationale Grenzen hinweg europäischen Bürgerinnen, Bürgern und Unternehmen auf dem [Your-Europe-Portal](https://europa.eu/youreurope/index_de.htm)  bereitgestellt werden. Dieses Portal ist vergleichbar mit dem [deutschen Verwaltungsportal](https://verwaltung.bund.de/portal/DE/). Leistungen, die während des Redaktionsprozesses als SDG-relevant identifiziert werden, sind dank der Anbindung des deutschen Verwaltungsportals an das Your-Europe-Portal unter https://europa.eu/youreurope/ auffindbar.

Die einheitliche Bereitstellung von Informationen über Verwaltungsleistungen und Online-Dienste ermöglicht Europäerinnen und Europäern sowie Unternehmen einen schnellen und nutzerfreundlichen Zugriff auf qualitativ hochwertige und aktuelle Informationen. Dadurch können Anfragen digital und effizient über Ländergrenzen hinweg erledigt werden. 
Beispiel: Wer auf der Suche nach Informationen zum Grundstückskauf in Deutschland (Berlin) ist, kann die erforderlichen Informationen über die Suchfunktion des Your-Europe-Portal finden und wird über das deutsche Verwaltungsportal zu der digitalen Antragsstrecke und den relevanten Informationen in Berlin [weitergeleitet](https://verwaltung.bund.de/leistungsverzeichnis/DE/leistung/99012120001000/herausgeber/BE-L100108_330797/region/110000000000). 

Der Einsatz von FIM im Rahmen der SDG-Umsetzung verdeutlicht, dass FIM nicht nur die nationalen Digitalisierungsstrategien unterstützt. Es trägt auch zur Erfüllung europäischer Standards bei und fördert dabei den „Einer für Alle“-Gedanken (EfA). FIM harmonisiert Verwaltungsleistungen über nationale Anforderungen hinaus und unterstützt so grenzüberschreitend eine nachhaltige und moderne Verwaltungslandschaft.

Autorin: Zentrale Bundesredaktion

Quellen und weiterführende Informationen:
- [Integrationsleitfaden Bund](https://www.digitale-verwaltung.de/SharedDocs/downloads/Webs/DV/DE/integrationsleitfaden-bund.pdf?__blob=publicationFile&v=7)
- [FAQ-Lexikon OZG](https://www.digitale-verwaltung.de/Webs/DV/DE/aktuelles-service/faq-lexikon/faq-ozg/faq-node.html#doc21103540bodyText4)
- [SDG im OZG-Kontext](https://www.digitale-verwaltung.de/Webs/DV/DE/onlinezugangsgesetz/info-sdg/info-sdg-node.html)
- [Single-Digital-Gateway VO (EU 2018/1724)](https://eur-lex.europa.eu/legal-content/de/TXT/?uri=CELEX%3A32018R1724)	

