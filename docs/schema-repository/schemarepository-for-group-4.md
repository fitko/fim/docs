# Infos für Entwickler:innen

Um Systeme an die API des Sammelrepository angebunden werden sollen, finden Entwickler:innen in diesem Abschnitt Links zu wichtigen Informationen. Hinweise zur Nutzung und zum fachlichen Verständnis der API finden sich in den folgenden Abschnitten

## Für Eilige

- TEST Umgebung: Die frühere TEST Umgebung <https://test.schema-repository.fitko.dev/> wurde nach Rollout von STAGE (s.u.) abgeschaltet.
- STAGE Umgebung: OpenAPI https://stage.schema.fim.fitko.net/docs und Weboberfläche  https://stage.schema.fim.fitko.net (letztere nur zu Testzwecken, kein STAGE-Qualitätsversprechen)
- PROD Umgebung: OpenAPI https://schema.fim.fitko.net/docs# und Weboberfläche https://schema.fim.fitko.net (letztere nur zu Testzwecken, kein PROD-Qualitätsversprechen)
- Öffentliche Entwicklungsumgebung bei OpenCode: <https://gitlab.opencode.de/fitko/fim/schema-repository>; Issues können per E-Mail erstellt werden: git+fitko-fim-schema-repository-1010-issue-@opencode.de
- Anwendungsdokumentation (in der diese Zeilen veröffentlicht werden): <https://docs.fitko.de/fim/docs>

## Für API-Anbindung

Für die Nutzung des Sammelrepository wurde der API-first-Ansatz gewählt. Entwickler:innen anderer Systeme nutzen das Sammelrepository über eine REST-API.

- API-Dokumentation: XXX
- Anwendungsbeispiele: XXX
- Testfälle: XXX
- Spezifikation des JSON-Schema-Konverters: XXXX
- Spezifikation des XML-Schema-Konverters: XXXX
- Spezifikation des Up-/Downcasts: XXX
- Spezifikation des Qualitäts-Checks: XXXX
- Dokumentation zum zugrundeliegenden Datenmodell: XXX
- Funktionsweise des Sammelrepository: XXX

Weitergehende Dokumentation findet sich im zugrundeliegenden Standard [XDatenfelder](https://www.xrepository.de/details/urn:xoev-de:fim:standard:xdatenfelder) sowie im [Fachkonzept](https://fimportal.de/securedl/sdl-eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDgzNjA1MjUsImV4cCI6MTcwODQ1MDUyNSwidXNlciI6MCwiZ3JvdXBzIjpbMCwtMV0sImZpbGUiOiJmaWxlYWRtaW4vQmlibGlvdGhlay9Eb2t1bWVudGUvQmF1c3RlaW5fRGF0ZW5mZWxkZXIvMjAxOTEyMThfRGF0ZW5mZWxkZXJfRmFjaGtvbnplcHQucGRmIiwicGFnZSI6Mzl9.4DsFqbad2gQ0aXsRGArvvCwTxoZ2f8GqFjPRTyAbNE4/20191218_Datenfelder_Fachkonzept.pdf) (was kann mit dem Standard erreicht werden) sowie den [QS-Kriterien](https://fimportal.de/securedl/sdl-eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDgzNjA1MjUsImV4cCI6MTcwODQ1MDUyNSwidXNlciI6MCwiZ3JvdXBzIjpbMCwtMV0sImZpbGUiOiJmaWxlYWRtaW4vQmlibGlvdGhlay9Eb2t1bWVudGUvQmF1c3RlaW5fRGF0ZW5mZWxkZXIvMjAyMTA5MjNfQmF1c3RlaW4tRGF0ZW5mZWxkZXItUXVhbGl0YWV0c2tyaXRlcmllbl9fMV8ucGRmIiwicGFnZSI6Mzl9.VRF5bzqcKYwecXE8By3_pR5iee-OoS9iEfX8MsxmeYc/20210923_Baustein-Datenfelder-Qualitaetskriterien__1_.pdf) (wie kann etwas mit dem Standard erreicht werden).

## Schema Repository API

Die REST-API ist [Teil dieser Dokumentation](https://docs.fitko.de/fim/docs/schema-repository/schema-repository-api) und findet sich identisch auch auf der [PROD Umgebung](https://schema.fim.fitko.net).

Die API hat drei Gruppen:

- api: Suche mit Filtern nach, Download spezifischer und Upload veröffentlichter Dateien; diese müssen XDF2 oder XDF3-Format haben.
- immutable: Das Sammelrepository verspricht, Dateien, die einmal hochgeladen wurden, unter einer eindeutigen URL beliebig lange vorzuhalten und über diese Endpunkte die Schematas, Dokumentsteckbriefe und Codelisten zum Download bereit zu stellen.
- tools: Zur Nutzung der XDF-Dateien stehen Dienste bereit zur Konvertierung (von XDF nach JSON-Schema bzw. XSD), zum Upcast (von XDF2 zu XDF3) und Downcast (XDF3 zu XDF2) und zum Qualitäts-Check.

Anwendungsbeispiele, in denen Nutzung der Endpunkte in den drei Gruppen detailliert werden, finden sich in den folgenen Abschnitten.

## Anwendungsbeispiele - Vorbemerkung

Die hier beschriebenen Beispiele sind praxisnah, aber nicht erschöpfend. Entwickler:innen, die weitere Nutzungsszenarien ergänzen wollen, können dies gerne über die E-Mail-Adresse des Projekts tun.

## Anwendungsbeispiel FIT-Connect-Einreichung

Das Sammelrepository dient - neben den anderen Nutzensszenarien - als Ablösung für die bisher von FIT-Connect verwendete Ablage https://schema.fitko.de/fim

Die [FIT-Connect-Einreichung](https://docs.fitko.de/fit-connect/docs/getting-started/submission/structure) enthält u.a. einen Fachdatensatz, der die Daten eines Antrags oder Berichts enthält und einem im Zustellpunkt vorgegebenen Datenschema (`submissionSchema`) entspricht.

Auch der [FIT-Connect Metadatensatz](https://docs.fitko.de/fit-connect/docs/getting-started/submission/metadata) enthält eine Referenz auf das Fachdatenschema.

Die Fachschemareferenz besteht aus der  `schemaUri`, die das zu verwendende Fachschema referenziert, und dem `mimeType`.

Bisher hat FIT-Connect in der `schemaUri` eine Adresse verwendet, die zu einem Verzeichnis von mit einem [Proof-of-Concept-Werkzeug](https://docs.fitko.de/fit-connect/docs/getting-started/schema-validation#fim-json) konvertierten JSON-Schemata führte. Die Adresse hatte den Aufbau `https://schema.fitko.de/fim/s{FIM-ID}_{Version}.schema.json`. Eine Beispiel-URL wäre also `https://schema.fitko.de/fim/s00000121_1.0.schema.json`.

Zukünftig wird die `schemaUri` auf die unveränderliche URL eines Schemas im Sammelrepository verweisen. Diese hat den Aufbau `schema.fim.fitko.net/immutable/schemas/S{FIM-ID}}V{Version}}_{Zeitstempel des Uploads}}.schema.json`. Die Beispiel-URL wäre also `schema.fim.fitko.net/immutable/schemas/S00000121V1.0_2024-01-25-1706199754837.schema.json`.

Im Kontext einer Zustellpunkt-Beschreibung wäre dies

```json
"submissionSchemas":[  
	{  
		"schemaUri":"schema.fim.fitko.net/immutable/schemas/S00000121V1.0_2024-01-25-1706199754837.schema.json",  
		"mimeType":"application/json"  
	}  
]
```

Die Details zur Ermittlung der `schemaUri` finden sich im Abschnitt "Ermittlung der URL im Sammelrepository" weiter unten.

## Anwendungsbeispiel Formularerstellung

Verschiedene Formularsysteme, Antragsmanagementssysteme oder Onlinedienste stellen Webseiten für Antragstellende und Berichtspflichtige zur Verfügung, mit denen die Antrags- bzw. Berichtsdaten online erfasst werden können. Der Aufbau dieser Webseiten muss gleichermaßen Kriterien an Bedienungsfreundlichkeit, Barrierefreiheit sowie Rechtssicherheit erfüllen.

Um Rechtssicherheit zu gewährleisten, bietet sich Arbeit auf Basis von FIM-Stamminformationen an, das resultierende Datenschema heisst im FIM-Kontext Referenzdatenschema und findet sich - sofern von den Modellierern veröffentlicht - im Sammelrepository und kann von dort über eine eindeutige URL abgerufen werden, z.B. diese für die Version 1.4 des Wohngelds im XSD-Format: https://schema.fim.fitko.net/immutable/schemas/S00000159V1.4_2024-01-22-1705942219513.xsd

## Anwendungsbeispiel Datenvalidierung

Fachverfahren, die Daten aus externen Quellen erhalten, legen in der Regel ein striktes Datenschema fest. Ohne dieses wären kleine und größere Abweichungen die Ursache für Fehler, abgelehnte Dateneinreichungen, sinkende Datenqualität oder manuelle Nacharbeit.

Um Schemakonformität zu gewährleisten, kann das Datenschema in bilateraler Kommunikation übermittelt werden, z.B. per E-Mail; dieses Vorgehen ist manuell aufwändig und fehleranfällig.

Für den Abruf des Datenschemas im Sammelrepository ist es lediglich notwendig zu ermitteln, ob es eine XDF-Datei gibt, die den Dateneinreichungen zugrunde liegt und ins Sammelrepository veröffentlicht wurde. Liegt diese vor, dann können statt XDF auch andere Schema-Formate der Datei heruntergeladen werden.

Wenn der fachliche Name, die FIM-ID oder der Dateiname selbst bekannt sind, kann damit die URL ermittelt werden (s.u.) und die Schema-Datei zur Validierung über die API eingebunden werden, z.B. die Version 1.4 der Wohngeld-Erstantrag-JSON-Schema-Datei: https://schema.fim.fitko.net/immutable/schemas/S00000159V1.4_2024-01-22-1705942219513.xsd

## Ermittlung der URL im Sammelrepository

In obigen Beispielen wurde eine unveränderliche URL verwendet, die zu ermitteln einige oder alle der folgenden Schritte voraussetzt:

1. Der genaue Name des Schemas ist bekannt, z.B. "Wohngeld Mietzuschuss - Erstantrag", nicht aber die FIM-ID. Dann wird die FIM-ID über einen Aufruf der Such-API ermittelt: https://schema.fim.fitko.net/api/v0/schemas?name=Wohngeld%20Mietzuschuss%20-%20Erstantrag&offset=0&limit=200. Die Response ist eine JSON-Struktur, die u.a. `"fim_id": "S00000159"` enthält.
2. Die FIM-ID ist bekannt, nicht aber die Version. Dann werden alle Versionen über einen Aufruf der Such-API ermittelt: https://schema.fim.fitko.net/api/v0/schemas/S00000159. Die Response ist eine JSON-Struktur, die jede Version enthält, im Beispiel u.a. `"fim_version": "1.4"`
3. Die FIM-ID und Version sind bekannt, nicht aber der Dateiname. Dann wird dieser über einen Aufruf der Such-API ermittelt: https://test.schema-repository.fitko.dev/api/v0/schemas/S00000159/1.4. Alternativ ist auch eine Suche nach der letzten Version über `latest` statt `1.4` möglich. Die Response ist eine JSON-Struktur, die alle relevanten Dateinamen enthält: Ursprüngliche XDF-Datei (hier: XDF2), ursprüngliche Codelisten (hier wegen XDF2 nicht wie bei XDF3 referenziert, sondern hochgeladen), sowie die konvertierten Dateien im JSON-Schema und XML Schema Format:

```json
  "uploads": [
    {
      "filename": "S00000159V1.4_2024-01-10-1704899693925.xdf2.xml",
      "upload_time": "2024-01-10T16:14:53.925698+01:00",
      "code_lists": [
        {
          "id": 1117,
          "genericode_canonical_version_uri": "urn:de:fim:codeliste:verwandtschaftsverhältniswohngeld_2020-03-30",
          "genericode_canonical_uri": "urn:de:fim:codeliste:verwandtschaftsverhältniswohngeld",
          "genericode_version": "2020-03-30",
          "is_external": false
        },
        {
          "id": 1118,
          "genericode_canonical_version_uri": "urn:de:fim:codeliste:wohngeldzahlungsempfaenger_2020-10-30",
          "genericode_canonical_uri": "urn:de:fim:codeliste:wohngeldzahlungsempfaenger",
          "genericode_version": "2020-10-30",
          "is_external": false
        },
		…
      ]
    }
  ],
  "json_schema_files": [
    {
      "filename": "S00000159V1.4_2024-01-10-1704899695155.schema.json",
      "upload_time": "2024-01-10T15:14:55.155692+00:00",
      "generated_from": "S00000159V1.4_2024-01-10-1704899693925.xdf2.xml"
    }
  ],
  "xsd_files": [
    {
      "filename": "S00000159V1.4_2024-01-10-1704899696719.xsd",
      "upload_time": "2024-01-10T15:14:56.719452+00:00",
      "generated_from": "S00000159V1.4_2024-01-10-1704899693925.xdf2.xml"
    }
  ]
}
```

Zum verwendeten Namensschema finden sich Details unter "Eindeutige URIs der immutable-Ressourcen" im Abschnitt "Funktionsweise des Sammelrepository".

## Testfälle

Testfälle im Kontext des Sammelrepository können zwei verschiedene Ziele erfüllen:

* Tests für die korrekte Funktion des Sammelrepository
* Tests für die korrekte Anbindung des jeweiligen Systems an das Sammelrepository

Für den ersten Zweck gibt es

* eine Sammlung von Test-Skripts, die im Build-Prozess automatisiert ausgeführt werden; diese findet sich im [Gitlab-Projekt](https://gitlab.opencode.de/fitko/fim/schema-repository/-/tree/main/tests),
* eine Sammlung von API-Aufrufen, gesammelt in einem öffentlichen Postman-Workspace; diese sind noch nicht veröffentlicht. XXX

Für den zweiten Zweck wird empfohlen, die API-Aufrufe des Postman-Workspace für die jeweils eigenen Zwecke anzupassen.

## JSON Schema Konverter

Die folgende Spezifikation fasst das Verhalten des Dienstes, der XDF2 und XDF3 nach JSON Schema konvertiert, zusammen.

XXX

## XML Schema Konverter

## Upcast/Downcast

Beim Upcast von XDF2 nach XDF3 werden die in der Quelldatei referenzierten Codelisten zu code/name-Strukturen innerhalb der XDF3-Datei.

Beim Downcast von XDF3 nach XDF2 werden die Codelisten, die in der XDF3-Datei referenziert wurden, nicht mit exportiert; sie sind von der jeweiligen Referenz (in der Regel XRepository) zu beziehen.

## Spezifikation des Qualitäts-Checks

Die beim Qualitäts-Check zum Zuge kommenden Prüfungen sind weitestgehend identisch mit denen, die bereits über Jahre im Umfeld zunächst von FRED Classic anwendet wurden. Der Maintainer hat sie zuletzt als Open Source Projekt veröffentlicht: https://github.com/vs5000/FIM-XSLT

Die Liste der Fehler und Warnungen wird nachgetragen. XXX

## Dokumentation zum zugrundeliegenden Datenmodell

Das Sammelrepository arbeitet mit einem physischen Datenmodell in der Datenbank sowie mit zwei Datenmodellen gemäß Spezifikation XDF2 und XDF3.

Das Datenbankmodell orientiert sich an XDF3, so dass importierte Dateien geparsed und XDF3-bezogene Informationen in der Datenbank eingetragen werden.

An den Schnittstellen nach außen werden zum Up-/Download die beiden Formate XDF2 und XDF3 genau gemäß Spezifikation angeboten.

Ausnahme ist der Upload von XDF2: Viele zurzeit (Januar 2024) im Umlauf befindliche XDF2-Dateien sind nicht konform mit dem Spezifikations-Schema (der XSD-Datei), so dass sie beim Import zu einem Fehler führen würden. Daher wurde auf einen Schema-Konformitäts-Test beim Import von XDF2 verzichtet.

## Details zur Arbeit mit dem Sammelrepository

Zur technischen Funktionsweise des Sammelrepository gibt es eine [README-Datei](https://gitlab.opencode.de/fitko/fim/schema-repository/-/blob/main/README.md) und [Architekturbeschreibung](https://gitlab.opencode.de/fitko/fim/schema-repository/-/tree/main/docs/arc42), die Teil des Gitlab-Projekts sind.

Einige Punkte, die zu Rückfragen führen könnten, haben wir hier zusammengetragen:

### Eindeutige URIs der immutable-Ressourcen:

Diese wirken komplizierter als notwendig, wenn nur FIM-ID und Version nötig erscheinen. Für den Abruf der Ressourcen wurde zusätzlich eine eindeutige Datumsangabe in den Ressourcennamen eingefügt, z.B. `2024-01-25-1706199754837` in https://schema.fim.fitko.net/immutable/schemas/S00000121V1.0_2024-01-25-1706199754837.schema.json.

Zur Erklärung: Sollte die Software, die aus den XDatenfelder-Schema-Dateien die JSON-Schema-Datei konvertiert, geändert werden, können die bisherigen JSON-Schema-Dateien weiterhin unverändert abgerufen werden, es wird zugleich eine neuere Fassung des JSON-Schemas mit der neueren Konvertierungs-Software geben, erkannbar an der neueren Datumsangabe.

Zur Nutzung: Die Adresse zum Abruf einer Schema-Ressource wird in einer Weise ermittelt, die im Abschnitt "Ermittlung der URL im Sammelrepository" beschrieben ist.

### Einzel-Uploads

Bei XDF2-Uploads über die API können Codelisten mit hochgeladen werden. Fehlende Codelisten werden als extern markiert.

### Massen-Uploads

Die zum Zeitpunkt Januar 2024 noch in Betrieb befindliche "FIM-Portal-Schnittstelle" für den Import aus Landesredaktions-Repositories in das FIM-Portal wird auch für den Import in das Sammelrepository genutzt.

### Fehler bei Massen-Uploads

Fehler bei diesem Import bei einem Datenschema aufgrund von Parser- oder Netzwerk-Fehlern führen dazu, dass betroffene Datenschematas ignoriert und beim nächsten Schema des Quell-Systems weiter gemacht wird.

Fehler bei diesem Import beim Zugriff auf die Startseite des Landesrepositories führen dazu, dass das betreffende System übrersprungen wird und der Import bei anderen Repositories fortgesetzt wird.

Alle Fehler werden geloggt und können mit dem Anwendungsbetrieb im Nachgang untersucht werden.

### Volltextsuche

Der Request-Parameter `fts_query` steht für "full text search query", eine Volltextsuche, die auf Basis der [PostgreSQL-Volltextsuche](https://www.postgresql.org/docs/current/textsearch.html) umgesetzt wurde.

### Suche in der Web UI

Die Suche in der Web UI nutzt die Volltextsuche (s.o.) zur suche über Datenschemata und Dokumentsteckbriefe.

### Upload von Dateien

Dateien, die in das Sammelrepository veröffentlicht, importiert oder hochgeladen werden, werden in unveränderter Weise in der Datenbank vorgehalten, um später ebenso unverändert wieder heruntergeladen werden zu können. Sie werden nicht im Filesystem des Servers abgelegt.

### Standardkonformität, Parser

Nur Standard-konforme Dateien werden in das Sammelrepository hochgeladen (Ausnahmen s.u.); diese werden mit einem Parser in einen Strukturbaum zerlegt, der im Wesentlichen den XDF3-Strukturelementen entspricht; dies ermöglicht u.a. den Zugriff auf die Datenfelder und Datenfeldgruppen innerhalb des Schemas.

Der Parser hat folgende Regeln, die über die Standard-Schema-Konformität hinausgehen und beim Upload von Dateien berücksichtigt werden müssen:

- Version als Pflichtfeld: Die `ElementIdentifikation` in den Standards XDF2 und XDF3 sieht die `id` als Pflicht-, die `version` hingegen als optionales Element vor. Für die Zwecke des Schema-Repositories ist es erforderlich, die `version` immer anzugeben. Konkretes Beispiel: Ein Upload wird abgelehnt werden, wenn in einem Schema ein Feld zwar mit FIM-ID, nicht aber mit Versionsnummer beschrieben wird.

### Upload-Datum

Dateien, die in das Sammelrepository veröffentlicht, importiert oder hochgeladen werden, werden in der zugrundliegenden Datenbank mit einem Datumsstempel versehen. Dieser sichert die Eindeutigkeit der Datei im Fall von Namenskonflikten. Schnittstellenpartner, die Änderungen am Datenbestand des Sammelrepository in ihre Systeme synchronisieren wollen, verwenden dazu das Merkmal `updated_since` (nicht das Merkmal `gueltig_ab`,  das sich auf die zugrundeliegenden Rechtsgrundlagen bezieht)

### Download von Dateien

Ein Download von Dateien und Datenstrukturen ist nur über die /immutable Endpunkte möglich. Die /api-Endpunkte ermöglichen den Abruf von Metadaten zu den Dateien und Datenstrukturen.

### Versionskonflikte

Ein Datenschema, eine Datenfeldgruppe und ein Datenfeld sind alle über die FIM-ID und Versionsnummer eindeutig innerhalb eines Redaktionssystems identifiziert. Jedes Redaktionssystem ist mit einem Nummernkreis eindeutig verbunden; keine zwei Bundesländer oder andere Organisationen können Datenschematas des gleichen Nummernkreis bearbeiten. Aus diesen zwei Aussagen folgt, dass ein Datenschema mit FIM-ID und Versionsnummer über alle Grenzen hinweg eindeutig sein sollte.

Für den Fall, dass die Regeln der Redaktionssysteme verletzt werden und zwei Datenschemata mit identischer FIM-ID und Versionsnummer existieren und beide ins Sammelrepository hochgeladen werden, gibt es hier keine Konfliktauflösung. Es wird auch nicht geprüft, ob beispielsweise lediglich der Freigabestatus geändert wurde. Der letzte Upload wird behalten und erhält eine eindeutige (immutable) Adresse zum Download. Der ältere Upload behält seine eindeutige (immutable) Adresse zum Download, ist aber nicht mehr über die Suche zu ermitteln.

Dass damit fachliche Konflikte möglich sind, ist bekannt, kann aber vom Sammelrepository nicht aufgelöst werden. Es wird vielmehr erwartet, dass zuliefernde Systeme die Konflikte unterbinden, indem geänderte Dateien eine neue Versionsnummer erhalten.

### XDF2 und XDF3

Der Aufbau des Sammelrepository fiel in eine Zeit, zu der ausschließlich XDF2-Dateien über das FIM-Portal veröffentlicht wurden. Mit wachsender Verbreitung von XDF3 und den diesem Standard innewohnenden Vorteilen wurde entschieden, die interne Repräsentation von XDatenfelder-Dateien weitestgehend an XDF3 zu orientieren.

Für Nutzende von XDF2 bringt dies keine Einschränkung mit sich: Das Sammelrepository unterstützt XDF2 bei Upload und Download, bei Qualitäts-Check und Konvertierung in andere Datenschema-Formate, beim Upcast nach XDF3 und beim Downcast zurück von XDF3 nach XDF2.

Manche Besonderheiten gibt es: Der Status eines Schemas wird nicht mit den im [XDF2-Spezifikationsdokument](https://www.xrepository.de/api/xrepository/urn:xoev-de:fim:standard:xdatenfelder_2.0:dokument:XDatenfelder_Spezifikation:datei:XDatenfelder_2.0_Spezifikation.pdf) vorgesehenen drei Werten "aktiv", "in Vorbereitung", "inaktiv", sondern mit den acht Werten aus der Codeliste urn:xoev-de:xprozess:codeliste:status, die in der [XDF3-Spezifikation](https://www.xrepository.de/api/xrepository/urn:xoev-de:fim:standard:xdatenfelder_3.0.0:dokument:XDatenfelder_3.0.0_Spezifikation:datei:XDatenfelder_3.0.0_Spezifikation.pdf) referenziert wird. In der Datenbank wird der Status XDF3-kkonform gespeichert, die vom FJD-Redaktionssystem für das FIM-Portal bereitgestellten Veröffentlichungsinformationen enthalten bereits diesen XDF3-Status.

### Der Namespace baukasten

Die Datenfeldgruppen und Datenfelder sind einem namespace zugeordnet, weil Schemata auch Schema-lokale Elemente (Nummernkreis 99, vgl. [Fachkonzept Baustein Datenfelder](https://fimportal.de/securedl/sdl-eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDcxMzgwODYsImV4cCI6MTcwNzIyODA4NiwidXNlciI6MCwiZ3JvdXBzIjpbMCwtMV0sImZpbGUiOiJmaWxlYWRtaW4vQmlibGlvdGhlay9Eb2t1bWVudGUvQmF1c3RlaW5fRGF0ZW5mZWxkZXIvMjAxOTEyMThfRGF0ZW5mZWxkZXJfRmFjaGtvbnplcHQucGRmIiwicGFnZSI6Mzl9.eVEpXyGD_S_rmH_UkHahXTUu_aJnrK_rhZy8yTjYNP4/20191218_Datenfelder_Fachkonzept.pdf)) enthalten können, die nur innerhalb eines Schemas eindeutig über die ID identifiziert werden. Der Namespace wird bei Schema-lokalen Elemente entsprechend als die ID des Schemas gesetzt, damit so Konflikte bei identischen IDs Schema-lokaler Elemente vermieden werden.

Beim Parsen wird der namespace mit "baukasten" belegt. Bei Elementen, die nicht Schema-lokal sind (Nummernkreis != 99), wird der namespace auf den globalen Wert "baukasten" gelegt, da es sich hier um den Namespace der global verfügbaren Baukastenelemente handelt und es per Definition nicht zu Mehrfachverwendungen einer ID für unterschiedliche Elemente kommen darf.

Die Bezeichnung "Baukasten" ist frei gewählt, denkbar wäre hier auch gewesen, dies leer zu lassen (aus technischen Gründen unerwünscht) oder eine andere Bezeichnung zu wählen.

### Elementsuche im Baukasten

Im Sammelrepository ist die Suche nach einzelnen Datenfeldern oder Datenfeldgruppen möglich; diese werden vom Parser (s.o.) in der Datenbank gespeichert. Um gleichbenannte

### Nummernkreis, Herausgeber, Landesredaktion

In der API ist nur vom Nummernkreis die Rede, nicht von Herausgeber oder Landesredaktion. Nutzende sollen wissen, dass die (semantische) Herausgeber-Eigenschaft technisch vom Nummernkreis repräsentiert wird. Für den Zweck der API-Nutzung sind diese Begriffe also äquivalent.

Landesredaktionen sind Herausgeber, es kann beispielsweise mit dem Baustein Datenfelder auch andere Herausgeber geben.