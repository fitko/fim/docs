# LeiKa-Struktur

Im LeiKa werden alle Leistungen der öffentlichen Verwaltung im vorgenannten Sinne verzeichnet. Zentrales Referenzierungsmerkmal jeder erfassten Leistung ist der LeiKa-Schlüssel. Für einzelne Leistungen von ausschließlich regionaler Bedeutung, die keinen Eingang in den LeiKa finden, können die „freien Adressräume“ des LeiKa unter Nutzung der Länderinstanz verwendet werden.

Die Struktur des LeiKa wird in der folgenden Abbildung dargestellt:

![Grundstruktur des LeiKa](Grundstruktur%20des%20LeiKa.jpg) 

**Erläuterungen zur LeiKa-Struktur:**

| **Strukturkomponente**    | **Beschreibung**                                                                                                                                                                                                                                                                                                         |
| ------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| Staatenkennung            | Bei Verwendung des LeiKa auf europäischer Ebene wird der LeiKa um die Staatenkennung gem. ISO 3166-2 erweitert. Die Staatenkennung ist im regelmäßigen Gebrauch innerhalb Deutschlands entbehrlich.                                                                                                                      |
| Instanz                   | Die Kennung für den LeiKa ist "99".<br/><br/>In Deutschland erfolgt die Kennzeichnung von landesspezifischen Leistungskatalogen gemäß Länderkennung des Amtlichen Gemeindeschlüssels (AGS). Die Vergabe weiterer Instanzen ist grundsätzlich möglich.<br/><br/>Instanzen anderer Staaten werden durch die GK LeiKa vergeben. |
| **Strukturkomponente**    | **Beschreibung**                                                                                                                                                                                                                                                                                                         |
| Leitungsobjekt (LG+LK)    | Das Leistungsobjekt bezieht sich unmittelbar auf einen rechtlichen Regelungsgegenstand und stellt somit die Grundlage für das Verwaltungshandeln (=Verrichtung) dar. Durch die Kopplung von LG und LK wird die eindeutige Identifizierung von Leistungen für Fremdsysteme („Fingerabdruck einer Leistung“) ermöglicht.   |
| Leistungsgruppierung (LG) | Die Leistungsgruppierungen erfüllen eine Systematisierungsfunktion innerhalb des LeiKa. Die Leistungsgruppierungen haben keinen Einfluss auf die Bildung einer Leistungsbezeichnung.                                                                                                                                     |
| Leistungskennung (LK)     | Die Leistungskennung ist das zentrale Merkmal eines Leistungsobjektes und stellt den Regelungsgegenstand einer Leistung dar. <br/><br/>Durch die Verknüpfung LG + LK = LO entsteht eine „feste Verdrahtung“, die nachträglich nicht verändert werden sollte, um große Aufwendungen zur Nachpflege zu vermeiden.            |
| Verrichtung (VK+VD)       | Verrichtungen beschreiben, welches Verwaltungshandeln an einem Leistungsobjekt durchgeführt wird.                                                                                                                                                                                                                        |
| Verrichtungskennung (VK)  | Die Verrichtungskennung beschreibt das Ergebnis des Verwaltungshandelns in Bezug auf das Leistungsobjekt. Verrichtungskennungen werden im LeiKa standardisiert in Form von Substantivierungen vorgegeben.                                                                                                                |
| Verrichtungsdetail (VD)   | Das Verrichtungsdetail spezifiziert die Verrichtungskennung insbesondere in Bezug auf verschiedene Verfahrensabläufe, Zielgruppen oder Ausnahmen innerhalb einer Leistung. Verrichtungsdetails werden zu jeder Verrichtung eines Leistungsobjektes fortlaufend vergeben.                                                 |
Der Leistungsschlüssel wird nach einer „sprechenden“ Systematik automatisch vergeben. Der Schlüssel umfasst immer 14 Stellen und ist wie folgt aufgebaut:
![](Zusammensetzung%20LeiKa-Schlüssel.jpg)

- Die Nummernbereiche 001-899 (LG, LK, VK, VD) werden durch die GK LeiKa vorgegeben und verwaltet.
- Die Nummernbereiche 900-999 (LG, LK, VK, VD) stehen den Ländern zur individuellen Verwendung zur Verfügung. Dieser freie Adressraum kann z.B. dafür genutzt werden, eine vorgeschlagene neue Leistung bis zur Übernahme in den zentralen LeiKa auszuweisen oder spezifische Leistungen der LeiKa-Nutzer in ihren dezentralen Systemen innerhalb der LeiKa-Struktur zu verwalten.
- Bei Nutzung eines individuellen Bereiches ist die Länderkennung gemäß des Amtlichen Gemeindeschlüssels (AGS) als Instanz zu setzen.
- Im Bereich LK, VK und VD fungiert „000“ als Platzhalter, sofern hier keine Informationen hinterlegt sind. 

Mit dieser Schlüsselsystematik können Leistungen nach einer einheitlichen Struktur in unterschiedlicher Granularität dargestellt werden. Zudem ermöglicht der Aufbau des LeiKa-Schlüssels die Anbindung von modulareren Stammtexten sowie die Bildung von „Container-Strukturen“, diese können von LeiKa-Nutzern beispielsweise in Navigationsleitfäden, Lebenslagen oder zur Gliederung von Verzeichnissen individuell genutzt werden. 