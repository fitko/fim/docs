# Benachrichtigung der Nutzer über erfolgte Änderungen

Über Änderungen am LeiKa (Benachrichtigung über neue oder geänderte Leistungsbeschreibungen) werden alle Nutzer über einen monatlichen Newsletter per E-Mail informiert. Änderungen an LeiKa-Schlüsseln werden gesondert gekennzeichnet. Darüber hinaus werden aktuelle Änderungen im [LeiKa-plus Portal](http://leika.zfinder.de/portal/?activeTab=LeiKaNews) bekannt gegeben.
