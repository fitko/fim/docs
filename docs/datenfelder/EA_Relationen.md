# Relationen

## Grundlegendes

Zwischen den Elementen im Baustein Datenfelder gibt es verschiedene Beziehungen, welche über Relationen abgebildet werden. Die Codeliste der möglichen Relationen, die vom Standard XDatenfelder 3 genutzt wird, findet sich hier: [urn:xoev-de:fim-datenfelder:codeliste:relation](https://www.xrepository.de/details/urn:xoev-de:fim-datenfelder:codeliste:relation).

## Was zeichnet eine Relation aus?

Eine Relation wird durch drei Attribute vollständig beschrieben: die Elemente, zwischen denen die Relation besteht, das sind **Start- und Zielelement** der Relation, sowie die **Art der Relation**.

## Welche Arten von Relationen gibt es? :sparkles:

Die Werte der Relations-Codeliste lauten seit dem 20.04.2023 wie folgt:

|Code|Beschreibung|
|----|------------|
|ABL| ist abgeleitet von|
|ERS| ersetzt|
|EQU| ist äquivalent zu|
|VKN| ist verknüpft mit|

### ABL - ist abgeleitet von

Die Relationsart **ist abgeleitet von** stellt eine Beziehung zwischen Elementen gleichen Typs dar. Zum Beispiel wird eine Datenfeldgruppe von einer anderen Datenfeldgruppe abgeleitet.

### ERS - ersetzt

Die Relationsart **ersetzt** stellt eine Beziehung zwischen Baukastenelementen dar, z. B. eine Datenfeldgruppe ersetzt eine andere Datenfeldgruppe.

### EQU - ist äquivalent zu

Die Relationsart **ist äquivalent zu** stellt u. a. eine Beziehung zwischen Baukastenelementen dar, z. B. ein Datenfeld ist äquivalent zu einer Datenfeldgruppe.

### VKN - ist verknüpft mit

Die Relationsart **ist verknüpft mit** stellt eine Beziehung zwischen abstrakten und nicht abstrakten Dokumentsteckbriefen her.

## Start- und Zielelemente von Relationen

In der folgenden Tabelle wird gelistet, zwischen welchen Elementen welche Relationen existieren können. Dabei werden jeweils die oben erwähnten Abkürzungen für die Relationen verwendet. An den Punkten der Tabelle, an denen ein Spiegelstrich steht, sind keine validen Relationen möglich.

| |Dokumentsteckbrief|Datenschema|Datenfeldgruppe|Datenfeld|
|----|----|----|----|----|
|**Dokumentsteckbrief**|ABL, ERS, VKN|-|-|-|
|**Datenschema**|-|ABL, ERS, EQU|-|-|
|**Datenfeldgruppe**|-|-|ABL, ERS, EQU|ERS, EQU|
|**Datenfeld**|-|-|ERS, EQU|ABL, ERS, EQU|

## Wann werden Relationen verwendet?

Kurz zusammengefasst: Relationen sollten dann erstellt werden, wenn es wichtig ist eine Beziehung zwischen zwei Elementen festzuhalten. Folgend ein paar klassische Fälle:
- Zwischen einem Stammdatenschema und dem daraus entwickelten Referenzdatenschema sollte beispielsweise eine *Ableitungsbeziehung* existieren (ABL - ist abgeleitet von).
- Aus abstrakten Datenfeldgruppen abgeleitete Datenfeldgruppen, die für die konkrete Anwendung gedacht sind, sollte auch eine *Ableitungsbeziehung* erhalten.
- Möchte man ein Baukastenelement aussondern und kennt bereits seinen Nachfolger, so erstellt man eine *ersetzt* Relation.
- ...

## Richtung der Relation

:::info

Die Relation geht laut Standard XDatenfelder 3 immer vom Startelement zum Zielelement. Redaktionssysteme können eine abweichende Notation haben, im Export sollte sich aber stets die vom Standard intendiert Richtung wiederspiegeln.

:::

## Beispiele für Relationen

- Zwischen der Datenfeldgruppe G11 000 000 123 *neue Gruppe* (Startelement der Relation) und der Datenfeldgruppe G11 000 000 002 *alte Gruppe* (Zielelement der Relation) wurde eine ERS - ersetzt Relation erstellt. Das heißt G11 000 000 123 **ersetzt** G11 000 000 002 - die *neue Gruppe* ersetzt die *alte Gruppe*.
- Zwischen der Datenfeldgruppe G11 000 000 223 *Antragsteller Einbürgerung* (Startelement der Relation) und der Datenfeldgruppe G60 000 000 082 *natürliche Person (abstrakt, umfassend)* (Zielelement der Relation) besteht eine ist abgeleitet von Relation. Das heißt G11 000 000 223 **ist abgeleitet von** G60 000 000 082 - *Antragsteller Einbürgerung* ist abgeleitet von *natürliche Person (abstrakt, umfassend).

## Qualitätskriterien zum Metadatum

### Art der Relation

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Die Art der Relation die erstellt werden soll. Es gibt momentan vier verschiedene: **ist abgeleitet von**, **ersetzt**, **ist äquivalent zu** und **verknüpft**.|
| Zielgruppe      | Modellierende, Dienstleister nachnutzender Systeme |
| Pflichtfeld | Ja, falls man eine Relation erstellen möchte.|
| Elementspezifisches Qualitätskriterium |Es ist immer nur eine zu Start- und Zielelement passende Relation zu verwenden.|
| Empfehlung für die maximale Länge |nicht relevant|
| Art der Prüfung     | keine |
| Verweis auf den QS-Bericht      | x |
| betroffene Elemente |Dokumentsteckbriefe, Datenschemata, Datenfeldgruppen und Datenfelder|
| Beispiel      |siehe [oben](./EA_Relationen.md#beispiele-für-relationen) |
|mögliche Abweichungen im Referenzkontext| nein |

### Startelement

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Das Startelement bzw. Subjekt einer Relation.|
| Zielgruppe      | Modellierende |
| Pflichtfeld | Ja, falls man eine Relation erstellen möchte.|
| Elementspezifisches Qualitätskriterium |siehe Tabelle|
| Empfehlung für die maximale Länge |Die maximale Länge entspricht der Länger einer XDatenfelder 3 konformen ID inklusive der Versionskennung.|
| Art der Prüfung     | keine |
| Verweis auf den QS-Bericht      | x |
| betroffene Elemente |Dokumentsteckbriefe, Datenschemata, Datenfeldgruppen und Datenfelder|
| Beispiel      |siehe [oben](./EA_Relationen.md#beispiele-für-relationen)  |
|mögliche Abweichungen im Referenzkontext| nein |

### Zielelement

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition |Das Zielelement bzw. Objekt einer Relation.|
| Zielgruppe      | Modellierende |
| Pflichtfeld | Ja, falls man eine Relation erstellen möchte.|
| Elementspezifisches Qualitätskriterium |siehe Tabelle|
| Empfehlung für die maximale Länge |Die maximale Länge entspricht der Länger einer XDatenfelder 3 konformen ID inklusive der Versionskennung.|
| Art der Prüfung     | keine |
| Verweis auf den QS-Bericht      | x |
| betroffene Elemente |Dokumentsteckbriefe, Datenschemata, Datenfeldgruppen und Datenfelder|
| Beispiel      |siehe [oben](./EA_Relationen.md#beispiele-für-relationen)  |
|mögliche Abweichungen im Referenzkontext| nein |
