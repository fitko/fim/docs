---
sidebar_label: Referenz
---

# Referenz

Referenzdokumente enthalten Informationen zum Nachschlagen:

XÖV-Standards
- [Spezifikation XProzess](https://www.xrepository.de/details/urn:xoev-de:mv:em:standard:xprozess) für den Baustein Prozess
- [Spezifikation XZuFi](https://www.xrepository.de/details/urn%253Axoev-de%253Afim%253Astandard%253Axzufi) für den Baustein Leistung
- [Spezifikation XDatenfelder](https://www.xrepository.de/details/urn:xoev-de:fim:standard:xdatenfelder) für den Baustein Datenfelder

Qualitätskriterien
- des Baustein Prozess
- des Baustein Leistung
- [des Baustein Datenfelder](/docs/datenfelder/VB_Einleitung)