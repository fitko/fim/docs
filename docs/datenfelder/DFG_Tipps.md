# Tipps für die praktische Modellierung

:::tip

Stellen Sie sicher, dass Sie auch die entsprechenden Informationen in der [Sektion über Datenschemata](./DS_Tipps.md) gelesen haben.
:::

## Wenn ich eine Datenfeldgruppe brauche, wo fange ich an zu suchen?

Diese Liste soll Ihnen dabei helfen, beim Modellieren benötigter Datenfeldgruppen in der richtigen Reihenfolge vorzugehen.

1. **Suche bei BOB**: durchsuchen Sie zunächst den zentralen Baukasten nach Elementen, die für Ihren Kontext passend sind. Suchen Sie dabei zunächst nach einem passenden harmonisierten Element; wenn Sie dabei nicht fündig werden, durchsuchen Sie die abstrakten Elemente.
2. **Suche im lokalen Baukasten**: überprüfen Sie als nächstes, ob Ihr lokaler Baukasten ein geeignetes Element bereitstellt. Dursuchen Sie dabei zunächst die lokalen harmonisierten Elemente, danach die rechtsnormgebundenen.
3. **Erstellen eines neuen Elements**: Erst wenn Sie in beiden Fällen nicht fündig geworden sind, sollten Sie ein neues Element erstellen.

:warning: Bei **fachübergreifenden Elementen** wird man bei **BOB** eher fündig, bei **fachspezifischen** eher nicht. Ihr **lokaler** Baukasten dürfte bei fachspezifischen Elementen eine reiche Auswahl an Elementen bereitstellen.

## Wie nutze ich eine Datenfeldgruppe nach?

Nachdem Sie BOB und danach Ihren lokalen Datenfeldbaukasten nach einer passenden Gruppe, die Ihrem Anwendungszweck entspricht, durchsucht haben, sind Sie hoffentlich fündig geworden. Binden Sie die benötigte Gruppe in Ihr Datenschema bzw. in Ihre Datenfeldgruppe ein. Hinterlegen Sie für die Nutzung dieser Datenfeldgruppe einen **Strukturbezug**. Dies können Sie auch dann tun, wenn die gewünschte Datenfeldgruppe aus einem anderen Rechtskontext kommt, als Ihr Datenschema bzw. Ihre Datenfeldgruppe.

## Handlungsgrundlagen von rechtsnormgebundenen Gruppen

Die Handlungsgrundlagen einer Gruppe bestehen aus der Summe aller Handlungsgrundlagen ihrer rechtsnormbezogenen Unterelemente und allen Verwendungsbegründungen ihrer harmonisierten Unterelemente. Das heißt auch, dass die Gestaltungsbegründungen der harmonisierten Elemente hierzu nicht dazu gezählt werden.

## Sollte ich meine Nachweisfelder am Ende eines Datenschemas in einer expliziten Gruppe bündeln?

Das ist abhängig vom Fall. Grob kann man sagen, wenn es sich um ein kleines Datenschema handelt, kann das Sinn machen. Bei größeren oder bei Unsicherheit, lassen Sie die Nachweisfelder in den Gruppen, die dafür thematisch passend sind.<br/>

Beispielsweise ist das Datenfeld “Geburtsurkunde” am besten in einer übergeordneten Gruppe “Angaben zum Lebenspartner” verortet. Dadurch wird auch seine Bedeutung klarer; hier handelt es sich um die Geburtsurkunde des Lebenspartners.

## Was ist der Unterschied zwischen Eingabe und Ausgabe?

Beide Begriffe kommen im Kontext der **Bezeichnung** und des **Hilfetexts** vor:

- **Eingabe**: Das ist der Text, der initial auf einem Formular erscheint, bevor dieses ausgefüllt wurde.
- **Ausgabe**: Sie erscheint auf Dokumenten, die der Antragstellende von der Verwaltung zugesendet bekommt, Bescheiden, Urkunden, etc.; die Ausgabe ist auch relevant für ein Webinterface nach getätigter Eingabe durch den Antragstellenden.

## Wie verwende ich Auswahlgruppen? :new:

Um das neue XDatenfelder 3 Feature Auswahlgruppe verwenden zu können, benötigen Sie zunächst eine Datenfeldgruppe mit **mindestens zwei Unterelementen**.
Die Datenfeldgruppe selbst muss dann zunächst in Ihrem Redaktionssystem als Auswahlgruppe gekennzeichnet werden.
Alle Unterelemente der Gruppe sollten dazu die **Multiplizität 0:x** haben. Wenn Sie diese Schritte ereldigt haben, haben Sie Ihre Auswahlgruppe erfolgreich modelliert.

### Beispiel: Modellierung klassisch und mit Auswahlgruppe

![Auswahlgruppe](./images/Auswahlgruppe.jpg)

### klassische Modellierung

Auf der linken Seite der Grafik, sehen wir eine klassische Form der Modellierung mit Regel aus BOB, die Datenfeldgruppe G60 000 000 093 "Anschrift Inland oder Ausland mit Frage". Ziel dieser Gruppe ist es, dem Antragstellenden eine Option zu bieten, ob an dieser Stelle die Angabe der Anschrift Inland erfolgt oder die der Anschrift Ausland, gemäß dem zugrundeliegenden fachlichen Kontext. Um sicherzustellen, dass eine von beiden Angaben getätigt wird, schaltet man die Frage F60 000 000 263 "Abfrage Anschrift Inland oder Ausland" vor und hat Regeln an der übergeordneten Gruppe selbst modelliert, die diese Logik abbilden sollen.

### Modellierung mit Auswahlgruppe

Auf der rechten Seite der Grafik, sehen wir die Umsetzung des gleichen Modellierungskonstrukts mittels Auswahlgruppe (erkennbar an dem A in der Raute). Das Abfragefeld und die Regeln selbst entfallen. Durch die Tatsache, dass es sich um eine Auswahlgruppe handelt, steht schon fest, dass entweder die Gruppe Anschrift Inland oder die Gruppe Anschrift Ausland verpflichtend zu befüllen ist.

## Wie könnte eine Auswahlgruppe in einer Visualisierung aussehen? :new:

Die Visualisierung bietet Ihnen alle Unterelemente der Datenfeldgruppe zur Auswahl an, aus der Sie genau ein Element auswählen dürfen. Wurde die Wahl getroffen, können danach die Eingabefelder des gewählten Elements ausgefüllt werden.

## Welche Fehler kann man bei der Erstellung von Datenfeldgruppen machen?

<ul><li>
Gruppen mit nur einem Unterelement sind zu vermeiden und nur in begründeten Ausnahmefällen zu verwenden (z. B. Orientierung an anderen Standards).</li><li>
Falls eine verwendete Gruppe ausschließlich aus optionalen Unterelemente besteht, muss sichergestellt werden, dass zumindest eines der Unterelemente befüllt werden muss. In den meisten Fällen wird es sich hierbei um eine Auswahlgruppe handeln. Falls dies nicht der Fall ist, muss zumindest eine passende Regel hinterlegt werden.</li><li>
Auf der gleichen Strukturebene dürfen die gleichen Unterelemente nicht mehrfach verwendet werden.</li><li>
Ein Baukastenelement darf nicht in zwei verschiedenen Versionen innerhalb einer Datenfeldgruppe vorkommen.</li>
</ul>

## Welche Vorteile entstehen durch das geschickte modellieren von Datenfeldgruppen?

Geschickt modellierte Gruppen:
- Fassen alle Datenfelder und Datenfeldgruppen zu einem Thema
- Schaffen Übersicht und Struktur
- Erleichtern das Nachnutzen dieser Gruppe, weil sie möglichst fachunspezifisch modelliert wurden
- Ermöglichen es in einem Nachnutzungsszenario präzisere Fehlermeldungen zu stellen

## Kleine Tipps & Tricks

:::tip

- :warning: **Wiederverwendung ist wichtig!** Erstellen Sie keine unnötigen Elemente, wie etwaige Dubletten von Elementen, die schon in BOB oder Ihrem Baukasten vorhanden sind.
- Bei der Nachnutzung von harmonisierte Elemente muss ein **Strukturbezug** hinterlegt werden.
- Verwenden Sie so **wenig fachspezifische Elemente** wie nötig, konzentrieren Sie sich auf die fachübergreifenden.
- Verwenden Sie abstrakte Gruppen nicht direkt, sondern leiten Sie von Ihnen ab.
- Bezeichnung Eingabe und Ausgabe sollten kurz und knapp gehalten werden.
- In Bezeichnung Eingabe oder Ausgabe dürfen keine Fragen stehen.
- Wenn Sie mehr zu sagen haben: nutzen Sie Hilfetexte zur Ergänzung. Aber Achtung, ein Hilfetext sollte auch nicht zu lang sein.
- Achten Sie bei alldem auf [bürgerfreundliche Sprache](./EA_Sprache.md).
- Es oft zweckmäßig für Hilfetexte nur das Metadatum **Hilfetext Eingabe** zu befüllen.
- Regeln die an einer Gruppe hängen, können sich nur auf Elemente innerhalb dieser Gruppe beziehen.

:::
