# Tipps für die praktische Modellierung

## Nachnutzung des zentralen Katalogs KATE
:::warning

Dies ist einer der wichtigsten Tipps im Zusammenhang mit Dokumentsteckbriefen: **nutzen Sie globale Dokumentsteckbriefe nach.**

 Legen Sie nur übergangsweise lokale Dokumentsteckbriefe an. Wie Sie auf Dokumentsteckbriefe aus KATE zugreifen und einen Dokumentsteckbrief aus KATE nachnutzen können, wird im Folgenden erklärt. Falls KATE keinen Dokumentsteckbrief zur Nachnutzung bereit stellt, beachten Sie bitte den Abschnitt Zusammenspiel lokaler Repositorys mit KATE.

:::

## weitere Hinweise

- :warning: Falls ein Dokumentsteckbrief den Status „inaktiv“ hat, sollten alle verknüpften Datenschemata auch den Status „inaktiv“ haben. Mit einem inaktiven Dokumentsteckbrief sollten keine neuen Datenschemata verknüpft werden.

- Ein Dokumentsteckbrief kann fachlich freigegeben werden.