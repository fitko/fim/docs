---
sidebar_label: Bedarfsmeldung
---

# Bedarfsmeldung

Um eine Leistung neu festzulegen oder zu ändern, ist eine Bedarfsmeldung zu erstellen. Grundlage ist dieses [Musterformular](../ressources/Musterformular_Leistungsbeschreibung_V2.0.docx), ein Word-Dokument. 

Hilfreich ist dabei die ausgefüllte [Musterleistungsbeschreibung „Gewerbliche Drachenhaltung - Erlaubnis beantragen“](../ressources/20211102_Musterleistungsbeschreibung_Drachenhaltung.pdf).

Ergänzend zum Musterformular gilt die [Liste der Module und Pflichtfelder](../ressources/Anlage-Musterformular_Module-Leistungsbeschreibung.pdf) für Steckbriefe, Stammtexte und Leistungsbeschreibungen.