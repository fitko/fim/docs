// diese Datei enthält die Struktur der Sidebar für den Baustein DATENFELDER
// export der Struktur wird in "sidebar.js" verwendet.

const df_structure = [
        // Abschnitt für Baustein Datenfelder
		{
        	'type': 'category',
        	'label': 'Baustein Datenfelder (new)',
        	'items': [
				{
					type: 'link',
					label: 'Einleitung (inaktiv)',
					href: './',
				},
				{
					'type': 'category',
					'label': 'Fachkonzept',
					'items': [
						// 'datenfelder/Fachkonzept/Einleitung',
						'datenfelder/Fachkonzept/Lesehilfe',
						'datenfelder/Fachkonzept/Identifikatoren',
						'datenfelder/Fachkonzept/Datenfelder',
						'datenfelder/Fachkonzept/Datenfeldgruppen',
						// 'datenfelder/Fachkonzept/Regeln',
						'datenfelder/Fachkonzept/Datenschemabibliothek',
						'datenfelder/Fachkonzept/Datenfeldkatalog',
						'datenfelder/Fachkonzept/Metamodell',
						'datenfelder/Fachkonzept/Relationen',
						'datenfelder/Fachkonzept/Status',
						// 'datenfelder/Fachkonzept/Schnittstellen',
						'datenfelder/Fachkonzept/zentrale_Repositorys',
						'datenfelder/Fachkonzept/Vererbung_Status',
					]
				},
				{
					'type': 'category',
					// label to be discussed
					'label': 'Modellierung',
					'items': [
						{
							type: 'link',
							label: 'Redaktionsprozess (inaktiv)',
							href: './',
						},
						{
							type: 'link',
							label: 'Hilfsmittel (inaktiv)',
							href: './',
						},
						{
							'type': 'category',
							'label': 'QS-Kriterien',
							'items': [
							{
							  type: 'category',
							  label: 'Überblick',
							  collapsed: true,
							  items: [
								'datenfelder/VB_Einleitung',
								'datenfelder/VB_Umgang',
								'datenfelder/VB_Releasenotes',
							  ],
							},
						]},
					],
				},
	


				
				{
					'type': 'category',
					'label': 'Organisation',
					'items': [
					{
					  type: 'category',
					  label: 'Überblick',
					  collapsed: true,
					  items: [
						{
							type: 'link',
							label: '(Platzhalter)',
							href: './',
						},						
					  ],
					},
					{
						type: 'link',
						label: 'Gremien und Arbeitsgruppen',
						href: './',
					},	

					{
						'type': 'category',
						'label': 'Austauschrunden',
						'items': [
							{
								'type': 'category',
								'label': 'ME-Austausch (in Arbeit)',
								'items': [
									'datenfelder/ME-Austausch/uebersicht',
									'datenfelder/ME-Austausch/agenda',
									'datenfelder/ME-Austausch/informationen',
									'datenfelder/ME-Austausch/archiv',
								]
							},
						]
					},
				]},
				{
					'type': 'category',
					'label': 'Technik',
					'items': [
					{
						'type': 'category',
						'label': 'Schnittstellen',
						'items': [
							{
								'type': 'category',
								'label': 'FIM Schema-Repository',
								'items': [
									'schema-repository/about-this-schemarepository',
									'schema-repository/about-this-doc',
									'schema-repository/context',
									'schema-repository/schemarepository-for-group-1',
									'schema-repository/schemarepository-for-group-2',
									'schema-repository/schemarepository-for-group-4',
									'schema-repository/schema-repository-api',
									'schema-repository/roadmap',
									'schema-repository/glossary',
									'schema-repository/changelog',
								]
							},
							{
								type: 'link',
								label: '(Platzhalter)',
								href: './',
							},	
			
						]
					},
				]},

			]
		},
    
]

    // export the structure to enable use in other files
module.exports = df_structure;