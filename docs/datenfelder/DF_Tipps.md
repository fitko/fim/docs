# Tipps für die praktische Modellierung

## Wie kann einem Datenfeld ein Defaultwert zugewiesen werden?

Wenn Ihnen bereits bei der Modellierung bekannt ist, dass es für dieses Eingabefeld einen sinnvollen Defaultwert gibt, dann können Sie dieses über das Metadatum ‘Inhalt’ pflegen.

**Beispiel:**

Ein Datenschema welches eine Abfrage der Staatsangehörigkeit erwartet. Hier könnte man als Defaultwert ‘deutsch’ ergänzen.

## Sollte ein Defaultwert gepflegt werden?

Wenn die Möglichkeit besteht, dann ja, definitiv.

## Sollten Präzisierungsangaben für ein Datenfeld gepflegt werden?

Wenn die Möglichkeit besteht, dann ja, definitiv. Dadurch kann der Wertebereich der Eingabe, die durch den Bürger in diesem Feld möglich ist, bereits vorab sinnvoll eingeschränkt werden. Beispielsweise kann eine Eingabe einer negativen Anzahl von Kindern verhindert werden.

## Wie erstelle ich ein Auswahlfeld mit Mehrfachauswahl?

Sie binden in Ihr Datenschema ein Datenfeld ein, an dem eine Code- oder Werteliste hängt. Bei einer Nachnutzung soll es dem Antragstellenden ermöglicht werden, aus dieser Code- oder Werteliste mehr als einen Wert auswählen. Dies kann realisiert werden, indem die Multiplizität des Datenfeld entsprechend angepasst wird und zusätzlich am Feld eine Regel ergänzt wird.

**Beispiel:**

Ein Datenfeld "vorhandene Fahrerlaubnisse", an dem eine Werteliste mit den in Deutschland gültigen Fahrerlaubnisklassen gepflegt wurde.
An dieser Stelle soll es möglich sein, dass vom Antragstellenden mehr als eine Auswahl, entsprechend den vorhandenen Fahrerlaubnissen, gepflegt wird.
Das Feld erhält die Multiplizität 1:17; 1, um zu signalisieren, dass mindestens eine Auswahl getroffen werden muss und 17, um die Anzahl der Auswahlen entsprechend der Werteliste nach oben zu begrenzen.
Eine Regel an diesem Feld könnte dann folgendermaßen formuliert sein: "Bereits getroffene Auswahloptionen dürfen bei einer erneuten Auswahl kein weiteres Mal zur Auswahl angeboten werden."
Dies soll verhindern, dass der Antragstellende die Möglichkeit bekommt, 17-mal die gleiche Auswahloption zu wählen.

## Mir ist immer noch nicht klar, ob ich eine Code- oder eine Werteliste nutzen sollte. Wie finde ich eine Antwort?

Gibt es die Liste, die Sie in Ihrem Datenschema nutzen wollen, bereits auf dem XRepository?

<ul><li>Falls ja, nutzen Sie diese nach und binden diese als Codeliste in Ihr Datenschema ein.</li>

<li>Falls nein, klären Sie, ob diese Liste immer wieder benötigt wird.</li>
<ul>

<li>Falls ja, legen Sie eine Codeliste im XRepository an. Diese benötigt einen verantwortungsbewussten Pflegebeauftragten, der diese stets aktuell hält.</li>

<li>Falls nein, legen Sie eine Werteliste an Ihrem Datenfeld an.</li></ul></ul>

## Meine Codeliste hat sich geändert, was kann ich tun um nicht immer das komplette Datenschema anzupassen?

Referenzieren Sie an Ihren Datenfeldern Codelisten ohne Versionskennung und vereinbaren Sie, dass in Ihrem Fall damit immer die aktuelle Version der Codeliste gemeint ist. Wenn sich dann Ihre Codeliste ändert, müssen Sie am Datenschema selbst keine Änderungen mehr vornehmen.

## Wann verwende ich welche Kombination aus Feldart und Datentyp?
Beachten Sie dazu den Abschnitt: [Grundsätzliches über Datenfelder](./DF_Allgemeines#was-sind-die-metadaten-feldart-und-datentyp-)


## Wie geht man in Referenzdatenschemata mit Datenfeldern um, die auf keiner Handlungsgrundlage basieren?

:::info

Für referenzbasierte Datenfelder sind trotz ihres Referenzcharakters Handlungsgrundlagen zu pflegen. D. h. ein Großteil der Elemente eines Refrenzdatenschemas beinhaltet Elemente, die rechtsnormgebunden sind und auf einer Handlungsgrundlage basieren.

:warning: Besonders wichtig ist dies für personenbezogene Daten. :warning:

:::

Zunächst wird das Datenfeld wie gewöhnt mit der Strukturelementart “rechtsnormgebunden” erstellt. Unter dem Beschreibungsfeld wird idealerweise ergänzt, warum dieses Datenfeld gegenüber dem zugehörigen Stammdatenschema ergänzt wurde. Mit der Fachlichkeit ist zu klären ob ggf. eine Gesetzesanpassung notwendig ist. Unter Handlungsgrundlage wird folgendes erfasst: Handlungsgrundlage noch ausstehend, da referenzbasiert.

## Wie nutze ich ein Datenfeld nach?

Wählen Sie ein bereits existierenden Datenfeld aus Ihrem Datenfeldbaukasten aus und binden Sie es in die gewünschte Gruppe bzw. das gewünschte Schema ein. Hinterlegen Sie zur Multiplizität einen Strukturbezug.

Eine gute Fundstelle für nachnutzbare Datenfelder ist BOB; [im Abschnitt über BOB](./EA_BOB.md) wird die Nachnutzung noch einmal näher erklärt. Das Vorgehen ist bei Datenfelder, die nicht aus BOB stammen, analog.

## Was ist der Unterschied zwischen Eingabe und Ausgabe?

Beide Begriffe kommen im Kontext der **Bezeichnung** und des **Hilfetexts** vor:

- **Eingabe**: Das ist der Text, der initial auf einem Formular erscheint, bevor dieses ausgefüllt wurde.
- **Ausgabe**: Sie erscheint auf Dokumenten, die der Antragstellende von der Verwaltung zugesendet bekommt, Bescheiden, Urkunden, etc.; die Ausgabe ist auch relevant für ein Webinterface nach getätigter Eingabe durch den Antragstellenden.

## Kleine Tipps & Tricks

:::tip

<ul>
<li>
Binden Sie Nachweisfelder mit der Multiplizität x:* in Ihr Datenschema ein.
</li>
<li>
Bezeichnung Eingabe und Ausgabe sollten kurz und knapp gehalten werden.
</li>
<li>
Wenn Sie mehr zu sagen haben: nutzen Sie Hilfetexte zur Ergänzung. Aber Achtung, ein Hilfetext sollte auch nicht zu lang sein.
</li>
<li>
Wenn Sie wirklich viel zu sagen haben: Binden Sie statische Textfelder in Ihr Datenschema ein.
</li>
<li>
Achten Sie bei alldem auf bürgerfreundliche Sprache.
</li>
<li>
Auch bei Einträgen von Wertelisten sollte auf bürgerfreundliche Sprache geachtet werden.
</li>
<li>
Es oft zweckmäßig, für Hilfetexte nur das Metadatum “Hilfetext Eingabe” zu befüllen.
</li>
<li>
Regeln hängen eher selten an Feldern.
</li>
<li>
Erstellen Sie kein Feld für die Anrede.
</li>
<li>
Erstellen Sie kein Feld für Adelstitel, etc.
</li>
<li>
Geben Sie bei der Abfrage von Geldbeträgen an, in welcher Währung diese erfasst werden sollen.
</li>
</ul>

:::