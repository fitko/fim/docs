# Hilfestellung zum QS-Bericht I (WIP)

In diesem Abschnitt werden die Fehlercodes des QS-Berichts näher beleuchtet.

## 1001: Eine Regel kann nicht auf ein referenziertes Baukastenelement zugreifen.

#### Warum ist das wichtig?

In Regeln werden die betroffenen Baukastenelemente referenziert. Aus Grafik XXX geht hervor, in welchen Fällen Regeln auf Baukastenelemente zugreifen können und wann nicht.

#### Wie löse ich den Fehler auf?

Prüfen Sie zunächst, ob die referenzierte ID vollständig ist, d. h. hat sie ausreichend Stellen und entspricht sie dem ID Format, welches in XXX beschrieben wird. Danach prüfen Sie, ob die referenzierten Elemente im Einflussbereich der Regel liegen. Falls nicht, müssen Sie die Elemente umstrukturieren oder Ihre Regel an anderer Stelle verorten.

## 1002: Der Name eines Elements ist nicht gesetzt.

#### Warum ist das wichtig?

Der Name eines Elements ist ein Pflichtattribut in XDatenfelder 3, deswegen muss der Name stets gesetzt sein.

#### Wie löse ich den Fehler auf?

Pflegen Sie das Attribut Namen am gekennzeichneten Element nach.

## 1003: In einem Datenschema darf ein Baukastenelement nicht mehrfach in unterschiedlichen Versionen enthalten sein.

#### Warum ist das wichtig?

In der Regel gibt es wenige Gründe ein Baukastenelement in unterschiedlichen Versionen in einem Datenschema zu verwenden. Falls doch, sind diese Gründe von fachlicher Natur, z. B. weil eine Codeliste in zwei verschiedenen Versionen benötigt wird.

#### Wie löse ich den Fehler auf?

Passen Sie die verschiedenen Versionen eines Elements in Ihrem Datenschema so an, dass nur eine der verwendeten Versionen verwendet wird. Oft handelt es sich dabei um die aktuelle Version.

## 1004: In einer Datenfeldgruppe darf ein Baukastenelement nicht mehrfach in derselben Version enthalten sein.

#### Warum ist das wichtig?

In der Regel gibt es wenige Gründe ein Baukastenelement in unterschiedlichen Versionen in einer Datenfeldgruppe zu verwenden. Falls doch, sind diese Gründe von fachlicher Natur, z. B. weil eine Codeliste in zwei verschiedenen Versionen benötigt wird.

#### Wie löse ich den Fehler auf?

Passen Sie die verschiedenen Versionen eines Elements in Ihrer Datenfeldgruppe so an, dass nur eine der verwendeten Versionen verwendet wird. Oft handelt es sich dabei um die aktuelle Version.

## 1005: Die Kennung einer Codeliste ist nicht als URN formuliert, z. B. urn:de:fim:codeliste:dokumenttyp.

#### Warum ist das wichtig?

Das XRepository ist die Bezugsquelle für Codelisten und über die Kennung sind Codelisten definiert. Diese Kennung wird in Form einer URN notiert, d. h., wenn man sich nicht an diese Notation hält, kann keine Codeliste referenziert werden.

#### Wie löse ich den Fehler auf?

Überprüfen Sie Ihre Eingabe. Im Zweifelsfall rufen Sie direkt die Seite des XRepositorys aus und kopieren dort die Kennung Ihrer Codeliste in die Zwischenablage, um Sie so in Ihr Redaktionssystem zu übertragen.

## 1006: Der Bezug zur Handlungsgrundlage darf bei Elementen mit der Strukturelementart 'rechtsnormgebunden' nicht leer sein.

#### Warum ist das wichtig?

Rechtsnormgebundene Elemente tragen Ihren Namen zu Recht: jedes dieser Elemente muss auf einer Handlungsgrundlage basieren.

#### Wie löse ich den Fehler auf?

Ergänzen Sie das Metadatum an Ihrem Element.

## 1007: Es ist kein Dokumentsteckbrief zugeordnet.

#### Warum ist das wichtig?

Jedem Datenschema muss ein Dokumentsteckbrief zugeordnet sein.

#### Wie löse ich den Fehler auf?

Ordnen Sie Ihrem Datenschema einen gültigen Dokumentsteckbrief zu.

## 1008: Der Bezug zur Handlungsgrundlage darf bei Dokumentsteckbriefen nicht leer sein.

#### Warum ist das wichtig?

Der Bezug zur Handlungsgrundlage ist ein Pflichtattribut für Dokumentsteckbriefe.

#### Wie löse ich den Fehler auf?

Ergänzen Sie das Metadatum Handlungsgrundlage an Ihrem Dokumentsteckbrief.

## 1009: Die 'Bezeichnung Eingabe' muss befüllt werden.

#### Warum ist das wichtig?

Die **Bezeichnung Eingabe** eines Elements ist ein Pflichtattribut in XDatenfelder 3, deswegen muss die **Bezeichnung Eingabe** stets gepflegt werden.

#### Wie löse ich den Fehler auf?

Pflegen Sie das Attribut **Bezeichnung Eingabe** am gekennzeichneten Element nach.

## 1010: Die 'Bezeichnung Ausgabe' muss befüllt werden.

#### Warum ist das wichtig?

Die **Bezeichnung Ausgabe** eines Elements ist ein Pflichtattribut in XDatenfelder 3, deswegen muss die **Bezeichnung Ausgabe** stets gepflegt werden.

#### Wie löse ich den Fehler auf?

Pflegen Sie das Attribut **Bezeichnung Ausgabe** am gekennzeichneten Element nach.

## 1011: Bei Datenfeldern mit der Feldart 'Auswahl' sollte der Datentyp 'Text' sein - in seltenen Fällen 'Ganzzahl'.

#### Warum ist das wichtig?

Je nachdem, wie die Attribute Feldart und Datentyp miteinander kombiniert werden, ergibt sich die Bedeutung des Datenfelds. Um ein Auswahlfeld mit einer Code- oder Werteliste zu hinterlegen, ist die Kombination aus der Feldart *Auswahl* un dem Datentyp *Text* notwendig.

#### Wie löse ich den Fehler auf?

Ändern Sie den Datentyp auf *Text*, wenn Sie an diesem Datenfeld eine Code- oder Werteliste pflegen wollen.

## 1012: Bei Datenfeldern mit der Feldart 'Auswahlfeld' oder 'Statisches, read-only Feld' dürfen weder die minimale noch die maximale Feldlänge angegeben werden.

#### Warum ist das wichtig?

Feldlängenbegrenzungen sind nur relevant, wenn es sich um Eingabefelder für den Bürger handelt. Bei Statischen Feldern wird nur ein (statischer) Text angezeigt und bei Auswahlfeldern wird aus einer Code- oder Werteliste ein passender Wert ausgewählt. Für beides ist es also nicht sinnvoll Feldlängenbegrenzungen zu setzen.

#### Wie löse ich den Fehler auf?

- Entfernen Sie die Begrenzung der Feldlänge. Achten Sie dabei darauf, dass sowohl die minimale, als auch die maximale Länge entfernt werden.
- ODER: Ändern Sie die Feldart Ihres Datenfeldes.

## 1013: Innerhalb von Datenschemata und Datenfeldgruppen dürfen nur Elemente mit der Strukturelementart harmonisiert oder rechtsnormgebunden verwendet werden.

#### Warum ist das wichtig?

Abstrakte Datenfeldgruppen sind nicht zur direkten Verwendung in Datenschemata oder Datenfeldgruppen gedacht.

#### Wie löse ich den Fehler auf?

- Entfernen Sie das abstrakte Element aus Ihrem Datenschema bzw. Ihrer Datenfeldgruppe. 
- Falls Sie es verwenden müssen: prüfen Sie, ob die Strukturelementart auf harmonisiert geändert werden kann und ändern Sie diese.

## 1014: Datenfelder dürfen nicht die Strukturelementart 'abstrakt' haben.

#### Warum ist das wichtig?

Die Strukturelementart abstrakt darf nur für Datenfeldgruppen vergeben werden. Datenfelder sind in der Regel von der Strukturelementart *rechtsnormgebunden*; sie können aber auch die Strukturelementart *harmonisiert* haben.

#### Wie löse ich den Fehler auf?

Ändern Sie die Strukturelementart Ihres Datenfelds auf *rechtsnormgebunden* oder *harmonisiert* (falls zutreffend).

## 1015: Eine Feldlänge darf nur bei einem Datenfeld mit dem Datentyp 'Text' oder 'String.Latin+' angegeben werden.

#### Warum ist das wichtig?

Nur Datenfelder der Feldart Eingabe und des Datentyps Text oder String.Latin+ sind in der Feldlänge zu begrenzen. Die Feldlänge bezeichnet, wieviele Zeichen die Eingabe eines Antragstellenden an diesem Feld haben darf.

#### Wie löse ich den Fehler auf?

- Entfernen Sie die Angaben zur Feldlänge.
- ODER: Passen Sie den Datentyp des Feldes entsprechend an.

## 1016: Ein Wertebereich darf nur bei einem Datenfeld mit einem nummerischen Datentyp (Nummer, Ganzzahl, Geldbetrag) oder einem Zeitdatentyp (Uhrzeit, Datum, Zeitpunkt) angegeben werden.

#### Warum ist das wichtig?

Die Präzisierung eines Datenfeldes soll einen Mehrwert für nachnutzende Systeme liefern; dabei ist es natürlich wichtig die passende Präzisierung zum Datentyp zu wählen. Der Wertebereich betrifft nur die Datentypen Nummer, Ganzzahl und Geldbetrag, sowie Uhrzeit, Datum und Zeitpunkt.

#### Wie löse ich den Fehler auf?

Entfernen Sie den Wertebereich, wenn es sich nicht um einen der genannten Datentypen handelt.

## 1017: Ist eine Code- oder Werteliste zugeordnet, muss die Feldart 'Auswahl' sein.

#### Warum ist das wichtig?

Nur Datenfelder der Feldart Auswahl dürfen Code- oder Wertelisten zugeordnet werden.

#### Wie löse ich den Fehler auf?

- Ändern Sie die Feldart ab, wählen Sie Auswahl, wenn die Code- oder Werteliste an dieser Stelle benötigt wird.
- ODER: Entfernen Sie die Code- oder Werteliste von Ihrem Datenfeld.

## 1018: Wenn ein Datenfeld die Feldart 'Auswahl' hat, muss entweder eine Code- oder eine Werteliste zugeordnet sein.

#### Warum ist das wichtig?

Einem Datenfeld mit der Feldart Auswahl ist immer entweder eine Code- oder eine Werteliste zuzuordnen.

#### Wie löse ich den Fehler auf?

- Weisen Sie dem Datenfeld eine Code- oder werteliste zu.
- ODER: Ändern Sie die Feldart auf einen passenderen Wert.

## 1019: Baukastenelemente, die in Regeln referenziert werden, dürfen keine Versionsangaben beinhalten.

#### Warum ist das wichtig?

Wenn Sie Baukastenelemente mit Versionskennung in der Freitextregel referenzieren, ist die Freitextregel nicht mehr gültig, sowie sich die Version der Baukastenelemente ändert.
Um dies zu verhindern und einen allgemeineren Bezug zum Baukastenelement zu setzen, nutzen Sie nur die nummerische ID und nicht die nummerische ID inklusive der Versionskennung.

#### Wie löse ich den Fehler auf?

Entfernen Sie alle Versionskennungen aus der Freitext Regel. D. h. belassen Sie nur die nummerische ID. Die Versionskennung eines Elements sieht z. B. so aus: V1.0.0.

## 1021: Es ist keine Multiplizität angegeben.

#### Warum ist das wichtig?

Die **Multiplizität** eines Datenfelds oder einer Datenfeldgruppe ist ein Pflichtattribut, deswegen muss diese stets gepflegt werden.

#### Wie löse ich den Fehler auf?

Ergänzen Sie die Multiplizität am genannten Element.

## 1022: Bei Verwendung von Elementen mit der Strukturelementart 'abstrakt' oder 'harmonisiert' innerhalb von Datenfeldgruppen mit der Strukturelementart 'rechtsnormgebunden' muss zur Multiplizität ein Bezug zu einer Handlungsgrundlage angegeben werden.

#### Warum ist das wichtig?

Ein harmonisiertes Element ist im Sinne der Nachnutzung erstellt worden, d. h. es wird in verschiedenen Kontexten nachgenutzt. Die Handlungsbegründung dieses Elements stellt jedoch nur eine Gestaltungsbegründung dar; warum Sie dieses Element an einer anderen Stelle verwenden, ist durch die Verwendungsbegründung gegeben. Die Verwendungsbegründung können Sie über einen Bezug zur Handlungsgrundlage hinterlegen.

#### Wie löse ich den Fehler auf?

aPflegen Sie die Verwendungsbegründung an allen gelisteten Elementen nach. Analysiere Sie das Fachrecht und hinterlegen Sie den entsprechenden Passus. Fragen Sie im Zweifelsfalle Ihre Fachlichkeit um Rat.sdf

## 1023: Datenfeldgruppe hat keine Unterelemente.

#### Warum ist das wichtig?

Im Standard XDatenfelder 3 müssen Datenfeldgruppen Unterelemente besitzen, ansonsten sind diese nicht standardkonform.

#### Wie löse ich den Fehler auf?

Fügen Sie Ihrer Datenfeldgruppe Unterelemente hinzu.

## 1024: Die Freitextregel muss befüllt sein.

#### Warum ist das wichtig?

Laut Standard XDatenfelder 3 ist dies bei Regeln ein Pflichtattribut.

#### Wie löse ich den Fehler auf?

Ergänzen Sie unter Freitextregel einen passenden Text.

## 1025: In einem Datenschema dürfen Baukastenelement auf oberster Ebene nicht mehrfach in derselben Version enthalten sein.

#### Warum ist das wichtig?

Es gibt keinen sinnvollen Grund für dieses Vorgehen. Wenn Sie auf oberster Ebene mehrfach das gleiche Element benötigen, können Sie dies über die Multiplizität kennzeichnen.

#### Wie löse ich den Fehler auf?

Klären Sie, warum an dieser Stelle mehrfach das gleiche Element auf einer Ebene auftaucht.
-	Wenn jeweils das gleiche Element gemeint ist, dann ergänzen Sie eine passende Multiplizität.
-	Wenn das gleiche Element, aber in unterschiedlichem semantischem Kontext gemeint ist, dann sollten Sie die Struktur Ihres Schemas anpassen. Beispielsweise wird Ihr Element in einer Datenfeldgruppe verbaut, die den geänderten Kontext liefert.

## 1026: Zirkelbezug

#### Warum ist das wichtig?

Ein Zirkelbezug liegt dann vor, wenn eine Gruppe sich selbst als Unterelement enthält. Damit besitzt diese Gruppe gleichzeitig unendlich viele Unterelemente.

#### Wie löse ich den Fehler auf?

Entfernen Sie den Zirkelbezug aus der entsprechenden Gruppe.

## 1027: Unbekannte Feldart.

#### Warum ist das wichtig?

Die Liste der erlaubten Feldarten wird im Standard XDatenfelder 3 vorgegeben. Werte, die nicht durch den Standard genutzt werden, dürfen nicht gewählt werden.

#### Wie löse ich den Fehler auf?

Passen Sie Ihre Feldart auf einen, im Standard definierten, Wert an.

## 1028: Unbekannter Datentyp.

#### Warum ist das wichtig?

Die Liste der erlaubten Datentypen wird im Standard XDatenfelder 3 vorgegeben. Werte, die nicht durch den Standard genutzt werden, dürfen nicht gewählt werden.

#### Wie löse ich den Fehler auf?

Passen Sie Ihren Datentyp auf einen, im Standard definierten, Wert an.

## 1029: Unbekannte Relationsart.

#### Warum ist das wichtig?

Die Liste der erlaubten Relationsarten wird im Standard XDatenfelder 3 vorgegeben. Werte, die nicht durch den Standard genutzt werden, dürfen nicht gewählt werden.

#### Wie löse ich den Fehler auf?

Passen Sie Ihre Relationsart auf einen, im Standard definierten, Wert an.

## 1030: Unbekannte Vorbefüllungsart.

#### Warum ist das wichtig?

Die Liste der erlaubten Vorbefüllungsarten wird im Standard XDatenfelder 3 vorgegeben. Werte, die nicht durch den Standard genutzt werden, dürfen nicht gewählt werden.

#### Wie löse ich den Fehler auf?

Passen Sie Ihre Vorbefüllungsart auf einen, im Standard definierten, Wert an.

## 1031: Unbekannte Strukturelementart.

#### Warum ist das wichtig?

Die Liste der erlaubten Strukturelementarten wird im Standard XDatenfelder 3 vorgegeben. Werte, die nicht durch den Standard genutzt werden, dürfen nicht gewählt werden.

#### Wie löse ich den Fehler auf?

Passen Sie Ihre Strukturelementart auf einen, im Standard definierten, Wert an.

## 1032: Unbekannter Status.

#### Warum ist das wichtig?

Die Liste der erlaubten Status wird im Standard XDatenfelder 3 vorgegeben. Werte, die nicht durch den Standard genutzt werden, dürfen nicht gewählt werden.

#### Wie löse ich den Fehler auf?

Passen Sie Ihren Status auf einen, im Standard definierten, Wert an.

## 1033: Unbekannte Änderbarkeit der Struktur.

#### Warum ist das wichtig?

Die Änderbarkeit der Struktur wird durch eine Codeliste im Standard XDatenfelder 3 vorgegeben. Werte, die nicht in dieser Codeliste vorhanden sind, dürfen nicht gewählt werden.

#### Wie löse ich den Fehler auf?

Passen Sie Ihre Änderbarkeit der Struktur auf einen, im Standard definierten, Wert an.

## 1034: Unbekannte Änderbarkeit der Repräsentation.

#### Warum ist das wichtig?

Die Änderbarkeit der Repräsentation wird durch eine Codeliste im Standard XDatenfelder 3 vorgegeben. Werte, die nicht in dieser Codeliste vorhanden sind, dürfen nicht gewählt werden.

#### Wie löse ich den Fehler auf?

Passen Sie Ihre Änderbarkeit der Repräsentation auf einen, im Standard definierten, Wert an.

## 1035: Hinweis noch im alten Format.

#### Warum ist das wichtig?

Der Hinweis entspricht nicht den Vorgaben durch den Standard XDatenfelder 3.

#### Wie löse ich den Fehler auf?

Passen Sie Ihren Hinweis auf das im neuen Standard vorgegebene Format an.

## 1036: In einer Codeliste muss die Kombination aus Kennung und Version immer eindeutig sein.

#### Warum ist das wichtig?

Wenn keine eindeutige Kombination aus Kennung und Version verwendet wird, kann keine Zuordnung zu der entsprechen Version einer Codeliste im XRepository erfolgen.

#### Wie löse ich den Fehler auf?

Überprüfen Sie die Kennung, sowie die Version mithilfe des XRepositorys.

## 1037: In einem Datenschema darf eine Codeliste nicht in mehreren Versionen verwendet werden.

#### Warum ist das wichtig?

In der Regel trifft dieser Fall nicht zu, falls doch liegen fachliche Gründe vor.

#### Wie löse ich den Fehler auf?

Analysieren Sie zunächst, welche Version einer Codeliste Sie verwenden möchten. Passen Sie danach alle passenden Codelisten so an, dass überall die gleiche Version verwendet wird.

## 1038: Die ID eines Objektes ist nicht gesetzt.

#### Warum ist das wichtig?

Die ID ist ein Pflichtattribut im Standard XDatenfelder 3.

#### Wie löse ich den Fehler auf?

Pflegen Sie die ID am entsprechenden Element nach.

## 1039: Das Format der ID entspricht nicht den Vorgaben.

#### Warum ist das wichtig?

Es gibt klare Anforderungen an eine ID im FIM-Baustein Datenfelder; diese müssen von jedem Element erfüllt werden.

#### Wie löse ich den Fehler auf?

Passen Sie Ihre ID an die Vorgaben an.

## 1040: Die Kennung einer Codeliste ist leer.

#### Warum ist das wichtig?

Eine Codeliste ist durch Ihre Kennung eindeutig definiert. Ohne diese Angabe bleibt unklar, auf welche Codeliste man sich bezieht.

#### Wie löse ich den Fehler auf?

Ergänzen Sie eine passende Codelistenkennung.