# Meldungen im Zusammenhang mit Code- und Wertelisten

## Ungültige Kennung der Codeliste

### Welche Fehlermeldungen betrifft das?

- **1005**: Die Kennung einer Codeliste ist nicht als URN formuliert, z. B. urn:&ZeroWidthSpace;de:fim:codeliste:dokumenttyp.
- **1040**: Es wurde eine Codeliste angegeben, aber deren Kennung ist leer.
- **1048**: Die URN-Kennung einer Codeliste entspricht nicht den Formatvorgaben, z. B. urn:xoev-de:fim-datenfelder:codeliste:dokumentart (Sonderzeichen, wie z. B. Umlaute, sind u. A. nicht erlaubt!).
- **1115**: Auf die Codeliste kann nicht im XRepository zugegriffen werden. Dies könnte auf einen Fehler in der URN oder der Version der Codeliste zurückzuführen sein.

### Allgemeine Informationen

Codelisten werden aus dem XRepository abgerufen; wenn die Kennung einer Codeliste nicht stimmt, weil die Referenzierung der URN fehlerhaft ist, besteht keine Eindeutigkeit mehr, welche Codeliste hier verwendet werden soll. Dies ist in Nachnutzungsszenarien problematisch.

### Wie löse ich den Fehler auf?

- Achten Sie darauf, dass bei der Verwendung von Codelisten immer eine Kennung angegeben ist.
- Ist eine Kennung vorhanden, aber fehlerhaft, müssen Sie diese korrigieren.
- Kopieren Sie Ihre URN und versuchen Sie diese im XRepository zu finden.
- Vergleichen Sie die Kennungen miteinander und stellen Sie sicher, dass keine Fehler vorhanden sind.
- Falls Sie Fehler finden sollten, korrigieren Sie diese.

- **TIPP:** Es kann auch vorkommen, dass das XRepository für kurze Zeiträume nicht verfügbar ist. Versuchen Sie es dann zu einem späteren Zeitpunkt erneut.

## Erfassung von Spaltendefinitionen

### Welche Fehlermeldungen betrifft das?

- **1058**: Spaltendefinitionen (CodeKey, NameKey, HelpKey) dürfen nur angegeben werden, wenn im Datenfeld eine referenzierte Codeliste enthalten ist.
- **1122**: Die referenzierte Spalte existiert nicht in der zugeordneten Codeliste.
- **1123**: Die referenzierte Spalte existiert nicht in der aktuellsten Version der zugeordneten Codeliste.

### Allgemeine Informationen

Bei der Nachnutzung einer Codeliste kann man jeder Spalte der Codeliste eine besondere Bedeutung zuordnen. Man kann definieren, bei welcher der Spalten es sich um den Cdoe, den Namen oder die Hilfe handelt (CodeKey, NameKey und HelpKey). Diese Angabe macht nur Sinn, wenn im entsprechenden Datenfeld auch tatsächlich eine Codeliste enthalten ist.

Bei der Zuordnung der Bedeutung muss darauf geachtet werden, dass die in der referenzierten Codeliste definierten IDs der Spalten angegeben werden.

### Wie löse ich den Fehler auf?

Je nach gewünschter Intention:
- Hinterlegen Sie die, zu den Spaltendefinitionen zugehörige, Codeliste am Datenfeld.
- Stellen Sie sicher, dass die Spaltendefinitionen ausschließlich IDs der Spalten der Codeliste enthalten. Im folgenden Auszug aus einer Codeliste sind die zulässigen IDs Code sowie Codename.
  ```xml
      <Column Id="Code" Use="required">
         <ShortName>Schlüssel</ShortName>
         <LongName>Schlüssel</LongName>
         <Data Type="string"/>
      </Column>
      <Column Id="Codename" Use="required">
         <ShortName>Name</ShortName>
         <LongName>Name</LongName>
         <Data Type="string"/>
      </Column>
  ```

ODER

- Löschen Sie alle Informationen bezüglich der Spaltendefinitionen.

## Aufbau von Code- und Wertelisten

### Welche Fehlermeldungen betrifft das?

- **1044**: Eine Code- oder Werteliste muss mindestens zwei Einträge haben.
- **1045**: Innerhalb einer Code- oder Werteliste dürfen Codes (Schlüssel) nicht doppelt verwendet werden.

### Allgemeine Informationen

Eine Code- oder Werteliste besteht aus einer gewissen Anzahl von Einträgen. Diese Einträge haben mindestens einen Code (Schlüssel) und einen Namen (Wert). Falls die Code- oder Werteliste nur einen Eintrag enthalten sollte, muss überhaupt keine Code- oder Werteliste verwendet werden.
Die Schlüssel der Code- oder Werteliste müssen eindeutig sein, da sonst keine eindeutige Zuordnung zu einem bestimmten Eintrag möglich ist.

### Wie löse ich den Fehler auf?

Bezüglich **1044**:
- Stellen Sie sicher, dass Ihre Code- oder Werteliste mindestens zwei Einträge hat.
- Falls keine zwei Einträge benötigt werden, und es einfach nur darum geht eine Abfrage zu modellieren, in dem ein bestimmter Wert wahr oder falsch ist; sollten Sie besser ein Datenfeld des Datentyps Wahrheitswert verwenden.

Bezüglich **1045**:
- Identifizieren Sie den doppelten Schlüssel.
- Benennen Sie einen der beiden Schlüssel sprechend um.

## Verwendung von Codelisten oder Wertelisten

### Welche Fehlermeldungen betrifft das?

- **1076**: Der Inhalt stimmt nicht mit der zugeordneten Codeliste überein.
- **1085**: Der Inhalt muss ein Code-Wert der Werteliste sein.

### Allgemeine Informationen

Wird ein Datenfeld als Auswahlfeld definiert, welches auf einer Code- oder Werteliste beruht, dann dürfen zur Befüllung des Feldes ausschließlich die Inhalte der Schlüsselspalte (Code) verwendet werden. Dies bezieht sich auch auf die Vorbefüllung eines Feldes über das Metadatum *Inhalt*.

### Wie löse ich den Fehler auf?

- Achten Sie darauf, dass bei einem Auswahlfeld im Metadatum Inhalt nur Einträge aus der Codespalte der zugrundeliegenden Werteliste oder Codeliste verwendet werden. Beispiel: Wird ein Auswahldatenfeld auf Basis der nachfolgenden Codeliste definiert, dann darf im Feld Inhalt nur der Wert 001, 002 oder 003 eingetragen werden. Der Inhalt kann selbstverständlich auch leer bleiben.

| Code | Name | Hilfe|
|------|------------------|-------------------------------------------------------------------------------------------------------------|
|001 | Renten, Pensionen | | 	
002 | Einnahmen aus bestehenden oder ruhenden Arbeitsverhältnissen, Ferien-, Gelegenheitsarbeiten, Mini-Jobs | |
003 | Vermietung und Verpachtung | |



