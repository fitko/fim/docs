# Informationen zu den Metadaten

## Name

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | FIM-interner Name der Regel, sichtbar für alle Anwender eines Redaktionssystems des Bausteins Datenfelder (fachliche Ersteller, Methodenexperten, Informationsmanager, FIM-Koordinierungsstelle, FIM-Nutzer).|
| Zielgruppe      | Modellierende|
| Pflichtfeld | Ja|
| Elementspezifisches Qualitätskriterium |<ol><li>Der Name soll prägnant sein.</li><li>Der Name der Regel soll identisch zu dem Namen des zu steuernden Elementes sein.</li><li>Vorgaben durch eine Rechtsnorm sind umzusetzen.</li><li>Vorgaben durch einen Standard sind zu berücksichtigen und ggf. zu über- nehmen oder begründet abzuändern.</li><li>Die Bedeutung eines Namens wird anhand von dem Kontext der Regelverwendung vorgegeben.</li></ol>|
| Empfehlung für die maximale Länge |255 Zeichen|
| Im Umfang der methodischen QS? |Komplett manuell |
| Verweis auf den QS-Bericht      | x |
| Beispiel      |*Datum korrekt* – diese Regel steuert bei der Gruppe *Geburtsdatum (teilbekannt)*, ob die Angaben von Tag, Monat und Jahr zusammen ein valides Datum ergeben. |
|mögliche Abweichungen im Referenzkontext| nein |

## Bezug zur Handlungsgrundlage

:::info
Die Namensgebung zu diesem Metadatum hat sich geändert, in XDatenfelder 2 hieß dieses Metadatum noch **Bezug zu Rechtsnorm oder Standardisierungsvorhaben**.
:::

### Bezug zur Handlungsgrundlage

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Dieses Attribut ermöglicht es, Bezüge zu einschlägigen Rechtsnormen, Verordnungen oder Richtlinien zu dokumentieren. Auch ein Bezug zu XÖV-Vorhaben (oder ähnlichem) und den entsprechenden Datencontainern kann hier gesetzt werden, sowie die Nutzung dieses Standards rechtlich nachweisbar ist. Aus dem Bezug zur Rechtsnorm ergibt sich die Gestaltung des Datenfeldes.|
| Zielgruppe      | Modellierende, Fachlichkeit|
| Pflichtfeld | Nein |
| Elementspezifisches Qualitätskriterium |Falls Bedarf besteht, kann zu einer Regel die entsprechende Handlungsgrundlage hinterlegt werden.|
|Empfehlung für die maximale Länge| 255 Zeichen pro Bezug; ein Element kann davon mehrere haben. |
| Im Umfang der methodischen QS? |Komplett manuell |
| Verweis auf den QS-Bericht      |  |
| Beispiel      | FEHLT|
|mögliche Abweichungen im Referenzkontext| nein |

### Link :new:

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Dieses Attribut ermöglicht es zu jedem Bezug einen entsprechenden Link anzugeben.|
| Zielgruppe      | Modellierende, Fachlichkeit |
| Pflichtfeld | Nein|
| Elementspezifisches Qualitätskriterium |<ol><li>Es ist immer ein gültiger Link zu erfassen.</li><li>Eine gute Quelle für Links zu Rechtsgrundlagen finden sich beispielsweise unter: [Gesetze im Internet](https://www.gesetze-im-internet.de/)</li><li>Eine gute Quelle für Links zu XÖV-Standards findet sich unter: [XRepository 3.0](https://www.xrepository.de/) </li><li>Es sind keine unpassenden Links zu ergänzen.</li></ol>|
| Empfehlung für die maximale Länge |255 Zeichen|
| Im Umfang der methodischen QS? | nein |
| Verweis auf den QS-Bericht      | x |
| Beispiel      |Eine Regel, die an einem Datenfeld *Geokoordinate* beschreibt, in welchem Format diese Georkoordinate anzugeben ist, basierend auf der Handlungsgrundlage WGS84 <br/> Link: https://apps.dtic.mil/sti/pdfs/ADA280358.pdf |
|mögliche Abweichungen im Referenzkontext| nein |

## Beschreibung

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Zusätzliche Beschreibung und Erläuterungen eines Datenfelds für FIM-Nutzer. Dies kann z. B. ein Hinweis an den Fachpaten bzw. ans Fachverfahren sein.|
| Zielgruppe      | Modellierende, Dienstleister nachnutzender Systeme |
| Pflichtfeld | Nein|
| Elementspezifisches Qualitätskriterium |Über die Beschreibung können Umsetzungshinweise zu einer Regel ergänzt werden, außerdem können hier Hinweise an andere Modellierende hinterlegt werden.|
| Empfehlung für die maximale Länge |5000 Zeichen|
| Im Umfang der methodischen QS? | keine |
| Verweis auf den QS-Bericht      | x |
| Beispiel      |Eine Regel, die an einem Datenfeld *Geokoordinate* beschreibt, enthält folgende Beschreibung: Das Format der Geokoordinate wurde aus Sicherheitsgründen nicht als Pattern hinterlegt, sondern als Regel formuliert. |
|mögliche Abweichungen im Referenzkontext| nein |

## Freitext Regel :sparkles:

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Fasst den Inhalt einer Regel aus redaktioneller Sicht zusammen, d.h. aus Sicht der Rollen Ersteller, Methodenexperte, Informationsmanager, FIM- Koordinierungsstelle, FIM-Nutzer.|
| Zielgruppe      | Modellierende, Fachlichkeit, Dienstleister nachnutzender Systeme |
| Pflichtfeld | Ja|
| Elementspezifisches Qualitätskriterium |<ol><li>Die Definition soll kurz und prägnant wiedergeben, wer das zu regelnde Objekt bzw. die zu regelnden Objekte ist und unter welchen Bedingungen eine oder mehrere Aktionen in Bezug auf das Objekt bzw. die Objekte durchzuführen sind.</li><li>Die Ansprache der Elemente, die mit dieser Regel gesteuert werden soll, erfolgt mit dem kompletten Namen inklusive der ID aber ohne Nennung der Versionsangabe.</li><li>Die Regel ist nach Möglichkeit präzise zu verschriftlichen. Idealerweise deckt sie den gesamten Funktionsumfang ab, welcher durch sie gesteuert werden soll (siehe Beispiel 1b: Die erste Bedingung kann als Regel, die zweite als Gegenregel bezeichnet werden).</li><li>Falls Punkt drei nicht möglich sein sollte, ist darauf zu achten, dass der Gedanke hinter der Regel klar zu erkennen ist.</li></ol>|
| Empfehlung für die maximale Länge |5000 Zeichen|
| Im Umfang der methodischen QS? | keine |
| Verweis auf den QS-Bericht      | x |
| Beispiel      |Zu einer Datenfeldgruppe *Geburtsdatum (teilbekannt)*: wenn alle drei Felder F60000231 "Tag (ohne Monat und Jahr)", F60000232 "Monat" und F60000233 "Jahr" befüllt sind, muss die Validität des sich ergebenden Kalenderdatums sichergestellt werden. D.h. es muss z.B. 30.02.2019 oder der 31.11.1968 verhindert werden, da diese keine korrekten Datumsangaben sind.|
|mögliche Abweichungen im Referenzkontext| nein |

## Metadaten ohne Umsetzung

:::info
Es gibt eine Reihe von Metadaten, die Regeln betreffen, die im Standard XDatenfelder 3 zwar eingeplant sind, aber momentan nicht genutzt werden. Dies liegt daran, dass es momentan keine gültige Regelsprache im FIM-Baustein Datenfelder gibt.

Die nicht genutzten Metadaten lauten:

**Typ / Parameter / Ziel / Skript / Fehler**
:::
