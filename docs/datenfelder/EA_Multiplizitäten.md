# Multiplizitäten

## Grundsätzliches

- Multiplizitäten liefern Strukturinformationen für Unterelemente in einer Struktur; d. h. für Datenfeldgruppen und Datenfelder innerhalb eines Stammdatenschemas oder einer Datenfeldgruppe.
- Sie steuern, wie oft ein Unterelement innerhalb einer Struktur vorkommen muss und vorkommen kann.
- Die Multiplizität selbst kann durch das Verwenden von Regeln gesteuert werden. In den einfachsten Fällen wird hierbei z. B. ein vorher optionales Feld zu einem Pflichtfeld gemacht.
- Diese Strukturinformation wird in der Form Min : Max (sprich Min zu Max) angegeben; dabei gibt Min an, wie oft das Unterelement mindestens vorkommen muss und Max gibt an, wie oft es maximal vorkommen kann.
- Die Anzahl der minimalen Vorkommnisse darf die Anzahl der maximalen Vorkommnisse nicht überschreiten.
- Eine Multiplizität von 1:1 kann auch verkürzt als 1 dargestellt werden.
<br/>

:::info 

Der Begriff **Kardinalität** wurde früher fälschlicherweise in diesem Kontext verwendet und ist damit veraltet; deswegen sollte er nicht mehr verwendet werden. Näheres zur Unterscheidung der Begriffe Mulitplizität und Kardinalität finden Sie [hier](https://de.wikipedia.org/wiki/Multiplizit%C3%A4t_(UML)) und [hier](https://de.wikipedia.org/wiki/Kardinalit%C3%A4t_(Datenbankmodellierung).).

:::

## Beispiele mit Erläuterungen

|Multiplizität|Wie oft muss das Element minimal vorkommen?|Wie oft kann das Element maximal vorkommen?|Erklärung & Beispiel|
|:------|:------|:------|:------|
|1:1|1|1|Bei einem Element mit dieser Multiplizität handelt es sich um eine **Pflichtangabe**. Die Angabe muss vom Endnutzer genau einmal gemacht werden.<br/><br/>**Beispiel** aus der Datenfeldgruppe Geburtsdatum (teilbekannt): das Geburtsjahr ist ein Pflichtfeld.|
|0:1|0|1|Diese Multiplizität kennzeichnet ein **optionales** Element. <br/> Die Angabe kann vom Endnutzer genau einmal gemacht werden, muss aber nicht erfolgen. <br/><br/> **Beispiel** aus einer Datenfeldgruppe Antragsteller: der Geburtsname ist ein optionales Feld.<br/><br/> Wenn dieses Element durch eine Regel gesteuert wird, kann diese bewirken, dass das Element für den Endnutzer zum Pflichtfeld wird oder gar nicht erst angezeigt wird. <br/><br/>**Beispiel** in der Datenfeldgruppe „Anschrift Inland oder Ausland mit Frage“ werden die Untergruppen „Anschrift Inland“ und „Anschrift Ausland“ je nach Ergebnisse der Abfrage aus- oder eingeblendet.|
|2:5|2|5|Es müssen 2 Angaben getätigt werden, es können aber auch bis zu 5 Angaben getätigt werden. <br/><br/> **Beispiel** aus der Datenfeldgruppe Auslandsanschrift: die Anschrift besteht aus 2 bis 5 Freitext Zeilen.|
|1:*|1|unendlich|Eine Angabe muss zwingend gemacht werden, es können aber beliebig viele weitere Angaben gemacht werden.<br/><br/>**Beispiel** aus einer Datenfeldgruppe Antragsteller: der Antragsteller kann eine oder beliebig viele Staatsangehörigkeiten haben.<br/>|

## Qualitätskriterien zum Metadatum

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Die Multiplizität gibt an, wie oft ein Unterelement Teil einer übergeordneten Struktur sein kann und teilt dies in die minimale und maximale Anzahl der Vorkommnisse auf.|
| Zielgruppe      | Modellierende, Fachlichkeit, Dienstleister nachnutzender Systeme |
| Pflichtfeld | Ja; Defaultwert 1:1|
| Elementspezifisches Qualitätskriterium |siehe Tabelle|
| Empfehlung für die maximale Länge |Die Länge ist durch die Struktur der Multiplizität vorgegeben.|
| Art der Prüfung     | keine |
| Verweis auf den QS-Bericht      | x |
| betroffene Elemente |Datenfelder und Datenfeldgruppen |
| Beispiel      |siehe Tabelle |
|mögliche Abweichungen im Referenzkontext| nein |
