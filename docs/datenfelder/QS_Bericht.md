---
title: QS-Bericht
---
# QS-Bericht

## Was ist der QS-Bericht?

Der QS-Bericht ist ein Tool, welches der Qualitätssicherung Ihrer Datenschemata dient. Sie können den Bericht entweder über Ihr Tool direkt nutzen oder [extern](./A_Links.md) beziehen.<br/>
Der QS-Bericht gibt eine Reihe von Meldungen aus, durch welche die Qualität Ihrer Datenschemata analysiert werden kann. Bei diesen Meldungen gibt es verschieden Arten von Meldungen.<br/>
Mögliche Optionen hierfür sind:

- **kritischer Fehler**
- methodischer Fehler
- *Warnung*
- <u>Hinweis</u>

Die Reihenfolge ist absteigend, d. h. kritische Fehler sind gravierender als methodische Fehler, methodische Fehler sind gravierender als Warnungen und Warnungen sind wichtiger als Hinweise.

### kritische Fehler

Bei kritischen Fehlern ist es am allerwichtigsten diese zu beseitigen. Sie können ein Zeichen für mangelnde Standardkonformität Ihres Datenschemas sein. Solange kritische Fehler vorliegen, ist nicht gesichert, dass ein reibungsloser Austausch zwischen zwei Systemen, die den XDatenfelder Standard unterstützen, möglich ist.

### methodischer Fehler

Solange methodische (und kritische) Fehler vorliegen, kann Ihr Datenschema nicht methodisch oder fachlich freigeben werden. Methodische Fehler verhindern nicht den reibungslosen Austausch, zwischen zwei Systemen, die den XDatenfelder Standard unterstützen.

### Warnungen

Zeigen potentielle methodische Fehler auf, jedoch liegt es im Ermessensspielraum des Modellierenden, ob dies tatsächliche methodische Fehler sind, oder ob die Warnungen ignoriert werden können.
Auf eine Freigabe oder einen Austausch hat dies keinen Einfluss.

### Hinweise

Hinweise liefern wertvolle Tipps, worauf Sie beim Modellieren ggf. noch achten müssen.


## Was hat der QS-Bericht mit der methodischen Qualitätssicherung zu tun?

Erst wenn der QS-Bericht keine methodischen oder technischen Fehler mehr anzeigt, ist Ihr Datenschema bereit für die methodische Qualitätssicherung durch Ihre Landesredaktion oder den Baustein.
Daraus ergibt sich auch, dass die Fehlerfreiheit des QS-Berichts eine Grundvoraussetzung für den Status **methodisch freigegeben** ist.

## Liste der möglichen Meldungen

Bei dieser Tabelle handelt es sich um die Version 1.0 der XDF Meldungsliste ([siehe hier](https://github.com/vs5000/FIM-XSLT/blob/main/Meldungen.xml)).

| Code | Fehlertyp | Meldung|
|------|------------------|-------------------------------------------------------------------------------------------------------------|
| [<span id="M1001">1001</span>](./QS_new4.md#position-der-regel)| methodischer Fehler | Eine Regel kann nicht auf ein referenziertes Baukastenelement zugreifen.|
| [<span id="M1002">1002</span>](./QS_new2.md#metadaten-die-pflichtattribute-sind) | **kritischer Fehler** | Der Name eines Elements ist nicht gesetzt.|
| [<span id="M1003">1003</span>]| *Warnung* | In einem Datenschema sollte ein Baukastenelement i. d. R. nicht mehrfach in unterschiedlichen Versionen enthalten sein.|
| [<span id="M1004">1004</span>]| methodischer Fehler | In einer Datenfeldgruppe darf ein Baukastenelement nicht mehrfach in   derselben Version enthalten sein.|
| [<span id="M1005">1005</span>](./QS_new3.md#ungültige-kennung-der-codeliste) | **kritischer Fehler** | Die Kennung einer Codeliste ist nicht als URN formuliert, z. B. urn:&ZeroWidthSpace;de:fim:codeliste:dokumenttyp.|
| [<span id="M1006">1006</span>](./QS_new2.md#handlungsgrundlagen-als-pflichtattribute) | **kritischer Fehler** | Der Bezug zur Handlungsgrundlage darf bei Elementen mit der   Strukturelementart 'rechtsnormgebunden' nicht leer sein.|
| [<span id="M1007">1007</span>](./QS_new2.md#metadaten-die-pflichtattribute-sind)| **kritischer Fehler** | Es ist kein Dokumentsteckbrief zugeordnet.|
| [<span id="M1008">1008</span>](./QS_new2.md#handlungsgrundlagen-als-pflichtattribute) | XDF 2 Fehler | Der Bezug zur Handlungsgrundlage darf bei Dokumentsteckbriefen nicht leer sein.|
| [<span id="M1009">1009</span>](./QS_new2.md#metadaten-die-pflichtattribute-sind) | **kritischer Fehler** | Die 'Bezeichnung Eingabe' muss befüllt werden.|
| [<span id="M1010">1010</span>](./QS_new2.md/#metadaten-die-pflichtattribute-sind) |**kritischer Fehler**| Die 'Bezeichnung Ausgabe' muss befüllt werden.|
| [<span id="M1011">1011</span>](./QS_new5/#kombinationen-aus-feldart-und-datentyp)| **kritischer Fehler** | Bei Datenfeldern mit der Feldart 'Auswahl' sollte der Datentyp 'Text' sein.|
| [<span id="M1012">1012</span>](./QS_new.md#präzisierungsangaben-für-text--und-nummerische-felder)| methodischer Fehler | Bei Datenfeldern mit der Feldart 'Auswahlfeld' oder 'Statisches,   read-only Feld' dürfen weder die minimale noch die maximale Feldlänge   angegeben werden.|
 [<span id="M1013">1013</span>](./QS_new5.md#strukturelementart) | methodischer Fehler | Innerhalb von Datenschemata und Datenfeldgruppen dürfen nur Elemente mit der Strukturelementart harmonisiert oder rechtsnormgebunden verwendet werden.|
| [<span id="M1014">1014</span>](./QS_new5.md#strukturelementart)| **kritischer Fehler** | Datenfelder dürfen nicht die Strukturelementart 'abstrakt' haben.|
| [<span id="M1015">1015</span>](./QS_new.md#präzisierungsangaben-für-text--und-nummerische-felder) | **kritischer Fehler** | Eine Feldlänge darf nur bei einem Datenfeld mit dem Datentyp 'Text' oder   'String.Latin+' angegeben werden.|
| [<span id="M1016">1016</span>](./QS_new.md#präzisierungsangaben-für-text--und-nummerische-felder) | **kritischer Fehler** | Ein Wertebereich darf nur bei einem Datenfeld mit einem nummerischen   Datentyp (Nummer, Ganzzahl, Geldbetrag) oder einem Zeitdatentyp (Uhrzeit,   Datum, Zeitpunkt) angegeben werden.|
| [<span id="M1017">1017</span>](./QS_new5.md#kombinationen-aus-feldart-und-datentyp) | **kritischer Fehler**  | Ist eine Code- oder Werteliste zugeordnet, muss die Feldart 'Auswahl'   sein.|
| [<span id="M1018">1018</span>] | *Warnung* | Wenn ein Datenfeld die Feldart 'Auswahl' hat, sollte i. d. R.  entweder eine Code- oder eine Werteliste zugeordnet sein. |
| [<span id="M1019">1019</span>](./QS_new4.md#referenzieren-von-baukastenelementen) | methodischer Fehler | Baukastenelemente, die in Regeln referenziert werden, dürfen keine Versionsangaben beinhalten.|
| [<span id="M1021">1021</span>](./QS_new2.md#multiplizität) | **kritischer Fehler** | Es ist keine Multiplizität angegeben.|
| [<span id="M1022">1022</span>](./QS_new2.md#handlungsgrundlagen-als-pflichtattribute) | methodischer Fehler | Bei Verwendung von Elementen mit der Strukturelementart 'abstrakt' oder 'harmonisiert' innerhalb von Datenfeldgruppen mit der Strukturelementart   'rechtsnormgebunden' muss zur Multiplizität ein Bezug zu einer Handlungsgrundlage angegeben werden.|
| [<span id="M1023">1023</span>](./QS_new5.md#leere-strukturen) | **kritischer Fehler** | Datenfeldgruppe hat keine Unterelemente.|
| [<span id="M1024">1024</span>](./QS_new2.md#metadaten-die-pflichtattribute-sind) | methodischer Fehler | Die Freitextregel muss befüllt sein.|
| [<span id="M1025">1025</span>] | methodischer Fehler | In einem Datenschema dürfen Baukastenelement auf oberster Ebene nicht mehrfach in derselben Version enthalten sein.|
| [<span id="M1026">1026</span>](./QS_new5.md#fehlerhafte-strukturen) | **kritischer Fehler**  | Zirkelbezug|
| [<span id="M1027">1027</span>](./QS_new2.md#unbekannte-werte) | **kritischer Fehler**  | Es ist keine oder eine unbekannte Feldart angegeben.|
| [<span id="M1028">1028</span>](./QS_new2.md#unbekannte-werte) | **kritischer Fehler**  | Es ist kein oder ein unbekannter Datentyp angegeben.|
| [<span id="M1029">1029</span>](./QS_new2.md#unbekannte-werte) | **kritischer Fehler** | Es ist keine oder eine unbekannte Relationsart angegeben.|
| [<span id="M1030">1030</span>](./QS_new2.md#unbekannte-werte) | **kritischer Fehler**  | Es ist keine oder eine unbekannte Vorbefüllungsart angegeben.|
| [<span id="M1031">1031</span>](./QS_new2.md#unbekannte-werte) | **kritischer Fehler**  | Es ist keine oder eine unbekannte Strukturelementart angegeben.|
| [<span id="M1032">1032</span>](./QS_new2.md#unbekannte-werte) | **kritischer Fehler**  | Es ist kein oder ein unbekannter Status angegeben.|
| [<span id="M1033">1033</span>](./QS_new2.md#unbekannte-werte) | **kritischer Fehler**  | Es ist keine oder eine unbekannte Änderbarkeit der Struktur angegeben.|
| [<span id="M1034">1034</span>](./QS_new2.md#unbekannte-werte) | **kritischer Fehler**  | Es ist keine oder eine unbekannte Änderbarkeit der Repräsentation angegeben.|
| [<span id="M1037">1037</span>] | *Warnung* | In einem Datenschema sollte eine Codeliste i. d. R. nicht in mehreren Versionen verwendet werden.|
| [<span id="M1038">1038</span>](./QS_new2.md#id-identifier) | **kritischer Fehler**  | Die ID eines Objektes ist nicht gesetzt.|
| [<span id="M1039">1039</span>](./QS_new2.md#id-identifier) | **kritischer Fehler**  | Das Format der ID entspricht nicht den Vorgaben.|
| [<span id="M1040">1040</span>](./QS_new3.md#ungültige-kennung-der-codeliste)| **kritischer Fehler**|Es wurde eine Codeliste angegeben, aber deren Kennung ist leer.|
| [<span id="M1041">1041</span>](./QS_new4.md#referenzieren-von-baukastenelementen) | methodischer Fehler | Baukastenelemente, die in Regeln referenziert werden, verwenden noch die   alte, verkürzte Element-ID.|
| [<span id="M1042">1042</span>](./QS_new2.md#unbekannte-werte) | **kritischer Fehler**  | Es ist keine oder eine unbekannte Gruppenart angegeben.|
| [<span id="M1043">1043</span>] | methodischer Fehler | Eine Auswahlgruppe muss mindestens zwei Unterelemente haben.|
| [<span id="M1044">1044</span>](./QS_new3.md#verwendung-von-code--und-wertelisten) | methodischer Fehler | Eine Code- oder Werteliste muss mindestens zwei Einträge haben.|
| [<span id="M1045">1045</span>](./QS_new3.md#verwendung-von-code--und-wertelisten) | **kritischer Fehler** | Innerhalb einer Code- oder Werteliste dürfen Codes (Schlüssel) nicht doppelt verwendet werden.|
| [<span id="M1046">1046</span>](./QS_new2.md#multiplizität) | **kritischer Fehler**  | Die Multiplizität entspricht nicht den Formatvorgaben.|
| [<span id="M1047">1047</span>](./QS_new2.md#multiplizität) | **kritischer Fehler** | In der Multiplizität muss die minimale Anzahl kleiner oder gleich der maximalen Anzahl sein.|
| [<span id="M1048">1048</span>](./QS_new3.md#ungültige-kennung-der-codeliste) | methodischer Fehler | Die URN-Kennung einer Codeliste entspricht nicht den Formatvorgaben, z. B. urn:xoev-de:fim-datenfelder:codeliste:dokumentart (Sonderzeichen, wie z. B. Umlaute, sind u. A. nicht erlaubt!).|
| [<span id="M1049">1049</span>](./QS_new2.md#metadaten-die-pflichtattribute-sind) | **kritischer Fehler**  | Die Definition eines Dokumentsteckbriefs muss befüllt sein.|
| [<span id="M1050">1050</span>](./QS_new2.md#metadaten-die-pflichtattribute-sind) | **kritischer Fehler**  | Die Bezeichnung muss befüllt sein.|
| [<span id="M1051">1051</span>](./QS_new2.md#handlungsgrundlagen-als-pflichtattribute) | **kritischer Fehler**  | Der Bezug zur Handlungsgrundlage darf in einem Datenschema nicht leer sein.|
| [<span id="M1052">1052</span>](./QS_new2.md#handlungsgrundlagen-als-pflichtattribute) | **kritischer Fehler**  | Der Bezug zur Handlungsgrundlage darf nur bei abstrakten   Dokumentsteckbriefen leer sein.|
| [<span id="M1053">1053</span>](./QS_new5.md#abstrakte-und-konkrete-dokumentsteckbriefe) | **kritischer Fehler**  | Zu einem konkreten Dokumentsteckbrief dürfen keine Dokumentsteckbriefe zugeordnet werden.|
| [<span id="M1054">1054</span>](./QS_new5.md#abstrakte-und-konkrete-dokumentsteckbriefe) | **kritischer Fehler**  | Zu einem abstrakten Dokumentsteckbrief müssen mindestens zwei konkrete Dokumentsteckbriefe zugeordnet werden.|
| [<span id="M1055">1055</span>](./QS_new2.md#unbekannte-werte) | methodischer Fehler  | Es ist keine oder eine unbekannte Dokumentart angegeben.|
| [<span id="M1056">1056</span>](./QS_new.md#präzisierungsangaben-für-anlagefelder) | **kritischer Fehler** | Die Dateigröße darf nur befüllt sein, wenn der Datentyp des Feldes den   Wert 'Anlage (Datei)' hat.|
| [<span id="M1057">1057</span>](./QS_new.md#präzisierungsangaben-für-anlagefelder) | **kritischer Fehler** | Erlaubte Datentypen dürfen nur angegeben werden, wenn der Datentyp des   Feldes den Wert 'Anlage (Datei)' hat.|
| [<span id="M1058">1058</span>](./QS_new3.md#erfassung-von-spaltendefinitionen) | methodischer Fehler | Spaltendefinitionen (CodeKey, NameKey, HelpKey) dürfen nur angegeben werden, wenn im Datenfeld eine referenzierte Codeliste enthalten ist.|
| [<span id="M1059">1059</span>](./QS_new.md#feldlängenbegrenzung-für-texteingabefelder) | **kritischer Fehler** | Die minimale Feldlänge muss eine ganze Zahl größer oder gleich Null sein.|
| [<span id="M1060">1060</span>](./QS_new.md#feldlängenbegrenzung-für-texteingabefelder) | **kritischer Fehler** | Die maximale Feldlänge muss eine ganze Zahl größer Null sein.|
| [<span id="M1061">1061</span>](./QS_new.md#inkonsistente-angabe-der-unteren-und-oberen-grenze) | **kritischer Fehler** | Die minimale Feldlänge darf nicht größer sein als die maximale Feldlänge.|
| [<span id="M1062">1062</span>](./QS_new.md#detailierte-meldungen-für-text--und-nummerische-felder) | **kritischer Fehler** | Die untere Wertgrenze muss eine Zahl sein.|
| [<span id="M1063">1063</span>](./QS_new.md#detailierte-meldungen-für-text--und-nummerische-felder) | **kritischer Fehler** | Die obere Wertgrenze muss eine Zahl sein.|
| [<span id="M1064">1064</span>](./QS_new.md#inkonsistente-angabe-der-unteren-und-oberen-grenze) | **kritischer Fehler** | Die untere Wertgrenze darf nicht größer sein als die obere Wertgrenze.|
| [<span id="M1065">1065</span>](./QS_new.md#detailierte-meldungen-für-text--und-nummerische-felder)| **kritischer Fehler** | Die untere Wertgrenze muss eine ganze Zahl sein.|
| [<span id="M1066">1066</span>](./QS_new.md#detailierte-meldungen-für-text--und-nummerische-felder) | **kritischer Fehler** | Die obere Wertgrenze muss eine ganze Zahl sein.|
| [<span id="M1067">1067</span>](./QS_new.md#detailierte-meldungen-für-text--und-nummerische-felder) | **kritischer Fehler** | Die untere Wertgrenze muss ein Datum sein.|
| [<span id="M1068">1068</span>](./QS_new.md#detailierte-meldungen-für-text--und-nummerische-felder) | **kritischer Fehler** | Die obere Wertgrenze muss ein Datum sein.|
| [<span id="M1069">1069</span>](./QS_new.md#defaultwerte-passend-zu-den-präzisierungsangaben) | **kritischer Fehler** | Der Inhalt muss eine Zahl sein.|
| [<span id="M1070">1070</span>](./QS_new.md#defaultwerte-passend-zu-den-präzisierungsangaben) | **kritischer Fehler** | Der Inhalt muss eine ganze Zahl sein.|
| [<span id="M1071">1071</span>](./QS_new.md#defaultwerte-passend-zu-den-präzisierungsangaben) | **kritischer Fehler** | Der Inhalt muss ein Datum sein.| <a id="M1072"/>
| [<span id="M1072">1072</span>](./QS_new.md#defaultwerte-wiedersprechen-den-präzisierungsangaben) | **kritischer Fehler** | Der Inhalt unterschreitet die untere Wertgrenze.|
| [<span id="M1073">1073</span>](./QS_new.md#defaultwerte-wiedersprechen-den-präzisierungsangaben) | **kritischer Fehler** | Der Inhalt überschreitet die obere Wertgrenze.|
| [<span id="M1074">1074</span>](./QS_new.md#defaultwerte-wiedersprechen-den-präzisierungsangaben) | **kritischer Fehler** | Der Inhalt unterschreitet die Minimallänge.|
| [<span id="M1075">1075</span>](./QS_new.md#defaultwerte-wiedersprechen-den-präzisierungsangaben) | **kritischer Fehler** | Der Inhalt überschreitet die Maximallänge.|
| [<span id="M1076">1076</span>](./QS_new.md#defaultwerte-passend-zu-den-präzisierungsangaben) | methodischer Fehler | Der Inhalt stimmt nicht mit der zugeordneten Codeliste überein.|
| [<span id="M1077">1077</span>](./QS_new.md#defaultwerte-passend-zu-den-präzisierungsangaben) | **kritischer Fehler** | Der Inhalt muss ein Wahrheitswert sein (true oder false).|
| [<span id="M1078">1078</span>](./QS_new5.md#leere-strukturen) | **kritischer Fehler**  | Ein Datenschema muss mindestens ein Unterlement enthalten.  |
| [<span id="M1079">1079</span>](./QS_new.md#detailierte-meldungen-für-text--und-nummerische-felder) | **kritischer Fehler** | Die untere Wertgrenze muss ein Zeitpunkt (Datum und Uhrzeit) sein.|
| [<span id="M1080">1080</span>](./QS_new.md#detailierte-meldungen-für-text--und-nummerische-felder) | **kritischer Fehler** | Die obere Wertgrenze muss ein Zeitpunkt (Datum und Uhrzeit) sein.|
| [<span id="M1081">1081</span>](./QS_new.md#detailierte-meldungen-für-text--und-nummerische-felder) | **kritischer Fehler** | Die untere Wertgrenze muss eine Uhrzeit sein.|
| [<span id="M1082">1082</span>](./QS_new.md#detailierte-meldungen-für-text--und-nummerische-felder) | **kritischer Fehler** | Die obere Wertgrenze muss eine Uhrzeit sein.|
| [<span id="M1083">1083</span>](./QS_new.md#defaultwerte-passend-zu-den-präzisierungsangaben) | **kritischer Fehler** | Der Inhalt muss ein Zeitpunkt (Datum und Uhrzeit) sein.|
| [<span id="M1084">1084</span>](./QS_new.md#defaultwerte-passend-zu-den-präzisierungsangaben) | **kritischer Fehler** | Der Inhalt muss eine Uhrzeit sein.|
| [<span id="M1085">1085</span>](./QS_new.md#defaultwerte-passend-zu-den-präzisierungsangaben) | methodischer Fehler | Der Inhalt muss ein Code-Wert der Werteliste sein.|
| [<span id="M1087">1087</span>] | **kritischer Fehler** | Relation zu einem unzulässigen Objekt.|
| [<span id="M1088">1088</span>] | methodischer Fehler | Diese Relation darf in diesem Objekt nicht verwendet werden.|
| [<span id="M1089">1089</span>] | kritischer Fehler | Bei Datenfeldern mit der Feldart 'Eingabe' darf der Datentyp nicht 'Statisches Objekt' sein - wird vorübergehend abgestellt.|
| [<span id="M1090">1090</span>] | **kritischer Fehler** | Bei Datenfeldern mit der Feldart 'Statisch' darf der Datentyp nur 'Statisches Objekt', 'Text' oder 'Text (String Latin)' sein.|
| [<span id="M1091">1091</span>] | **kritischer Fehler** | In der Relation ist kein Objekt angegeben.|
| [<span id="M1092">1092</span>] | **kritischer Fehler** | Die Angabe zu IstAbstrakt ist falsch oder leer.|
| [<span id="M1093">1093</span>] | *Warnung* | Innerhalb einer Code- oder Werteliste sollten Namen (Bezeichnungen) nicht doppelt verwendet werden.|
| [<span id="M1094">1094</span>]| methodischer Fehler | Datenschemata oder Datenfeldgruppen mit mindestens einem weiteren Unterelement dürfen das Element F60000000001 'Default-Datenfeld' nicht enthalten.|
| [<span id="M1095">1095</span>] | *Warnung*| Datenschemata oder Datenfeldgruppen sollten das Element F60000000001 'Default-Datenfeld' nur in Ausnahmefällen beinhalten.|
| [<span id="M1096">1096</span>] | *Warnung*| Ein Datenschema sollte nur übergangsweise dem Dokumentsteckbrief D99000000001 'Default-Dokumentsteckbrief' zugeordnet werden.|
| [<span id="M1097">1097</span>] | methodischer Fehler | Ein methodisch oder fachlich freigegebenes Datenschema darf nicht dem Dokumentsteckbrief D99000000001 'Default-Dokumentsteckbrief' zugeordnet sein.|
| [<span id="M1098">1098</span>] | methodischer Fehler | Bezeichung und Hilfetext dürfen nicht gleich sein.|
| [<span id="M1099">1099</span>] | methodischer Fehler | Name und Hilfetext dürfen nicht gleich sein.|
| [<span id="M1100">1100</span>] | **kritischer Fehler** | Bei Datenfeldern mit den Datentypen 'Anlage (Datei)' oder 'Statisches Objekt' darf der Inhalt nicht gefüllt sein.|
| [<span id="M1101">1101</span>](./QS_new2.md#handlungsgrundlagen-als-pflichtattribute) | *Warnung*| Der Bezug zur Handlungsgrundlage sollte bei Elementen mit der Strukturelementart 'harmonisiert' möglichst nicht leer sein.|
| [<span id="M1102">1102</span>] | *Warnung*| Bei Eingabedatenfeldern mit dem Datentyp 'Text' oder 'String.Latin+'   sollten, wenn möglich, die minimale und maximale Feldlänge angegeben werden.|
| [<span id="M1103">1103</span>] | *Warnung*| Bei Datenfeldern mit dem Datentyp ‘Nummer', 'Ganzzahl', 'Geldbetrag' oder   'Datum’ sollte, wenn möglich, ein Wertebereich angegeben werden.|
| [<span id="M1104">1104</span>] | *Warnung* | Zu einer Regel ist kein Script definiert - wird vorübergehend abgestellt|
| [<span id="M1106">1106</span>] | *Warnung*| Eine Datenfeldgruppe sollte mehr als ein Unterelement haben.|
| [<span id="M1107">1107</span>] | *Warnung*| Der Hilfetext eines Dokumentsteckbriefs sollte befüllt werden.|
| [<span id="M1108">1108</span>] | Warnung| Wenn ein Datenfeld die Feldart 'Auswahl' hat, sollte der Datentyp i. d.   R. vom Typ 'Text' sein - wird vorübergehend abgestellt.|
| [<span id="M1109">1109</span>] | *Warnung*| Bei einem Feld mit nur lesendem Zugriff, der Feldart 'Statisch' wird i.   d. R. der Inhalt mit einem Text befüllt.|
| [<span id="M1110">1110</span>] | *Warnung*| In Codelisten sollte der Bezug zur Handlungsgrundlage nicht leer sein. |
| [<span id="M1115">1115</span>] | *Warnung*| Auf die Codeliste kann nicht im XRepository zugegriffen werden. Dies könnte auf einen Fehler in der URN oder der Version der Codeliste zurückzuführen sein.|
| [<span id="M1116">1116</span>] | *Warnung*| Baukastenelemente im Status 'inaktiv' sollten nur in Ausnahmefällen verwendet werden.|
| [<span id="M1117">1117</span>] | <u>Hinweis</u>   | Das Feld 'Fachlicher Ersteller' darf zur Versionierung oder   Veröffentlichung nicht leer sein.|
| [<span id="M1118">1118</span>] | **kritischer Fehler** | Ein Unterelement einer  Auswahlgruppe muss immer die Multiplizität 0:1 haben.|
| [<span id="M1120">1120</span>] | methodischer Fehler | Bei Datenfeldern mit den Datentypen 'Anlage (Datei)' oder 'Statisches Objekt' darf kein Pattern angegeben werden.|
| [<span id="M1121">1121</span>] | *Warnung* | Der Inhalt stimmt nicht mit der aktuellsten Version der zugeordneten Codeliste überein.|
| [<span id="M1122">1122</span>] | methodischer Fehler | Die referenzierte Spalte existiert nicht in der zugeordneten Codeliste.|
| [<span id="M1123">1123</span>] | *Warnung* | Die referenzierte Spalte existiert nicht in der aktuellsten Version der zugeordneten Codeliste.|
