# Tipps für die praktische Modellierung

:::info

:warning: Für Stammdatenschemata gilt stets: **Es dürfen nur Daten erfasst werden, die durch die Handlungsgrundlagen begründbar sind.**

:::

## Welche Fragen sollte ich mir vor der Modellierung stellen?

Als Einstieg in die Modellierung ist es zunächst gut Fragen zu stellen.
Diese lassen sich grob in drei Blöcke gliedern:

- **Fragen bezüglich der Quellen**
    - Kenne ich alle Handlungsgrundlagen zu diesem Datenschema?
    - Sind mir alle Handlungsgrundlagen klar und verständlich?
    - Welche Daten werden in den Handlungsgrundlage erwähnt?
    - Gibt es verbindliche Vordrucke?

- **Fragen bezüglich der zu erfassenden Daten**

    - Welche Daten werden für dieses Datenschema benötigt?
    - Wie sehen diese aus?
    - [Welche Daten werden für den Prozess gebraucht?](./DS_Tipps.md#welche-daten-werden-für-den-prozess-gebraucht)

- **Weiterführende Fragen**
    - Bezüglich des Antragstellers: Wer darf diesen Antrag Stellen? Eine natürliche Person, eine juristische Person?
    - Bei natürlichen Personen: Wer genau darf es sein? Die betroffene Person selbst, ein gesetzlicher Vertreter, ein Bevollmächtigter?
    - Welche Staatsangehörigkeit/en darf der Antragsteller haben?
    - Welche Anschriften muss ich berücksichtigen?
        - Inland?
        - Ausland?
        - EU ja/nein?
    - Welche Daten zur natürliche Person dürfen erfasst werden?

:::tip

Hinterfragen Sie so viel wie möglich; binden Sie hierbei immer Ihre Fachlichkeit ein.

:::

:::note

### Welche Daten werden für den Prozess gebraucht?

Über den zugehörige Prozess erhält man einen guten Überblick, welche Daten in den Prozessen eingehen und welche Daten aus dem Prozessen herausgehen.
Es ist anzustreben, dass die Daten innerhalb des gesamten Prozesses gleich modelliert werden.

### Warum ist das wichtig?

Dies ist wichtig, um die **Medienbruchfreiheit** sicherzustellen. Das heißt, dass Daten, die in einem bestimmten Format im Verlauf des Prozesses gebraucht werden, am Anfang des Prozesses bereits im richtigen Format abgefragt werden.

:::

## Wo fange ich bei der Modellierung am besten an? :star:

- Beginnen Sie stets mit den Daten zu der Person, die den Antrag ausfüllt. (Antragsteller, bevollmächtigter, gesetzl. Vertreter)
- ab da durch die Handlungsgrundlagen durcharbeiten
- fachliche Angaben
- ggf. Belehrung
- falls Schriftformerfordernis: Signatur

## Ich möchte mein Schema qualitätssichern, wie gehe ich vor?

Nutzen Sie hierbei die Tool-seitig bereitgestellten Möglichkeiten. Erzeugen Sie einen QS-Bericht Ihres Datenschemas und übertroffen Sie diesen auf Fehler. Wenn der Bericht keine Fehler mehr ausgibt, dann hat Ihr Datenschema bereits eine gute Qualität. Ansonsten arbeiten Sie sich am besten langsam durch die verschiedenen Fehlermeldungen und lösen diese nacheinander auf. Im Abschnitt über [Qualitätssicherung](QS_methodische) finden Sie dazu mehr Detailinformationen.

## Ich möchte mein Schema visualisieren, wie gehe ich vor?

Nutzen Sie (falls vorhanden) die Optionen, die Ihnen Ihr Redaktionssystem zur Verfügung stellt.
Außerdem können Sie das frei verfügbare [Visualisierungstool](https://github.com/vs5000/FIM-XSLT) nutzen.

## Handlungsgrundlagen von Datenschemata

Die Handlungsgrundlagen eines Datenschemas bestehen aus der Summe aller Handlungsgrundlagen ihrer rechtsnormbezogenen Unterelemente und aller Verwendungsgrundlagen ihrer harmonisierten Unterelemente.

## Freigabe & Veröffentlichung

### Was muss ich tun um mein Datenschema zu veröffentlichen?

Zur Veröffentlichung ist es notwendig eine Version Ihres Datenschemas zu erzeugen. Nur Versionen können in einen Status gesetzt werden, der eine Freigabe impliziert und nur Datenschemata mit einem Freigabestatus können veröffentlicht werden. <br/>
Status, die eine Freigabe beinhalten sind die Status: **Entwurf**, **methodisch freigegeben**, sowie **fachlich freigegeben (Silber/Gold)**.
Nach diesen zwei Schritten können Sie Ihr Datenschema veröffentlichen.

### Wer darf wann den Freigabestatus setzen? :star:

Modellierende mit der Zusatzberechtigung **Fachliche Freigabe** dürfen den Freigabestatus setzen. <br/>
- Der **Entwurf**sstatus wird durch den Modellierenden selbst gesetzt; einzige Voraussetzung hier: das Datenschema muss einen guten Reifegrad haben.
- Der Status **methodisch freigeben** wird bei Typ 1, 2 und 3 Leistungen durch den FIM-Bausten Datenfelder gesetzt.
- Der Status **fachlich freigegeben (Silber/Gold)** wird durch die Modellierenden nach entsprechenden fachlicher Abnahme gesetzt.

### Wer darf ein Datenschema veröffentlichen? :star:

Modellierende mit der Zusatzberechtigung **Veröffentlichung** dürfen Datenschemata veröffentlichen, nachdem ein Freigabestatus gesetzt wurde.

### Was heißt das überhaupt, wenn ein Datenschema veröffentlicht ist?

Wenn Ihr Datenschema veröffentlicht ist, dann ist dieses Datenschema im FIM-Portal bzw. Sammelrepository von jedem einsehbar. Voraussetzung hierfür ist, dass Ihr Redaktionssystem an das FIM-Portal bzw. das Sammelrepository angeschlossen ist.

### Mein veröffentlichtes Schema ist veraltet, wie kann ich es löschen?

Löschen Sie niemals ein veröffentlichtes Datenschema, es ist möglich, dass dieses Datenschema bereits an anderer Stelle nachgenutzt wird. Setzen Sie es stattdessen auf den Status **inaktiv**.

## Was muss ich bezüglich Datenschutz beachten?

Wenn Sie nur Daten erheben, die durch Handlungsgrundlagen begründbar sind, dann nichts. <br/>
Im Referenzkontext kann dies abweichen, gehen Sie mit Bedacht vor.

## Wann darf ich mich auf das Verwaltungsverfahrensgesetz beziehen?

Immer. Es sei denn das Fachgesetz widerspricht dem Verwaltungsverfahrensgesetz.

## Wie gehe ich mit Schriftformerfordernis um?

Ergänzen Sie die Gruppe *Signatur* (aus BOB) in ihrem Schema.

## Wo finde ich Hilfestellung, wenn ich referenzbasierte Datenfelder erstellen möchte? :star:
Das zugrundeliegende Rahmengerüst für referenzbasierte Datenfelder ist nicht so anders, wie das für rechtsnormgebundene Datenfelder. Im unten beschriebenen Teil, der die Qualitätskriterien für jedes Metadatum einzeln beschreibt, finden sich pro Metadatum Hilfestellung dazu unter “Empfehlung für referenzbasierte Datenschemata”, diese gilt es zu beachten.
Unterschiede werden in den Tabellen rausgearbeitet.
