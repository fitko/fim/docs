// diese Datei enthält die Struktur der Sidebar für den Baustein DATENFELDER
// export der Struktur wird in "sidebar.js" verwendet.

const df_structure = [
        // Abschnitt für Baustein Datenfelder
		{
        	'type': 'category',
        	'label': 'Baustein Datenfelder',
        	'items': [
				{
					'type': 'category',
					'label': 'FIM Schema-Repository',
					'items': [
						'schema-repository/about-this-schemarepository',
						'schema-repository/about-this-doc',
						'schema-repository/context',
						'schema-repository/schemarepository-for-group-1',
						'schema-repository/schemarepository-for-group-2',
						'schema-repository/schemarepository-for-group-4',
						'schema-repository/schema-repository-api',
						'schema-repository/roadmap',
						'schema-repository/glossary',
						'schema-repository/changelog',
					]
				},
				{
					'type': 'category',
					'label': 'Fachkonzept (in Arbeit)',
					'items': [
						// 'datenfelder/Fachkonzept/Einleitung',
						'datenfelder/Fachkonzept/Lesehilfe',
						'datenfelder/Fachkonzept/Identifikatoren',
						'datenfelder/Fachkonzept/Datenfelder',
						'datenfelder/Fachkonzept/Datenfeldgruppen',
						// 'datenfelder/Fachkonzept/Regeln',
						'datenfelder/Fachkonzept/Datenschemabibliothek',
						'datenfelder/Fachkonzept/Datenfeldkatalog',
						'datenfelder/Fachkonzept/Metamodell',
						'datenfelder/Fachkonzept/Relationen',
						'datenfelder/Fachkonzept/Status',
						// 'datenfelder/Fachkonzept/Schnittstellen',
						'datenfelder/Fachkonzept/zentrale_Repositorys',
					]
				},
				{
					'type': 'category',
					'label': 'QS-Kriterien',
					'items': [
					{
					  type: 'category',
					  label: 'Überblick',
					  collapsed: true,
					  items: [
						'datenfelder/VB_Einleitung',
						'datenfelder/VB_Umgang',
						'datenfelder/VB_Releasenotes',
					  ],
					},
					{
					  type: 'category',
					  label: 'Elementunabhängige Angaben',
					  collapsed: true,
					  items: [
						'datenfelder/EA_Sprache',
						'datenfelder/EA_Handlungsgrundlagen',
						'datenfelder/EA_Metadaten',
						'datenfelder/EA_Relationen',
						'datenfelder/EA_Multiplizitäten',
						'datenfelder/EA_BOB',
						'datenfelder/EA_KATE',
					  ],
					},
					{
					  'type': 'category',
					  'label': 'Datenfeldbaukasten',
					  items: [
							{
							  type: 'category',
							  label: 'Datenfelder',
							  collapsed: true,
							  items: [
								'datenfelder/DF_Allgemeines',
								'datenfelder/DF_Tipps',
								'datenfelder/DF_Metadaten',
							  ],
							},
							{
							  type: 'category',
							  label: 'Datenfeldgruppen',
							  collapsed: true,
							  items: [
								'datenfelder/DFG_Allgemeines',
								'datenfelder/DFG_Tipps',
								'datenfelder/DFG_Metadaten',
							  ],
							},
							{
							  type: 'category',
							  label: 'Regeln',
							  collapsed: true,
							  items: [
								'datenfelder/R_Allgemeines',
								'datenfelder/R_Tipps',
								'datenfelder/R_Metadaten',
							  ],
							}
						  ]
					},
					{
					  'type': 'category',
					  'label': 'Datenschemabibliothek',
					  'items': [
							'datenfelder/DS_Allgemeines',
							'datenfelder/DS_Tipps',
							'datenfelder/DS_Metadaten',
					  ]
					},
					{
					  'type': 'category',
					  'label': 'Datenfeldkatalog',
					  'items': [
							'datenfelder/DSB_Allgemeines',
							'datenfelder/DSB_Tipps',
							'datenfelder/DSB_Namensgebung',
							'datenfelder/DSB_Metadaten',
					  ]
					},
					{
					  'type': 'category',
					  'label': 'Qualitätssicherung',
					  'items': [
									'datenfelder/QS_Bericht',
									'datenfelder/QS_new',
									'datenfelder/QS_new2',
									'datenfelder/QS_new3',
									'datenfelder/QS_new4',
									'datenfelder/QS_new5',
									  ]
					},
					{
					  'type': 'category',
					  'label': 'Anhang',
					  'items': [
									'datenfelder/A_Versionierung',
									'datenfelder/A_Links',
					  ]
					},
				  ]
				},
				{
					'type': 'category',
					'label': 'ME-Austausch (in Arbeit)',
					'items': [
						'datenfelder/ME-Austausch/uebersicht',
						'datenfelder/ME-Austausch/agenda',
						'datenfelder/ME-Austausch/informationen',
						'datenfelder/ME-Austausch/archiv',
					]
				},
        	]
        },
    
]

    // export the structure to enable use in other files
module.exports = df_structure;