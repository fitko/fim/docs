# Handlungsgrundlagen

Der Handlungsrahmen des LeiKa wird durch folgende Dokumente und Entscheidungen konkretisiert:

- Beschlüsse der Fachgruppe LeiKa vom 07.12.10 und 25.08.11 zu Begriffsharmonisierung, Aufbau eines Leistungsberichts und einheitlichen Qualitätskriterien
- Aktionsplan des IT-Planungsrats
- Protokoll der 12. Sitzung des IT-Planungsrates vom 02. Oktober 2013
- Beschluss der Fachgruppe LeiKa vom 12.12.13 zur Anpassung der einheitlichen Qualitätskriterien
- Geschäftsordnung der Fachgruppe LeiKa-plus (Anlage 1)