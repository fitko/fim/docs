import React, { CSSProperties } from 'react'; // CSSProperties allows inline styling with better type checking.
import clsx from 'clsx'; // clsx helps manage conditional className names in a clean and concise manner.
const Badge = ({
  className, // Custom classes for the badge component
  style, // Custom styles for the badge component
  label, // used to display the badge text
  template, // used to determine the color of the badge based (cuurently supports 'L' = Leistung, 'D' = Datenfeld, 'P' = Prozesse)
  bgcolor, //alternative color for the badge
}) => {
  const defaultStyle = {
    padding: '0.2em 0.5em 0.3em 0.5em',
    borderRadius: '1em',
    margin: '0em 0.2em 0em 0.2em',
    };

  // const templatestyle = { color: 'blue', backgroundColor: 'red', }[template === 'L'] || { color: 'red', backgroundColor: 'blue', }[template === 'D'] || { color: 'white', backgroundColor: bgcolor ? bgcolor : '#1a658f',  }; ;

  const templateStyle = 
    template === 'L' ? { color: '#FFFFFF', backgroundColor: '#8A96B1', } : // Template L = Baustein Leistung
    template === 'D' ? { color: '#FFFFFF', backgroundColor: '#A29182', } : // Template D = Baustein Datenfelder
    template === 'P' ? { color: '#FFFFFF', backgroundColor: '#B3C36F', } : // Template P = Baustein Prozesse
    { color: '#FFFFFF', backgroundColor: bgcolor ? bgcolor : '#1a658f',  }; 
  


  const combinedStyle = { ...templateStyle,...defaultStyle, ...style };
  return (
    <span className={clsx('badge', className)} style={combinedStyle}>
     {label}
    </span>
  );
};
export default Badge;