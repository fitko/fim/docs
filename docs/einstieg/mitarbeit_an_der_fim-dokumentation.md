---
sidebar_label: Mitarbeit an der FIM-Dokumentation
---

# Mitarbeit an der FIM-Dokumentation

Diese Seite wird zukünftig die Schritte detaillieren, wie Interessierte an dieser FIM-Dokumentation mitarbeiten können. Es gibt dazu zwei Zugänge:

## Mitarbeit mit GIT und MarkDown

Git ist ein Versionsverwaltungssystem und weltweit bei Software-Entwickelnden im Einsatz. Die Nutzung ist nicht schwer, setzt aber eine Einarbeitung und etwas Praxis voraus. Zahlreiche Tutorien stehen im Netz bereit. 

An dieser Stelle soll es zukünftig einen für die Belange der Autoren der FIM-Dokumentation besonders geeigneten Leitfaden für GIT geben.

MarkDown ist ein sehr einfacher Weg, Texte mit wenigen Auszeichnungssymbolen wie "#", "*" oder "_" als Überschriften, Fett- oder Kursivdruck hervorzuheben, Bilder einzubetten oder Tabellen zu gestalten. Die Nutzung ist erfahrungsgemäß in wenigen Minuten erlernbar. 

### Mitarbeit mit Word o.a.

Autoren, die zwar die Inhalte bereit stellen können, sich aber nicht mit Git auseinandersetzen wollen, können diese Inhalte als Word-Datei o.a. an die ihnen bekannten Ansprechpersonen für die FIM-Dokumentation oder an ticket@ƒimportal.de schicken, Betreff "FIM-Dokumentation". Die Inhalte aus den Dateien werden dann von Dritten eingearbeitet werden. Voraussetzungen:

- Verortung in der Ablagestruktur im Inhaltsverzeichnis am linken Rand
- Verlinkung von und zu vorhandenen Seiten
- Tabellen sollten möglichst einfach gestaltet sein.
- Querverweise in der Word-Datei o.a. sollten so geschrieben sein, dass sie auch in einer Web-Umgebung sinnvoll sind (nicht: "siehe oben", "vgl. Kapitel 3.4")