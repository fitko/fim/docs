# Identifier

\#Modellierende, \#Techniker, \#Fortgeschrittene

\#Nachschlagen, \#Verstehen, \#Grundlagen, \#Umsetzung

:::note[zu diesem Thema]

<Tabs>
  <TabItem value="apple" label="Kurz und Knapp"> Kurzzusammenfassung in wenigen Worten </TabItem>
  <TabItem value="pear" label="Lernpfad Datenfelder Basics"> Teil 2 von 30, weiter zu Teil 3 (LINK), zurück zu Teil 1 (LINK) </TabItem>

</Tabs>

:::

## überschrift 1

Jedem Repository ist ein eindeutiger Nummernkreis zugeordnet, der nachträglich nicht mehr geändert werden kann. In diesem Repository liegen die bekannten Elemente des Bausteins in Form von Arbeitskopien und Versionen vor.

Ein Identifikator in einem Repository hat eindeutig zu sein; d. h. der gleiche Identifikator kann nicht mehrfach vergeben werden.

Ein Repository kann Inhalte verschiedener anderer Repositorys in Form von Kopien beheimaten, jedoch dürfen Fremdinhalte dort nicht mehr editiert werden. Das einzige, was innerhalb eines Repositorys bearbeitbar ist, sind Arbeitskopien von Elementen des eigenen Nummernkreises.

## überschrift 2

Jedem Repository ist ein eindeutiger Nummernkreis zugeordnet, der nachträglich nicht mehr geändert werden kann. In diesem Repository liegen die bekannten Elemente des Bausteins in Form von Arbeitskopien und Versionen vor.

Ein Identifikator in einem Repository hat eindeutig zu sein; d. h. der gleiche Identifikator kann nicht mehrfach vergeben werden.

Ein Repository kann Inhalte verschiedener anderer Repositorys in Form von Kopien beheimaten, jedoch dürfen Fremdinhalte dort nicht mehr editiert werden. Das einzige, was innerhalb eines Repositorys bearbeitbar ist, sind Arbeitskopien von Elementen des eigenen Nummernkreises.

## überschrift 3

Jedem Repository ist ein eindeutiger Nummernkreis zugeordnet, der nachträglich nicht mehr geändert werden kann. In diesem Repository liegen die bekannten Elemente des Bausteins in Form von Arbeitskopien und Versionen vor.

Ein Identifikator in einem Repository hat eindeutig zu sein; d. h. der gleiche Identifikator kann nicht mehrfach vergeben werden.

Ein Repository kann Inhalte verschiedener anderer Repositorys in Form von Kopien beheimaten, jedoch dürfen Fremdinhalte dort nicht mehr editiert werden. Das einzige, was innerhalb eines Repositorys bearbeitbar ist, sind Arbeitskopien von Elementen des eigenen Nummernkreises.

import Tabs from '@theme/Tabs';

import TabItem from '@theme/TabItem';

:::note[zu diesem Thema]

"Was könnte Sie noch interessieren?"

:::