# Grundsätzliches über Datenschemata

## Unterscheidung Stammdatenschema und Datenschema

:::info

Um eine klare Abgrenzung vorzunehmen, ist es wichtig zwischen den beiden Begriffen unterscheiden zu können. In älteren Unterlagen war oft die Rede von Stammdatenschemata, obwohl eigentlich Datenschemata gemeint waren.
**Datenschema** ist der **allgemeinere** Begriff und **umfasst sowohl Stammdatenschemata**, als **auch (OZG-) Referenzdatenschemata**, u. a. Schemata.
Von Stammdatenschemata ist nur zu sprechen, wenn es auch um diese geht, d. h. um Schemata, die ausschließlich aufgrund von Handlungsgrundlagen durch federführende Ministerien o. ä. erstellt wurden.

Daraus ergibt sich wie folgt:

-   Ein Stammdatenschema besteht aus Unterelementen, die in der Regel vollständig durch entsprechende Handlungsgrundlage vorgegeben sind. Siehe hierzu auch den Abschnitt: [*fehlende Handlungsgrundlagen*](EA_Handlungsgrundlagen#fehlende-handlungsgrundlagen).
-   Sowie man davon abweicht, handelt es sich um ein Referenzdatenschema.
-   Ein Referenzdatenschema sollte auf ein oder mehrere Stammdatenschemata aufbauen bzw. dieses referenzieren (Relationen nutzen!).
-   Daraus ergibt sich, dass auch die Unterelemente eines Referenzdatenschemas zum weitaus größten Teil handlungsgrundlagen-basiert sind.
-   Bei Unterelementen, bei denen dies nicht möglich ist, ist in der Handlungsgrundlage ein Verweis auf dessen Referenznaturell zu ergänzen.
-   Zusätzlich sollte im Beschreibungsfeld ergänzt werden, wieso an dieser Stelle im Referenzdatenschema eine zusätzlich Information erfasst wird.

:::

## Zuordnung zu Dokumentsteckbriefen

Ein Datenschema muss immer genau einem Dokumentsteckbrief zugeordnet sein. Hierzu ist ein Dokumentsteckbrief aus dem zentralen Katalog KATE zu verwenden. Entsteht noch kein Dokumentsteckbrief in KATE, so muss dieser beantragt werden.

## Verwendung von BOB

Die Elemente des zentralen Baukastens BOB, dem Baukasten optimierter Bausteine, sind bei der Modellierung eines Datenschemas zu verwenden. Genaueres über das Nutzungsszenario von BOB finden Sie in dem Kapitel [*BOB - der zentrale Baukasten*](EA_BOB).

## Zusammenfassung: Was hat sich beim Wechsel des Standards explizit für Datenschemata geändert?

:::info

- Der Dokumentsteckbrief wird nun fest beim Datenschema hinterlegt.
- Ein Datenschema benötigt mindestens ein Unterelement, um konform zum Standard XDatenfelder 3 zu sein.

:::