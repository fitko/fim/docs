# Datenschemata

## Grundlegendes über Datenschemata

Ein Datenschema beschreibt die Struktur von beispielsweise Formularen oder anderen Dokumenten. Diese Struktur besteht wiederum aus Datenfeldern, Datenfeldgruppen und Regeln.

## Metadaten

### Identifikationsangaben

|Metadatum|Definition|
|-----|-----|
|<u>**Identifizierungsangabe (ID)***</u> (1)|Eine eindeutige ID, siehe Identifikatoren.|
|<u>**Versionsangabe***</u> (1 für versionierte Datenschemata, 0 sonst)|Die Versionsnummer wird verwaltet, um die Existenz unterschiedlicher Versionen von Elementen zu ermöglichen.|
|**Dokumentsteckbrief (1)**|Der Dokumentsteckbrief, dem ein Datenschema zugeordnet ist. Diese Zuordnung muss von Seiten des Datenschemas editierbar sein.|

### Definitionsangaben

|Metadatum|Definition|
|-----|-----|
|**Name** (1)|FIM-interner Name des Datenfelds, der sichtbar ist für alle Rollen in FIM, die auf der Ebene der abstrakten Formulare tätig sind (fachliche Ersteller, Methodenexperten, Informationsmanager, FIM-Koordinierungsstelle, FIM-Nutzer).|
|*Definition* (0…1)|Fasst den Inhalt eines Datenfelds aus redaktioneller Sicht zusammen, d. h. aus Sicht der Rollen fachlicher Ersteller, Methodenexperte, Informationsmanager, FIM-Koordinierungsstelle, FIM-Nutzer.|
|*Bezug zur Handlungsgrundlage* (0…n)|Dieses Attribut ermöglicht es, Bezüge zu einschlägigen Rechtsnormen sowie zu in anderen Vorhaben wie den XÖV-Vorhaben (z. B. XWaffe) oder Vorhaben wie P23R gleich oder ähnlich definierten Datencontainer zu erstellen und diese Verbindung zu dokumentieren. Hierdurch lässt sich die Ausgestaltung eines Stammdatenschemas gemäß einschlägigen Rechtsnormen oder Standardisierungsverfahren belegen. Es besteht die Möglichkeit zu jeder Handlungsgrundlage eine URI zu hinterlegen.|
|*Beschreibung* (0…1)|Zusätzliche Beschreibung und Erläuterung eines Datenfelds für FIM-Nutzer.|
|*Stichwörter (Tags)* (0…n)|Dieses Metadatum beinhaltet eine Liste an vorgegebenen und frei wählbaren Stichwörtern, die es dem FIM-Nutzer ermöglichen, eine spezielle Semantik zu kennzeichnen oder eine Suche innerhalb eines Redaktionssystem zu erleichtern.|
|*Relation* (0…n)|Relationen dokumentieren Beziehungen der Elemente untereinander, wie z.B. "ist abgeleitet von", "ersetzt" oder "ist äquivalent zu". Eine Relation wird durch die Unterangaben Prädikat und Objekt vollständig beschrieben.|
|*Versionshinweis* (0…1)|Ein Hinweis, der bei der Versionierung eines Elements erstellt wird. Es wird beispielsweise beschrieben, was sich an einem Element geändert hat.|
|**Ableitungsmodifikation Struktur (1)**|Definiert, ob die Struktur eines (Stamm-)Datenschemas von einer nachgelagerten Verwaltungsebene bei der Erstellung eines neuen Stammdatenschemas oder konkreten Formulars verändert werden darf oder nicht. Über dieses Attribut kann spezifiziert werden, dass ein Stammdatenschema auf struktureller Ebene bundesweit einheitlich gestaltet ist. Mögliche Ausprägungen dieses Attributs sind folgende: „alles modifizierbar“, “nur erweiterbar“, „nur einschränkbar“, „nicht modifizierbar“.Der Defaultwert für die Ableitungsmodifikation Struktur ist „alles modifizierbar“, alle anderen Werte müssen durch den Nutzer aktiv gesetzt werden.|
|**Ableitungsmodifikation Repräsentation (1)**|Definiert, ob die repräsentationsbezogenen Metadaten eines Datenfeldes, einer Datenfeldgruppe oder Regel (Bezeichnung Eingabe, Bezeichnung Ausgabe, Hilfetext Eingabe, Hilfetext Ausgabe, Inhalt Eingabe, Inhalt Ausgabe, Position) von einer nachgelagerten Verwaltungsebene bei der Erstellung eines abgeleiteten Stammdatenschemas oder konkreten Formulars verändert werden dürfen oder nicht. Folglich wird dadurch festgelegt, ob die repräsentationsbezogenen Metadaten auf den nachgelagerten Ebenen auf der Relation zwischen dem abgeleiteten Stammdatenschema und dem Datenfeld/Datenfeldgruppe/Regel definiert und damit überschrieben werden können. Mögliche Ausprägungen sind folgende: „modifizierbar“, „nicht modifizierbar“. Der Defaultwert für die Ableitungsmodifikation Repräsentation ist „modifizierbar“, alle anderen Werte müssen durch den Nutzer aktiv gesetzt werden.|
|**Struktur** (1...n)|Die Unterelemente eines Datenschemas bilden dessen Struktur. Mögliche Unterelemente sind Datenfelder und Datenfeldgruppen. Jedes Unterelement eines Datenschemas hat Angaben zur **Multiplizität** (1) und zum *Bezug aus Struktur* (0...n).|
|*Regel* (0...n)|Eine Referenz auf ggf. vorhandene Regeln; diese definieren die Logik innerhalb der Unterelemente eines Datenschemas.|

### Angaben für Formular-Nutzer

|Metadatum|Definition|
|-----|-----|
|**Bezeichnung**  (1)|Bezeichnung des Feldes, welche für den Bürger/das Unternehmen auf Input-Formularen sichtbar ist.|
|*Hilfetext* (0…1)|Erläuternder Hilfetext für den Formularanwender (Bürger/Unternehmen) eines Input-Dokuments, um zu verdeutlichen, welche Inhalte in einer Datenfeldgruppe einzugeben sind bzw. welche Aktion vorzunehmen ist.|

### Zustandsangaben

|Metadatum|Definition|
|-----|-----|
|**Fachlicher Ersteller** (0…1)|Ersteller oder erstellende Institution des Elements. Der fachliche Ersteller entspricht der Freigabeinstanz für eine Silber- oder Goldfreigabe.|
|<u>**Erstelldatum**</u> :bomb:|Datum der Erstellung des Datenfelds|
|<u>**Letztes Änderungsdatum**</u> (1)|Datum der letzten Änderung des Datenfelds.|
|<u>**Letzter Bearbeiter**</u> :bomb:|Der letzte Bearbeiter des Datenfelds.|
|<u>**Status**</u> (1)|Der Status gibt Aufschluss darüber, ob und wie das Element im Rahmen eines Editors zu verwenden ist. Mögliche Ausprägungen dieses Metadatums sind durch die Codeliste urn:xoev-de:xprozess:codeliste:status gegeben. Näheres dazu finden Sie im Kapitel Status, Freigabe und Veröffentlichung. Der Defaultwert für den Status ist „in Bearbeitung“, alle anderen Status müssen durch den Nutzer aktiv gesetzt werden.|
|*Gültig ab* (0…1)|Sofern es eine Beschränkung der Gültigkeit gibt, ist der Gültigkeitszeitraum zu erfassen. Mit diesem Metadatum wird der Beginn der Gültigkeit erfasst.|
|*Gültig bis* (0…1)|Sofern es eine Beschränkung der Gültigkeit gibt, ist der Gültigkeitszeitraum zu erfassen. Mit diesem Metadatum wird das Ende der Gültigkeit erfasst.|
|<u>**Freigabedatum**</u> (0…1)|Datum der Freigabe durch die Freigabeinstanz dieses Datenfelds.|
|<u>**Veröffentlichungsdatum**</u> (0…1)|Datum der Veröffentlichung durch die Veröffentlichungsinstanz dieses Datenfelds.|

## Repräsentation im Standard XDatenfelder 3

### Header eines Datenschemas

~~~
<xdf3:xdatenfelder.stammdatenschema.0102 xmlns:xdf3="urn:xoev-de:fim:standard:xdatenfelder_3.0.0">
    <xdf3:header>
        <xdf3:nachrichtID>2639df32-5f3b-4cd0-8ac0-34f7596754c8</xdf3:nachrichtID>
        <xdf3:erstellungszeitpunkt>2024-04-16T13:10:55+02:00</xdf3:erstellungszeitpunkt>
        <xdf3:produktbezeichnung>Beispiel Redaktionssystem</xdf3:produktbezeichnung>
        <xdf3:produkthersteller>Beispiel GmbH</xdf3:produkthersteller>
        <xdf3:produktversion>1.11.0</xdf3:produktversion>
        <xdf3:datenqualitaet listURI="urn:xoev-de:fim:codeliste:datenfelder.datenqualitaet" listVersionID="1.0">
            <code>TEST</code>
        </xdf3:datenqualitaet>
    </xdf3:header>
    <xdf3:stammdatenschema>
    ...
    </xdf3:stammdatenschema>
</xdf3:xdatenfelder.stammdatenschema.0102>
~~~

### Aufbau eines Datenschemas (mit Bezug zum Dokumentsteckbrief und Struktur)

~~~
<xdf3:stammdatenschema>
    <xdf3:identifikation>
        <xdf3:id>S05000000001</xdf3:id>
    </xdf3:identifikation>
    <xdf3:name>Beispielschema</xdf3:name>
    <xdf3:definition>asdf</xdf3:definition>
    <xdf3:bezug>§2 BspG</xdf3:bezug>
    <xdf3:freigabestatus listURI="urn:xoev-de:xprozess:codeliste:status" listVersionID="2020-03-19">
        <code>2</code>
    </xdf3:freigabestatus>
    <xdf3:statusGesetztDurch>asfd</xdf3:statusGesetztDurch>
    <xdf3:letzteAenderung>2024-04-16T11:10:06+00:00</xdf3:letzteAenderung>
    <xdf3:bezeichnung>Beispiel</xdf3:bezeichnung>
    <xdf3:ableitungsmodifikationenStruktur listURI="urn:xoev-de:fim:codeliste:xdatenfelder.ableitungsmodifikationenStruktur" listVersionID="1.0">
        <code>3</code>
    </xdf3:ableitungsmodifikationenStruktur>
    <xdf3:ableitungsmodifikationenRepraesentation listURI="urn:xoev-de:fim:codeliste:xdatenfelder.ableitungsmodifikationenRepraesentation" listVersionID="1.0">
        <code>0</code>
    </xdf3:ableitungsmodifikationenRepraesentation>
    <xdf3:dokumentsteckbrief>
        <xdf3:id>D05000000003</xdf3:id>
    </xdf3:dokumentsteckbrief>
    <xdf3:struktur>
        <xdf3:anzahl>1:1</xdf3:anzahl>
        <xdf3:enthaelt>
            <xdf3:datenfeld>
                <xdf3:identifikation>
                    <xdf3:id>F05000000001</xdf3:id>
                </xdf3:identifikation>
                ...
            </xdf3:datenfeld>
        </xdf3:enthaelt>
    </xdf3:struktur>
    <xdf3:struktur>
        <xdf3:anzahl>1:1</xdf3:anzahl>
        <xdf3:enthaelt>
            <xdf3:datenfeld>
                <xdf3:identifikation>
                    <xdf3:id>F05000000003</xdf3:id>
                </xdf3:identifikation>
                ...
            </xdf3:datenfeld>
        </xdf3:enthaelt>
    </xdf3:struktur>
    ....
</xdf3:stammdatenschema>
~~~