# Einleitung

Die FIM UAG, der Redaktionszirkel und der Baustein Leistungen haben gemeinsam die Qualitätssicherungskriterien des Bausteins Leistungen überarbeitet.

Vor Ihnen liegt der Entwurf dieser Überarbeitung. Basis der Qualitätssicherungskriterien sind die auf dem FIM Portal veröffentlichten QS-Kriterien - *Download* [PDF (165.7 kB)](https://fimportal.de/securedl/sdl-eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTI0NzQ0NDEsImV4cCI6MTcxMjU2NDQ0MSwidXNlciI6MCwiZ3JvdXBzIjpbMCwtMV0sImZpbGUiOiJmaWxlYWRtaW4vQmlibGlvdGhlay9Eb2t1bWVudGUvQmF1c3RlaW5fTGVpc3R1bmdlbi9GSU0tQmF1c3RlaW5fTGVpc3R1bmdlbi1RdWFsaXRhZXRzc2ljaGVydW5nc2tyaXRlcmllbl9WMS4wMC5wZGYiLCJwYWdlIjozOX0.J85JSSJZeAimEWDhnvres-AHvHudwmGXdLMv2JdRMJw/FIM-Baustein_Leistungen-Qualitaetssicherungskriterien_V1.00.pdf)

In die Überarbeitung sind alle bis zum 31.03.2022 beim Baustein Leistungen eingegangenen Rückmeldungen.

Die abschließende Qualitätssicherung des Entwurfs ist im Redaktionszirkel des Bausteins Leistungen erfolgt.

Das vorliegende Dokument soll allen, die mit der Erstellung von Stammtexten und

Leistungsbeschreibungen betraut sind, als Arbeitsgrundlage dienen. Und Hilfestellung bei deren Erfassung leisten.  

Es gibt keine Antworten darauf, wie erstellte Leistungsbeschreibungen z. B. in den Verwaltungsportalen angezeigt werden sollten. 