---
sidebar_label: Sandkasten Überblick
---

# Sandkasten Überblick

Der Sandkasten in der FIM-Dokumentation dient der Abstimung von Inhalten und Designs vor der eigentlichen Veröffentlichung. Alle Inhalte dieses Abschnitts sind hier also ausschließlich zu Testzwecken.