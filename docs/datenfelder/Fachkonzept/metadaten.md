# Metadaten

Hier finden Sie eine Beschreibung der einzelnen Metadaten aus technischer Sicht.

## Identifikationsangaben

### Identifikationsangaben (ID)
Eine eindeutige ID, siehe Identifikatoren.

### Versionsangabe
Die Versionsnummer wird verwaltet, um die Existenz unterschiedlicher Versionen von Elementen zu ermöglichen.

## Definationsangaben

### Name

FIM-interner Name des Datenfelds, der sichtbar ist für alle Rollen in FIM, die auf der Ebene der abstrakten Formulare tätig sind (fachliche Ersteller, Methodenexperten, Informationsmanager, FIM-Koordinierungsstelle, FIM-Nutzer).

### Definition

Fasst den Inhalt eines Datenfelds aus redaktioneller Sicht zusammen, d. h. aus Sicht der Rollen fachlicher Ersteller, Methodenexperte, Informationsmanager, FIM-Koordinierungsstelle, FIM-Nutzer.

### Bezug zur Handlungsgrundlage

Dieses Attribut ermöglicht es, Bezüge zu einschlägigen Rechtsnormen sowie zu in anderen Vorhaben wie den XÖV-Vorhaben (z. B. XWaffe) oder Vorhaben wie P23R gleich oder ähnlich definierten Datencontainer zu erstellen und diese Verbindung zu dokumentieren. Hierdurch lässt sich die Ausgestaltung eines Stammdatenschemas gemäß einschlägigen Rechtsnormen oder Standardisierungsverfahren belegen. Es besteht die Möglichkeit zu jeder Handlungsgrundlage eine URI zu hinterlegen.
