# FIM-Baustein Datenfelder

## Passen die FIM-Methodik und das XÖV-Framework zusammen?
Zwischen der FIM-Methodik und dem XÖV-Framework gibt es eine Reihe Gemeinsamkeiten, aber auch einige Unterschiede im Bereich der Vorgehensweisen und der primären Ergebnisse. 

Dennoch ist die Erstellung von kompatiblen FIM-Datenschemata und XÖV-Standards gut möglich. Details zu den Unterschieden zwischen FIM und XÖV sowie Handlungsempfehlungen zu einer koordinierten Erstellung von FIM- und XÖV-Ergebnissen finden auf der Website "Dokumentation zur Interoperabilisierung von FIM und XÖV":

https://docs.fitko.de/fim-xoev/docs/
