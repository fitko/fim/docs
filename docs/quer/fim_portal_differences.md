---
sidebar_label: FIM-Portal Unterschiede
---

# FIM-Portal - Unterschiede zu anderen Portalen/Systemen

:::info

Diese Seite wird nach Fertigstellung detailliert die Unterschiede zwischen dem neuem FIM-Portal und dem alten FIM-POrtal sowie zu anderen Portalen/Systemen beschreiben.

Diese Seite ist noch unvollständig und fehlerhaft (07.10.2024)

:::


## neues FIM-Portal
| Zielgruppe                                                     | Hauptzweck                                           | Daten                                                                                              | Funktionen                                                                                                                                                                                                 |
| -------------------------------------------------------------- | ---------------------------------------------------- | -------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Mitarbeitende der Verwaltung, IT-Dienstleistende, Fachressorts | Unterstützung bei der Digitalisierung von Leistungen | FIM-Informationen und Leistungsbeschreibungen für Länder aus LeiKa, PVOG und den Landesredaktionen | Suchfunktionen, Detaildarstellungen (ggf. auch zu älteren Versionen), Visualisierungen von Datenschemata und Prozessen, Verknüpfungen zwischen den FIM-Elementen, Hilfswerkzeuge zum Konvertieren u.v.a.m. |

## bisheriges FIM-Portal
| Zielgruppe                                                     | Hauptzweck                                           | Daten                                                                                        | Funktionen                                                                                                     |
| -------------------------------------------------------------- | ---------------------------------------------------- | -------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------- |
| Mitarbeitende der Verwaltung, IT-Dienstleistende, Fachressorts | Unterstützung bei der Digitalisierung von Leistungen | FIM-Informationen und Leistungsbeschreibungen für Länder aus LeiKa und den Landesredaktionen | Suchfunktionen, Detaildarstellungen (ggf. auch zu älteren Versionen), Verknüpfungen zwischen den FIM-Elementen |

## Bundesportal

| Zielgruppe                | Hauptzweck                                                                 | Daten-Unterschiede                                                 | Funktions-Unterschiede                                                                                                                  |
| ------------------------- | -------------------------------------------------------------------------- | ------------------------------------------------------------------ | --------------------------------------------------------------------------------------------------------------------------------------- |
| Bürger:innen, Unternehmen | Informationen zu allen Leistungen aus Sicher der Bürger:innen, Unternehmen | Leistungsbeschreibungen auch für Kommunen, keine FIM-Informationen | Suche nur im Volltext, keine Visualisierungen für Datenschemata/Prozesse, keine Verknüpfungen, keine Downloads, keine Qualitätsberichte |

## Landesverwaltungsportal
wie Bundesportal

## Landesredaktions-System
| Zielgruppe                      | Hauptzweck                                                                         | Daten-Unterschiede                                                                  | Funktions-Unterschiede |
| ------------------------------- | ---------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------- | ---------------------- |
| Landesredaktion für Leistungen  | Neuanlage und Bearbeitung von FIM-Stammtexten und Leistungsbeschreibungen          | Daten aus PVOG                                                                      | Editorfunktionalität   |
| Landesredaktion für Prozesse    | Neuanlage und Bearbeitung von FIM-Stammprozessen                                   | Daten aus Landes-Prozess-Repository                                                 | Editorfunktionalität   |
| Landesredaktion für Datenfelder | Neuanlage und Bearbeitung von FIM-Stammdatenschemata und OZG-Referenzdatenschemata | Daten aus Landes-Datenfelder-Repository, ggf. ergänzt um Daten anderer Bundesländer | Editorfunktionalität   |
## Suchen und Finden
wie Bundesportal

## LeiKa
| Zielgruppe        | Hauptzweck                                     | Daten-Unterschiede                                 | Funktions-Unterschiede |
| ----------------- | ---------------------------------------------- | -------------------------------------------------- | ---------------------- |
| Baustein Leistung | Verwaltung von FIM-Informationen zu Leistungen | Nur Steckbriefe und Stammtexte für alle Leistungen | Editorfunktionalität   |
| Bundesredaktion   | Verwaltung von Leistungsbeschreibungen Typ 1   | Nur Leistungsbeschreibungen Typ 1                  | Editorfunktionalität   |

## Metaredaktion
| Zielgruppe        | Hauptzweck                                     | Daten-Unterschiede                                 | Funktions-Unterschiede |
| ----------------- | ---------------------------------------------- | -------------------------------------------------- | ---------------------- |
| Baustein Leistung | Verwaltung von FIM-Informationen zu Leistungen | Nur Steckbriefe und Stammtexte für alle Leistungen | Editorfunktionalität   |
| Bundesredaktion   | Verwaltung von Leistungsbeschreibungen Typ 1   | Nur Leistungsbeschreibungen Typ 1                  | Editorfunktionalität   |


## bisheriges FIM-Portal



## Weiterführende Informationen:
- [altes FIM-Portal](https://fimportal.de)
- [neues FIM-Portal](https://schema.fim.fitko.net)
- [Bundesportal](https://verwaltung.bund.de/portal/DE)
- [Landesverwaltungsportal, z.B. Hessen](https://verwaltungsportal.hessen.de/)
- [LeiKa Schulsystem](https://adm-leika-schul.infodienste.de/login)
- [Metaredaktion](https://metaredaktion.de/)