# Grundsätzliches über Dokumentsteckbriefe

## Allgemeines

:::info
:warning: Dokumentsteckbriefe sollten nicht wahllos erstellt werden. :warning: Es gibt den [zentralen Datenfeldkatalog KATE](#kate---der-zentrale-datenfeldkatalog), in dem global alle Dokumentsteckbriefe gepflegt werden.<p/>

### Wie ist das konkrete Vorgehen, wenn ein Dokumentsteckbrief benötigt wird?

- Überprüfen Sie zunächst, ob es bereits einen Dokumentsteckbrief, der zu Ihrem Datenschema passt, in KATE gibt. Sind Sie fündig geworden, nutzen Sie diesen.
- Falls Sie keinen Dokumentsteckbrief gefunden haben, können Sie diesen beantragen. Bei erfolgreicher Beantragung wird ein Dokumentsteckbrief in KATE für Sie erstellt.
- Sie können zwischenzeitlich einen lokalen Dummy-Dokumentsteckbrief nutzen und dazu ein Datenschema erstellen. Nachdem Ihr Dokumentsteckbrief in KATE erstellt wurde, müssen Sie Ihr Datenschema mit dem neuen Dokumentsteckbrief verknüpfen.

### Zu vermeiden sind:

- Doppelanlegung von Dokumentsteckbriefen
- Erstellung lokaler Dokumentsteckbriefe

:::

## Was für Dokumentsteckbriefe gibt?

Es gibt **abstrakte** und **nicht abstrakte** / normale Dokumentsteckbriefe.

### nicht-abstrakte Dokumentsteckbriefe

Das sind Dokumentsteckbriefe, wie man sie auch aus dem Standard XDatenfelder kennt. Diese Dokumentsteckbriefe sind **nicht abstrakt**, d. h. man kann auch von einem normalen oder konkreten Dokumentsteckbrief sprechen.
Mit ihnen können beliebig viele (oder keine) Datenschemata verknüpft sein; wie verschiedene Versionen eines Datenschemas, Datenschemata mit unterschiedlicher zeitlicher Gültigkeit oder Datenschemata aus unterschiedlichen Bundesländern.

### abstrakte Dokumentsteckbriefe :new:

Abstrakte Dokumentsteckbriefe bündeln in der Regel mehrere nicht-abstrakte Dokumentsteckbrief. 

Beispielsweise gibt es einen abstrakten Dokumentsteckbrief *Identitätsnachweis*, diesem abstrakten Dokumentsteckbrief sind mehrere nicht-abstrakte Dokumentsteckbriefe zugeordnet, wie z. B. *Reisepass*, *Personalausweis* und die *Aufenthaltskarte*. Der abstrakte Dokumentsteckbriefe kann dann in Prozessen referenziert werden, an der Stelle, an der ein Identitätsnachweis benötigt wird, der in Form eines Reisepasses, Personalausweises oder mit einer Aufenthaltskarte erbracht werden kann.

## Dokumentart :new:

Dokumentsteckbriefe bilden verschiedene Dokumentarten (z. B. Antrag, Anzeige, Bescheid, …) ab. Diese Information findet sich im Metadatum **Dokumentart** wieder. Dazu passend ist wird die Codeliste auf dem XRepository [Dokumentart - Art des Dokuments](https://www.xrepository.de/details/urn:xoev-de:fim-datenfelder:codeliste:dokumentart) bereitgestellt. Die Dokumentart muss sowohl für normale, als auch für abstrakte Dokumentsteckbriefe gewählt werden.
In der nachfolgenden Tabelle ist die entsprechende Codeliste abgebildet; die letzte Spalte *Hilfestellung / Erläuterung* soll dabei helfen die korrekte Dokumentart zu identifizieren.

|Code|Dokumentart|Hilfestellung / Erläuterung|
|----|-----------|-------------|
|001|Antrag|Antrag um eine Verwaltungsleistung in Anspruch zu nehmen oder anzustoßen.|
|002|Anzeige|Eine Anzeige muss gemäß einer fachrechtlich bestimmten Anzeigepflicht gegenüber der Verwaltung abgegeben werden, um bestimmte Tatsachen aus dem Bereich der anzeigepflichtigen Person zur Kenntnis zu bringen.|
|003|Bericht|Hierbei handelt es sich zum Beispiel um Prüfberichte von Sachverständigen, die das Treffen von verfahrensrelevanten Entscheidungen ermöglichen sollen.|
|004|Bescheid|Ergebnis im Rahmen des Verwaltungshandelns, wird z.B. nach Prüfung eines Antrags in Zustimmung oder Ablehnung seitens der Verwaltung gegenüber Antragstellenden Personen erteilt und kann auch den Gebührenbescheid enthalten.|
|005|Erklärung|Form eines Nachweises in dem Antragsstellende eine Erklärung abgeben müssen, z.B. dass sie keine Kenntnis über bestimmte Sachverhalte haben.|
|006|Kartenmaterial|Geodaten oder Kartenbezogene Spezialformate|
|007|Mitteilung|Information in Form einer Nachricht/Benachrichtigung|
|008|Multimedia|Bild, Video, Ton-Dokumente|
|009|Registeranfrage|Enthält Daten, die eine Registerabfrage anstoßen|
|010|Registerantwort|Enthält Daten, die aus einer Registerabfrage als Antwort seitens Register zurückfließen|
|011|Urkunde|Betrifft Zeugnisse, Befähigungsnachweise, Führerschein|
|012|Vertrag|Kaufvertrag, Mietvertrag, ect.|
|013|Vollmacht|Eine Vollmacht soll Bevollmächtitge berechtigen im Vertretungsfall tätig zu werden. Die Vollmacht enthält Angaben zum Zweck der Vollmacht, Vollmachtgeber:in, Daten zur bevollmächtigten Person|
|014|Willenserklärung|Eine Willenserklärung ist eine Willensäußerung im Rechtsverkehr, die auf die Herbeiführung einer privatrechtlichen Rechtsfolge gerichtet ist. Sie beinhaltet die Äußerung dieses Willens.|
|999|unbestimmt|Platzhalter, wenn keine Dokumentart zugewiesen werden kann|



## KATE - der zentrale Datenfeldkatalog :new:

Im Bausteinrepository KATE finden sich künftig alle existierenden Dokumentsteckbriefe des FIM-Bausteins Datenfelder wieder. Einen Dokumentsteckbrief aus KATE erkennt man an dem Nummernkreis 99.