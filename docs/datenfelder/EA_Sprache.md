# Sprachliche Qualitätskriterien

## Verwendung von Sprache

:::info

Die Wichtigkeit der sprachlichen Qualitätskriterien ist ausgesprochen hoch. Stellen Sie sicher, dass Sie die folgenden Punkte verinnerlicht haben und setzen Sie diese bei der Modellierung um. Die Qualität eines Datenschemas spiegelt sich nicht nur in der Modellierung, sondern auch in der korrekten Verwendung von Sprache wieder.

Prüfen Sie Ihr Datenschema stets auf **korrekte Rechtschreibung, Zeichensetzung und Grammatik**.

:::

Durch einheitliche sprachliche Kriterien lässt sich ein hohes Maß an Kohärenz erreichen.

Es folgen einige Punkte, in denen sprachliche Qualitätskriterien gelistet werden. Wo möglich sind Beispiele hinterlegt. Dadurch soll sichergestellt werden, dass Sprache so verwendet wird, dass jeder versteht, was über sie vermittelt werden soll.

Es sollte dabei immer im Auge behalten werden, an welche Zielgruppe sich Texte wenden, d. h. es ist wichtig zwischen dem Systemnutzer (Methodenexperten) und dem Endnutzer (Bürger) zu unterscheiden. Für den Endnutzer relevant sind die Angaben zur Bezeichnung, Hilfetexte, das Metadatum Inhalt bei statischen Textfeldern, sowie Codelisteneinträge. Die folgenden Unterkapitel zur Sprache sind für diese Anwendungsfälle unbedingt zu beachten.

Angaben, die den Systemnutzer betreffen wie Name, Definition und Beschreibung sind sprachlich weniger kritisch, was nicht heißt, dass für diese nicht zumindest Rechtschreibung, Zeichensetzung und Grammatik wichtig sind.

## Sprachstil

<ul>

<li> Verständliche Sprache

:x: Statt: „Für alle Menschen besteht grundsätzlich ein Anspruch auf Sozialhilfe“.

:heavy_check_mark: Besser: „Sie können Sozialhilfe erhalten, wenn Sie […]“ 

</li>

<li> Kurze, klare Sätze

:x: Statt: „Die Gültigkeitsdauer des Internationalen Führerscheins, die 3 Jahre beträgt, kann nicht verlängert werden.“

:heavy_check_mark: Besser: „Der Internationale Führerschein ist 3 Jahre gültig. Die Gültigkeit kann nicht verlängert werden.“

</li>

<li> Füllwörter vermeiden

:x: Statt: „getroffene Vereinbarung“ und „überprüfen“.

:heavy_check_mark: Besser: „Vereinbarung“ und „prüfen“

:x: Statt „geben Sie hier an…“

:heavy_check_mark: Besser „geben Sie an…“

</li>

<li> Aktive Verben verwenden

:x: Statt: „Der Zuschuss wird ab dem Monat, in dem der Antrag bei der Behörde eingegangen ist, gezahlt.“

:heavy_check_mark: Besser: „Sie erhalten den Zuschuss ab dem Monat, in dem Ihr Antrag eingegangen ist.“

</li>

<li> Substantivierungen vermeiden

:x: Statt: „Änderung durchführen“ und „Zuständigkeit“.

:heavy_check_mark: Besser: „ändern“ und „zuständig“

</li>

<li> Faktenorientierter, sachlicher Schreibstil ohne Werbung

:x: Statt: „Das Regierungsprogramm der Regierungskoalition sieht vor, dass Eltern, die ihre Kinder zuhause betreuen, einen Zuschuss erhalten können.“

:heavy_check_mark: Besser: „Wenn Sie Ihr Kind zuhause betreuen, können Sie einen Zuschuss erhalten.“

</li>

<li> Begriffe wie „müssen“ möglichst vermeiden

:x: Statt: „Sie müssen den Antrag persönlich abgeben“.

:heavy_check_mark: Besser: „Geben Sie den Antrag dann persönlich ab“

</li>

<li> „Bitte" in Zusammenhang mit der Ansprache des Bürgers vermeiden

:x: Statt: „Bitte geben Sie ein…“

:heavy_check_mark: Besser: „Geben Sie ein…“

</li>

</ul>

:::info

## Kohärenz des verwendeten Sprachstils

Bemühen Sie sich um einen kohärenten Sprachstil innerhalb eines Datenschemas. Verwenden Sie beispielsweise immer die gleiche Form der Bürgeransprache, die gleichen Begriffe, etc.

Kleinere Brüche im Stil aufgrund der Nachnutzung von Baukastenelementen sind jedoch in Ordnung.

:::

## Singular-/Pluralformen

<ul>

<li> Die Singularform bei Datenfeldern und Datenfeldgruppen ist grundsätzlich zu verwenden.

:x: Statt: „Anschriften“

:heavy_check_mark: Besser: „Anschrift“

</li>

<li> Falls durch die Multiplizität einer Datenfeldgruppe vorgegeben, ist die Pluralform zu verwenden.

:heavy_check_mark: Beispiel: Weitere Anschriften

</li>

<li> Falls die Gepflogenheiten bzw. die gängige Praxis vorhandener Formulare die Pluralform vorsieht ist diese zu verwenden.

:x: Statt: „Angabe“ und „Information“

:heavy_check_mark: Besser „Angaben“ und „Informationen“

</li>

<li> Eine Vermischung von Singularform und Pluralform ist grundsätzlich zu vermeiden.

:x: Statt: „Vornamen und Nachname“ bzw. „Vorname und Nachnamen“

:heavy_check_mark: Besser „Vorname und Nachname“ bzw. „Vornamen und Nachnamen“

</li>

<li> Falls die Gepflogenheiten bzw. die gängige Praxis vorhandener Formulare eine Vermischung von Singularform und Pluralform vorsieht ist diese zu verwenden.

:heavy_check_mark: Beispiel: „Angaben zur Person“

</li>

</ul>

## Fachbegriffe, Fremdwörter und Abkürzungen

<ul>

<li> Fremdwörter möglichst vermeiden

:x: Statt: „essentiell“ und „transferieren“

:heavy_check_mark: Besser: „notwendig“ und „übertragen“

</li>

<li> Fachbegriffe und Fremdwörter, falls sie verwendet werden, bei erstmaliger Verwendung erläutern

:heavy_check_mark: Beispiel: „Der Regelsatz ist ein fester Betrag für …“

</li>

<li> Allgemein eingeführte Fremdwörter, Abkürzungen und Akronyme können ohne Erläuterung verwendet werden (Referenzwörterbuch: Duden)

:heavy_check_mark: Beispiel: „Kfz“

</li>

<li> Andere Abkürzungen und Akronyme bei der Erstnennung ausschreiben, die Abkürzung als Klammerzusatz anfügen und im weiteren Textabschnitt ohne nochmalige Erläuterung verwenden.

:heavy_check_mark: Beispiel: „Europäische Union (EU)“

</li>

<li> Abkürzungen mit Punkt im Fließtext weitgehend vermeiden (in Übersichten und Aufzählungen möglich).

:x: Statt „i. d. R.“ und „d. h.“

:heavy_check_mark: Besser: „in der Regel“ und „das heißt“

</li>

<li> Festabstand zwischen mehrteiligen Abkürzungen durch geschütztes Leerzeichen

:heavy_check_mark: Beispiel: „z. B.“ statt „z.B.“

</li>

<li> Formulierungen und Begriffe der Verwaltungs- und Gesetzessprache verständlich wiedergeben.

Die fachsprachliche Form wird nur verwendet, wenn die abweichende Schreibung rechtlich relevant oder unabdingbar ist, da sonst Verwechselungsgefahr bestünde. Dies ist z. B. bei der Bezeichnung von Antragsverfahren, Gesetzen und Formularen der Fall.

:heavy_check_mark: Beispiel: „Antrag auf Lohnsteuerermäßigung“

</li>

</ul>

## Schreibweisen

<ul>

<li> Bei mehreren erlaubten Schreibweisen ist die Vorzugsvariante des Dudens zu verwenden

:x: Statt „Requien“

:heavy_check_mark: Besser: „Requiem“

</li>

<li> Zahlen von eins bis zwölf im Fließtext ausschreiben, in Aufzählungen und Übersichten als Ziffer ausschreiben.

</li>

</ul>

## Fließtexte

<ul>

<li> Überschaubare Sätze und Textabschnitte, nach Bedarf strukturieren (Absätze, Zwischenüberschriften) </li>

<li> Keine Verschachtelungen (Aufzählungen statt Häufung von Haupt- und Nebensätzen) </li>

<li> Auf Links im Fließtext verzichten </li>

:x: Statt: „In diesem Merkblatt der XYZ-Behörde können weitere Informationen gefunden werden“

:heavy_check_mark: Besser: „Weitere Informationen finden sie hier: Merkblatt der XYZ-Behörde“

</ul>

## Gendern
Anbei ein paar einfache Regeln, die in den Angaben zu **Bezeichnung, Hilfetexten, Codelisteneinträgen** und dem Metadatum **Inhalt** bei Datenfeldern des Datentyps Text und der Feldart Statisch Verwendung finden sollten.

Bei der Angabe des FIM internen Namens sollten diese Regeln vorerst ignoriert werden, damit der Name möglichst kurz und prägnant bleibt. 

In erster Instanz ist die neutrale Sprache zu verwenden, hier genügt es in vielen Fällen auf die richtige Endung zu achten. Es gibt aber auch Fälle, in denen einzelne Begriffe direkt ausgetauscht werden sollten.

<ul>

<li>

**-person**

:x: Statt: „ein anderer“

:heavy_check_mark: Besser: „eine andere Person“

:x: Statt: „Vertrauensmann“

:heavy_check_mark: Besser: „Vertrauensperson“

</li>

<li>

**-mitglied**

:x: Statt: „Ratsherr“

:heavy_check_mark: Besser: „Ratsmitglied“

</li>

<li>

**-kraft**

:x: Statt: „Mitarbeiter in Teilzeit“

:heavy_check_mark: Besser: „Teilzeitkraft“

:x: Statt: „Lehrer“

:heavy_check_mark: Besser: „Lehrkraft“

</li>

<li>

**-ling**

:heavy_check_mark: Beispiel: Prüfling, Flüchtling, …

</li>

<li>

**-ung**

:heavy_check_mark: Beispiel: Vertretung, Referatsleitung, …

</li>

<li>

**-leute**

:x: Statt: „Ausbildung zum/zur Einzelhandelskaufmann/-frau“

:heavy_check_mark: Besser: „Ausbildung von Einzelhandelskaufleuten“

</li>

<li> 

**-schaft**

:x: Statt: „Beamtinnen und Beamten“

:heavy_check_mark: Besser: „Beamtenschaft“

:x: Statt: „Mitarbeiterinnen und Mitarbeiter“

:heavy_check_mark: Besser „Beschäftigte“

</li>

</ul>

Falls die Verwendung neutraler Sprache nicht möglich ist, sollte auf die Paar-Form zurückgegriffen werden:

<ul>

<li> Bürgerinnen und Bürger </li>

<li> Erzieherin und Erzieher </li>

<li> Hebamme und Entbindungspfleger </li>

<li> … </li>

</ul>

Falls weder die Verwendung von neutraler Sprache noch die der Paar Form möglich ist, wird auf das generische Maskulinum zurückgegriffen. Dies ist z. B. der Fall bei der juristischen Person, wenn sich Mieter und Vermieter gleichermaßen auf natürliche wie juristische Personen beziehen können.

:warning: Verwenden Sie in Stammdatenschemata kein **Binnen-I**, keinen **Genderdoppelpunkt** und auch kein **Gendersternchen**.
Wenn die oben genannten Beispiele nicht ausreichen sollten, können Sie auf die **Verlaufsform** zurückgreifen.

:x: Statt: "LeserInnen", "Leser*innen" oder "Leser:innen"

:heavy_check_mark: Besser: "Lesende"