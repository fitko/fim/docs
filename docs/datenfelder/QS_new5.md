# Meldungen im Kontext der Modellierung

## Kombinationen aus Feldart und Datentyp

### Welche Fehlermeldungen betrifft das?

- **1011**: Bei Datenfeldern mit der Feldart 'Auswahl' muss der Datentyp 'Text' sein.
- **1017**: Ist eine Code- oder Werteliste zugeordnet, muss die Feldart 'Auswahl' sein.


### Allgemeine Informationen

Beachten Sie hierbei stets die Informationen aus der [Tabelle der erlaubten Kombinationen aus Feldart und Datentyp](DF_Allgemeines.md#was-sind-die-metadaten-feldart-und-datentyp-).
Kombinationen, die in der Tabelle kein grünes Häkchen haben, sind keine validen Kombinationen.
Datenfelder, bei denen eine Code- oder Werteliste zugeordnet werden soll, muss von der Feldart Auswahl und vom Datentyp Text sein.

### Wie löse ich den Fehler auf?

- Klären Sie, ob an diesem Datenfeld eine Code- oder Werteliste hinterlegt werden soll.
- Falls ja, wählen Sie die Feldart Auswahl und den Datentyp Text aus.
- Falls nein, entfernen Sie (falls schon hinterlegt) die, dem Datenfeld zugeordnete Codeliste bzw. die, am Datenfeld direkt modellierte Werteliste.

## Leere Strukturen

### Welche Fehlermeldungen betrifft das?

- **1023**: Datenfeldgruppe hat keine Unterelemente.
- **1078**: Ein Datenschema muss mindestens ein Unterelement enthalten.

### Allgemeine Informationen

Im Standard XDatenfelder 3 ist es nicht möglich ein standardkonformes Datenschema zu erzeugen, welches eine leere Datenfeldgruppe enthält oder gar selbst vollständig leer ist.

### Wie löse ich den Fehler auf?

- Ergänzen Sie Unterelemente an Ihrem Datenschema oder Ihrer Datenfeldgruppe.

## Fehlerhafte Strukturen

### Welche Fehlermeldungen betrifft das?

- **1026**: Zirkelbezug

### Allgemeine Informationen

Zirkelbezüge oder Kreisreferenzen treten dann auf, wenn eine Gruppe als Unterelement von sich selbst genutzt wird; d. h. wenn eine Gruppe A ein Unterelement der Gruppe A ist, welches wiederum ein Unterelement der Gruppe A ist, usw.

### Wie löse ich den Fehler auf?

- Prüfen Sie genau, ob die Datenfeldgruppe, die sie verwenden wollen, sich selbst als Unterelement nutzt. Falls ja, entfernen Sie diese.

## Strukturelementart

### Welche Fehlermeldungen betrifft das?

- **1013**: Innerhalb von Datenschemata und Datenfeldgruppen dürfen nur Elemente mit der Strukturelementart harmonisiert oder rechtsnormgebunden verwendet werden.
- **1014**: Datenfelder dürfen nicht die Strukturelementart 'abstrakt' haben.

### Allgemeine Informationen

Die Strukturelementart abstrakt ist für Datenfelder nicht vorgesehen. Abstrakte Datenfeldgruppe sind in der Regel eine Ansammlung von Unterelementen, die zu einem bestimmten Thema existieren. Sie sind nur als Referenz gedacht, nicht zur direkten Modellierung.

### Wie löse ich den Fehler auf?

Falls Sie ein abstraktes Datenfeld haben:
- Ändern Sie die Strukturelement ab; mögliche Optionen sind rechtsnormgebunden oder harmonisiert.

Falls Sie eine abstrakte Datenfeldgruppe nachnutzen wollen:
- Wenn Sie ein gewöhnliches Datenschema modellieren wollen, ändern Sie die Strukturelement ab.
- Falls die abstrakte Datenfeldgruppe nur als Version vorliegt; kopieren Sie diese und nehmen Sie entsprechende Anpassungen an der Struktur, der Handlungsgrundlage und der Strukturelementart vor.
- Falls Sie selbst eine abstrakte Datenfeldgruppe modellieren wollen, oder ein Schema modellieren, welches allein als Hilfsmittel dient, um wichtige Baukastenelemente gebündelt darzustellen, dann (nur dann) können Sie diese Meldung ignorieren.

## Verwendung von Versionen

### Welche Fehlermeldungen betrifft das?

- **1013**: Innerhalb von Datenschemata und Datenfeldgruppen dürfen nur Elemente mit der Strukturelementart harmonisiert oder rechtsnormgebunden verwendet werden.
- **1014**: Datenfelder dürfen nicht die Strukturelementart 'abstrakt' haben.


### Allgemeine Informationen

Abstrakte Datenfeldgruppen dienen als Referenz, z. B. um darzustellen, welches die maximale Ausprägung der Gruppe *natürliche Person* ist. Da dies nur eine Referenz ist, soll diese Datenfeldgruppe auch nicht direkt verwendet werden.
Mit einem Datenfelder kann man solche ein maximale Ausprägung nicht modellieren, deswegen findet die Strukturelementart 'abstrakt' in dem Zusammenhang auch keine Verwendung.

### Wie löse ich den Fehler auf?

- Ersetzen Sie die abstrakte Datenfeldgruppe in Ihrem Datenschema durch eine harmonisierte oder rechtsnormgebundene Gruppe.
- Falls Sie dabei Probleme haben die richtige Datenfeldgruppe zu finden:
    - Wie lautet der Name Ihrer abstrakten Gruppe?
    - Gibt es vielleicht eine Gruppe mit dem gleichen Namen, aber dem Zusatz 'wenig' im Namen?
    - Falls Sie nichts passendes finden, können Sie auch eine Kopie der abstrakten Gruppe machen. Vergessen Sie dabei nicht, nicht benötigte Unterelemente zu streichen.
- Falls Sie ein abstraktes Datenfeld verwendet haben: setzen Sie die Strukturelementart auf 'rechtsnormgebunden' oder 'harmonisiert'.

## abstrakte und konkrete Dokumentsteckbriefe

### Welche Fehlermeldungen betrifft das?

- **1053**: Zu einem konkreten Dokumentsteckbrief dürfen keine Dokumentsteckbriefe zugeordnet werden.
- **1054**: Zu einem abstrakten Dokumentsteckbrief müssen mindestens zwei konkrete Dokumentsteckbriefe zugeordnet werden.

### Allgemeine Informationen

Konkrete Dokumentsteckbriefen sind Datenschemata zugeordnet, abstrakten Dokumentsteckbriefen sind zwei oder mehrere konkrete Dokumentsteckbriefe zugeordnet.

### Wie löse ich den Fehler auf?

zu **1053**:
- Prüfen Sie, ob es nicht doch ein abstrakter Dokumentsteckbrief sein sollte, und Sie sich bei der Erstellung vertan haben.
- Falls ja, ändern Sie Ihren Dokumentsteckbrief auf 'abstrakt'.
- Falls nein, entfernen Sie alle Dokumentsteckbriefe, die Ihrem konkreten Dokumentsteckbrief zugeordnet sind.

zu **1054**:
- Prüfen Sie, ob an dieser Stelle ein abstrakter Dokumentsteckbrief intendiert ist.
- Falls nein, ändern Sie Ihren Dokumentsteckbrief auf 'konkret'.
- Falls ja, ordnen Sie Ihrem abstrakten Dokumentsteckbrief mindestens einen weiteren konkreten Dokumentsteckbrief zu.

