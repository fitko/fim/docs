# Release-Notes

## Aktuelles

### 2023

Erstellung der Webversion der QS-Kriterien für XDatenfelder 3.

## Historisches

### 07/2021

Veröffentlichung der überarbeiteten Version der QS-Kriterien für XDatenfelder 2.

### 2017

Erstellung der ersten QS-Kriterien für den FIM-Baustein Datenfelder.
