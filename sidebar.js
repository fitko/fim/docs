// get sections from individual files
const bsl_structure_old = require('./sidebar_sections/bs_l_structure_old');
// const bsl_structure = require('./sidebar_sections/bs_l_structure');
const df_structure = require('./sidebar_sections/bs_df_structure');
const df_structure_old = require('./sidebar_sections/bs_df_structure_old');
const pz_structure = require('./sidebar_sections/bs_pz_structure');
const bred_structure = require('./sidebar_sections/bred_structure');

module.exports = {
    defaultSidebar: [
        'einstieg/intro',
		'einstieg/schnelleinstieg',
		{
        	'type': 'category',
        	'label': 'Über FIM',
        	'items': [
				'einstieg/fim_einleitung',
				'einstieg/fim_einsatzgebiete',	
			],
		},

		,
		
		// include Abschnitt BS Leistungen
		bsl_structure_old,
		// include Abschnitt BS Datenfelder (new and old)
		df_structure,
		df_structure_old,
		// include Abschnitt BS Prozesse
		pz_structure,
		// include Abschnitt Bundesredaktion
		bred_structure,

			
        {
        	'type': 'category',
        	'label': 'Querschnittliches',
        	'items': 
			[
				{'type': 'category', 'label': 'Leistungszuschnitt', 'items': 
				[
				'quer/leistungszuschnitt/lz_einleitung',
				'quer/leistungszuschnitt/abgaben',
				'quer/leistungszuschnitt/anerkennung',
				'quer/leistungszuschnitt/anzeige',
				'quer/leistungszuschnitt/verbot',
				'quer/leistungszuschnitt/befreiung',
				'quer/leistungszuschnitt/bestellung',
				'quer/leistungszuschnitt/plan',
				'quer/leistungszuschnitt/foerderung',
				'quer/leistungszuschnitt/register',
				'quer/leistungszuschnitt/statistik',
				'quer/leistungszuschnitt/wissenserklaerung',
				]},
				'quer/fim_portal_overview',
				'quer/fim_portal_sources',
				'quer/fim_portal_differences',
			],
		},
        'glossar',
        {
        	'type': 'category',
        	'label': 'FAQ',
        	'items': [
                'faq/allgemeine_fragen_zu_fim',
                'faq/bausteinuebergreifende_fragen',
                'faq/fim_baustein_leistungen',
                'faq/fim_baustein_datenfelder',
                'faq/fim_baustein_prozesse',
                'faq/bundesredaktion'
        	]
        },
		'einstieg/docs_einleitung', 
    ]
}
