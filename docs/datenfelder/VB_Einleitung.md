# Einleitung

Hier finden Sie die Qualitätssicherungskriterien des FIM-Bausteins Datenfelder, die komplett auf den neuen Standard XDatenfelder 3 basieren. Die Informationen aus dem Vorgängerdokument sind so angepasst worden, dass Sie den Anforderungen an den neuen Standard genügen. Weiterhin sind allgemeine Informationsseiten zu den jeweiligen Elementen des Bausteins ergänzt worden, sowie Hilfestellungen in Form von Tipps und Tricks.
Vereinzelt kann es sein, dass sich Abschnitte noch in der Überarbeitung befinden.

## Feedback

Wenn Sie uns Ihr Feedback mitteilen wollen, erreichen Sie uns unter [ticket@fimportal.de](mailto:ticket@fimportal.de).
Gerne können Sie das betreffende Unterkapitel auch in ein Word Dokument kopieren und dort Ihr Feedback einfügen.
Vergessen Sie nicht zu Ihrem Feedback die Versionsnummer des Dokuments zu benennen, dies erleichtert es uns Ihr Feedback einzupflegen.<br/>
Falls Sie an dieser Seite direkt mitarbeiten wollen, melden Sie sich gerne bei uns.

## Version

:::note

Dies ist die Version 0.9.9 des Dokuments vom 31.08.2024

:::

![Logo](./images/FIM_logo.png)