const pz_structure = [
	{
		type: 'category', 
		link: {
			type: 'generated-index',
			description: 'Hier finden Sie einen Überblick der behandelten Themen. Für einen schnellen Einstieg empfehlen wir die Einleitung zu lesen.'
		},
		label: 'Bundesredaktion', items: [
			'zentrale-bundesredaktion/einleitung',
			  {type: 'category', label: 'Aufträge & Bedarfsmeldungen', items: 
				[
				'zentrale-bundesredaktion/bedarfsmeldungen/allgemeines',
				'zentrale-bundesredaktion/bedarfsmeldungen/redaktionsfenster',
				
				]},
			  {type: 'category', label: 'Leistungszuschnitt', items: 
				['zentrale-bundesredaktion/stream_lz/Einleitung',
					{type: 'category', label: 'Redaktionsprozess', items:
						[
						'zentrale-bundesredaktion/stream_lz/redaktionsprozess/Einreichung',
						'zentrale-bundesredaktion/stream_lz/redaktionsprozess/Vorpruefung',
						'zentrale-bundesredaktion/stream_lz/redaktionsprozess/Abstimmung',
						'zentrale-bundesredaktion/stream_lz/redaktionsprozess/Veroeffentlichung',
						]},
					'zentrale-bundesredaktion/stream_lz/Hilfestellungen',

				]},
			
		] 
	  },
]

    // export the structure to enable use in other files
module.exports = pz_structure;