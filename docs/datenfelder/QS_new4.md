# Meldungen im Zusammenhang mit Regeln

## Referenzieren von Baukastenelementen

### Welche Fehlermeldungen betrifft das?

- **1019**: Baukastenelemente, die in Regeln referenziert werden, dürfen keine Versionsangaben beinhalten.
- **1041**: Baukastenelemente, die in Regeln referenziert werden, verwenden noch die alte, verkürzte Element-ID.

### Allgemeine Informationen

Regeln werden in einem Freitextfeld sprachlich beschrieben. In diesem werden andere, relevante Baukastenelemente mittels ID benannt. Dadurch dass diese IDs keine Versionsangabe enthalten, verhindert man, dass Regeln editiert werden müssen, sobald ein Element neu versioniert wird.

### Wie löse ich den Fehler auf?

- Analysieren Sie genau die IDs der, in Regeln referenzierten, Baukastenelemente.
- Die IDs dürfen nicht mehr im XDatenfelder 2 Format sein, dieses hat drei nummerische Stellen weniger als eine XDatenfelder 3 konforme ID.
- Falls Ihre ID zu kurz ist, fügen Sie nach dem Nummernkreis drei Nullen ein.
- Falls Ihre ID eine Versionskennung enthält, entfernen Sie diese.

### Beispiele
- Aus F11 000 123 wird F11 000 000 123.
- Aus G08 000 123 456 V1.0.0 wird G08 000 123 456.

## Position der Regel

### Welche Fehlermeldungen betrifft das?

- **1001**: Eine Regel kann nicht auf ein referenziertes Baukastenelement zugreifen.

### Allgemeine Informationen

Der mit Abstand häufigste Fehler, der im Zusammenhang mit Regeln gemacht wird.

### Wie löse ich den Fehler auf?

:warning: Überprüfen Sie zunächst alle IDs innerhalb der Regel auf Zahlendreher etc., erst wenn diese ausgeschlossen sind, sollten Sie mit der näheren Analyse des Fehlers starten.

- An welchem Element wird die Meldung angezeigt? Schauen Sie sich diese Regel noch einmal genau an.
- Versuchen Sie die Regel im Kontext des Schemas zu analysieren.
    - An welcher Stelle ist die Regel momentan verortet?
    - Was soll durch diese Regel erreicht werden? Was ist das *Ziel* der Regel?
    - Welche Informationen aus anderen Elementen werden benötigt, um diese Regel auszulösen? Welches ist der *Auslöser* der Regel?
- Eine Regel muss so verortet werden, dass Sie Zugriff auf ihr Ziel **und** ihren Auslöser hat.
- Nachdem Sie Ziel und Auslöser der Regel identifiziert haben, können Sie jetzt prüfen, ob Ihre Regel auf beide zugreifen kann.
- Sie können sich z. B. eine gedachte rote Linie in der Struktur Ihres Datenschemas vorstellen, die jeweils vom Ziel und vom Auslöser startet. Beide Linien gehen jeweils in der Struktur nach oben. An dem Element, an dem sich beide Linien treffen, befindet sich das Element, an dem diese Regel verortet werden muss.
- Erstellen Sie an diesem Element Ihre Regel neu.
- Entfernen Sie die alte Regel.

### Beispiel

#### Ausgangssituation

![Frame 0](./images/Regel_Beispiel0.jpg)

Sie haben ein Datenschema mit der ID S00 000 000 123 modelliert, laut QS-Bericht liegt hier der Fehler 1001 vor. Dieser Fehler betrifft die Regel betrifft, die an Feld F00 000 000 120 "Auswahl Führerscheinklasse" hängt. Die Fehlermeldung besagt, dass die Regel nicht auf ein referenziertes Baukastenelement zugreifen kann; d. h. sie hängt wahrscheinlich am falschen Element.

#### Schritt 1

![Frame 1](./images/Regel_Beispiel1.jpg)

Ein Blick in die Regel selbst ergibt, dass das Feld F00 000 000 120 "Auswahl Führerscheinklasse" nur ausgefüllt werden darf, wenn in einem anderen Feld, dem Feld F00 000 000 402 "Besitz einer Fahrerlaubnis", "ja" ausgewählt wurde.
In der Praxis heißt das für die antragstellende Person, dass Sie Ihre Führerscheinklasse erst dann verpflichtend angeben muss, so wie sie angibt, dass sie eine Fahrerlaubnis besitzt.

#### Schritt 2

![Frame 2](./images/Regel_Beispiel2.jpg)

Die beiden Felder F00 000 000 402 "Besitz einer Fahrerlaubnis" und F00 000 000 120 "Auswahl Führerscheinklasse" sind wichtig für die Logik dieser Regel. Das Feld F00 000 000 402 "Besitz einer Fahrerlaubnis" ist in unserem Fall der Auslöser der Regel. Der Auslöser klärt die Grundvoraussetzungen für unsere Logik. Ist ein Führerschein vorhanden, falls ja, hat das Auswirkungen auf den  Antragstellenden, dieser muss die Führerscheinklassen angeben. Ist kein Führerschein vorhanden, muss er dies natürlich nicht. Das Feld F00 000 000 120 "Auswahl Führerscheinklasse" ist also das Ziel der Regel.

#### Schritt 3

![Frame 3](./images/Regel_Beispiel3.jpg)

Die roten Linien, ausgehend von Ziel und Auslöser der Regel, führen zu dem Element, an dem die Regel eigentlich hängen sollte. In unserem Fall ist sie direkt auf Ebene des Datenschemas verortet.

#### Schritt 4

![Frame 4](./images/Regel_Beispiel4.jpg)

Im letzten Schritt wird eine neue Regel erstellt, auf Höhe des Datenschemas. Bei der Formulierung der Freitextregel muss auf gründliche Referenzierung der betroffenen Elemente, d. h. Ziel und Auslöser, geachtet werden. Die alte Regel an der falschen Position kann danach entfernt werden. Der QS-Bericht sollte jetzt keinen Fehler 1001 mehr anzeigen.