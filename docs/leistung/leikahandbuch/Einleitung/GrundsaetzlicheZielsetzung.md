# Grundsätzliche Zielsetzung 

Mit dem Leistungskatalog (LeiKa) wird in Deutschland erstmalig ein einheitliches und umfassendes Verzeichnis der Verwaltungsleistungen des Bundes, der Länder und Kommunen aufgebaut. Ziel ist es, eine zentrale Informationsbasis in Form eines Stammtext-Managements zur Verfügung zu stellen, die von allen Verwaltungsbereichen anwendungs- und vorhabenübergreifend für alle Informations- und Kommunikationskanäle genutzt wird. 

Der LeiKa orientiert sich dabei vorrangig an den Bedürfnissen der Bürgerinnen und Bürger sowie Unternehmen. Diese Adressaten unterscheiden nicht nach sachlicher und örtlicher Zuständigkeit, sondern erwarten, dass Sie auf direktem und einfachem Wege Hilfestellungen für ihr Anliegen bekommen.

Durch die einheitliche Zuordnung von verbindlichen Schlüsseln (LeiKa-Schlüssel) zu Verwaltungsleistungen werden Informationsangebote der Verwaltungen in Deutschland vergleichbar. Die Schlüssel können staatenübergreifend und in allen Sprachen zur Identifizierung von Verwaltungsleistungen verwendet werden.

Mit LeiKa-plus werden im LeiKa enthaltenen Leistungen sukzessive um bundesweit einheitliche Informationen erweitert. Die so genannten Stammtexte werden in einer einheitlichen Struktur sowie einheitlichen Qualitätskriterien bereitgestellt und können von Ländern und Kommunen innerhalb dieser Struktur ergänzt werden. LeiKa bildet die Säule Leistungen im Föderativen Informationsmanagement (FIM).

Villa Kunterbunt
