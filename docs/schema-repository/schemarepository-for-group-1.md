# Infos für Entscheider:innen

Personen, die FIM verstehen und den Einsatz planen (Behördenleitungen, Führungskräfte, politisch Entscheidende sowie Lernende des eGovCampus), finden hier Informationen: 

- Vorteile des Sammelrepository
- Vorhandene Plattformen und Werkzeuge, die das Sammelrepository nutzen oder nutzen könnten

Personen, die FIM verwenden, um den Gesetzgebungsprozess digitaltauglich zu gestalten, Verwaltungsprozesse zu digitalisieren oder als Mitarbeitende bei IT-Dienstleistern, finden in diesem Abschnitt Informationen zu Anwendungsfällen, die mit dem Sammelrepository heute möglich sind oder zukünftig möglich sein könnten.

- Abläufe, die mit dem Sammelrepository heute möglich sind oder zukünftig möglich sein könnten.

## Vorteile des Sammelrepository
Zusammenfassend ermöglicht das Sammelrepository, einmal erstellte Datenstrukturen wieder zu verwenden, rechtssichere Umsetzungen zu gewährleisten und diese untereinander auszutauschen.

- **Datenstrukturen** umfassen Datenschemata, die für Formulare und Datenaustausch essentiell sind, z.B. die Datenstruktur für das Wohngeldformular bzw. den effektiven Austausch von Wohngelddaten.
- Datenstrukturen umfassen weiterhin Datenfeldgruppen und Datenfelder, aus denen Datenschemata zusammen gesetzt sind, z.B. das Datenfeld Wohnfläche oder die Datenfeldgruppe Anschrift. 
- Zu den Datenstrukturen im weiteren Sinn gehören auch Codelisten und Regeln, z.B. die Liste möglicher Staatsangehörigkeiten und eine Regel, die je nach Anzahl der Wohnenden unterschiedliche Teile des Formulars zur Anzeige steuert.
- **Wiederverwendung** ist essentiell für die effektive Digitalisierung (Formulare, Datenaustausch, Fachverfahren); eine vielfache Neugestaltung von Datenstrukturen ist ineffektiv und fehleranfällig.
- **Rechtssichere** Verwendung von Datenstrukturen ist notwendig, um die unrechtmäßige oder auf alten Rechtsgrundlagen beruhende Datenerhebung zu unterbinden (z.B. Datenfelder, die auf Papierformularen in der Vergangenheit erhoben wurden, weil dies den Behörden vor Ort sinnvoll erschien, dies jedoch einer Rechtsgrundlage entbehrte).
- **Effektiver Austausch** ist innerhalb eines Umsetzungsprojekts wichtig, um schnell von einer frühen unvollständigen Lösung durch Rückmeldungen iterativ zu einer korrekten Lösung zu kommen; sie ist auch zwischen unterschiedlichen Behörden wichtig, um die bereits vorhandenen Lösungen andernorts als Vorlage für die eigenen Umsetzungen zu verwenden.

## Vorhandene Plattformen und Werkzeuge, die das Sammelrepository nutzen oder nutzen könnten

### FIM-Portal

Das FIM-Portal ist die zentrale Anlaufstelle für alle Informationen im FIM-Umfeld und umfasst insbesondere

* das FIM-Haus mit Dokumenten
* Ansprechpartner im FIM-Umfeld, die Mitglieder Fachgruppe FIM und die FIM-Coaches
* Schulungsmaterial
* eine Suchfunktion über FIM-Artefakte.

Das FIM-Portal ist zurzeit nicht dafür vorgesehen,

* die Suche über FIM-Artefakte des Bausteins Datenfelder granular zu gestalten; diese Lücke füllt das Sammelrepository; beide Systeme haben den gleichen Datenbestand,
* die FIM-Artefakte des Bausteins Datenfelder (Dokumentsteckbrief, Datenschemata, Codelisten) in maschinenlesbarer Form bereit zu stellen; es gibt insbesondere nicht wie beim Sammelrepository stabile URLs,
* die FIM-Artefakte für den Einsatz im Systemumfeld durch Konvertierung anzupassen.

Das Sammelrepository unterstützt all diese letzten Anwendungsfälle.

### OZG-Informationsplattform

Die OZG-Informationsplattform ist im OZG-Umfeld hilfreich hinsichtlich

* aktueller Version des OZG-Katalogs und OZG-Leistungen und darin gebündelten LeiKa-Leistungen,
* einer Übersicht der Beteiligten und verantwortlichen Stellen,
* einem Überblick über den Fortschritt der Umsetzung,
* einem Download von Ergebnissen.

Die OZG-Informationsplattform ist nicht dafür vorgesehen,

* während des Projektsverlaufs Daten auszutauschen, ein oft hochdynamischer Prozess mit vielen Iterationen; dies ist eine Stärke des Sammelrepository,
* langfristig stabile URLs für den Zugriff auf Datenstrukturen zu gewähren; dies ist ein Features des Sammelrepository,
* Dienste für die Konvertierung oder die Qualitätsprüfung von Datenstrukturen anzubieten; dies ermöglicht das Sammelrepository.

Das Sammelrepository unterstützt all diese letzten Anwendungsfälle.

### Editor

FRED3 (für XDF3) oder FRED Classic oder ARIS4FIM

* sind Editor-Systeme, die von "Redaktionsteams" (oft eines pro "Bundesland") genutzt werden können,
* um Dokumentsteckbriefe und Datenschemata zu erstellen, zu pflegen und zu veröffentlichen,
* werden von IT-Dienstleistern gehostet,
* verfügen über einen Datenbestand, der lokal für die jeweilige Redaktion ist
* erlauben (nach Anbindung durch die jeweiligen Editor-Hersteller) einen Such-Zugriff auf den Bestand des Sammelrepository,
* verfügen über eigene Konvertierungsfunktionen und Qualitätssicherungsmechanismen, unabhängig von denen des Sammelrepository.

Editoren sind nicht

* für die zentrale Sammlung von Daten über verschiedene Repositories vorgesehen,
* können die Felder, Gruppen und Regeln, die in anderen Repositores liegen, nicht für die Wiederverwendung in lokalen Schemata einbinden.

Das Sammelrepository unterstützt all diese letzten Anwendungsfälle.

### Formularmanagementsysteme

Formularmanagementsysteme

* dienen der Erstellung von Formularen,
* Aufnahme von Benutzerdaten
* transportieren erfasste Formulardaten an nachgelagerte Systeme

Formularmanagementsysteme haben nicht 

* immer eine zentrale Ablage der Datenschemata, die den Formularen und dem Transport zugrunde liegen und auf das sie lesend zugreifen können.
* eine Datenkonvertierung, die die zentral angebotenen Datenschemata in das für die Formularerstellung erforderliche Format (z.B. JSON) konvertiert,
* eine automatische Konvertierung des bisher genutzten XDF2- in das neue XDF3-Format.

Das Sammelrepository unterstützt all diese letzten Anwendungsfälle.

### PVOG

Der Portalverbund Online Gateway

* hat über den Sammeldienst FIM-Leistungsbeschreibungen, die Verwaltungsportale über den Bereitstellungsdienst nutzen können.

Der PVOG

* hat keine Informationen zu Dokumentsteckbriefen und Datenschemata.

Das Sammelrepository unterstützt diesen letzten Bedarf.

## Abläufe, die mit dem Sammelrepository heute möglich sind oder zukünftig möglich sein könnten.

### Auswirkungen auf OZG-Umsetzungsprojekte für Onlinedienste

**Kontext:** OZG-Umsetzungsprojekte für Onlinedienste sind immer wieder gehalten, die FIM-Stamminformationen zu erstellen, die Grundlage der OZG-Umsetzung sind; die Auswirkungen ähneln dann denen bei Mitarbeitenden von Redaktionen und im FIM-Baustein (s.o.). Dies umfasst u.a. die OZG-Referenzdatenschemata, die für die Formulererstellung bzw. Datenübertragung aus Formularen nach Fachverfahren benötigt werden. Diese Schemata können mit Landesredaktionssystemen erstellt werden.

Es ist zugleich möglich, einen beliebigen Editor zu nutzen, mit dem XDatenfelder bearbeitet werden, sofern Fachkonzept und Qualitätskriterien des Bausteins Datenfelder berücksichtigt werden. Letztere werden in einem Redaktionssystem, das auf XDF spezialisiert werden, in einer für die Nutzungserfahrung besseren Weise berücksichtigt; technisch spricht aber nichts gegen den Einsatz eines beliebigen XML- oder Texteditors.

**Voraussetzung** für die Nutzung der Vorteile des Sammelrepository ist, dass der Editor die API des Sammelrepository angebunden hat.

Ebenfalls möglich wäre ein Zugriff über eine Web-Oberfläche; diese ist bisher nicht im Umsetzungsumfang des Sammelrepository enthalten und müssten angefordert werden.

**Folgende Anwendungsfälle und Vorteile** stehen Anwendenden von an die API angebundenen Editoren zur Verfügung:

* **Einfache Auffindbarkeit von Stammdatenschemata**, die Grundlage zur Formularerstellung von OZG-Leistungen sind, z. B. Suche nach Stammdatenschema per Web-UI oder per API. Beispiel: Alle Schemata mit Handlungsgrundlage [Mutterschutzgesetz](https://schema.fim.fitko.net/api/v0/schemas?bezug=muschg&offset=0&limit=200).
* **OZG-Referenzdatenschemata** sind genauso auffindbar. Sofern die Unterscheidung zum Stammdatenschema am Merkmal `Detaillierungsstufe` gepflegt ist, kann die Suche auf OZG-Referenzdatenschemata eingeschränkt werden.
* **Zentrale Ablage für OZG-Referenzdatenschemata,** die für den Datenaustausch zwischen Online-Diensten und Fachverfahren benötigt werden (Verbesserung gegenüber OZG-IP), z. B. per Web-UI auf der Schema-Detail-Seite oder per API. Beispiel: Suche nach allen über das Stichwort-Element gekennzeichneten [OZG-Referenzdatenschema](https://schema.fim.fitko.net/api/v0/schemas?stichwort=Detaillierungsstufe%3A%3AOZG-Referenzinformation&offset=0&limit=200).
* **FIT-Connect-Zustellpunkte** referenzieren OZG-Referenzdatenschemata im Sammelrepository über eine URL, die unverändert bleibt. Dies bedeutet, dass auch bei Erscheinen neuer Datenschema-Versionen die bisherige URL und das davon referenzierte OZG-RDS unverändert bleiben - wichtig für die Betriebsstabilität von Online-Systemen. Neue Versionen des gleichen Datenschemas oder gänzlich neue Datenschematas erhalten neue URLs.
* **Konvertierungsdienst für XML-/JSON-Schema** zwecks Weitergabe an Onlinedienst- und Fachverfahrens-Betreiber, z. B. per Web-UI auf der Schema-Detail-Seite oder per API oder per Konverter-Web-UI
* **Qualitätsprüfung** zur Ermittlung von Verbesserungsmöglichkeiten (beim Import oder per Qualitätscheck-Web-UI)

### Nutzung durch Register- und Fachverfahrensentwickler und -betreiber

**Kontext:** Mitarbeitende bei IT-Systemdienstleistern für Fachverfahren haben verschiedene Aufgabenfelder, die von der Konzeption von IT-Systemen, der beauftragten Umsetzung von Digitalisierungen bis zum Betrieb von Hard- und Software für Register und Fachverfahren reichen.

**Voraussetzung** für die Nutzung der Vorteile des Sammelrepository ist, dass die in Frage stehenden Systeme an die API des Sammelrepository angebunden sind oder die benötigten Daten über die API manuell abgerufen und den Systemen anderweitig zur Verfügung gestellt werden.

Ebenfalls möglich wäre ein Zugriff über eine Web-Oberfläche; diese ist bisher nicht im Umsetzungsumfang des Sammelrepository enthalten und müssten angefordert werden.

**Folgende Anwendungsfälle und Vorteile** stehen Verwendenden von an die API angebundenen Systemen zur Verfügung:

* Zentrale Ablage für Referenzdatenschemata, die für den Datenaustausch zwischen Online-Diensten und Fachverfahren benötigt werden (Verbesserung gegenüber OZG-IP). Beispiel: Zugriff auf letzte Version des XDF2-Datenschemas für [Wohngeld Lastenzuschuss - Erhöhungsantrag](https://schema.fim.fitko.net/api/v0/schemas/S00000207/latest)
* Verfügbarkeit unter eindeutiger und unveränderlicher URL für Referenzdatenschemata, die für den Datenaustausch benötigt werden (Verbesserung gegenüber OZG-IP), z. B. per Web-UI auf der Schema-Detail-Seite oder per API. Beispiel: Wohngeld-Datenschema als [XDF2-Datei](https://schema.fim.fitko.net/immutable/schemas/S00000207V1.1_2024-01-22-1705942620461.xdf2.xml). Dies bedeutet, dass auch bei Erscheinen neuer Datenschema-Versionen die bisherige URL und das davon referenzierte OZG-RDS unverändert bleiben - wichtig für die Betriebsstabilität von Fachverfahren. Neue Versionen des gleichen Datenschemas oder gänzlich neue Datenschematas erhalten neue URLs.
* Konvertierungsdienst für XML-/JSON-Schema zwecks Weitergabe an Onlinedienst- und Fachverfahrens-Betreiber, z. B. Zugriff auf JSON-Schema per API, auch auf XML-Schema per API. Beispiel: Wohngeld-Datenschema als [JSON-Schema-Datei](https://schema.fim.fitko.net/immutable/schemas/S00000207V1.1_2024-01-22-1705942622789.schema.json)