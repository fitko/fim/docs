# Informationen zu den Metadaten

## Name

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | FIM-interner Name des Stammdatenschemas, sichtbar für alle Anwender eines Redaktionssystems des Bausteins Datenfelder (fachliche Ersteller, Methodenexperten, Informationsmanager, FIM-Koordinierungsstelle, FIM-Nutzer).|
| Zielgruppe      | Modellierende |
| Pflichtfeld | Ja  |
| Elementspezifisches Qualitätskriterium |Konstruktionsregel für die Namensgebung: <ul><li>Es ist eine kurze, aber sprechende Kombination aus der entsprechenden Dokumentart und dem Namen des Output-Formulars sowie dessen Synonym in Klammern zu verwenden.</li><li>Falls es sich nicht um die Dokumentart Antrag handelt, sind Name bzw. Synonym durch eine Rechtsnorm (Gesetz, Verwaltungsvorschrift oder Vordruck) vorgegeben.</li></ul>|
| Empfehlung für die maximale Länge |255 Zeichen|
| Art der Prüfung     | Komplett manuell |
| Verweis auf den QS-Bericht      |  |
| Beispiel      |<ul><li>Falls die Dokumentart Antrag ist: „Antrag Kleiner Waffenschein“</li><li>Falls die Dokumentart Antrag ist: „Antrag Waffenbesitzkarte Sportschützen (WBK gelb)“</li><li>Falls die Dokumentart Urkunde ist: „Urkunde Munitionserwerbsschein“</li></ul>|
|mögliche Abweichungen im Referenzkontext| nein |

## Definition

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Fasst den Inhalt eines Stammdatenschemas aus redaktioneller Sicht zusammen, d.h. aus Sicht der Rollen Ersteller, Methodenexperte, Informationsmanager, FIM- Koordinierungsstelle, FIM-Nutzer.|
| Zielgruppe |Modellierende, Fachlichkeit|
| Pflichtfeld | Nein|
| Elementspezifisches Qualitätskriterium |In der Regel ist die Definition des Stammdatenschemas mit der des Dokumentsteckbriefs identisch – das Metadatum muss aus diesem Grund nicht befüllt werden. Falls jedoch die Notwendigkeit zu einer abweichenden Definition bestehen sollte, ist die Definition entsprechend zu ergänzen. Damit dieser Punkt sichergestellt werden kann, ist es notwendig, dass die Definition des Dokumentsteckbriefs vor (oder spätestens zeitgleich) zur Erstellung des entsprechenden Stammdatenschemas vorliegt.|
| Empfehlung für die maximale Länge |5000 Zeichen|
| Art der Prüfung     | Komplett manuell |
| Verweis auf den QS-Bericht      | x |
| Beispiel      |leer |
|mögliche Abweichungen im Referenzkontext| nein |

## Bezug zur Handlungsgrundlage

:::info
Die Namensgebung zu diesem Metadatum hat sich geändert, in XDatenfelder 2 hieß dieses Metadatum noch **Bezug zu Rechtsnorm oder Standardisierungsvorhaben**.
:::

### Bezug zur Handlungsgrundlage

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Dieses Attribut ermöglicht es, Bezüge zu einschlägigen Rechtsnormen, Verordnungen oder Richtlinien zu dokumentieren. Auch ein Bezug zu XÖV-Vorhaben (oder ähnlichem) und den entsprechenden Datencontainern kann hier gesetzt werden, sowie die Nutzung dieses Standards rechtlich nachweisbar ist. Aus dem Bezug zur Rechtsnorm ergibt sich die Gestaltung des Stammdatenschemas.|
| Zielgruppe      | Modellierende, Fachlichkeit |
| Pflichtfeld | Ja|
| Elementspezifisches Qualitätskriterium |<ol><li>Falls die Bezugsquelle der Rechtsnorm oder des Standardisierungsvorhabens eine eigene Notation vorsieht, dann ist diese zu befolgen.</li><li>Bei Angaben zu einem Bezug sollen diese möglichst konkret sein.</li><li>Die Regeln zur Erfassung von Rechtsbezügen sind zu beachten.</li><li>Der Rechtsbezug sollte mit einer Version versehen werden, damit man einen sicheren (zeitlichen) Bezugspunkt für die Gültigkeit der Rechtsgrundlage des Stammdatenschemas hat.</li></ol>|
| Empfehlung für die maximale Länge |255 Zeichen pro Bezug; ein Element kann davon mehrere haben.|
| Art der Prüfung     | komplett manuell |
| Verweis auf den QS-Bericht      | x |
| Beispiel      |Aus „Antrag nationales Visum“ § 2, § 5, § 11, §§ 27 ff., § 86 AufenthG vom 17.08.2019; § 3 BDSG vom 18.04.2020; §§ 31 ff. AufenthV vom 12.05.2019 Aus „Anzeige Tätigkeiten mit Asbest“:TRGS 519 v. 31.10.2019; GefStoffV v. 29.03.2017; §§ 22-23 ArbSchG v. 19.06.2020; DGUV Information 201-012 v. 01.12.2006|
|mögliche Abweichungen im Referenzkontext| nein |

### Link :new:

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Dieses Attribut ermöglicht es, zu jedem Bezug einen entsprechenden Link anzugeben.|
| Zielgruppe      | Modellierende, Fachlichkeit |
| Pflichtfeld | Nein|
| Elementspezifisches Qualitätskriterium |<ol><li>Es ist immer ein gültiger Link zu erfassen.</li><li>Eine gute Quelle für Links zu Rechtsgrundlagen finden sich unter: [Gesetze im Internet](https://www.gesetze-im-internet.de/)</li><li>Eine gute Quelle für Links zu XÖV-Standards findet sich unter: [XRepository 3.0](https://www.xrepository.de/)</li><li>Es sind keine unpassenden Links zu ergänzen.</li></ol>|
| Empfehlung für die maximale Länge |255 Zeichen|
| Art der Prüfung     | Exemplarisch manuell |
| Verweis auf den QS-Bericht      | x |
| Beispiel      | GRUPPEN BEISPIELE ERGÄNZEN <ol><li>Datenfeld “meldepflichtige Krankheiten” basierend auf § 6 IfSG - Link: § 6 IfSG - Einzelnorm </li><li>Datenfeld “Ort der wirtschaftlichen Tätigkeit” Xunternehmen.Kerndatenmodell.Ort der wirtschaftlichen Tätigkeit.Art Version 1.0; basierend auf Codeliste urn:xoev-de:xunternehmen:codeliste:artortwirtschaftlichetaetigkeit_1 Version 1 Link: XRepository 3.0</li></ol>  |
|mögliche Abweichungen im Referenzkontext| nein |

## Beschreibung

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Die Beschreibung enthält zusätzliche Beschreibung und Erläuterungen zum Datenschema.|
| Zielgruppe      | Modellierende, Dienstleister nachnutzender Systeme |
| Pflichtfeld | Nein|
| Elementspezifisches Qualitätskriterium | <ul><li>Enthält über die Definition hinausgehende Umsetzungs- und Nutzungshinweise.</li><li>In der Beschreibung sollten die Unterschiede eines (OZG-) Referenzdatenschemas zu dessen Stammdatenschema herausgearbeitet werden.</li><li>Im Referenzkontext können hier auch Abbruchbedingungen genannt werden.</li></ul>|
|Empfehlung für die maximale Länge|5000 Zeichen|
| Art der Prüfung     | keine |
| Verweis auf den QS-Bericht      | x |
| Beispiel      |"Für den Fall, dass keiner der erforderten Nachweise erbracht werden kann, muss der Antragstellende darauf hingewiesen werden, dass sich sein Antrag an dieser Stelle verzögern wird." |
|mögliche Abweichungen im Referenzkontext| nein |

## Bezeichnung

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Bezeichnung des Datenschemas, welche für den Bürger/das Unternehmen auf Formularen sichtbar ist. Sie ist wie eine Art Titel zu betrachten.|
| Zielgruppe | Modellierende, Fachlichkeit, Dienstleister nachnutzender Systeme, Antragstellende |
| Pflichtfeld | Ja|
| Elementspezifisches Qualitätskriterium |<ul><li>Die Bezeichnung sollte identisch mit der des entsprechenden Dokumentsteckbriefs sein.</li><li>Die Bezeichnung soll kurz und prägnant sein.</li><li>Die Bezeichnung soll keine expliziten Bezüge zum Kontext herstellen, wenn dies nicht zwingend notwendig ist.</li><li>Die Bezeichnung soll nicht als Frage formuliert sein.</li></ul>|
| Empfehlung für die maximale Länge |255 Zeichen|
| Art der Prüfung     | Komplett manuell |
| Verweis auf den QS-Bericht      | x |
| Beispiel      |<ul><li>Antrag auf Approbation - Ärztin oder Arzt aus Drittstaaten</li><li>Antrag auf Einbürgerung/ Miteinbürgerung Inland</li></ul>|
|mögliche Abweichungen im Referenzkontext| nein |

## Hilfetext

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Die Art der Relation die erstellt werden soll. Es gibt momentan vier verschiedene: **ist abgeleitet von**, **ersetzt**, **ist äquivalent zu** und **verknüpft**.|
| Zielgruppe      | Modellierende, Fachlichkeit, Dienstleister nachnutzender Systeme, Antragstellende |
| Pflichtfeld | Ja, falls man eine Relation erstellen möchte.|
| Elementspezifisches Qualitätskriterium |Es ist immer nur eine zu Start- und Zielelement passende Relation zu verwenden.|
| Empfehlung für die maximale Länge |500 Zeichen|
| Art der Prüfung     | keine |
| Verweis auf den QS-Bericht      | x |
| Beispiel      |siehe Tabelle |
|mögliche Abweichungen im Referenzkontext| nein |

## Ableitungsmodifikation Struktur

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Definiert, ob die Struktur eines Stammdatenschemas von einer nachgelagerten Verwaltungsebene bei der Erstellung eines neuen Stammdatenschemas oder konkreten Formulars verändert werden darf oder nicht. Über dieses Attribut kann spezifiziert werden, dass ein Formular auf struktureller Ebene bundesweit einheitlich gestaltet ist.|
| Zielgruppe      | Modellierende, Fachlichkeit, Dienstleister nachnutzender Systeme |
| Pflichtfeld | Ja, Default **alles modifizierbar**|
| Elementspezifisches Qualitätskriterium |Mögliche Ausprägungen dieses Metadatums sind: <ul><li>Alles modifizierbar</li><li>Nur erweiterbar</li><li>Nur einschränkbar</li><li>Nicht modifizierbar</li></ul>Typ 1 Leistungen sind generell **nicht modifizierbar**.|
| Empfehlung für die maximale Länge |nicht relevant|
| Art der Prüfung     | **Wird nicht geprüft, sollte aber unbedingt bei Typ 2 bis 5 Leistungen vor der Veröffentlichung und Freigabe mit dem Fachresort geklärt werden.** |
| Verweis auf den QS-Bericht      | x |
| Beispiel      |<ul><li>Antrag auf Berechtigungszertifikat Typ 1 – nicht modifizierbar</li><li>Antrag auf Zustimmung Sondernutzung Straße für Telekommunikation Typ 2/3 – alles modifizierbar</li></ul>|
|mögliche Abweichungen im Referenzkontext| nein |

## Ableitungsmodifikation Repräsentation

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Definiert, ob die repräsentationsbezogenen Metadaten eines Datenfeldes, einer Datenfeldgruppe oder Regel (Bezeichnung, Hilfetext Eingabe, Hilfetext Ausgabe, Inhalt) von einer nachgelagerten Verwaltungsebene bei der Erstellung eines abgeleiteten Stammdatenschemas oder konkreten Formulars verändert werden dürfen oder nicht.|
| Zielgruppe      | Modellierende, Fachlichkeit, Dienstleister nachnutzender Systeme |
| Pflichtfeld | Ja,  Default **modifizierbar**|
| Elementspezifisches Qualitätskriterium |Mögliche Ausprägungen dieses Metadatums sind:<ul><li>Modifizierbar</li><li>Nicht modifizierbar</li></ul>|
| Empfehlung für die maximale Länge |nicht relevant|
| Art der Prüfung     | **Wird nicht geprüft, sollte aber unbedingt bei Typ 2 bis 5 Leistungen vor der Veröffentlichung und Freigabe mit dem Fachresort geklärt werden.**|
| Verweis auf den QS-Bericht      | x |
| Beispiel      |<ul><li>Antrag auf Berechtigungszertifikat Typ 1 – nicht modifizierbar</li><li>Antrag auf Zustimmung Sondernutzung Straße für Telekommunikation Typ 2/3 – modifizierbar</li></ul> |
|mögliche Abweichungen im Referenzkontext| nein |

## Strukturelement :sparkles:

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Die von einem Datenschema verwendeten Unterelemente (Datenfelder und Datenfeldgruppen) bilden die Struktur des Schemas und werden damit als Strukturelemente der Gruppe bezeichnet.|
| Zielgruppe      | Modellierende |
| Pflichtfeld | **Ja**, ein Datenschema muss Unterelemente enthalten, um konform zum XDatenfelder 3 Standard zu sein.|
| Elementspezifisches Qualitätskriterium |<ul><li>Ein Datenschema muss mehr als ein Unterelement enthalten.</li><li>Eine Gruppe kann mehrmals das gleiche Element enthalten, dies muss aber durch die Multiplizität gekennzeichnet werden.</li></ul>|
| Empfehlung für die maximale Länge |nicht relevant|
| Art der Prüfung     | teilweise manuell |
| Verweis auf den QS-Bericht      | x |
| Beispiel      |Das Stammdatenschema S00 000 000 187 *Antrag nationales Visum* besteht u. a. aus den Datenfeldgruppen G00 000 001 590 *Persönliche Angaben zur antragstellenden Person (nationales Visum)*, G00 000 000 970 *Reisedokument*, dem Datenfeld F00 000 002 745 *Lebensunterhalt* und weiteren.|
|mögliche Abweichungen im Referenzkontext| nein |

## Bezug aus Struktur

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Bezeichnet die Gesamtheit an Informationen, die beschreiben wie oft und warum ein Unterelement eines Datenschemas (Datenfeldgruppen oder Datenfelder) an einer bestimmten Stelle in der Struktur des Schemas eingefügt wurde. Die hierbei relevanten Informationen sind die Multiplizität und der Rechtsbezug. <ul><li>Die Multiplizität steuert (siehe oben) das Vorkommen des Unterelements in einer Gruppe.</li><li>Der Bezug zur Handlungsgrundlage gibt an, aus welchem Grund ein harmonisiertes Unterelement oder ein Unterelement aus einem fremden Rechtsgebiet an der gewählten Position innerhalb der Gruppe eingefügt wurde.</li></ul>|
| Zielgruppe      | Modellierende, Fachlichkeit |
| Pflichtfeld | Ja, für harmonisierte Unterelemente. / Nein, für rechtsnormgebundene Unterelemente.|
| Elementspezifisches Qualitätskriterium |<ol><li>Die Angaben zur Multiplizität ergeben sich aus dem Rechtskontext des Datenschemas.</li><li>Die Angabe zu Rechtsnorm oder Standardisierungsvorhaben muss erfolgen, wenn ein Element in eine Struktur eingefügt wird, die auf einer anderen Handlungsgrundlage basiert als das Element selbst.</li><li>Die Angabe ist dann nicht erforderlich, wenn die Handlungsgrundlage aus dem gleichen Kontext stammt wie das eingefügte Element selbst.</li><li>Falls die Bezugsquelle der Rechtsnorm oder des Standardisierungsvorhabens eine eigene Notation vorsieht, dann ist diese zu befolgen.</li><li>Bei Angaben zu einem Bezug sollen diese möglichst konkret sein.</li><li>Die Regeln zur Erfassung von Rechtsbezügen sind zu beachten.</li></ol>|
| Empfehlung für die maximale Länge |255 Zeichen pro Bezug; ein Element kann davon mehrere haben.|
| Art der Prüfung     | automatisiert |
| Verweis auf den QS-Bericht      |  |
| Beispiel |<ol><li>Datenfeldgruppe „Früherer Bezug von Unterhaltsvorschuss“: die Gruppe besteht aus drei Feldern (einer Abfrage zum früheren Bezug von Unterhaltsleistungen, der Bezeichnung der Behörde/ des Jugendamtes und einer Angabe zum letzten Bezug der Leistung), bei den letzten zwei Feldern handelt es sich um harmonisierte Felder, die auch in einem anderen Kontext genutzt werden könnten. An dieser Stelle in der Datenfeldgruppe können sie gemäß RL Nr. 9.7.4. UhVorschG; RL Nr. 9.7.5. UhVorschG; RL Nr. 9.8. UhVorschG genutzt werden.</li><li>Datenfeldgruppe „Angaben zum Arbeitgeber (Asbest)“ aus dem Stammdatenschema „Anzeige Tätigkeiten mit Asbest“: es wird u. a. der Name des Unternehmens abgefragt, bezugnehmend auf § 23 (1) ArbSchG.</li></ol> |
|mögliche Abweichungen im Referenzkontext| nein |

## Verweis auf Regeln

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Die von einem Datenschema verwendeten Regeln sind direkt am Datenschema verortet, d. h. das Datenschema referenziert die IDs dieser Regeln.|
| Zielgruppe      | Modellierende, Fachlichkeit |
| Pflichtfeld | Nein|
| Elementspezifisches Qualitätskriterium |<ol><li>Es dürfen nur gültige Regel Identifikatoren referenziert werden.</li><li>Es können mehrere Regeln referenziert werden.</li></ol>|
| Empfehlung für die maximale Länge |nicht relevant|
| Art der Prüfung     | Wird nicht geprüft. |
| Verweis auf den QS-Bericht      | x |
| Beispiel      |R00 000 001 490 |
|mögliche Abweichungen im Referenzkontext| nein |

## Verweis auf Dokumentsteckbrief :sparkles:

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Der Dokumentsteckbrief, an dem ein Datenschema hängt. Jedes Datenschema muss stets (genau) einem Dokumentsteckbrief zugeordnet sein.|
| Zielgruppe      | Modellierende|
| Pflichtfeld | Ja|
| Elementspezifisches Qualitätskriterium |<ul><li>Es dürfen nur gültige Dokumentsteckbrief Identifikatoren referenziert werden.</li><li>Der Verweis erfolgt nur auf die nummerische ID des Dokumentsteckbriefs. Auf Versionen wird nicht verwiesen.</li><li>Es kann nur auf genau einen Dokumentsteckbrief verwiesen werden.</li><li>Es darf nur auf **nicht abstrakte** Dokumentsteckbriefe verwiesen werden.</li><li>Es sollten Dokumentsteckbrief aus KATE referenziert werden.</li><li>Referenzen auf Dokumentsteckbriefe außerhalb von KATE dürfen nur auf kurze Zeit bestehen.</li></ul>|
| Empfehlung für die maximale Länge |nicht relevant|
| Art der Prüfung     | Wird nicht geprüft. |
| Verweis auf den QS-Bericht      | x |
| Beispiel      |:x: D00 000 001 490 V1.0.0 <br/> :heavy_check_mark: D99 000 000 525 |
|mögliche Abweichungen im Referenzkontext| nein |
