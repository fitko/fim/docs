# Meldungen im Zusammenhang mit Präzisierungsangaben

Ein großer Teil der Meldungen des QS-Berichts betrifft den falschen Einsatz von Präzisierungsangaben. Stellen Sie sicher, dass Ihnen die Informationen aus dem Abschnitt [Präzisierung von Datenfeldern](./DF_Allgemeines.md#präzisierung-von-datenfeldern-) bekannt sind, wenn solch ein Fehler auftritt.

## Präzisierungsangaben für Text- und nummerische Felder

### Welche Fehlermeldungen betrifft das?

- **1012**: Bei Datenfeldern mit der Feldart 'Auswahlfeld' oder 'Statisches, read-only Feld' dürfen weder die minimale noch die maximale Feldlänge angegeben werden.
- **1015**: Eine Feldlänge darf nur bei einem Datenfeld mit dem Datentyp 'Text' oder 'String.Latin+' angegeben werden.
- **1016**: Ein Wertebereich darf nur bei einem Datenfeld mit einem nummerischen Datentyp (Nummer, Ganzzahl, Geldbetrag) oder einem Zeitdatentyp (Uhrzeit, Datum, Zeitpunkt) angegeben werden.

### Allgemeine Informationen

Präzisierungsangaben sind spezifisch, je nach Datentyp eines Feldes.
Die genannten Fehler treten also nur dann auf, wenn Präzisierungsangaben nicht passend zum Elementen gemacht werden.

### Wie löse ich den Fehler auf?

- Prüfen Sie, ob die Präzisierungsangaben an einem Datenfeld gemacht wurden; Datenfeldgruppen haben niemals Präzisierungsangaben.
- Klären Sie dann, ob Sie an Ihrem Datenfeld den passenden Datentyp / die passende Feldart gewählt haben.
- Falls dem nicht so sein sollte, passen Sie Datentyp und/oder Feldart an.
- Falls Datentyp und/oder Feldart richtig gewählt wurde, entfernen Sie zunächst die fehlerhafte Präzisierung.
- Danach pflegen Sie die richtigen Präzisierungsangaben ein.

### Beispiel

Sie haben ein Datenfeld, welches die Eingabe einer Ganzzahl erwartet. Dafür wurden Angaben zur minimalen und maximalen Länge hinterlegt. Diese müssen zunächst entfernt werden, Angaben zur Länge werden nur bei Texteingabefeldern gemacht. Im  zweiten Schritt können Sie an Ihr Datenfeld präzisieren, indem Sie einen Wertebereich angeben.

## Detaillierte Meldungen für Text- und nummerische Felder

### Welche Fehlermeldungen betrifft das?

- **1062**: Die untere Wertgrenze muss eine Zahl sein.
- **1063**: Die obere Wertgrenze muss eine Zahl sein.
- **1065**: Die untere Wertgrenze muss eine ganze Zahl sein.
- **1066**: Die obere Wertgrenze muss eine ganze Zahl sein.
- **1067**: Die untere Wertgrenze muss ein Datum sein.
- **1068**: Die obere Wertgrenze muss ein Datum sein.
- **1079**: Die untere Wertgrenze muss ein Zeitpunkt (Datum und Uhrzeit) sein.
- **1080**: Die obere Wertgrenze muss ein Zeitpunkt (Datum und Uhrzeit) sein.
- **1081**: Die untere Wertgrenze muss eine Uhrzeit sein.
- **1082**: Die obere Wertgrenze muss eine Uhrzeit sein.

### Allgemeine Informationen

Die genannten Fehlermeldungen prüfen nicht nur, ob die Präzisierung generell passt, sondern auch explizit ob jeweils die untere und obere Grenze der Präzisierung passend zum Datentyp gewählt wurden.

### Wie löse ich den Fehler auf?

- Prüfen Sie, ob die Präzisierungsangaben an einem Datenfeld gemacht wurden; Datenfeldgruppen haben niemals Präzisierungsangaben.
- Klären Sie dann, ob Sie an Ihrem Datenfeld den passenden Datentyp / die passende Feldart gewählt haben.
- Falls dem nicht so sein sollte, passen Sie Datentyp und/oder Feldart an.
- Falls Datentyp und/oder Feldart richtig gewählt wurde, entfernen Sie zunächst die fehlerhafte untere bzw. obere Wertgrenze.
- Danach pflegen Sie die richtige untere bzw. obere Wertgrenze ein.

## Feldlängenbegrenzung für Texteingabefelder

### Welche Fehlermeldungen betrifft das?

- **1059**: Die minimale Feldlänge muss eine ganze Zahl größer oder gleich Null sein.
- **1060**: Die maximale Feldlänge muss eine ganze Zahl größer Null sein.

### Allgemeine Informationen

Eine Textlänge kann nicht aus einer negativen Anzahlen von Zeichen bestehen, deswegen werden die beiden Grenzen für die Präzisierung der Textlänge auf diesen Punkt hin überprüft. Die untere Grenze kann bei Null beginnen, die obere Grenze muss immer größer Null sein.

### Wie löse ich den Fehler auf?

- Prüfen Sie zunächst, ob es sich um ein Datenfeld handelt und ob die gewählten Präzisierungsangaben passend zur Intention des Feldes sind.
- Prüfen Sie, ob Ihre untere Grenze in Form einer positiven Zahl eingegeben wurde, die Null ist auch ok.
- Prüfen Sie, ob Ihre obere Grenze in Form einer positiven Zahl eingegeben wurde.
- Falls eines von beiden nicht erfüllt ist, passen Sie den Wert an.

## Inkonsistente Angabe der unteren und oberen Grenze

### Welche Fehlermeldungen betrifft das?

- **1061**: Die minimale Feldlänge darf nicht größer sein als die maximale Feldlänge.
- **1064**: Die untere Wertgrenze darf nicht größer sein als die obere Wertgrenze.

### Allgemeine Informationen

Die Wertebereiche, für die Präzisierungsangaben getätigt werden, müssen in sich schlüssig sein. D. h. eine obere Grenze muss einen höheren Wert enthalten, als eine untere Grenze und umgekehrt.

### Wie löse ich den Fehler auf?

- Prüfen Sie zunächst, ob es sich um ein Datenfeld handelt und ob die gewählten Präzisierungsangaben passend zur Intention des Feldes sind.
- Prüfen Sie danach Ihre untere und obere Grenzen. Ist die untere Grenze wirklich kleiner als die obere Grenze? Ist die obere Grenze vielleicht niedriger als die untere Grenze?
- Passen Sie die Grenzen entsprechend an.

### Wie löse ich den Fehler auf?

## Präzisierungsangaben für Anlagefelder

### Welche Fehlermeldungen betrifft das?

- **1056**: Die Dateigröße darf nur befüllt sein, wenn der Datentyp des Feldes den Wert 'Anlage (Datei)' hat.
- **1057**: Erlaubte Datentypen dürfen nur angegeben werden, wenn der Datentyp des Feldes den Wert 'Anlage (Datei)' hat.

### Allgemeine Informationen

Die Dateigröße und der Datentyp sind typische Präzisierungsangaben für Datenfelder des Datentyps Anlage.
Verwenden Sie beides nur in diesem Kontext.

### Wie löse ich den Fehler auf?

- Prüfen Sie, ob die Präzisierungsangaben an einem Datenfeld gemacht wurden; Datenfeldgruppen haben niemals Präzisierungsangaben.
- Klären Sie dann, ob es sich hier um ein Anlagefeld handeln soll oder nicht.
- Falls es ein Anlagefeld ist, prüfen Sie Feldart und Datentyp. Sind diese auch passend gewählt? Die richtige Kombination ist in diesem Fall Feldart: Eingabe und Datentyp: Anlage.
- Falls es kein Anlagefeld ist, entfernen Sie bitte die Angaben zu Dateigröße und Datentyp.

## Defaultwerte passend zu den Präzisierungsangaben

### Welche Fehlermeldungen betrifft das?

- **1069**: Der Inhalt muss eine Zahl sein.
- **1070**: Der Inhalt muss eine ganze Zahl sein.
- **1071**: Der Inhalt muss ein Datum sein.
- **1076**: Der Inhalt stimmt nicht mit der zugeordneten Codeliste überein.
- **1077**: Der Inhalt muss ein Wahrheitswert sein (true oder false).
- **1083**: Der Inhalt muss ein Zeitpunkt (Datum und Uhrzeit) sein.
- **1084**: Der Inhalt muss eine Uhrzeit sein.
- **1085**: Der Inhalt muss ein Code-Wert der Werteliste sein.

### Allgemeine Informationen

Ebenso wie die Präzisierungsangaben selbst, muss auch der Defaultwert passend zum Datentyp gewählt werden.

### Wie löse ich den Fehler auf?

- Prüfen Sie, ob die Präzisierungsangaben an einem Datenfeld gemacht wurden; Datenfeldgruppen haben niemals Präzisierungsangaben und deswegen auch keinen Defaultwert.
- Klären Sie dann, ob Sie an Ihrem Datenfeld den passenden Datentyp / die passende Feldart gewählt haben.
- Falls dem nicht so sein sollte, passen Sie Datentyp und/oder Feldart an.
- Falls Datentyp und/oder Feldart richtig gewählt wurde, entfernen Sie den gesetzten Defaultwert, d. h. den der Wert im Inhaltsfeld.

## Defaultwerte widersprechen den Präzisierungsangaben

### Welche Fehlermeldungen betrifft das?

- **1072**: Der Inhalt unterschreitet die untere Wertgrenze.
- **1073**: Der Inhalt überschreitet die obere Wertgrenze.
- **1074**: Der Inhalt unterschreitet die Minimallänge.
- **1075**: Der Inhalt überschreitet die Maximallänge.

### Allgemeine Informationen

Der Defaultwert für ein Datenfeld muss innerhalb des Wertebereichs der Präzisierungsangaben liegen.
D. h. die, an einem Datenfeld gepflegten, Präzisierungsangaben bilden eine Schranke nach oben und nach unten. Innerhalb dieser Schranken muss der Defaultwert liegen.

### Wie löse ich den Fehler auf?

- Prüfen Sie, ob Sie Ihre untere/obere Wertegrenze oder Ihre Minimal-/Maximallänge richtig gewählt haben.
- Falls nein, passen Sie diese an.
- Falls ja, wählen Sie einen neuen Defaultwert, der innerhalb dieser Schranken liegt.
