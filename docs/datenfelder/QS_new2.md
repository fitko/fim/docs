# Meldungen in Zusammenhang mit Metadaten

## unbekannte Werte

### Welche Fehlermeldungen betrifft das?

- **1027**: Unbekannte Feldart.
- **1028**: Unbekannter Datentyp.
- **1029**: Unbekannte Relationsart.
- **1030**: Unbekannte Vorbefüllungsart.
- **1031**: Unbekannte Strukturelementart.
- **1032**: Unbekannter Status.
- **1033**: Unbekannte Änderbarkeit der Struktur.
- **1034**: Unbekannte Änderbarkeit der Repräsentation.
- **1042**: Unbekannte Gruppenart.
- **1055**: Unbekannte Dokumentart.

### Allgemeine Informationen

Es gibt Werte, die durch den Standard XDatenfelder 3 mittels Codeliste vordefiniert werden. Wenn die Meldung *unbekannter Wert* erscheint, handelt es sich in der Regel um einen solchen Wert. Um hierüber einen Überblick zu bekommen, ist die [Spezifikation zum Standard XDatenfelder 3](https://www.xrepository.de/api/xrepository/urn:xoev-de:fim:standard:xdatenfelder_3.0.0:dokument:XDatenfelder_3.0.0_Spezifikation) erwähnt. An den passenden Stellen werden die entsprechenden Listen jedoch auch in diesem Dokument genannt.

### Wie löse ich den Fehler auf?

- Wählen Sie nur Werte aus, die durch den Standard XDatenfelder 3 definiert wurden.
- Analysieren Sie den entsprechenden Wert.
- Ist er im Standard enthalten? Prüfen Sie die entsprechende Codeliste.
- Übernehmen Sie ggf. den richtigen Wert aus der passenden Codeliste.

## ID (Identifier)

### Welche Fehlermeldungen betrifft das?

- **1038**: Die ID eines Objektes ist nicht gesetzt.
- **1039**: Das Format der ID entspricht nicht den Vorgaben.

### Allgemeine Informationen

Eine ID ist ein Pflichtfeld für alle Elemente; sie muss vorhanden und entsprechend der Vorgaben aufgebaut sein. Beachten Sie dazu den entsprechenden [Abschnitt über den Aufbau einer ID](./EA_Metadaten.md#aufbau-einer-id).

### Wie löse ich den Fehler auf?

- Stellen Sie sicher, dass eine ID gepflegt wurde.
- Analysieren Sie, welcher Teil Ihres Identifiers fehlerbehaftet ist:
    - Ist es die nummerische ID?
    - Ist es die Versionskennung?
- Passen Sie den betroffenen Teil gemäß der Vorgaben an einen Identifier im FIM-Baustein Datenfelder an.

## Metadaten, die Pflichtattribute sind

### Welche Fehlermeldungen betrifft das?

- **1002**: Der Name eines Elements ist nicht gesetzt.
- **1007**: Es ist kein Dokumentsteckbrief zugeordnet.
- **1009**: Die 'Bezeichnung Eingabe' muss befüllt werden.
- **1010**: Die 'Bezeichnung Ausgabe' muss befüllt werden.
- **1024**: Die Freitextregel muss befüllt sein.
- **1049**: Die Definition eines Dokumentsteckbriefs muss befüllt sein.
- **1050**: Die Bezeichnung muss befüllt sein.

### Allgemeine Informationen

Vorgaben des Standards XDatenfelder 3 und der Methodik definieren, welche Metadaten der Elemente Pflichtattribute sind und welche nicht. Diese Metadaten müssen bei der Bearbeitung stets gepflegt sein.
<p/>

**Hinweis:** Die Bezeichnung Eingabe/Ausgabe bezieht sich auf Datenfeldgruppen und Datenfelder. Die Bezeichnung bezieht sich auf Datenschemata und Dokumentsteckbriefe.

### Wie löse ich den Fehler auf?

Pflege Sie das fehlende Pflichtattribut nach. Konsultieren Sie dazu ggf. die passende Stelle in diesem Dokument um nähere Informationen zu erhalten, auf was bei einer Befüllung zu achten ist. <br/> Für die Bezeichnung Ausgabe eines Datenfeldes findet sich beispielsweise der entsprechende Abschnitt im Unterkapitel Datenfeldbaukasten - Datenfelder - Informationen zu den Metadaten im Abschnitt [Bezeichnung Ausgabe](DF_Metadaten.md#bezeichnung-ausgabe).

## Handlungsgrundlagen als Pflichtattribute

### Welche Fehlermeldungen betrifft das?

- **1006**: Der Bezug zur Handlungsgrundlage darf bei Elementen mit der Strukturelementart 'rechtsnormgebunden' nicht leer sein.
- **1008**: Der Bezug zur Handlungsgrundlage darf bei Dokumentsteckbriefen nicht leer sein.
- **1022**: Bei Verwendung von Elementen mit der Strukturelementart 'abstrakt' oder 'harmonisiert' innerhalb von Datenfeldgruppen mit der Strukturelementart 'rechtsnormgebunden' muss zur Multiplizität ein Bezug zu einer Handlungsgrundlage angegeben werden.
- **1051**: Der Bezug zur Handlungsgrundlage darf in einem Datenschema nicht leer sein.
- **1052**: Der Bezug zur Handlungsgrundlage darf nur bei abstrakten Dokumentsteckbriefen leer sein.
- **1101**: Der Bezug zur Handlungsgrundlage sollte bei Elementen mit der Strukturelementart 'harmonisiert' möglichst nicht leer sein.

### Allgemeine Informationen

- Datenfelder und Datenfeldgruppen mit der Strukturelementart *rechtsnormgebunden* müssen stets durch eine Handlungsgrundlage begründet werden.
- Gleiches gilt bei der Verwendung / Nachnutzung eines *harmonisierten* Datenfeldes oder Datenfeldgruppe, auch hier muss für die Verwendung stets ein Strukturbezug (Verwendungsbegründung) hinterlegt werden.
- Auch im ausgesprochen seltenen Fall, in dem eine abstrakte Datenfeldgruppe nachgenutzt wird (z. B. als Teil einer anderen abstrakten Datenfeldgruppe), muss eine Verwendungsbegründung hinterlegt werden.
- Gleichwohl müssen Datenschemata stets auf einer Handlungsgrundlage basieren, ebenso konkrete Dokumentsteckbriefe.
- Nur bei abstrakten Dokumentsteckbriefen darf die Handlungsgrundlage weggelassen werden.
- Bei der Erstellung harmonisierten Datenfeldern oder Datenfeldgruppen ist es empfohlen eine Handlungsgrundlage zu hinterlegen, sowie dies möglich ist.

### Wie löse ich den Fehler auf?

- Prüfen Sie, ob an Ihrem Element eine Handlungsgrundlage hinterlegt ist. Vielleicht haben Sie dies beim Modellieren übersehen?
- Ergänzen Sie fehlende Handlungsgrundlagen gewissenhaft; achten Sie dabei auf die Notation zur [einheitlichen Erfassung von Handlungsgrundlagen](EA_Handlungsgrundlagen.md).
- Wenn Sie keine Handlungsgrundlage erfassen können, weil z. B. eine Gesetzeslücke vorliegt, versehen Sie das Element bitte mit einem entsprechenden Vermerk. Dies sollte sehr selten vorkommen.

## Multiplizität

### Welche Fehlermeldungen betrifft das?

- **1021**: Es ist keine Multiplizität angegeben.
- **1046**: Die Multiplizität entspricht nicht den Formatvorgaben.
- **1047**: In der Multiplizität muss die minimale Anzahl kleiner oder gleich der maximalen Anzahl sein.

### Allgemeine Informationen

Für alle Unterelemente von Datenschemata oder Gruppen muss eine Multiplizität angegeben werden.
Die Multiplizität wird in Form von zwei Zahlen und einem Doppelpunkt dazwischen dargestellt, siehe [hier](./EA_Multiplizitäten.md), z. B. 1:1. Die linke Ziffer ist die minimale Anzahl, die rechte die maximale Anzahl. Beide Werte können gleich sein, die minimale Anzahl kann aber nicht größer sein als die maximale Anzahl.

### Wie löse ich den Fehler auf?

- Pflegen Sie immer eine Multiplizität an Ihren Unterelementen in der Notation Min:Max, wobei für Min und Max jeweils ganze Zahlen verwendet werden.
- Stellen Sie sicher, dass der Wert für Min, also die minimale Anzahl, kleiner ist, als der Wert für Max, also die maximale Anzahl.
