# Allgemeine Fragen zu FIM

## Was ist FIM?
Das Föderale Informationsmanagement (FIM) ist ein dauerhaft etabliertes Produkt des IT-Planungsrats, um den Vollzug von Verwaltungsleistungen durch standardisierte Informationen (die auf Grundlage von Rechtsvorschriften erstellt werden) zu harmonisieren. Die standardisierten Informationen werden als FIM-Informationen oder (FIM-) Stamminformationen bezeichnet und unterteilen sich in:

- **FIM-Leistungsbeschreibungen** erklären in einer bürgerfreundlichen Sprache den Gegenstand einer Leistung
- **FIM-Datenschemata** geben an, welche Daten die zuständige Behörde erfassen und verarbeiten muss
- **FIM-Prozesse** zeigen abstrakt den behördeninternen Bearbeitungsprozess.

## Was sind die Mehrwerte und Ziele von FIM?

- **Rechtssicherheit** für Verwaltungsvollzug und Bürger:innen / Unternehmen
- **Verringerung des redaktionellen Aufwands** durch Synergieeffekte bei der Erstellung/ Pflege dieser Informationen
- **Erhöhung der Benutzerfreundlichkeit** von Verwaltungsverfahren für Bürger:innen / Unternehmen
- **Erstellung von notwendigen Grundbausteinen für Digitalisierungsprojekte** (bspw. für Onlinedienste im Kontext OZG, aber auch sonstige Digitalisierungsvorhaben durch einheitliche Qualitäts- und Modellierungsstandards)
- **Identifizierung von Digitalisierungshindernissen** und Rechtsanpassungsbedarfen