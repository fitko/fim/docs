---
title: "Infos für Praktiker:innen"
---

# Infos für Praktiker:innen

Personen, die FIM anwenden, um FIM-Stamminformationen zu erstellen, finden in diesem Abschnitt Informationen zu Anwendungsfällen, die mit dem Sammelrepository heute möglich sind oder zukünftig möglich sein könnten.

## Auswirkungen auf die Arbeit in den Redaktionen und im FIM-Baustein

**Kontext:** Die Landesredaktionen arbeiten in den meisten Fällen mit FIM-Leistungsbeschreibungen in einem und FIM-Datenbeschreibungen in einem anderen System.

**Voraussetzung** für die Nutzung der Vorteile des Sammelrepository ist, dass das System für FIM-Datenbeschreibungen die API des Sammelrepository angebunden hat.

Ebenfalls möglich wäre ein Zugriff über eine Web-Oberfläche; diese ist bisher nicht im Umsetzungsumfang des Sammelrepository enthalten und müsste angefordert werden.

**Folgende Anwendungsfälle und Vorteile** stehen Anwendenden von an die API angebundenen Daten-Redaktionssystemen zur Verfügung:

* **Echtzeit-Zugriff** auf Inhalte anderer Landesredaktionen (sofern “veröffentlicht“), z. B. Zugriff auf Web-GUI oder per API. Beispielaufruf: Zugriff auf die letzte Version des Stammdatenschemas [Entgegennahme Anzeige Tätigkeiten mit Asbest](https://schema.fim.fitko.net/api/v0/schemas?name=Entgegennahme%20Anzeige%20T%C3%A4tigkeiten%20mit%20Asbest)
* **Vielfältige Suchmöglichkeiten** für die Inhalte aller Redaktionen, z. B. Suche nach Schema per ID und Version, Suche nach Dokumentsteckbrief per ID und Version,  Rechtsgrundlage, Namen der erstellenden Person oder Freigabestatus. Beispielaufruf: [Version 1.3 des Schemas S00000159](https://schema.fim.fitko.net/api/v0/schemas/S00000159/1.3)
* **Einzelzugriff** einzelner Schemata statt Gesamtdatenaustausch ganzer Repositories, sogar Zugriff auf einzelne Baukastenelemente. Beispiel: Für die Wiederverwendung im eigenen Schema zunächst Suche nach einem Datenfeld [Geburtsdatum](https://schema.fim.fitko.net/api/v0/fields?name=Geburtsdatum&offset=0&limit=100&show_all=false), dann Einbindung in das lokal in Arbeit befindliche Datenschema.
* **Schemata für Typ 4/5-Leistungen** zur Nachnutzung bereitstellbar
* **Keine Zwischenschritte** mehr nötig über andere Plattformen wie SharePoint, keine Anrufe o.ä.
* **Keine zwei oder mehr Werkzeuge mehr** durch direkte Integration in die Redaktionssystemoberfläche, wie für die [Projekt-Test-Oberfläche](https://schema.fim.fitko.net) exemplarisch durchgeführt