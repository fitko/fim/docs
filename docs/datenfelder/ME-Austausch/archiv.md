# Archiv

## Materialien vergangener Termine

Im Archiv finden Sie Foliensätze und Unterlagen, der letzten Jahre. Bitte beachten Sie, dass Foliensätze ggf. schon veraltet sein können. Wann die Foliensätze zuletzt aktualisiert wurden, sehen Sie am Dateinamen.

- [methodische Fehler, Tipps und Tricks für die Modellierung](./assets/20221213_methodische_Fehler_Tipps.pdf)
- [Strukturierung und Wiederverwendung](./assets/20221213_Strukturierung_und_Wiederverwendung.pdf)

## Agenda vergangener Termine

# 24.09.2024

- Vorstellung der ME-Austausch Informationsseite (10 Minuten)
- DMN (25 - 35 Minuten)
- Platz für weitere Agendapunkte

# 22.10.2024

freie Agenda

# 19.11.2024

- Anforderungen an das neue Regelwerk

# 17.12.2024

- Stichwörter in BOB & Datenschutz

# 14.01.2025

freie Agenda

## Themensammlung für weitere Termine

- Wiederholung und aufbauende Informationen zum Thema DMN; Einführung in Schema Rules; Vergleich zwischen DMN und Schema Rules
- Mehrsprachigkeit
- Hinweise zur (Neu-) Veröffentlichung von Schemata im Standard XDatenfelder 3
