---
sidebar_label: Leistungszuschnitt
---

# Leistungszuschnitt

Dieser [Link](../ressources/20200506_Leistungen_Leitfaden_Leistungszuschnitt_1.0.pdf) öffnet einen bausteinübergreifenden Leitfaden für die Identifikation von Verwaltungsleistungen und Prozessen im Rahmen der Normenanalyse.

Zum Einsatz kommt dabei dieses [Musterformular Leistungszuschnitt](../ressources/Musterformular_Leistungszuschnitt_V2.3.xlsx), eine Excel-Datei, in der Daten zum Leistungszuschnitt eingetragen werden können.