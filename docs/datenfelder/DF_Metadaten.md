# Informationen zu den Metadaten

## Name

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | FIM-interner Name des Datenfelds, sichtbar für alle Anwender eines Redaktionssystems des Bausteins Datenfelder (fachliche Ersteller, Methodenexperten, Informationsmanager, FIM-Koordinierungsstelle, FIM-Nutzer).|
| Zielgruppe      | Modellierende |
| Pflichtfeld | Ja|
| Elementspezifisches Qualitätskriterium |<ul><li>Der Name soll prägnant sein.</li><li>Doppelbenennungen sind zu vermeiden.</li><li>Namen sollen nicht als Frage formuliert sein.</li><li>Namen sollten nicht als Teilsatz formuliert sein.</li><li>Vorgaben durch eine Rechtsnorm sind umzusetzen.</li><li>Vorgaben durch einen Standard sind zu berücksichtigen und ggf. zu übernehmen oder begründet abzuändern.</li><li>Der Name soll grundsätzlich keine expliziten Bezüge zum Kontext herstellen. So soll sich der Name erst mit Hilfe der Zugehörigkeit zu einer Datenfeldgruppe in Kontext bringen lassen.</li><li>Eine Notwendigkeit, den Kontext auch im Feldnamen anzugeben, ergibt sich etwa bei fachspezifischen Datenfeldern, die eigenständig in einem Stammdatenschema außerhalb einer Datenfeldgruppe verwendet werden. Darüber hinaus kann es Fälle geben, bei denen eine Datenfeldgruppe den Kontext nur ungenügend spezifiziert.</li><li>Falls im Namen ein Wort mit mehreren gänzlich unterschiedlichen Bedeutungen verwendet werden soll (Homonym), ist derjenige Begriff mit der weitläufigsten Verbreitung als Grundform zu definieren (Beispiel: Schloss – das Bauwerk und Schloss – das Türschloss). Die weiteren Verwendungen dieses Wortes sind unter Angaben eines Bezugs in Klammern festzulegen.</li></ul>|
| Empfehlung für die maximale Länge |255 Zeichen|
| Art der Prüfung     | Exemplarisch manuell |
| Verweis auf den QS-Bericht      | 1002 |
| Beispiel      |<ul><li>Augenfarbe <ul><li>Positives Beispiel eines fachspezifischen Datenfelds einer Datenfeldgruppe mit ungenügend spezifiziertem Kontext: Farbe der Augen. </li><li>Ein negatives Beispiel in diesem Fall ist: Farbe. Hier besteht Verwechslungsgefahr mit z. B. Hautfarbe.</li></ul></li><li>Datenschutz<ul><li>Positives Beispiel eines fachspezifischen Datenfelds, welches außerhalb einer Datenfeldgruppe verwendet werden kann: Datenschutzrechtlicher Hinweis.</li><li>Ein negatives Beispiel in diesem Fall ist: Hinweis.</li></ul></li><li> Positives Beispiel eines fachübergreifenden Datenfelds: Geburtsort<br/>In diesem Beispiel wurde der Kontext stärker expliziert, da der Name Ort zu unspezifisch wäre. Zudem wird der Geburtsort nicht immer im Kontext einer Datenfeldgruppe mit Geburtsangaben verwendet.</li><li>Beispiel für ein Homonym: Titel - wird als Grundform verwendet mit der Begriffsbedeutung: Ein vorangestellter Titel wird häufig im Zusammenhang mit Namen verwendet, ist aber kein originärer Bestandteil des Namens. Eine deutlich spezifischere Verwendung des Begriffs ist in der Rechtswissenschaft zu finden: Titel (Recht) - wird als ein verbrieftes Recht verstanden. </li></ul>|
|mögliche Abweichungen im Referenzkontext| nein |

## Definition

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Fasst den Inhalt des Datenfelds aus redaktioneller Sicht zusammen, d. h. aus Sicht der Rollen Ersteller, Methodenexperte, Informationsmanager, FIM- Koordinierungsstelle, FIM-Nutzer. Eine Definition muss einen Mehrwert bieten, ansonsten sollte sie weggelassen werden.|
| Zielgruppe      | Modellierende, Fachlichkeit |
| Pflichtfeld | Nein|
| Elementspezifisches Qualitätskriterium |<ol><li>Datenfelder sollen möglichst (kontext-)spezifisch definiert sein.</li><li>Falls vorhanden, soll die Definition aus einer Rechtsnorm oder aus einem Standardisierungsverfahren (wie z. B. XÖV) übernommen werden bzw. diese sollte der Definition zur Grundlage dienen. Abgeleitete Definitionen sollen wörtlich übernommen werden. Müssen diese angepasst werden, sind die Anpassungen (im Dokument XYZ oder Attribut ABC) deutlich zu machen und zu begründen.</li><li>Nichtssagende Floskeln und allgemein gültige Aussagen, die einen Sachverhalt doppelt wiedergeben (Tautologien) sind zu vermeiden.</li><li>Es sollte sich um eine kurze, präzise und eindeutige Erklärung des Feldes handeln.</li><li>Definition, die sich an verwaltungsinterne Nutzer/Formularentwickler richten, sollten entsprechende Fachsprache verwenden.</li><li>Für Datenfelder an denen eine (kurze) Codeliste hängt, dient es der Übersicht die entsprechenden Codelisteneinträge zu erwähnen.</li></ol>|
| Empfehlung für die maximale Länge |5000 Zeichen|
| Art der Prüfung     | Exemplarisch manuell |
| Verweis auf den QS-Bericht      | x |
| Beispiel |<ol><li>Datenfeld Familienname: "Der frühere Familienname einer natürlichen Person entspricht dem Familiennamen einer Person vor der Schließung einer neuen Ehe bzw. dem Eingehen einer neuen Lebenspartnerschaft."</li><li>Datenfeld Titel: "Ein vorangestellter Titel wird häufig im Zusammen- hang mit Namen verwendet, ist aber kein originärer Bestandteil des Namens. Im Unterschied dazu gehören Adelstitel zum Familiennamen und sind daher in diesem Verständnis kein vorangestellter Titel. Zu den vorangestellten Titeln zählen beispielsweise akademische Grade wie Doktortitel."</li><li>Datenfeld „Abfrage Wohnort des Kindes“: lebt beim antragstellenden Elternteil / lebt beim anderen Elternteil / absolviert ein Auslandschuljahr / lebt ausbildungsbedingt nicht im elterlichen Haushalt / lebt im Heim bzw. in einer Pflegestelle / lebt bei einer sonstigen Person / ist in Haft.</li><li>Floskeln, die in einer Definition nicht zu verwenden sind: „bereits schon“, „nie und nimmer“, …</li></ol> |
|mögliche Abweichungen im Referenzkontext| :bomb: ja |

## Bezug zur Handlungsgrundlage

:::info
Die Namensgebung zu diesem Metadatum hat sich geändert, in XDatenfelder 2 hieß dieses Metadatum noch **Bezug zu Rechtsnorm oder Standardisierungsvorhaben**.
:::

### Bezug zur Handlungsgrundlage 

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Dieses Attribut ermöglicht es, Bezüge zu einschlägigen Rechtsnormen, Verordnungen oder Richtlinien zu dokumentieren. Auch ein Bezug zu XÖV-Vorhaben (oder ähnlichem) und den entsprechenden Datencontainern kann hier gesetzt werden, sowie die Nutzung dieses Standards rechtlich nachweisbar ist. Aus dem Bezug zur Rechtsnorm ergibt sich die Gestaltung des Datenfeldes.|
| Zielgruppe      | Modellierende, Fachlichkeit |
| Pflichtfeld | **Ja** / Nein, aber trotzdem wünschenswert für harmonisierte Elemente.|
| Elementspezifisches Qualitätskriterium |<ol><li>Bei Datenfeldern, deren Strukturelementart „rechtsnormgebunden“ ist, ist dieses Metadatum ein Pflichtfeld, bei „harmonisierten“ Datenfeldern ist es keines. </li><li>Falls die Bezugsquelle der Rechtsnorm oder des Standardisierungsvorhabens eine eigene Notation vorsieht, dann ist diese zu befolgen.</li><li>Bei Angaben zu einem Bezug sollen diese möglichst konkret sein.</li><li>Die [Regeln zur Erfassung von Handlungsgrundlagen](EA_Handlungsgrundlagen) sind zu beachten.</li><li>Rechtsnormen sind insbesondere dann anzugeben, wenn diese die rechtliche Grundlage für die Erstellung und die Definition des Datenfelds darstellen.</li><li>Der Bezug zur Handlungsgrundlage sollte nach Möglichkeit bei harmonisierten Datenfeldern mit einer Version versehen werden, damit man einen sicheren (zeitlichen) Bezugspunkt für die Gültigkeit der Rechtsgrundlage hat.</li><li>Bei mehreren Handlungsgrundlagen an einem Datenfeld, sollte man diese mit Strichpunkt voneinander trennen.</li></ol>|
| Empfehlung für die maximale Länge |255 Zeichen pro Bezug; ein Element kann davon mehrere haben.|
| Art der Prüfung     | Exemplarisch manuell |
| Verweis auf den QS-Bericht      |  |
| Beispiel      |<ol><li>Datenfeld „Geschäftsbezeichnung / Organisationsbezeichnung“: XGewerbeanzeige.Betrieb.geschaeftsbezeichnung Version 2.2</li><li>Datenfeld „IBAN“: ISO 13616-1:2007: Financial services - International bank account number (IBAN) — Part 1: Structure of the IBAN</li><li>Datenfeld „Ärztliche Bescheinigung“: § 3 (6) BÄO</li></ol>|
|mögliche Abweichungen im Referenzkontext| :bomb: ja |

### Link :new:

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Dieses Attribut ermöglicht es, zu jedem Bezug einen entsprechenden Link anzugeben.|
| Zielgruppe | Modellierende, Fachlichkeit |
| Pflichtfeld | Nein |
| Elementspezifisches Qualitätskriterium | <ol><li>Es ist immer ein gültiger Link zu erfassen.</li><li>Eine gute Quelle für Links zu Rechtsgrundlagen finden sich beispielsweise unter: [Gesetze im Internet](https://www.gesetze-im-internet.de/)</li><li>Eine gute Quelle für Links zu XÖV-Standards findet sich unter: [XRepository 3.0](https://www.xrepository.de/) </li><li>Es sind keine unpassenden Links zu ergänzen.</li></ol>|
| Empfehlung für die maximale Länge |255 Zeichen|
| Art der Prüfung     | Exemplarisch manuell |
| Verweis auf den QS-Bericht      | x |
| Beispiel      |<ol><li>Datenfeld “meldepflichtige Krankheiten” basierend auf § 6 IfSG <br/> Link: https://www.gesetze-im-internet.de/ifsg/__6.html</li><li>Datenfeld “Ort der wirtschaftlichen Tätigkeit” Xunternehmen.Kerndatenmodell.Ort der wirtschaftlichen Tätigkeit.Art Version 1.0; basierend auf Codeliste urn:&ZeroWidthSpace;xoev-de:xunternehmen:codeliste:artortwirtschaftlichetaetigkeit_1 Version 1 <br/> Link: https://www.xrepository.de/details/urn:xoev-de:xunternehmen:standard:basismodul</li></ol> |
|mögliche Abweichungen im Referenzkontext| nein |

## Strukturelementart

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Mit Hilfe einer Klassifizierung nach Arten soll es möglich sein, zwischen harmonisierten und nicht harmonisierten Elementen bzw. nach geeigneten und ungeeigneten Elementen für die Zusammensetzung von Referenz- und Stammdatenschemata zu unterscheiden.|
| Zielgruppe      | Modellierende |
| Pflichtfeld | Ja, ein geeigneter Defaultwert ist: **rechtsnormgebunden**|
| Elementspezifisches Qualitätskriterium |<ol><li>Es ist immer eine der vordefinierten Strukturelementarten auszuwählen.</li><li>Es existieren zwei Arten von Feldern: harmonisierte (fachübergreifend oder fachspezifisch) oder rechtsnormgebundene (nicht harmonisierbar).</li><li>Rechtsnormgebundene Felder können aber auch zu einem späteren Zeitpunkt noch harmonisiert werden.</li><li>Die Strukturelementart “abstrakt” ist nur für Gruppen anzuwenden.</li></ol>|
| Empfehlung für die maximale Länge |nicht relevant|
| Art der Prüfung | keine, (darf nicht abstrakt sein) |
| Verweis auf den QS-Bericht      | [1031](./QS_methodische.md#1031-unbekannte-strukturelementart) |
| Beispiel |Rechtsnormgebunden |
|mögliche Abweichungen im Referenzkontext| :bomb: ja |

## Feldart :sparkles:

:::info

Im Standard XDatenfelder 3 sind neue Feldarten hinzugekommen: **versteckt** und **gesperrt**.

:::

<br/>

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Die Feldart bezeichnet, um welche Art von Datenfeld es sich handelt. Sie gibt Auskunft darüber, ob das Feld eine Eingabe erwartet, oder auch nicht. Zusammen mit dem Datentyp wird damit eindeutig die Verwendung eines Datenfeldes spezifiziert.|
| Zielgruppe      | Modellierende, Dienstleister nachnutzender Systeme |
| Pflichtfeld | Ja, ein geeigneter Defaultwert ist: **Eingabe**.|
| Elementspezifisches Qualitätskriterium |<ol><li>Es ist immer eine der im Standard vordefinierten Feldarten anzuwenden.</li><li> Die Kombination aus Feldart und Datentyp muss einer gültigen Kombination entsprechen. Siehe: [Was sind die Metadaten Feldart und Datentyp?](./DF_Allgemeines.md#was-sind-die-metadaten-feldart-und-datentyp-)</li></ol> |
| Empfehlung für die maximale Länge |nicht relevant|
| Art der Prüfung     | Exemplarisch manuell |
| Verweis auf den QS-Bericht      | [1027](./QS_methodische.md/#1027-unbekannte-feldart) |
| Beispiel      |<ol><li>Eingabe</li><li>Auswahl</li><li>Statisch</li></ol>|
|mögliche Abweichungen im Referenzkontext| nein |

## Datentyp :sparkles:

:::info

Im Standard XDatenfelder 3 sind neue Datentypen hinzugekommen: **String.Latin+**, **Zeit** und **Zeitpunkt**.

:::

<br/>

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Der Datentyp bezeichnet die Zusammenfassung konkreter Wertebereiche und ggf. darauf definierten Operationen zu einer Einheit, die mit Hilfe von Regeln abbildbar sind. Zudem spezifiziert der Datentyp die Struktur des Inhalts oder gibt Aufschlüsse über die Feldinhaltsänderbarkeit. <br/>Zusammen mit der Feldart wird damit eindeutig die Verwendung eines Datenfeldes spezifiziert.|
| Zielgruppe      | Modellierende, Dienstleister nachnutzender Systeme |
| Pflichtfeld | Ja, ein geeigneter Defaultwert ist: **Text**|
| Elementspezifisches Qualitätskriterium |<ol><li>Es ist immer einer der im Standard vordefinierten Datentypen anzuwenden.</li><li> Die Kombination aus Feldart und Datentyp muss einer gültigen Kombination entsprechen. Siehe: [Was sind die Metadaten Feldart und Datentyp?](./DF_Allgemeines.md#was-sind-die-metadaten-feldart-und-datentyp-)</li></ol>|
| Empfehlung für die maximale Länge |nicht relevant|
| Art der Prüfung     | Exemplarisch manuell |
| Verweis auf den QS-Bericht      | [1028](./QS_methodische.md#1028-unbekannter-datentyp) |
| Beispiel      |<ol><li>Text – in Kombination mit der Feldart Eingabe ergibt sich ein Texteingabefeld für „Vornamen“ oder auch „Familienname“</li><li>Objekt – in Kombination mit der Feldart Eingabe ergibt sich ein Eingabefeld, welches einen Dienststempel abbilden soll oder auch ein Unterschriftsfeld.</li></ol>|
|mögliche Abweichungen im Referenzkontext| nein |

## Inhalt

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Dieses Metadatum dient dazu, den Inhalt eines Datenfeldes zu spezifizieren. Dies kann einerseits ein vordefinierter (Default-)Wert sein z. B. für ein Textfeld oder auch ein statischer Text, der direkt angezeigt wird.|
| Zielgruppe | Modellierende, Fachlichkeit, Dienstleister nachnutzender Systeme, Antragstellende (für statische Textfelder) |
| Pflichtfeld | Ja, wenn es sich um ein Feld mit der Feldart „statisch“ und dem Datentyp „Text“ handelt. / Nein, in allen anderen Fällen.|
| Elementspezifisches Qualitätskriterium |Bei Rechtsbelehrungen oder ähnlichen Vorgaben, die sich aus Handlungsgrundlagen ergeben, ist der Bezug über das entsprechende Metadatum festzulegen.|
| Empfehlung für die maximale Länge |5000 Zeichen|
| Art der Prüfung     | Komplett manuell |
| Verweis auf den QS-Bericht      | Bezüglich Präzisierungsangaben: [1069](./QS_methodischeII.md#1069-der-inhalt-muss-eine-zahl-sein), [1070](./QS_methodischeII.md#1070-der-inhalt-muss-eine-ganze-zahl-sein), [1071](./QS_methodischeII.md#1071-der-inhalt-muss-ein-datum-sein), [1072](./QS_methodischeII.md#1072-der-inhalt-unterschreitet-die-untere-wertgrenze), [1073](./QS_methodischeII.md#1073-der-inhalt-überschreitet-die-obere-wertgrenze), [1074](./QS_methodischeII.md#1074-der-inhalt-unterschreitet-die-minimallänge), [1075](./QS_methodischeII.md#1075-der-inhalt-überschreitet-die-maximallänge), [1076](./QS_methodischeII.md#1076-der-inhalt-stimmt-nicht-mit-der-zugeordneten-codeliste-überein), [1077](./QS_methodischeII.md#1077-der-inhalt-muss-ein-wahrheitswert-sein-true-oder-false), [1083](./QS_methodischeIII.md#1083-der-inhalt-muss-ein-zeitpunkt-datum-und-uhrzeit-sein), [1084](./QS_methodischeIII.md#1084-der-inhalt-muss-eine-uhrzeit-sein) und [1085](./QS_methodischeIII.md#1085-der-inhalt-muss-ein-code-wert-der-werteliste-sein). <br/> Für statische Textfelder: [1109](./QS_methodischeIII.md#1109-bei-einem-feld-mit-nur-lesendem-zugriff-der-feldart-statisch-wird-i-d-r-der-inhalt-mit-einem-text-befüllt)|
| Beispiel      | <ol><li>Datenfeld „Hinweis zu unvollständigen Angaben (Wohngeld)“: Falls Sie den Wohngeldantrag unvollständig absenden, bestätigt die Wohngeldbehörde lediglich den Eingang zur Fristwahrung. Eine Entscheidung über den Antrag kann erst bei Vollständigkeit erfolgen.</li><li>(Default-)Wert für ein Datenfeld des Datentyps Wahrheitswert: falsch </li><li>(Default-)Wert für ein Datenfeld der Feldart Auswahl: 001</li></ol>
|mögliche Abweichungen im Referenzkontext| nein |

## Präzisierungsangaben

### Pattern

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition |Dieses Metadatum dient dazu, die Form der erwarteten Eingabe des Bürgers für ein Datenfeld in einem nachnutzenden System näher zu spezifizieren.|
| Zielgruppe      |Modellierende, Dienstleister nachnutzender Systeme |
| Pflichtfeld | Nein|
| Elementspezifisches Qualitätskriterium |Dieses Metadatum findet nur im Zusammenhang mit einem Datenfeld der Feldart **Eingabe** und des Datentyps **Text**, **String.Latin+**, **Nummer** oder **Ganzzahl** Anwendung. <br/> Siehe dazu auch: [Präzisierung von Datenfeldern](DF_Allgemeines#präzisierung-von-datenfeldern). <br/>Ein Pattern ist gemäß des RegEx-Syntaxes für HTML bzw. JavaScript zu erstellen. <br/> Das eingegebene Pattern ist vorher unbedingt auf seine Richtigkeit zu prüfen, da es sonst zu Ausführungsfehlern in einem nachnutzenden System kommen könnte. Bei Unsicherheit kann statt eines Patterns auch eine Regel für dieses Feld formuliert werden.|
| Empfehlung für die maximale Länge |255 Zeichen|
| Art der Prüfung     | keine |
| Verweis auf den QS-Bericht      |  |
| Beispiel      |<ol><li>Datenfeld „Postleitzahl“ enthält einen Pattern für eine deutsche Postleitzahl: <br/> \(\[0]{1}\[1-9]{1}\|\[1-9]{1}\[0-9]{1})[0-9]{3} <br/> In Worten: Eine “0” gefolgt von einer Ziffer von  “1” bis “9” ODER eine “1” gefolgt von einer Ziffer von “0” bis “9” UND drei Ziffern, jeweils von “0” bis “9”.</li><li>Alternative: Datenfeld „Postleitzahl“ ist mit einer Regel verknüpft mit folgender Definition „Bitte sicherstellen, dass in diesem Feld nur korrekte Deutsche Postleitzahlen angegeben werden können“.</li></ol>|
|mögliche Abweichungen im Referenzkontext| nein |

### Minimale bzw. maximale Länge

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Dieses Metadatum dient dazu, die Länge der erwarteten Text-Eingabe des Bürgers für ein Datenfeld in einem nachnutzenden System näher zu spezifizieren.|
| Zielgruppe      | Modellierende, Dienstleister nachnutzender Systeme |
| Pflichtfeld | Nein|
| Elementspezifisches Qualitätskriterium |Dieses Metadatum findet nur im Zusammenhang mit einem Datenfeld der Feldart **Eingabe** und des Datentyps **Text** oder **String.Latin+** Anwendung. <br/> Siehe dazu auch: [Präzisierung von Datenfeldern](DF_Allgemeines#präzisierung-von-datenfeldern).<br/> Falls es eine Vorgabe durch eine Handlungsgrundlage gibt, ist diese zu berücksichtigen. Die Eingabe erfolgt **NUR** in Form von Ganzzahlen. Es kann auch nur eine der beiden Angaben gemacht werden.|
| Empfehlung für die maximale Länge |20 Zeichen|
| Art der Prüfung     | wird nicht geprüft |
| Verweis auf den QS-Bericht      | [1012](./QS_methodische.md#1012-bei-datenfeldern-mit-der-feldart-auswahlfeld-oder-statisches-read-only-feld-dürfen-weder-die-minimale-noch-die-maximale-feldlänge-angegeben-werden), [1015](./QS_methodische.md#1015-eine-feldlänge-darf-nur-bei-einem-datenfeld-mit-dem-datentyp-text-oder-stringlatin-angegeben-werden), [1059](./QS_methodischeII.md#1059-die-minimale-feldlänge-muss-eine-ganze-zahl-größer-oder-gleich-null-sein), [1060](./QS_methodischeII.md#1060-die-maximale-feldlänge-muss-eine-ganze-zahl-größer-null-sein), [1061](./QS_methodischeII.md#1061-die-minimale-feldlänge-darf-nicht-größer-sein-als-die-maximale-feldlänge), [1102](./QS_methodischeIII.md#1102-bei-eingabedatenfeldern-mit-dem-datentyp-text-oder-stringlatin-sollten-wenn-möglich-die-minimale-und-maximale-feldlänge-angegeben-werden)|
| Beispiel      |<ol><li>Datenfeld „Postleitzahl“ mit der Feldart Eingabe hat eine minimale Länge von fünf und eine maximale Länge von fünf.</li><li>Datenfeld „Eingetragener Name / Organisationsname“ mit der Feldart Eingabe hat eine minimale Länge von einem und eine maximale Länge von 1000 Zeichen. In diesem Fall ist es wichtig darauf zu achten, dass der Hinweis woher diese Vorgabe stammt (XGewerbeanzeige.Betrieb.eingetragenerName) in der Beschreibung zu diesem Feld hinterlegt wird.</li><li>Datenfeld “Anzahl der Kinder” mit der Feldart Eingabe und dem Datentyp Ganzzahl hat einen minimalen Wert von 0 und keine Beschränkung des maximalen Werts. Mit der Beschränkung für den minimal Wert verhindert man, dass eine negative Anzahl von Kindern erfasst werden kann.</li></ol> |
|mögliche Abweichungen im Referenzkontext| nein |

### Minimaler bzw. maximaler Wert

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Dieses Metadatum dient dazu, den erwarteten Wertebereich der Eingabe des Bürgers für ein Datenfeld in einem nachnutzenden System näher zu spezifizieren.|
| Zielgruppe      | Modellierende, Dienstleister nachnutzender Systeme |
| Pflichtfeld | Nein|
| Elementspezifisches Qualitätskriterium |Dieses Metadatum findet nur im Zusammenhang mit einem Datenfeld der Feldart **Eingabe** und des Datentyps **Dezimalzahl**, **Ganzzahl**, **Geldbetrag**, **Datum**, **Zeit** oder **Zeitpunkt** Anwendung.<br/> Siehe dazu auch: [Präzisierung von Datenfeldern](DF_Allgemeines#präzisierung-von-datenfeldern).<br/> Falls es eine Vorgabe durch eine Handlungsgrundlage gibt, ist diese zu berücksichtigen. <ul><li>Die Eingaben für **Dezimalzahl**, **Ganzzahl** oder **Geldbetrag** sollten NUR in Form von Ganzzahlen erfolgen.</li><li>Die Eingaben für **Datum** und **Zeit** sollten sich an die Norm [ISO 8601](https://de.wikipedia.org/wiki/ISO_8601) halten, d. h. ein **Datum** ist in der Form YYYY-MM-DD einzugeben (Beispiel: 2024-02-24). Für **Zeit** gilt das Eingabeformat hh:mm:ss (Beispiel 14:12:10).</li><li>Die Eingabe für den **Zeitpunkt** ist eine Kombination aus den beiden zuvor genannten Angaben für **Datum** und **Zeit**. Eine Eingabe für den **Zeitpunkt** hat laut ISO 8601 das Format YYYY-MM-DDThh:mm:ss (Beispiel: 2024-06-08T13:10:42).</li><li>Es kann auch nur eine der beiden Angaben gemacht werden; d. h. es kann auch nur ein minimaler oder auch nur ein maximaler Wert angegeben werden.</li></ul>|
| Empfehlung für die maximale Länge |20 Zeichen|
| Art der Prüfung     |  |
| Verweis auf den QS-Bericht      | wird nicht geprüft |
| Beispiel      |<ol><li>Datenfeld „Monat“ vom Datentyp Ganzzahl hat einen minimalen Wert von eins und einen maximalen Wert von zwölf.</li><li>Datenfeld „Tag“ vom Datentyp Ganzzahl hat einen minimalen Wert von eins und einen maximalen Wert von 31.</li><li>Datenfeld „Datum“ vom Datentyp Datum hat einen minimalen Wert von 2000-01-01 und einen maximalen Wert von 2100-12-31.</li></ol> |
|mögliche Abweichungen im Referenzkontext| nein |

### Maximale Dateigröße :new:

:::info

Die maximale Dateigröße wird laut der Spezifikation zum Standard XDatenfelder 3 in Byte erfasst. Es kann sein, dass Ihr Editor zur besseren Übersicht die Angabe in Megabyte oder einem anderen Format einfordert.

:::

<br/>

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Dieses Metadatum dient dazu, eine maximale Größe für Dateien von Analgen festzulegen, die in einem nachnutzenden System bei einem Dateiupload nicht überschritten werden kann.|
| Zielgruppe      | Modellierende, Dienstleister nachnutzender Systeme |
| Pflichtfeld | Nein|
| Elementspezifisches Qualitätskriterium |Dieses Metadatum findet nur im Zusammenhang mit einem Datenfeld der Feldart **Eingabe** und des Datentyps **Anlage** Anwendung.<br/>Siehe dazu auch: [Präzisierung von Datenfeldern](DF_Allgemeines#präzisierung-von-datenfeldern).<br/>Falls es eine Vorgabe durch eine Handlungsgrundlage gibt, ist diese zu berücksichtigen. |
| Empfehlung für die maximale Länge |20 Zeichen|
| Art der Prüfung     | wird nicht geprüft |
| Verweis auf den QS-Bericht      | x |
| Beispiel      |Datenfeld „Geburtsurkunde“ vom Datentyp Anlage hat eine maximale Dateigröße von 5MB. |
|mögliche Abweichungen im Referenzkontext| nein |

### Medientyp :new:

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Dieses Metadatum dient dazu den Medientyp für Datenfelder vom Datentyp Analgen festzulegen. Nur die angegebenen Medientypen dürfen an dieser Stelle angehängt werden und ein nachnutzenden System muss dies überprüfen.|
| Zielgruppe      | Modellierende, Dienstleister nachnutzender Systeme |
| Pflichtfeld | Nein|
| Elementspezifisches Qualitätskriterium |Dieses Metadatum findet nur im Zusammenhang mit einem Datenfeld der Feldart **Eingabe** und des Datentyps **Anlage** Anwendung.<br/> Siehe dazu auch: [Präzisierung von Datenfeldern](DF_Allgemeines#präzisierung-von-datenfeldern).<br/>Falls es eine Vorgabe durch eine Handlungsgrundlage gibt, ist diese zu berücksichtigen.<br/>Es ist ein gültiger Medientyp gemäß des IANA Registrys (https://www.iana.org/assignments/media-types/mediatypes.xhtml) zu erfassen.|
| Empfehlung für die maximale Länge |nicht relevant|
| Art der Prüfung     | wird nicht geprüft |
| Verweis auf den QS-Bericht      | x |
| Beispiel      |Datenfeld „Geburtsurkunde“ vom Datentyp Anlage wird als PDF oder Foto spezifiziert.|
|mögliche Abweichungen im Referenzkontext| nein |

## Verweis auf Codeliste

### Codelisten Referenz :new:

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Dieses Metadatum dient dazu, eine Referenz auf eine Codeliste zu setzen, die an externer Stelle - dem XRepository - gepflegt wird.|
| Zielgruppe      |Modellierende, Fachlichkeit, Dienstleister nachnutzender Systeme |
| Pflichtfeld | Ja, bei Datenfeldern der Feldart **Auswahl** und des Datentyps **Text** muss immer eine Code- oder Werteliste zugewiesen oder gepflegt werden. / Nein, das Metadatum darf in allen anderen Fällen nicht gepflegt werden.|
| Elementspezifisches Qualitätskriterium |Es muss eine gültige Codeliste zugewiesen werden. Die Referenz wird in Form der Codelisten Kennung an einem Datenfeld gepflegt. Dabei macht es einen entscheidenden Unterschied, ob eine Kennung mit oder ohne Versionskennung referenziert wird. Im Kontext ist eine Festlegung zu treffen, auf welche Version der Codeliste sich die Versionskennungsfreie Kennung bezieht. Sollte im entsprechenden Kontext keine anderweitige Festlegung existieren, so bezeichnet die Versionskennungsfreie Kennung stets die aktuellste Version der Codeliste.|
| Empfehlung für die maximale Länge |nicht relevant|
| Art der Prüfung     | automatisiert |
| Verweis auf den QS-Bericht      | x |
| Beispiel      |<ol><li>urn:&ZeroWidthSpace;de:bund:destatis:bevoelkerungsstatistik:schluessel:staatsangehoerigkeit</li><li>urn:&ZeroWidthSpace;de:bund:destatis:bevoelkerungsstatistik:schluessel:staatsangehoerigkeit_2021-02-19</li></ol>|
|mögliche Abweichungen im Referenzkontext| nein |

### Code Key :new:

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Dieses Metadatum dient dazu, eine Referenz auf die Spalte einer Codeliste zu setzen, die den Code Key enthält.|
| Zielgruppe      | Modellierende, Dienstleister nachnutzender Systeme |
| Pflichtfeld | Nein, aber es muss immer auch ein Name Key erfasst werden, wenn ein Code Key gepflegt wird.|
| Elementspezifisches Qualitätskriterium |Es muss ein Spaltenname einer gültigen Codeliste angegeben werden. Dieser Spaltenname entspricht dem technischen Schlüssel einer Spalte.<br/> Die Angabe eines Code Keys ist sinnvoll, wenn ein Bezug zu einer konkreten Version einer Codeliste vorliegt.|
| Empfehlung für die maximale Länge |20 Zeichen|
| Art der Prüfung     |nicht |
| Verweis auf den QS-Bericht      | x |
| Beispiel      |Bezüglich der Codeliste urn:&ZeroWidthSpace;de:bund:destatis:bevoelkerungsstatistik:schluessel:staatsangehoerigkeit_2023-02-24 wählt man die Spalte: DESTATIS-Schluessel-Staatsangehoerigkeit |
|mögliche Abweichungen im Referenzkontext| nein |

### Name Key :new:

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Dieses Metadatum dient dazu, eine Referenz auf die Spalte einer Codeliste zu setzen, die den Name Key enthält.|
| Zielgruppe      | Modellierende, Dienstleister nachnutzender Systeme, Antragstellende  |
| Pflichtfeld | Nein, aber es muss immer auch ein Code Key erfasst werden, wenn ein Name Key gepflegt wird.|
| Elementspezifisches Qualitätskriterium |Es muss ein Spaltenname einer gültigen Codeliste angegeben werden. Dieser Spaltenname entspricht dem technischen Schlüssel einer Spalte.<br/> Die Angabe eines Name Keys ist sinnvoll, wenn ein Bezug zu einer konkreten Version einer Codeliste vorliegt.|
| Empfehlung für die maximale Länge |20 Zeichen|
| Art der Prüfung     | nicht |
| Verweis auf den QS-Bericht      | x |
| Beispiel      |Bezüglich der Codeliste urn:&ZeroWidthSpace;de:bund:destatis:bevoelkerungsstatistik:schluessel:staatsangehoerigkeit_2023-02-24 wählt man die Spalte: Staatsname-kurz |
|mögliche Abweichungen im Referenzkontext| nein |

### Help Key :new:

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Dieses Metadatum dient dazu, eine Referenz auf die Spalte einer Codeliste zu setzen, die den Help Key enthält.|
| Zielgruppe      | Modellierende, Dienstleister nachnutzender Systeme, Antragstellende   |
| Pflichtfeld | Nein|
| Elementspezifisches Qualitätskriterium |Es muss ein Spaltenname einer gültigen Codeliste angegeben werden. Dieser Spaltenname entspricht dem technischen Schlüssel einer Spalte. <br/> Die Angabe eines Help Keys ist sinnvoll, wenn ein Bezug zu einer konkreten Version einer Codeliste vorliegt.|
| Empfehlung für die maximale Länge |20 Zeichen|
| Art der Prüfung     | nicht |
| Verweis auf den QS-Bericht      | x |
| Beispiel      |Bezüglich der Codeliste urn:&ZeroWidthSpace;de:bund:destatis:bevoelkerungsstatistik:schluessel:staatsangehoerigkeit_2023-02-24 wählt man die Spalte: Staatsname-voll |
|mögliche Abweichungen im Referenzkontext| nein |

## Wertelisten

### Code / Schlüssel :new:

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Der Code (oder auch Schlüssel) ist Teil des Eintrags einer Werteliste. Er dient der einfachen Zuordnung von Einträgen einer Werteliste. |
| Zielgruppe      | Modellierende, Dienstleister nachnutzender Systeme  |
| Pflichtfeld | Nein, aber ohne diese Angaben hat die Werteliste keinen Inhalt.|
| Elementspezifisches Qualitätskriterium |<ol><li>Für einen Code von Wertelisten sind möglichst kurze und prägnante Codes zu verwenden.</li><li>Ein Code muss eindeutig sein und darf nicht mehrfach verwendet werden.</li><li>Alle Codes sollten nach Möglichkeit alphanummerisch sein, empfohlen wird eine Nummerierung in der Form 001 für den ersten Eintrag, 002 für den zweiten, 003, … 998.</li><li>Der Code 999 ist immer dem Codelisteneintrag Sonstige/s vorenthalten.</li><li>Ein Code kann nicht-nummerisch sein, so wie es sinnvoll ist. Dies ist nur dann sinnvoll, wenn der Code bereits etablierten Kürzeln entspricht.</li></ol>|
| Empfehlung für die maximale Länge | 20 Zeichen |
| Art der Prüfung     | Exemplarisch manuell |
| Verweis auf den QS-Bericht      | x |
| Beispiel      |<ol><li>Code: 001 Name: Inland; Code: 002 Name: Ausland</li><li>Merkzeichen Schwerbehindertenausweis Code: H Name: Merkzeichen H (hilflos); Code BI Name: Merkzeichen BI (blind); …</li><li>Fahrerlaubnisklassen Code: AM, Name: Leichte zweirädrige Kleinkrafträder der Klasse L1e-B (Mopeds); Code: B, Name: Kraftfahrzeuge (außer solche der Klassen AM, A1, A2 und A); … </li></ol>|
|mögliche Abweichungen im Referenzkontext| nein |

### Name / Wert :new:

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Der Name (oder auch Wert) ist Teil des Eintrags einer Werteliste. Er ist immer einem Code zugeordnet.|
| Zielgruppe      | Modellierende, Dienstleister nachnutzender Systeme, Antragstellende  |
| Pflichtfeld | Nein, aber ohne diese Angaben hat die Werteliste keinen Inhalt.|
| Elementspezifisches Qualitätskriterium |<ol><li>Der Name eines Codelisteneintrags muss einzigartig sein.</li><li>Hier ist besonders auf die Sprachhinweise vom Anfang des Dokumentes zu achten (Gendering, Singular/Plural, …). </li></ol>|
| Empfehlung für die maximale Länge |255 Zeichen|
| Art der Prüfung     | exemplarisch manuell |
| Verweis auf den QS-Bericht      | x |
| Beispiel      |<ol><li>Code: 001 Name: Inland; Code: 002 Name: Ausland</li><li>Merkzeichen Schwerbehindertenausweis Code: H Name: Merkzeichen H (hilflos); Code BI Name: Merkzeichen BI (blind); …</li><li>Fahrerlaubnisklassen Code: AM, Name: Leichte zweirädrige Kleinkrafträder der Klasse L1e-B (Mopeds); Code: B, Name: Kraftfahrzeuge (außer solche der Klassen AM, A1, A2 und A); … </li></ol>|
|mögliche Abweichungen im Referenzkontext| nein |

### Hilfe :new:

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Dieses Metadatum bietet die Möglichkeit zu jedem  Eintrag einer Werteliste einen expliziten Hilfetext zu hinterlegen.|
| Zielgruppe      | Modellierende, Dienstleister nachnutzender Systeme, Antragstellende  |
| Pflichtfeld | Nein|
| Elementspezifisches Qualitätskriterium |<ol><li>Beschreibung der Bedeutung des Eintrags einer Werteliste für den Endnutzer auf Inputformularen (Bürger/Unternehmen).</li><li>Hilfetext sollen bei der Auswahl des richtigen Wertes unterstützen. Dies kann beispielsweise durch eine Eintrag spezifische Erläuterung erfolgen.</li><li>Ein allgemeiner Hilfetext für die Werteliste sollte durch einen Hilfetext am Datenfeld selbst erfolgen.</li><li>Aktive Ansprache des Bürgers ist möglich. (“Wählen Sie diesen Wert aus, wenn… “.</li><li>Ist in den Einträgen der Werteliste Spezialvokabular verwendet worden, sollte es in der Hilfe erklärt werden; z. B. durch die Verwendung von Synonymen.</li><li>Fremdwörter sollten vermieden werden.</li><li>Hilfetexte können einen Hinweis liefern, welche Konsequenz die Auswahl eines bestimmten Wertes hat.</li><li>Hilfetexte dürfen nur angegeben werden, wenn Sie einen Mehrwert bieten. D. h. insbesondere, dass diese nicht identisch mit dem Namen / Wert der Werteliste sein sollten.</li><li>Es können auch nur für einzelne Einträge der Werteliste Hilfetexte ergänzt werden.</li></ol>|
| Empfehlung für die maximale Länge |255 Zeichen|
| Art der Prüfung     | exemplarisch manuell |
| Verweis auf den QS-Bericht      | x |
| Beispiel      |<ol><li>Code: 001 Name: Inland; Code: 002 Name: Ausland Hilfe: Wählen Sie diesen Eintrag aus, wenn Sie Ihre ausländische Meldeanschrift hinterlegen wollen.</li><li>Code: 999 Name: Sonstiges Hilfe: Wählen Sie diesen Wert nur aus, wenn keine der genannten Optionen auf Sie zutrifft.</li></ol>|
|mögliche Abweichungen im Referenzkontext| nein |

## Vorbefüllung :new:

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Dieses Metadatum dient dazu, zu klären, ob für dieses Datenfeld von der zuständigen Behörde / Stelle ein Vorgabewert getätigt werden muss. Siehe auch [Was ist das neue Metadatum Vorbefüllung und wie geht man damit um?](./DF_Allgemeines.md#was-ist-das-neue-metadatum-vorbefüllung-und-wie-geht-man-damit-um-)|
| Zielgruppe      | Modellierende, Fachlichkeit, Dienstleister nachnutzender Systeme |
| Pflichtfeld | Ja, ein geeigneter Defaultwert ist: **keine**|
| Elementspezifisches Qualitätskriterium |<ol><li>Es ist immer einer der vordefinierten Werte auszuwählen.</li><li>Die Vorbefüllung gibt an, ob dieses Feld durch die nachnutzende Behörde vorzubefüllen ist.</li><li>Mögliche Ausprägungen sind **keine**, **optional** oder **verpflichtend**.</li><li>Wenn **optional** ausgewählt wurde, müssen nachnutzende Systeme darauf achten, dass eine Vorbefüllung hinterlegt werden kann.</li><li>Wenn **verpflichtend** ausgewählt wurde, müssen nachnutzende Systeme darauf achten, dass eine Vorbefüllung hinterlegt werden muss.</li></ol>|
| Empfehlung für die maximale Länge |nicht relevant|
| Art der Prüfung     | Wird nicht geprüft. |
| Verweis auf den QS-Bericht      | [1030](./QS_methodische.md#1030-unbekannte-vorbefüllungsart) |
| Beispiel      |Ein Datenfeld "Flurstücknummer" erhält den Vorbefüllungswert **keine**.<br/> Ein Datenfeld "zuständige Behörde" erhält den Vorbefüllungswert **verpflichtend**.|
|mögliche Abweichungen im Referenzkontext| nein |

## Beschreibung

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Zusätzliche Beschreibung und Erläuterungen eines Datenfelds für FIM-Nutzer. Dies kann z. B. ein Hinweis an den Fachpaten bzw. ans Fachverfahren sein.|
| Zielgruppe      | Modellierende, Dienstleister nachnutzender Systeme |
| Pflichtfeld | Nein|
| Elementspezifisches Qualitätskriterium |Über die Beschreibung können Umsetzungshinweise zu einem Datenfeld ergänzt werden, außerdem können hier Hinweise an andere Modellierende hinterlegt werden.<br/>Vorstellbar sind Hinweise zu<ul><li>Codelisten und Quellen</li><li>Möglichkeit der Abkürzung</li><li>Zeichenbeschränkung</li><li>Notwendigkeit von verschiedenen Zeichensätzen</li><li>etc.</li></ul>|
| Empfehlung für die maximale Länge |5000 Zeichen|
| Art der Prüfung     | Wird nicht geprüft. |
| Verweis auf den QS-Bericht      | x |
| Beispiel      |<ol><li>Datenfeld „Datum (aktuelles)“: Falls technisch möglich, kann dieses Feld auch automatisch mit dem aktuellen Datum belegt werden. In diesem Fall sollte das Feld als read-only gestaltet werden.</li><li>Datenfeld „Staatsangehörigkeit“: Hier muss die 3. Spalte der Codeliste (Staatsangehörigkeit) angezeigt werden. Verwendete Codeliste: Staatsangehoerigkeit 2021-02-19</li><li>Datenfeld „Sterbeurkunde“: Falls die Informationen, die in der Sterbeurkunde stehen, direkt aus den Meldedaten abgerufen werden können, sollte dieser Nachweis nicht eingefordert werden.</li><li>Datenfeld „Familienname“: Laut BSI TR-03123 soll die Gesamtlänge für Familienname, Titel und Geburtsname nicht mehr als 120 Zeichen betragen. Laut PAuswV soll Name (Familienname und Geburtsname) nicht mehr als 2\*26 = 52 Zeichen bzw. 3\*40 = 120 Zeichen betragen.</li></ol>|
|mögliche Abweichungen im Referenzkontext| nein |

## Bezeichnung Eingabe

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Bezeichnung des Feldes, welche für den Bürger/das Unternehmen auf Formularen sichtbar ist.|
| Zielgruppe      | Modellierende, Fachlichkeit, Dienstleister nachnutzender Systeme, **Antragstellende** |
| Pflichtfeld | Ja|
| Elementspezifisches Qualitätskriterium |<ol><li>Die Bezeichnung Eingabe soll kurz und prägnant sein.</li><li>Die Bezeichnung Eingabe soll keine expliziten Bezüge zum Kontext herstellen, wenn dies nicht zwingend notwendig ist.</li><li>Die Bezeichnung Eingabe kann als Frage formuliert werden.</li><li>Einheiten sollen möglichst Teil der Bezeichnung Eingabe sein (z. B. Miete in Euro pro Monat).</li><li>Für Datenfelder, bei denen eine Codeliste hinterlegt ist, gilt: Die Bezeichnung muss mit den einzelnen Einträgen der Codeliste stimmig sein.</li></ol>|
| Empfehlung für die maximale Länge |255 Zeichen|
| Art der Prüfung     | komplett manuell |
| Verweis auf den QS-Bericht      | [1009](./QS_methodische.md#1009-die-bezeichnung-eingabe-muss-befüllt-werden) |
| Beispiel      |<ol><li>Positiv: Postfach</li><li>Negativ: Postfach Anschrift natürliche Person</li><li>Positiv: Bedürfnisgrund</li><li>Negativ: Bedürfnisgrund Waffe bzw. Munition (Freitext)</li><li>Positiv: Geben Sie an, über welches Kommunikationsmedium bzw. -mittel die Person erreichbar ist.</li><li>Negativ: Bitte geben Sie hier an, über welches Kommunikationsmedium bzw. -mittel die Person erreichbar ist.</li><li>Negativ (in Zusammenhang mit einer Codeliste): Das Kind: 001 wird vertreten durch einen Beistand / 002 wird vertreten durch einen Vormund / 999 Sonstiges</li><li>Positiv (in Zusammenhang mit einer Codeliste): Das Kind: 001 wird vertreten durch einen Beistand / 002 wird vertreten durch einen Vormund / 999 hat keine Vertretung durch einen Beistand oder Vormund </li></ol>|
|mögliche Abweichungen im Referenzkontext| nein |

## Bezeichnung Ausgabe

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Bezeichnung des Feldes, welche für den Bürger/das Unternehmen auf Formularen sichtbar ist.|
| Zielgruppe      | Modellierende, Fachlichkeit, Dienstleister nachnutzender Systeme, **Antragstellende** |
| Pflichtfeld | Ja|
| Elementspezifisches Qualitätskriterium |<ol><li>Die Bezeichnung soll kurz und prägnant sein.</li><li>Die Bezeichnung soll keine expliziten Bezüge zum Kontext herstellen, wenn dies nicht zwingend notwendig ist.</li><li>Die Bezeichnung Ausgabe sollte nicht als Frage formuliert werden.</li><li>Einheiten sollen möglichst Teil der Bezeichnung Ausgabe sein (z. B. Miete in Euro pro Monat).</li></ol>|
| Empfehlung für die maximale Länge |255 Zeichen|
| Art der Prüfung     | komplett manuell |
| Verweis auf den QS-Bericht      | [1010](./QS_methodische.md#1010-die-bezeichnung-ausgabe-muss-befüllt-werden) |
| Beispiel      |<ol><li>Positiv: Postfach</li><li>Negativ: Postfach Anschrift natürliche Person</li><li>Positiv: Bedürfnisgrund</li><li>Negativ: Bedürfnisgrund Waffe bzw. Munition (Freitext)</li></ol>|
|mögliche Abweichungen im Referenzkontext| nein |

## Hilfetext Eingabe

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition |Erläuternder Hilfetext für den Formularanwender eines Auslöser-Formulars, um zu verdeutlichen, welche Inhalte in einem Datenfeld einzugeben sind bzw. welche Aktion vorzunehmen ist.|
| Zielgruppe      | Modellierende, Fachlichkeit, Dienstleister nachnutzender Systeme, **Antragstellende** |
| Pflichtfeld | Nein|
| Elementspezifisches Qualitätskriterium |<ol><li>Beschreibung der Bedeutung des Feldes für den Endnutzer auf Inputformularen (Bürger/Unternehmen).</li><li>Hilfetext sollte eine Ausfüllhilfe für das Datenfeld seine; dies erfolgt z. B. durch aktive Ansprache des Bürgers („Geben Sie…  an“)</li><li>Es sollten sinnvolle Beispiele für die Befüllung des Datenfeldes gegeben werden.</li><li>Ist in der Bezeichnung Spezialvokabular verwendet worden, sollte es in der Hilfe erklärt werden; z. B. durch die Verwendung von Synonymen.</li><li>Fremdwörter sollten vermieden werden.</li><li>Hilfetexte dürfen nur angegeben werden, wenn Sie einen Mehrwert bieten. D. h. insbesondere, dass sie nicht identisch mit der Bezeichnung sein sollen.</li></ol>|
| Empfehlung für die maximale Länge |500 Zeichen|
| Art der Prüfung     | keine |
| Verweis auf den QS-Bericht      | x |
| Beispiel      |siehe Tabelle |
|mögliche Abweichungen im Referenzkontext| nein |

## Hilfetext Ausgabe

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Erläuternder Hilfetext für den Formularanwender (Bürger/Unternehmen) eines Ergebnis-Formulars.|
| Zielgruppe      | Modellierende, Fachlichkeit, Dienstleister nachnutzender Systeme, **Antragstellende** |
| Pflichtfeld | Nein|
| Elementspezifisches Qualitätskriterium |<ol><li>Beschreibung der Bedeutung des Feldes für den Endnutzer auf Output- Formularen (Bürger/Unternehmen).</li><li>Hilfetext sollte Erläuterung zu dem Inhalt des Felds geben („Dieses Feld enthält …“)</li><li>Ergibt sich aus Anforderungen an die Barrierefreiheit.</li><li>Hilfetexte dürfen nur angegeben werden, wenn Sie einen Mehrwert bieten, d. h. sie dürfen nicht identisch die Bezeichnung wiedergeben.</li></ol>|
| Empfehlung für die maximale Länge |500 Zeichen|
| Art der Prüfung     | keine |
| Verweis auf den QS-Bericht      | x |
| Beispiel      |Datenfeld „Sterbeort (Stadt)“: Ort, in dem die Person verstorben ist. |
|mögliche Abweichungen im Referenzkontext| nein |

## Verweis auf Regeln

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Die von einem Datenfeld verwendeten Regeln sind direkt an einem Datenfeld verortet, d. h. das Datenfeld referenziert die IDs dieser Regeln.|
| Zielgruppe | Modellierende, Dienstleister nachnutzender Systeme |
| Pflichtfeld | Nein|
| Elementspezifisches Qualitätskriterium |<ol><li>An einem Datenfeld selbst hängen selten Regeln.</li><li>Anstelle eines Patterns kann zur Präzisierung eines Datenfelds auch eine Regel genutzt werden, falls kein gültiges Pattern existiert oder falls es dem Modellierenden nicht möglich ist ein Pattern zu schreiben.</li><li>Es dürfen nur gültige Regel Identifikatoren referenziert werden.</li><li>Es können mehrere Regeln referenziert werden.</li></ol>|
| Empfehlung für die maximale Länge | nicht relevant|
| Art der Prüfung     | keine |
| Verweis auf den QS-Bericht      | x |
| Beispiel      |<ol><li>R00 000 001 489</li><li> Beispiel für ein Datenfeld ‘IP-Adresse’, in der eine Regel statt einem Pattern eingegeben wurde: Es muss sichergestellt werden, dass es sich bei der getätigten Angabe um eine gültige IP-Adresse handelt.</li></ol> |
|mögliche Abweichungen im Referenzkontext| nein |
