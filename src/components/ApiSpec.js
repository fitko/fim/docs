import React from 'react'
import BrowserOnly from '@docusaurus/BrowserOnly'
import useBaseUrl from '@docusaurus/useBaseUrl'

export default function ApiSpec({specUrl}) {
    return (
        <BrowserOnly fallback={<div>Lädt...</div>}>
            {() => {
                import('rapidoc')

                return (
                    <div>
                        Die aktuelle Version kann&nbsp;<a href={specUrl} download>hier</a>&nbsp;im OpenAPI Format heruntergeladen werden.

                        <rapi-doc
                            render-style="view"
                            layout="column"
                            spec-url={specUrl}
                            theme="light"
                            schema-description-expanded="true"
                            show-info="false"
                            show-header="false"
                            show-components="false"
                            allow-spec-file-load="false"
                            info-description-headings-in-navbar="false"
                            allow-try="false"
                            primary-color="#11171a"
                            allow-server-selection="false"
                            server-url=""
                        />
                    </div>
                )
            }}
        </BrowserOnly>
    )
}
