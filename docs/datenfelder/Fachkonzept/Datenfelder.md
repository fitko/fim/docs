# Datenfelder

## Grundlegendes über Datenfelder

Das Datenfeld bildet die kleinste Einheit des FIM-Baukastens aus Sicht des Bausteins „Datenfelder“. Es besteht in der Regel aus einer Bezeichnung und der Möglichkeit zur Eingabe von Informationen (z. B. in Form eines Freitexts, einer Liste oder einer Checkbox). Datenfelder, die keine Möglichkeit zur Eingabe von Informationen (z. B. reine Informationstexte) bieten , werden ebenfalls als Datenfelder im Baukasten geführt, verfügen jedoch über einen entsprechenden Datentyp.

Zu jedem Datenfeld im Baukasten werden Metadaten erfasst und gepflegt, die Informationen zu dessen Inhalt sowie strukturellen, redaktionellen und technischen Eigenschaften bereitstellen. Die Metadaten dienen im Rahmen der Befüllung und Nutzung des FIM-Baukastens zur besseren Strukturierung und Klassifizierung von Datenfeldern, zur strukturierten Ablage, zur besseren Auffindbarkeit im FIM-Baukasten sowie zur Einbindung der Felder in den FIM-Gesamtkontext. Auch im späteren Verlauf der FIM-Nutzung durch FMS-Anbieter – bzw. allgemeiner durch die FIM-Nutzer – können diese Metadaten genutzt werden. Ein Formularmanagementsystem wird dabei lediglich eine Untermenge der gelisteten Metadaten benötigen. Die hier definierte Menge an Metadaten sollte beständig sein und nur in Ausnahmefällen geändert werden. Falls dennoch eine Änderung durchgeführt werden muss, so stellt dies eine Anpassung der FIM-Logik dar, welche den dafür vorgesehenen Redaktionsprozess zu durchlaufen hat.

## Metadaten

Im Folgenden finden Sie eine Auflistung der Metadaten für Datenfelder inklusive Erläuterungen. Die Metadaten lehnen sich an verschiedene Standards für Metadaten, insbesondere aus dem Bereich der öffentlichen Verwaltung wie der deutschen Open Government Data-Metadaten-Struktur, an.

### Identifikationsangaben

|Metadatum|Definition|
|-----|-----|
|<u>**Identifizierungsangabe (ID)***</u> (1)|Eine eindeutige ID, siehe Identifikatoren.|
|<u>**Versionsangabe***</u> (1 für versionierte Datenfelder, 0 sonst)|Die Versionsnummer wird verwaltet, um die Existenz unterschiedlicher Versionen von Elementen zu ermöglichen.|

### Definitionsangaben

|Metadatum|Definition|
|-----|-----|
|**Name** (1)|FIM-interner Name des Datenfelds, der sichtbar ist für alle Rollen in FIM, die auf der Ebene der abstrakten Formulare tätig sind (fachliche Ersteller, Methodenexperten, Informationsmanager, FIM-Koordinierungsstelle, FIM-Nutzer).|
|*Definition* (0…1)|Fasst den Inhalt eines Datenfelds aus redaktioneller Sicht zusammen, d. h. aus Sicht der Rollen fachlicher Ersteller, Methodenexperte, Informationsmanager, FIM-Koordinierungsstelle, FIM-Nutzer.|
|**Bezug zur Handlungsgrundlage** (0…n)|Dieses Attribut ermöglicht es, Bezüge zu einschlägigen Rechtsnormen sowie zu in anderen Vorhaben wie den XÖV-Vorhaben (z. B. XWaffe) oder Vorhaben wie P23R gleich oder ähnlich definierten Datencontainer zu erstellen und diese Verbindung zu dokumentieren. Hierdurch lässt sich die Ausgestaltung eines Stammdatenschemas gemäß einschlägigen Rechtsnormen oder Standardisierungsverfahren belegen. Es besteht die Möglichkeit zu jeder Handlungsgrundlage eine URI zu hinterlegen.|
|*Beschreibung* (0…1)|Zusätzliche Beschreibung und Erläuterung eines Datenfelds für FIM-Nutzer.|
|*Stichwörter (Tags)* (0…n)|Dieses Metadatum beinhaltet eine Liste an vorgegebenen und frei wählbaren Stichwörtern, die es dem FIM-Nutzer ermöglichen, eine spezielle Semantik zu kennzeichnen oder eine Suche innerhalb eines Redaktionssystem zu erleichtern.|
|*Relation* (0…n)|Relationen dokumentieren Beziehungen der Elemente untereinander, wie z.B. "ist abgeleitet von", "ersetzt" oder "ist äquivalent zu". Eine Relation wird durch die Unterangaben Prädikat und Objekt vollständig beschrieben.|
|*Versionshinweis* (0…1)|Ein Hinweis, der bei der Versionierung eines Elements erstellt wird. Es wird beispielsweise beschrieben, was sich an einem Element geändert hat.|
|*Regel* (0…n)|Referenz auf eine Regel, die die möglichen Verwendungen des Feldes einschränkt.|
|**Strukturelementart** (1)|Ein Metadatum mit den Ausprägungen ‘harmonisiert’ oder 'rechtsnormgebunden', welches Datenfelder aufgrund ihres Nachnutzungscharakters klassifiziert. Der Defaultwert für die Strukturelementart ist ‘rechtsnormgebunden’, alle anderen Werte müssen durch einen Nutzer mit der entsprechenden Berechtigung aktiv gesetzt werden.|
|**Feldart** (1)|Die Datenfeldart klassifiziert, welche Bürgerinteraktion durch dieses Feld gefordert ist. Der Defaultwert für die Datenfeldart ist „Eingabe“, alle anderen Werte müssen durch den Nutzer aktiv gesetzt werden.|
|**Datentyp** (1)|Der Datentyp bezeichnet die Zusammenfassung konkreter Wertebereiche und ggf. darauf definierten Operationen zu einer Einheit, die mit Hilfe von Regeln abbildbar sind. Zudem spezifiziert der Datentyp die Struktur des Inhalts oder gibt Aufschlüsse über die Feldinhaltsänderbarkeit. Mögliche Ausprägungen dieses Metadatums sind: „Text“, „Datum“, „Zahl“, „Währung“, „Codeliste“, „Ja/Nein“, „Leerfeld“ (z. B. um einen Stempel abzubilden). Der Defaultwert für den Datentyp ist „Text“, alle anderen Werte müssen durch den Nutzer aktiv gesetzt werden.|
|*Präzisierung* (0…1)|Die möglichen Eingabewerte für ein Datenfeld können mit Hilfe der unter Präzisierung erfassten Werte eingeschränkt werden. Es gibt die Unterangaben *Minimale Länge* (0...1), *Maximale Länge* (0...1), *Minimaler Wert* (0...1), *Maximaler Wert* (0...1), *Pattern* (0...1). Der Pattern ist im Sinne eines Regulären Ausdruckes (RegEx) zu verstehen.|
|*max Size* (0…1)|Diese Eigenschaft darf nur bei Datenfeldern des Typs "file" verwendet werden. Sie legt die Maximalgröße in Byte fest.|
|*media Type* (0…*)|Diese Eigenschaft darf nur bei Datenfeldern des Typs "file" verwendet werden. Sie listet die erlaubten Dateiarten gemäß der Media Type Registry der IANA auf. Beispiel: "application/pdf" für PDF|
|*Inhalt* (0…1)|Dieses Metadatum dient dazu, den Input-Inhalt eines Datenfelds zu spezifizieren, d. h. darüber kann ein (Default-)Wert für ein Textfeld definiert werden.|
|*Verweis auf Codeliste* (0…1)|Referenz auf eine Codeliste, deren Werte das Feld annehmen kann. Die Codeliste findet sich in der Regel im XRepository.|
|*code Key* (0…1)|Gibt an, welcher Wert einer Codeliste den Schlüssel enthält.|
|*name Key* (0…1)|Gibt an, welcher Wert einer Codeliste den anzuzeigenden Namen enthält.|
|*help Key* (0…1)|Gibt an, welcher Wert einer Codeliste den Hilfetext enthält.|
|*Werte* (0…1)|Eine Liste von Werten, vergleichbar mit den Zeilen einer Codeliste, die ein Datenfeld annehmen kann. Es gibt die Unterangaben Code, Name und Hilfe.|
|**Vorbefüllung** (1)|Gibt an, ob die Behörde das Feld vorbefüllen kann oder muss. Mögliche Ausprägungen davon sind ‘keine’, ‘optional’ oder ‘verpflichtend’. Der Defaultwert für die Vorbefüllung ist „keine“, alle anderen Werte müssen durch den Nutzer aktiv gesetzt werden.|

### Angaben für Formular-Nutzer

|Metadatum|Definition|
|-----|-----|
|**Bezeichnung Eingabe** (1)|Bezeichnung des Feldes, welche für den Bürger/das Unternehmen auf Input-Formularen sichtbar ist.|
|**Bezeichnung Ausgabe** (0…1)|Bezeichnung des Feldes, welche für den Bürger/das Unternehmen auf Output-Formularen sichtbar ist.|
|*Hilfetext Eingabe* (0…1)|Erläuternder Hilfetext für den Formularanwender (Bürger/Unternehmen) eines Input-Dokuments, um zu verdeutlichen, welche Inhalte in einem Datenfeld einzugeben sind bzw. welche Aktion vorzunehmen ist.|
|*Hilfetext Ausgabe* (0…1)|Erläuternder Hilfetext für den Formularanwender (Bürger/Unternehmen) eines Output-Dokuments.|

### Zustandsangaben

|Metadatum|Definition|
|-----|-----|
|**Fachlicher Ersteller** (0…1)|Ersteller oder erstellende Institution des Elements. Der fachliche Ersteller entspricht der Freigabeinstanz für eine Silber- oder Goldfreigabe.|
|<u>**Erstelldatum**</u> :bomb:|Datum der Erstellung des Datenfelds|
|<u>**Letztes Änderungsdatum**</u> (1)|Datum der letzten Änderung des Datenfelds.|
|<u>**Letzter Bearbeiter**</u> :bomb:|Der letzte Bearbeiter des Datenfelds.|
|<u>**Status**</u> (1)|Der Status gibt Aufschluss darüber, ob und wie das Element im Rahmen eines Editors zu verwenden ist. Mögliche Ausprägungen dieses Metadatums sind durch die Codeliste urn:xoev-de:xprozess:codeliste:status gegeben. Näheres dazu finden Sie im Kapitel Status, Freigabe und Veröffentlichung. Der Defaultwert für den Status ist „in Bearbeitung“, alle anderen Status müssen durch den Nutzer aktiv gesetzt werden.|
|*Gültig ab* (0…1)|Sofern es eine Beschränkung der Gültigkeit gibt, ist der Gültigkeitszeitraum zu erfassen. Mit diesem Metadatum wird der Beginn der Gültigkeit erfasst.|
|*Gültig bis* (0…1)|Sofern es eine Beschränkung der Gültigkeit gibt, ist der Gültigkeitszeitraum zu erfassen. Mit diesem Metadatum wird das Ende der Gültigkeit erfasst.|
|<u>**Freigabedatum**</u> (0…1)|Datum der Freigabe durch die Freigabeinstanz dieses Datenfelds.|
|<u>**Veröffentlichungsdatum**</u> (0…1)|Datum der Veröffentlichung durch die Veröffentlichungsinstanz dieses Datenfelds.|


## Repräsentation im Standard XDatenfelder 3

### Feldart Eingabe - Datentyp Text

~~~
<xdf3:datenfeld>
    <xdf3:identifikation>
        <xdf3:id>F82000000015</xdf3:id>
    </xdf3:identifikation>
    <xdf3:name>Standard Datenfeld für Texteingaben</xdf3:name>
    <xdf3:bezug link="">§ 123 AbcG</xdf3:bezug>
    <xdf3:freigabestatus listURI="urn:xoev-de:xprozess:codeliste:status" listVersionID="2020-03-19">
        <code>2</code>
    </xdf3:freigabestatus>
    <xdf3:letzteAenderung>2023-05-10T13:51:41+02:00</xdf3:letzteAenderung>
    <xdf3:bezeichnungEingabe>Hier steht eine passende Eingabeaufforderung</xdf3:bezeichnungEingabe>
    <xdf3:bezeichnungAusgabe>Passender Ausgabetext zur Eingabe</xdf3:bezeichnungAusgabe>
    <xdf3:schemaelementart listURI="urn:xoev-de:fim:codeliste:xdatenfelder.schemaelementart" listVersionID="1.0">
        <code>RNG</code>
    </xdf3:schemaelementart>
    <xdf3:hilfetextEingabe>Erläuternde Hilfestellungen können wertvolle Hinweise liefern.</xdf3:hilfetextEingabe>
    <xdf3:feldart listURI="urn:xoev-de:fim:codeliste:xdatenfelder.feldart" listVersionID="2.0">
        <code>input</code>
    </xdf3:feldart>
    <xdf3:datentyp listURI="urn:xoev-de:fim:codeliste:xdatenfelder.datentyp" listVersionID="2.0">
        <code>text</code>
    </xdf3:datentyp>
    <xdf3:praezisierung pattern=""/>
    <xdf3:vorbefuellung listURI="urn:xoev-de:fim:codeliste:xdatenfelder.vorbefuellung" listVersionID="1.0">
        <code>keine</code>
    </xdf3:vorbefuellung>
</xdf3:datenfeld>
~~~

### Feldart Eingabe - Datentyp Wahrheitswert

~~~
<xdf3:datenfeld>
    <xdf3:identifikation>
        <xdf3:id>F82000000016</xdf3:id>
    </xdf3:identifikation>
    <xdf3:name>Standard Datenfeld für Wahrheitswertabfrage</xdf3:name>
    ...
    <xdf3:bezeichnungEingabe>Stimmen Sie der Aussage zu?</xdf3:bezeichnungEingabe>
    ...
    <xdf3:feldart listURI="urn:xoev-de:fim:codeliste:xdatenfelder.feldart" listVersionID="2.0">
        <code>input</code>
    </xdf3:feldart>
    <xdf3:datentyp listURI="urn:xoev-de:fim:codeliste:xdatenfelder.datentyp" listVersionID="2.0">
        <code>bool</code>
    </xdf3:datentyp>
</xdf3:datenfeld>
~~~

### Feldart Statisch - Datentyp Text

~~~
<xdf3:datenfeld>
    <xdf3:identifikation>
        <xdf3:id>F82000000017</xdf3:id>
    </xdf3:identifikation>
    <xdf3:name>Datenfeld für statische Texte</xdf3:name>
    ...
    <xdf3:bezeichnungEingabe>Hat einen Überschriftscharakter</xdf3:bezeichnungEingabe>
    ...
    <xdf3:feldart listURI="urn:xoev-de:fim:codeliste:xdatenfelder.feldart" listVersionID="2.0">
        <code>label</code>
    </xdf3:feldart>
    <xdf3:datentyp listURI="urn:xoev-de:fim:codeliste:xdatenfelder.datentyp" listVersionID="2.0">
        <code>text</code>
    </xdf3:datentyp>
    <xdf3:inhalt>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy
    eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
    At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren,
    no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet,
    consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore
    magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et
    ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit
    amet.</xdf3:inhalt>
    ....
 </xdf3:datenfeld>
~~~

### Feldart Eingabe - Datentyp Anlage (mit Link zur Handlungsgrundlage)

~~~
<xdf3:datenfeld>
    <xdf3:identifikation>
        <xdf3:id>F05000000003</xdf3:id>
    </xdf3:identifikation>
    <xdf3:name>Anlagefeld</xdf3:name>
    <xdf3:bezug link="https://de.wikipedia.org/wiki/Portable_Document_Format">§ 12 BspG</xdf3:bezug>
    <xdf3:freigabestatus listURI="urn:xoev-de:xprozess:codeliste:status" listVersionID="2020-03-19">
        <code>2</code>
    </xdf3:freigabestatus>
    <xdf3:statusGesetztDurch>FIM Baustein Datenfelder</xdf3:statusGesetztDurch>
    <xdf3:letzteAenderung>2024-04-16T11:05:20+00:00</xdf3:letzteAenderung>
    <xdf3:bezeichnungEingabe>PDF Anlage</xdf3:bezeichnungEingabe>
    <xdf3:bezeichnungAusgabe>PDF Anlage</xdf3:bezeichnungAusgabe>
    <xdf3:schemaelementart listURI="urn:xoev-de:fim:codeliste:xdatenfelder.schemaelementart" listVersionID="1.0">
        <code>RNG</code>
    </xdf3:schemaelementart>
    <xdf3:feldart listURI="urn:xoev-de:fim:codeliste:xdatenfelder.feldart" listVersionID="2.0">
        <code>input</code>
    </xdf3:feldart>
    <xdf3:datentyp listURI="urn:xoev-de:fim:codeliste:xdatenfelder.datentyp" listVersionID="2.0">
        <code>file</code>
    </xdf3:datentyp>
    <xdf3:vorbefuellung listURI="urn:xoev-de:fim:codeliste:xdatenfelder.vorbefuellung" listVersionID="1.0">
        <code>keine</code>
    </xdf3:vorbefuellung>
    <xdf3:maxSize>1500000</xdf3:maxSize>
    <xdf3:mediaType>application/pdf</xdf3:mediaType>
</xdf3:datenfeld>
~~~

### Präzisierung eines Datenfelds (mit optionaler Vorbefüllung)

~~~
<xdf3:datenfeld>
    <xdf3:identifikation>
        <xdf3:id>F05000000004</xdf3:id>
    </xdf3:identifikation>
    <xdf3:name>Ganzzahl</xdf3:name>
    ...
    <xdf3:feldart listURI="urn:xoev-de:fim:codeliste:xdatenfelder.feldart" listVersionID="2.0">
        <code>input</code>
    </xdf3:feldart>
    <xdf3:datentyp listURI="urn:xoev-de:fim:codeliste:xdatenfelder.datentyp" listVersionID="2.0">
        <code>num_int</code>
    </xdf3:datentyp>
    <xdf3:praezisierung maxValue="99" minValue="1" pattern=""/>
    <xdf3:inhalt>55</xdf3:inhalt>
    <xdf3:vorbefuellung listURI="urn:xoev-de:fim:codeliste:xdatenfelder.vorbefuellung" listVersionID="1.0">
        <code>optional</code>
    </xdf3:vorbefuellung>
</xdf3:datenfeld>
~~~

## Umgang mit Code- und Wertelisten

Der Umgang mit Code- und Wertelisten hat sich im Vergleich zum Standard XDatenfelder 2 geändert.
...
Hinweis darauf, dass auf die Nutzungstypen des Codelistenhandbuchs der KoSIT verwiesen wird.
Aber, dass hier bei Nutzungstyp 3 klar definiert ist, dass immer die aktuellste Version einer Codeliste gemeint ist, wenn keine Version zusätzlich zur Kennung angegeben wird.

### Werteliste

~~~
<xdf3:werte>
	<xdf3:wert>
		<xdf3:code>001</xdf3:code>
		<xdf3:name>Arbeitslosengeld II („Hartz 4", SGB II)</xdf3:name>
	</xdf3:wert>
	<xdf3:wert>
		<xdf3:code>002</xdf3:code>
		<xdf3:name>Grundsicherung im Alter/bei Erwerbsminderung oder Hilfe zum Lebensunterhalt ("Sozialhilfe", SGB XII)</xdf3:name>
	</xdf3:wert>
	<xdf3:wert>
		<xdf3:code>003</xdf3:code>
		<xdf3:name>Leistungen der Kinder- und Jugendhilfe (SGB VIII)</xdf3:name>
	</xdf3:wert>
…
</xdf3:werte>
~~~

### Codeliste

~~~
<xdf3:codelisteReferenz>
	<xdf3:canonicalIdentification>urn:de:bund:destatis:bevoelkerungsstatistik:schluessel:staatsangehoerigkeit</xdf3:canonicalIdentification>
	<xdf3:version>2021-02-19</xdf3:version>
	<xdf3:canonicalVersionUri>urn:de:bund:destatis:bevoelkerungsstatistik:schluessel:staatsangehoerigkeit_2021-02-19</xdf3:canonicalVersionUri>
</xdf3:codelisteReferenz>
~~~
