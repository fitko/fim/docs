# Einheitliche Erfassung von Handlungsgrundlagen

Für die Zuordnung von Elementen des FIM-Bausteins Datenfelder zu ihren entsprechenden **Handlungsgrundlagen** (das sind u. a. Gesetze, Urteile, Standards) ist ein einheitliches Vorgehen notwendig. 

Im Folgenden wird anhand des abstrakten Falls das Vorgehen erklärt, danach werden Beispiele gelistet.

## Gesetzestext als Bezug

### Allgemeines

Falls ein Gesetzestext als Bezug verwendet wird, ist folgendes Format zu verwenden:

 **§ P (A) S. X Nr. Y-Z MusterG**

Zunächst wird der entsprechende **Paragraf (P)** genannt, gefolgt von den entsprechenden **Absätzen (A)**, danach sind (falls vorhanden) **Satz (X)** und **Nummer (Y-Z)** zu nennen, gefolgt von dem Gesetz selbst (MusterG). Sollten es mehrere Absätze sein, sind alle Entsprechenden zu nennen. Mehrfachnennungen erfolgen durch Komata getrennt; sind es mehrere Absätze in Folge, kann auch ein Bereich definiert werden.

Darüber hinaus kann es auch noch Anhänge/Anlagen, (Unter-)Abschnitte und Muster zu Gesetzestexten geben.

### Beispiele

<ul>

<li>

**§ 27 (3) MuSchG**

Hier besteht ein Bezug zu Paragraf 27, Absatz 3 des Mutterschutzgesetzes.

</li>

<li>

**§§ 27 f. AufenthG**

Es wird Bezug genommen auf Paragraf 27 und 28 des Aufenthaltsgesetzes. Die Abkürzung f. zur Kennzeichnung, dass der folgende Paragraf auch gemeint ist, kann verwendet werden. Die Abkürzung ff. sollte nicht verwendet werden, da diese nicht eindeutig genug angibt, welche der folgenden Paragrafen gemeint sind.

</li>

<li>

**§ 27 (1) - (4) MuSchG**

(1) - (4) bedeutet, dass die Absätze 1 bis 4 zu beachten sind.

</li>

<li>

**§ 8 (1), (1a) BEEG**

In diesem Fall sind die Absätze 1 und 1a zu beachten.

</li>

<li>

**§ 6 (2) S. 2 Nr. 3 WoGG**

Hier ist vom zweiten Satz die Nummer 3 zu beachten.

</li>

<li>

**§ 9 Muster 5 SchwbAwV**

Dies bezieht sich auf das fünfte Muster zum Paragraf 9 der Schwerbehindertenausweisverordnung.

</li>

<li>

**Anhang 3 Abschnitt 1 PAuswV**

In diesem Fall ist der erste Abschnitt des dritten Anhangs zur Personalausweisverordnung gemeint.

</li>

</ul>

## Verordnung als Bezug

### Allgemeines

Falls eine Verordnung (VO) als Bezug verwendet wird, ist folgendes Format zu verwenden:

**VO (XX) Nr. 123/Jahreszahl**

Zunächst wird das Kürzel der entsprechenden Verordnung genannt. Hier kann in Klammern folgendes stehen:

<ul>

<li>

EWG: Bis zum November 1993 wurde das Kürzel des Vertrages „EWG“ genutzt.

</li>

<li>

EG: Ab dem 1. November 1993 (Vertrags von Maastricht) wird das Kürzel „EG“ verwendet.

</li>

<li>

EU: Ab dem 1. Dezember 2009 (Vertrags von Lissabon) wird das Kürzel „EU“ verwendet.

</li> </ul>

Gefolgt wird diese Angabe von der Angabe der Verordnung, sowie ihrem Erscheinungsjahr.

### Beispiel

**VO (EG) Nr. 883/2004**

Das ist die Verordnung Nr. 883 des Europäischen Parlaments (EG) vom Jahr 2004.

## Richtlinie als Bezug

### Allgemeines

Falls eine Richtlinie (RL) als Bezug verwendet wird, ist wenn möglich folgendes Format zu verwenden:

**RL Teil X Nr. 123 MusterG**

Zunächst wird gelistet, welcher Teil oder Anhang (falls vorhanden) einer Richtlinie betroffen ist, danach wird die entsprechende Nummer gelistet, gefolgt von der Amtlichen Bezeichnung (Abkürzungstitel) des betroffenen Gesetzes.

### Beispiele

**RL Teil I Nr. 2.0 - 2.4 BEEG**

Das ist der erste Teil der Richtlinien bezüglich des Bundeselterngeld- und Elternzeitgesetzes und davon sind Nr. 2.0 bis 2.4 relevant.

**RL Anhang II BEEG**

Hier ist Anhang 2 des Bundeselterngeld- und Elternzeitgesetzes zu beachten.

## Standard als Bezug

### Allgemeines

Falls auf einen Standard, wie z. B. ein XÖV-Vorhaben Bezug genommen wird, ist folgendes Format zu verwenden:

**NameVorhaben.Hierarchiestufe1.HierarchiestufeN.NameFormularfeldgruppe**

Hierbei sind unendlich viele Hierarchiestufen möglich, meistens sind es jedoch nur eine oder zwei die verwendet werden. Falls vorhanden kann auch die entsprechende URN verwendet werden.

### Beispiel

**XPersonenstand.portal2StA.Sterbefall.084010.sterbeort.strasse**

XPersonenstand ist ein Fachmodul des Standards XInneres, welcher dem elektronischen Datenaustausch innerhalb der Innenverwaltung dient.

:warning: Ein Standard kann nur als Handlungsgrundlage dienen, sowie dessen Verwendung gesetzlich vorgeschrieben ist. Ansonsten ist dies nicht möglich.

## weitere Quellen

### Allgemeines

Andere mögliche Quellen für Bezüge sind z. B.  ISO-Normen, DIN-Normen, Technische Richtlinien, RFC – Request for Comments (das bezieht sich auf eine Reihe technischer und organisatorischer Dokumente zum Internet), …

### Beispiele

**Tabelle 9 BSI TR-03123**

**RFC 5322**

## Weiterführende Hinweise

### Woraus setzen sich die Handlungsgrundlagen eines Datenschemas zusammen?

:::info

Die Handlungsgrundlagen eines Datenschemas sind die Summe aus **allen Handlungsgrundlagen** seiner **rechtsnormbezogenen** Unterelemente, sowie allen **Verwendungsbegründungen** seiner **harmonisierten** Unterelemente. Siehe dazu auch: [Handlungsgrundlagen von Datenschemata](./DS_Tipps.md#handlungsgrundlagen-von-datenschemata)

Zur Erfassung der Handlungsgrundlagen auf Ebene des Datenschema muss zwingend zu jeder Handlungsgrundlage ein zeitlicher Bezug erfasst werden. Das ist bei einem Gesetz das Datum, an dem das Gesetz selbst bzw. die letzte Gesetzesänderung (falls vorhanden) in Kraft tritt. Bei Standards ist es das Datum, ab der der Standard gültig ist.

 **§ P (A) S. X Nr. Y-Z MusterG vom 10.01.2020**

::: 

### Erfassung mehrerer Handlungsgrundlagen

:::info

Falls das Redaktionssystem bereits eine Notation vorgibt, ist diese zu nutzen. Ansonsten gilt: <br/>
Wenn man mehr als eine Handlungsgrundlage aus verschiedenen Kontexten erfassen muss, sind die Handlungsgrundlagen mit einem Strichpunkt voneinander zu trennen.

**RFC 5322; RL Teil I Nr. 2.0 - 2.4 BEEG; § P (A) S. X Nr. Y-Z MusterG**

:::

### Umgang mit fehlenden Handlungsgrundlagen

In seltenen Fällen kann es vorkommen, dass es zu einem Baukastenelement keine Handlungsgrundlage gibt. Zwei Gründe hierfür sind denkbar: 

- **Referenzkontext**
- **Lücke im Gesetz**

Je nach Fall empfiehlt sich folgendes Vorgehen:

- **Referenzkontext:** im Feld Handlungsgrundlage sollte folgendes ergänzt werden *"leer, da Referenzkontext"*. Vergessen Sie nicht in diesem Fall im Beschreibungsfeld zu hinterlegen, warum dieses Element im Referenzkontext benötigt wird.
- **Lücke im Gesetz:** Im Feld Handlungsgrundlage sollte folgendes ergänzt werden *"leer, da Gesetzeslücke"*. Vergessen Sie auch hier nicht im Beschreibungsfeld einen Hinweis diesbezüglich zu ergänzen.
