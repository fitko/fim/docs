# Status, Freigabe und Veröffentlichung

## Status im FIM-Baustein Datenfelder

Übergreifend für alle FIM-Bausteine wird die Codeliste ([urn:xoev-de:xprozess:codeliste:status](https://www.xrepository.de/details/urn:xoev-de:xprozess:codeliste:status)) genutzt, um den Status von Elementen anzuzeigen. Im Baustein Datenfelder wird diese Codeliste seit der Version 2020-03-19 verwendet. Davor wurde eine andere Codeliste verwendet (siehe [Änderung der Status und Transformation](#änderung-der-status-und-transformation)).

|Code|Codename|
|----|--------|
|1|in Planung|
|2|in Bearbeitung|
|3|Entwurf|
|4|methodisch freigegeben|
|5|fachlich freigegeben (silber)|
|6|fachlich freigegeben (gold)|
|7|inaktiv|
|8|vorgesehen zum Löschen|

In der Statuscodeliste spiegelt sich der Lebenszyklus der Elemente im Baustein Datenfelder wieder; d. h. ein Datenschema beginnt sein Leben in der Planungsphase, und durchläuft danach die Status aufsteigend - vor einer fachlichen Freigabe findet beispielsweise eine methodische Freigabe statt. In einem Redaktionssystem können die einzelnen Status auch übersprungen werden, d. h. ein Wechsel vom Status **in Bearbeitung** direkt in den Status **fachlich freigegeben (silber)** ist dort möglich.

### Bedeutung der einzelnen Status

- **in Bearbeitung** - ist der initial zu vergebende Status; die Modellierung ist möglicherweise noch nicht weit fortgeschritten
- **Entwurf** - das Element hat einen guten Reifegrad erreicht, ist jedoch noch nicht methodisch oder fachlich abgenommen; eine fachliche bzw. methodische Vorabprüfung kann stattgefunden haben; ab diesem Status gilt das Element als freigegeben, es kann jetzt veröffentlicht werden
- **methodisch freigegeben** - das Element hat eine erfolgreiche methodische Prüfung durchlaufen
- **fachlich freigegeben (silber)** - das Element ist von der zweithöchsten Freigabe Instanz abgenommen worden
- **fachlich freigegeben (gold)** - das Element ist von der höchsten Freigabe Instanz abgenommen worden
- **inaktiv** - das Element ist nicht mehr aktiv, da es beispielsweise eine neue Version gibt oder andere Gründe für dessen Stilllegung verantwortlich sind

Die Status mit dem Code 1 **in Planung**, sowie 8 **vorgesehen zum Löschen**, sind noch nicht fertig aus spezifiziert und werden vorerst nicht im Standard XDatenfelder 3.0 verwendet.

### Rechte für das Setzen der Status
Normale Mitglieder mit Schreibrechten dürfen nur die Status **in Bearbeitung** oder **inaktiv** setzen. Für alle anderen Status (Code 3 bis 6 bzw. 7) ist die Zusatzberechtigung **Fachliche Freigabe** notwendig. In folgendem Abschnitt werden die Berechtigungen im Baustein ausführlicher abgebildet (Rollen und Rechte).

### Änderung der Status und Transformation
Im Fachkonzept vom 2019-12-18 wurden die Status noch durch andere Werte repräsentiert. Diese lauteten:

- In Vorbereitung
- Aktiv
- Inaktiv

Diese Status sind wie folgt  zu transformieren:

- Arbeitskopien im Status **In Vorbereitung** oder **Aktiv** werden zu **in Bearbeitung**
- alle Elemente im Status **Inaktiv** bekommen den Status **inaktiv**
- Versionierte Elemente im Status **Aktiv** ohne Freigabe bekommen den Status **in Bearbeitung**
- Versionierte Elemente im Status **Aktiv** mit Freigabe bekommen die Status 3 bis 6, abhängig von ihrem Reifegrad

## Freigabe und Veröffentlichung
Die Freigabe und Veröffentlichung von Inhalten erfolgt in drei Schritten, die in der folgenden Reihenfolge durchlaufen werden müssen:

1. Version erstellen
2. (Fachliche) Freigabe
3. Veröffentlichung

Die Steuerung erfolgt über die beiden Attribute **Freigabedatum** (laut XDatenfelder 3 Spezifikation: *statusGesetztAm*) und **Veröffentlichungsdatum** (laut XDatenfelder 3 Spezifikation: *veröffentlichungsdatum*). Diese dürfen nicht manuell editiert werden, sondern werden vom Redaktionssystem beim Ausführen des zweiten bzw. dritten Schritt automatisch mit einem aktuellen Datum gesetzt.

### Version erstellen
Nachdem die Arbeitskopie eines Datenschemas wie gewünscht bearbeitet wurde, stößt ein Redakteur mit Schreibrechten die Erstellung einer Version an.

Das System führt eine Prüfung des zu versionierenden Elements durch, um die Einhaltung redaktioneller Regeln sicherzustellen. Es wird z. B. geprüft, ob dass das Feld **Fachlicher Ersteller** befüllt ist. Sofern keine Fehler auftreten, wird die Version als Kopie erstellt. Die neu erstellte Version erhält eine Erweiterung ihrer Identifikation um die Versionskennung (siehe Identifikatoren).

Die wesentlichen Attribute der Version (siehe Kapitel zur Versionierung) sind nicht mehr bearbeitbar; sie kann jedoch bis zur Veröffentlichung gelöscht und neu erzeugt werden.

### (Fachliche) Freigabe
Nach fachlicher oder methodischer Prüfung löst ein Benutzer mit dem Zusatzrecht **Fachliche Freigabe** die fachliche Freigabe implizit durch Änderung des Elementstatus aus. Das System speichert das aktuelle Datum in das Feld **Freigabedatum**. Sollte die Version vor der Veröffentlichung gelöscht werden, so erlischt damit auch die fachliche Freigabe und dieser Schritt muss erneut durchgeführt werden.

Die Änderungen auf folgende Status implizieren eine fachliche Freigabe:

Änderung vom Status **in Bearbeitung** in einen der vier Status **Entwurf**, **methodisch freigegeben**, **fachlich freigegeben (silber)** und **fachlich freigegeben (gold)**.

Da eine fachliche Freigabe nicht zurück gezogen werden kann, kann der Status ab diesem Zeitpunkt nicht mehr auf **in Bearbeitung** zurück gesetzt werden.

### Veröffentlichung
Analog zur fachlichen Freigabe löst ein Benutzer mit dem Zusatzrecht **Veröffentlichung** die Veröffentlichung eines Elements aus. Das System stellt sicher, dass dies nur für freigegebene Elemente möglich ist; d. h. es muss ein Freigabedatum gesetzt sein.

Das System speichert darauf das aktuelle Datum in das Feld **Veröffentlichungsdatum**.

Ein Löschen des Elements oder ein Zurückziehen der Veröffentlichung ist jetzt nicht mehr möglich.

Veröffentlichte Element können, abhängig von der Anbindung des Redaktionssystems, auf dem FIM-Portal erscheinen.

## Repräsentation im Standard XDatenfelder 3
~~~
<xdf3:freigabestatus listURI="urn:xoev-de:xprozess:codeliste:status" listVersionID="2020-03-19">
  <code>6</code>
</xdf3:freigabestatus>
<xdf3:statusGesetztAm>2019-03-06</xdf3:statusGesetztAm>
<xdf3:veroeffentlichungsdatum>2019-03-06</xdf3:veroeffentlichungsdatum>
~~~

## Statusänderungen von Datenschemata unter Berücksichtigung von Freigabe & Veröffentlichung

![Freigabeworkflow](./images/Workflows.png)

Die Grafik zeigt durch Pfeile auf, welche Statusänderungen bei einem Datenschema unter Berücksichtigung aller Regeln möglich sind.

*schwarze Pfeile* = es wird bei der Statusänderung kein Freigabedatum gesetzt; das Element gilt damit nicht als freigegeben.

*roter Pfeil* = bei dieser Statusänderung wird das Freigabedatum initial gesetzt

*grüne Pfeile* = Änderung eines freigegebenen Elements von oder zum Status “Inaktiv”

*blauer Pfeil* = Änderung des Status zwischen 3 und 6

:warning: Obwohl die Statuscodeliste den Lebenszyklus eines Datenschemas wieder spiegelt, kann es Sinn machen, Änderungen vorzunehmen, die gegen den Lebenszyklus laufen. :warning: