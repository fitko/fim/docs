# zentrale Repositorys

## BOB

BOB ist der zentrale Baukasten des Bausteins. Darin liegen Datenfelder und Datenfeldgruppen, die mit dem Hintergedanken der größtmöglichen Nachnutzbarkeit modelliert wurden. Die aktuellsten Elemente, die in BOB vorliegen, liegen dort im Status Gold vor. Veraltete Elemente in BOB werden in den Status inaktiv gesetzt. Der Hauptnummernkreis von BOB ist die 60.

## KATE

KATE ist der zentrale Datenfeldkatalog des Bausteins. Darin liegen abstrakte und nicht abstrakte Dokumentsteckbriefe, mit denen keine bis beliebig viele lokale Datenschemata verknüft sind. KATE schafft damit einen globalen Ordnungsrahmen für alle Schemata, die bundesweit modelliert werden und erleichtert so die Wiederauffindbarkeit. Dokumentsteckbriefe in KATE können in allen Status vorliegen, je nach Reifegrad.
Der Hauptnummernkreis von KATE ist die 99.

## lokale Verfügbarkeit

Die Verwendung der beiden zentralen Repositorys ist zwingend für eine saubere Anwendung der FIM-Methodik.
Zu diesem Zweck müssen die Baukastenelemente aus BOB bzw. respektive die Dokumentsteckbriefe aus KATE in jedem Redaktionssystem mindestens lokal verfügbar sein.

BOB und KATE können sowohl über das FIM-Portal, als auch über das Sammelrepository bezogen werden.
Dabei sind die verwendeten Identifier beizubehalten. An diesen Elementen dürfen lokal keine Änderungen erfolgen.

Eine aktuelle Version von BOB wird ca. zwei mal im Jahr bereitgestellt. Ein zeitnahes Update in einem Redaktionssystem wird empfohlen.
Dabei werden alte Elemente ggf. inaktiv gesetzt, diese Informationen muss in das eigene Redaktionssystem übernommen werden, damit die Personen, die mit dem System arbeiten, stets darüber informiert sind, welche Elemente gerade aktuell sind.

Updates des zentralen Katalogs KATE erfolgen kurzfristig, jedoch spätestens dann, wenn eine Bedarfsmeldung für einen neuen Dokumentsteckbrief vorliegt.