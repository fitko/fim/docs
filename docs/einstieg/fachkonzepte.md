---
sidebar_label: Fachkonzepte
---

# Fachkonzepte

Die Fachkonzepte bilden die methodische Grundlage für FIM und sind für Personen, die sich ein grundlegendes und detailliertes Verständnis erarbeiten wollen:

- [Fachkonzept des Baustein Prozesse](/docs/prozesse/einleitung.md)
- [Fachkonzept des Baustein Leistungen](/docs/leistung/leistungqskriterien/Einleitung.md)
- [Fachkonzept des Bausteins Datenfelder](/docs/datenfelder/Fachkonzept/Datenfelder.md)