# FIM Baustein Prozesse

## Was ist Quali-XProzess?
Mit Quali-XProzess können Sie eine XProzess-Nachricht, die als XML-Datei im Format des Standards XProzess vorliegt, auf Schemakonformität und  FIM-Konformität prüfen lassen.
Den Eintrag zu Quali-XProzess finden Sie im FIM-Haus (FIM-Dokumente) unter „System/Technik“.
Zugang: https://qualixprozess.fim.dvzdigital.de/


## Was ist die FIM-Normenanalyse?

Die Anwendung FIM-Normenanalyse unterstützt bei Analyse von relevanten Handlungsgrundlagen der öffentlichen Verwaltung zur Identifizierung von FIM-Leistungen im Rahmen der Leistungsklärung sowie zur Erstellung von FIM-Stamminformationen. 

Zugang: http://fim-normenanalyse.bfpi.de/

- Zugangsdaten können über das Ticketsystem des FIM-Portals beantragt werden: ticket@fimportal.de
- Anforderung an die Weiterentwicklung können über das Ticketsystem des FIM-Portals gemeldet werden.


## Welche Musterprozesse gibt es bereits?

Die Musterprozesse sind Bestandteil des Prozessbaukastens. Sie werden mit Typ 8 Leistungen (=Querschnittsleistungen), die der Leistungskategorie „Sonderleistungen“ angehören, verknüpft.

Auf Wunsch des „Redaktionszirkels Prozesse und Datenfelder“ wird nun für den Schnelleinstieg eine Übersicht der Musterprozesse zur Verfügung gestellt:

[Übersicht Musterprozesse](https://neu.fimportal.de/search?resource=sampleprocess&page=1&page_size=20)

Fragen oder Anregungen zu den Musterprozessen können über das Ticketsystem des FIM-Portals  ticket@fimportal.de eingereicht werden. Der FIM-Baustein Prozesse wird zu den Fragen oder Anregungen in Abstimmung mit dem „Redaktionszirkel Prozesse und Datenfelder“ Stellung nehmen.