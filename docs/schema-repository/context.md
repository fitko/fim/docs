# Kontext

Die Kontextbeschreibung gilt für den Zeitpunkt vor Rollout des Sammelrepository; die Inhalte können sich mit der Zeit ändern (wegen Änderungen des Kontexts) oder in dieser Dokumentation verlagert werden (wegen Übergang von Projekt zu Produkt).

## FIM

Das Sammelrepository ist ein Werkzeug, mit dem im Umfeld von FIM gearbeitet werden kann. [FIM](http://fimportal.de/), das föderale Informationsmanagement, ist eine Methodik mit rund zehn Jahren Entwicklungsgeschichte und einer [Empfehlung vom 28.06.2018](https://www.it-planungsrat.de/beschluss/beschluss-2018-23)des IT-Planungsrats zur Nutzung von FIM zur Umsetzung von OZG .

## Lage bei Projektbeginn, bekannte Probleme

* Dezentrale Redaktionen in Ländern, Bund und beim Baustein, abweichende Datenstände im FIM-Portal und in der OZG-Informationsplattform für Datenschema; es soll zukünftig eine zentrale Ablage geben.
* Zwei verschiedene Redaktionssysteme, eines überträgt veröffentlichte Daten täglich ins FIM-Portal; es soll zukünftig schnellerer Austausch möglich sein.
* Datenaustausch zwischen den Landesredaktionen entweder über SharePoint, E-Mail etc. oder über Freischaltung eines Zugriffs auf das Repository der anderen Redaktion; es soll zukünftig transparenter Zugriff auf alle veröffentlichten Daten anderer Redaktionen möglich sein.
* Datenschemata für Landesrecht und kommunales Satzungsrecht (Typ 4 und 5) werden noch kaum erfasst; Sammelrepository zukünftig nicht nur für Bundesrecht, sondern auch für Landesrecht und kommunales Satzungsrecht.
* Kein maschinenlesbarer Zugang zu den FIM Datenfeldinformationen über eine Schnittstelle für Fachverfahren und Online-Dienste; Sammelrepository für diese Systeme und insbesondere für die Nutzung bei FIT-Connect-Zustellpunkten

![Kontext vor Projektbeginn](Kontext-vor-Projekt.png)

Das Diagramm zeigt, dass Datenaustausch nur unidirektional und keine Nutzung der FIM-Artefakten über APIs durch Drittsysteme möglich war.

## Projektergebnisse

Das Sammelrepository

* ermöglicht einen einfachen, redaktionsübergreifenden Austausch der Datenschemata
* dient als Datenbasis für Such,- Filter,- und Downloadfunktionen des FIM-Portals
* stellt FIM-Datenfeldinformationen für Fachverfahren und Online-Dienste bereit

**Mehrwerte** für OZG-Umsetzungsprojekte: Das Sammelrepository

* stellt FIM-Datenfeldinformationen für Fachverfahren und Online-Dienste bereit
* ermöglicht jedes Datenschema über eindeutige URL zu identifizieren
* unterstützt automatisches Laden und Generieren von Formularen auf Grundlage der Schema-URL
* bietet neue Dienste zur Konvertierung, zur Qualitätssicherung und zum XDF-Up-/Downcast

**Abgrenzung:** Was ist nicht Gegenstand des Projekts (jedenfalls im Moment):

* Die Software hat keine offizielle Web-Oberfläche, es ist eine REST-API. Analog einem Webserver, der für die meisten Anwendungsfälle zur Nutzung einen Webbrowser braucht
* Es ist das Sammelrepository des Bausteins Datenfelder, nicht der zwei anderen Bausteine Leistungen und Prozesse.
* Es unterstützt beim Import den Standard XDatenfelder für Datenschemata, keine anderen Datenformate.
* Es ist kein Editor für XDatenfeld-Dateien

![Kontext nach Projektende](Kontext-nach-Projekt.png)

Das Diagramm zeigt die Nutzung der zentrale Programmierschnittstelle (API) für bidirektionalen Datenaustausch und für die Datennutzung durch Drittsysteme.