---
sidebar_label: Erläuterung
---

# Das Föderale Informationsmanagement (FIM)

## Was ist FIM?
Das Föderale Informationsmanagement (FIM) sorgt für die standardisierte Übersetzung der Rechtssprache in eine bürger:innen- und unternehmensfreundliche Sprache.

FIM reduziert den redaktionellen Aufwand in Ländern und Kommunen durch Nachnutzungsmöglichkeiten bereits erstellter, qualitätsgesicherter Beschreibungen von Verwaltungsleistungen sowie durch standardisierte Beschreibungen von Datenfeldern für die digitale Antragsbearbeitung.

Einen Überblick über FIM findet sich im [FIM-Portal](https://fimportal.de).



