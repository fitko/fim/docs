## Grundregeln
##### Status
Erfassen Sie in jedem Leistungssteckbrief für Typ 1 bis 3 Leistungen den jeweiligen Status.
##### Eindeutigkeit der Leistung
Eine Leistung ist immer über den Leistungsschlüssel eindeutig identifiziert. Es kann deshalb für eine Leistung nicht zwei Leistungsschlüssel geben.

Eine Leistung ist immer über den Leistungsschlüssel eindeutig identifiziert. Es kann deshalb für eine Leistung nicht zwei Leistungsschlüssel geben.

Die Leistungsbezeichnung II muss eindeutig sein. Es darf eine Leistungsbezeichnung II also nur einmal im gesamten Leistungskatalog geben

Die Codeliste der Status finden Sie [hier](https://www.xrepository.de/details/urn:xoev-de:xprozess:codeliste:status)
##### Aktualität mehrsprachiger Stammtexte/Leistungs -beschreibungen
Gibt es zu einem Stammtext/einer Leistungsbeschreibung eine human erstellte Übersetzung, prüfen Sie diese bitte auf Aktualität, wenn der/die ursprüngliche Stammtext/Leistungsbeschreibung geändert wurde
##### Stammtext vs. Leistungsbeschreibung
Achten Sie bitte darauf, dass sie in einem Stammtext keine Vollzugsinformationen erfassen.
##### Stammtexte - Schreiben Sie nur, was Sie abschließend regeln
Wenn Sie einen Stammtext erfassen, befüllen Sie ein Modul nur dann mit Informationen, wenn sie für alle nachgeordneten Ebenen zutreffen. Ansonsten lassen Sie das Modul leer. Das Modul kann dann durch eine nachgeordnete Ebene gefüllt werden (Stammtext- und Ergänzungsmodell).
##### Module richtig nutzen
Beachten Sie, was als Inhalt eines Moduls im Folgenden definiert ist.
##### Keine Fußnoten
 Vermeiden Sie Verweise auf Module untereinander. Verwenden Sie keine Fußnoten.
#### Korrekter Leitungszuschnitt
Ordnen Sie alle Informationen eines Themas (Leistungskennung) möglichst konkret den einzelnen Verrichtungen und Verrichtungsdetails zu.

Ihr Fachbereich entscheidet mit Ihnen, ob eine Leistungsbeschreibung auf der Ebene einer Verrichtungskennung oder ggfs. dem darunter liegenden Verrichtungsdetail erstellt wird. Mit dem „Leistungsadressat“ steht Ihnen eine Möglichkeit zur Verfügung, zu kennzeichnen, ob an einem jeweiligen LOV/LOVD ein Stammtext bzw. eine Leistungsbeschreibung erfasst wurde. Empfehlungen dazu, wann bzw. ob anstelle eines LOV ein LOVD erstellt werden sollte, finden Sie im [Leitfaden Zuschnittsindikatoren](https://fimportal.de/fim-haus)
#### keine Stammtext-Pflicht
Sie entscheiden zusammen mit dem für die Erstellung der Stamminformationen verantwortlichen Fachbereich, ob zu einem Leistungssteckbrief ein Stammtext bzw. eine Leistungsbeschreibung erstellt wird, oder nicht.
#### strukturierte Erfassung
Erfassen Sie bevorzugt strukturiert, wenn für ein Modul die unstrukturierte und die strukturierte Erfassung möglich sind 

Die strukturierte Erfassung gilt für: 
- die Organisationseinheit (zuständige Stelle bzw. Ansprechpunkt)
- den Online-Dienst
- Kosten
- Fristen
- Bearbeitungsdauer

Erfassen Sie in Textmodulen nur Texte. Die Textmodule sind in der Anlage zum Musterformular gekennzeichnet

## Verständlichkeit (Schreiben, was den Leser interessiert)
#### Für Laien schreiben
Bauen Sie die Texte aus der Perspektive der Bürgerinnen/Bürger und Unternehmen auf. Diese sind in der Regel Laien und nicht mit dem Thema und der Verwaltungsfachsprache vertraut. Beginnen Sie mit der Ausgangssituation. Schreiben Sie für jemanden, der sich die Fragen stellt „Betrifft mich das?“ und „Was habe ich davon?“. Beantworten Sie diese Fragen zuerst. Die Formulierung sollte beim ersten Lesen verständlich sein.

**Beispiel**: statt: *„Hunde unterliegen der Pflicht zur ordnungsbehördlichen Anmeldung“* besser: *„Wenn Sie einen Hund halten, müssen Sie ihn anmelden.“*
#### Allgemeines vor Speziellem
Schreiben Sie zuerst, was für alle gilt. Machen Sie deutlich, was nur in Ausnahmefällen gilt. Beginnen Sie auch bei den Ausnahmen mit der Ausgangssituation.

**Beispiel**: Schreiben Sie im Modul „Erforderliche Unterlagen“ statt: *Vollmacht (nur bei Vertretung)* besser: *Bei Vertretung: Vollmacht*
#### Den Regelfall beschreiben
Beschreiben Sie immer den Standardfall einer Leistung. Machen Sie jedoch deutlich, wenn Sie Sonderfälle auslassen. Für die Sonderfälle können Sie ggf. auf Beratungsangebote oder andere Leistungen verweisen. 

Beispiel: *„In der Regel ist für die Vergabe der Betriebsnummer die Bundesagentur für Arbeit zuständig. Unter bestimmten Voraussetzungen müssen Sie sich an einen anderen Ansprechpartner wenden (…)"*
#### Beispiele nennen
 Verdeutlichen Sie Kompliziertes durch Beispiele. Erläutern Sie komplexe Berechnungsgrundlagen durch ein Beispiel. 
 
 „Wenn Sie vor dem 1. Januar 1953 geboren sind, können Sie die Rente bereits ab 63 Jahren erhalten. Danach steigt die Altersgrenze mit jedem Jahrgang um zwei Monate. 
 
 **Beispiel**: *Sie sind am 1. Januar 1964 geboren und haben 45 Beitragsjahre erreicht. Dann können Sie am 1. Januar 2029 ohne Abschläge in Rente gehen, das heißt an Ihrem 65. Geburtstag.“* 
 
 Verwenden Sie keine Fettungen, Kursivierungen oder Unterstreichungen, um Betonungen in einem Satz hervorzuheben. Alle Sätze müssen auch ohne solche Betonung verständlich sein. 
 
 **Beispiel**: Statt: *„Eine Förderung für bereits angeschaffte Sachgüter ist **nicht** möglich.“* Besser: *„Sie können jedoch keine Förderung erhalten, wenn Sie die Sachgüter bereits angeschafft haben.“*

## Bürgerfreundlich schreiben
#### Höflichkeit
Schreiben Sie in einem höflichen und freundlichen Ton.
#### Zeitgemäße Sprache
Verwenden Sie zeitgemäße Sprache. Vermeiden Sie amtssprachliche oder veraltete Wörter und Wendungen.

**Beispiele**: statt: *„Lichtbild“* und *„von Hundert“* besser: *„Foto“* und *„Prozent“*
#### Betonung Dienstleistung
Vermeiden Sie das Über-/Unterordnungsverhältnis zu betonen. Verstehen Sie sich als Dienstleistungserbringende.

**Beispiel**: Vermeiden Sie Sätze im Obrigkeitsstil mit „ist zu" oder „haben zu": statt: *„Für diesen Zeitraum ist eine entsprechende Gewinnermittlung vorzulegen."* besser: *„Für diesen Zeitraum müssen Sie eine Gewinnermittlung beifügen."*
#### Auf Superlative verzichten
Verzichten Sie auf Modewörter, Sinndoppelungen und Superlative.
#### Sparsamer Umgang mit Anglizismen
Anglizismen sollten sparsam eingesetzt werden. Verwenden Sie Anglizismen nur, wenn sie im Deutschen etabliert sind oder es keine gleichwertigen deutschen Alternativen dafür gibt. Oft stehen etablierte Anglizismen bereits im Duden oder in anderen Wörterbüchern. Geläufige Wörter, wie z. B. E-Mail oder Leasing, sind zu verwenden und nicht durch fragwürdige deutsche Bildungen zu ersetzen, die niemand kennt
#### Persönliche Anrede
Reden Sie die Adressaten mit „Sie“ an. 

**Beispiel**: statt: *„Antragsteller haben die Möglichkeit […]“* besser: *„Sie können [...]“*

**Beispiele**: statt: *„anzeigen"* und *„belehren"* und *„nach Vollendung des 18. Lebensjahres"* besser: *„melden"* und *„informieren"* und *„ab 18 Jahren"*.
#### Fachbegriffe erläutern
Wenn Sie einen Fachbegriff verwenden müssen, erläutern Sie ihn.

**Beispiel**: *„Der Regelsatz ist ein fester Betrag für …“*
#### nur verwenden, was verstanden wird
Fachbegriffe und Fremdwörter sollten Sie nur verwenden, wenn sie sicher sind, dass sie verstanden werden.

| statt:              | besser:                               |
| ------------------- | ------------------------------------- |
| Billigkeitsmaßnahme | Ratenzahlung oder Stundung            |
| Baumscheibe         | offener Bodenbereich um den Baumstamm |
| Feedback            | Rückmeldung                           |
| realisieren         | verwirklichen                         |
| Administration      | Verwaltung                            |
| obsolet             | überflüssig                           |
| adäquat             | angemessen                            |
#### Positiv bleiben
Vermeiden Sie negative Aussagen. Eine kurze Begründung: siehe [hier](https://www.redenwelt.de/rede- tipps/negative-formulierungen-in-reden-vermeiden-schluss-mit- negativformulierungen/#Reden_halten_Grundlagen_der_Rhetorik _um_Negative_Formulierungen_zu_verbannen)

**Beispiel**: statt: *„[…] ist ohne Erlaubnis verboten.“* besser: *„Mit einer Erlaubnis dürfen Sie […]“*

Vermeiden Sie doppelte Verneinungen. siehe [hier](https://www.spiegel.de/kultur/zwiebelfisch/zwiebelfisch- nein-zweimal-nein-a-394969.html)

**Beispiel**: statt: *„nicht ohne“* besser: *„mit“*

#### Sachlich schreiben
Schreiben Sie die Texte unpolitisch und frei von Werbung und Wertung.

**Beispiel**: statt: *„Das Regierungsprogramm der Regierungskoalition sieht vor, dass Eltern, die ihre Kindern zuhause betreuen einen Zuschuss erhalten können.“* besser: *„Wenn Sie Ihr Kind zuhause betreuen, können Sie einen Zuschuss erhalten.“*

#### konkret bleiben
Bürgerfreundlichkeit ist auch immer: so konkret wie möglich formulieren. 

Wenn Sie Sachverhalte, Entscheidungen, Aufforderungen oder mögliche Konsequenzen konkreter formulieren, erleichtern Sie Bürgerinnen und Bürgern das Verständnis. Oft bietet es sich an, Beispiele einzufügen.

**Beispiel**: statt: *Unternehmen haben die Gelegenheit, bis zum ... die Unterlagen vorzulegen. Sollten die Unterlagen nicht bis zu diesem Datum vorliegen, so behalten sich die Behörden ordnungsbehördliche Maßnahmen vor.* besser: *Unternehmen sind aufgefordert ihre Unterlagen bis spätestens zum ... einzureichen. Sollten Sie die Unterlagen nicht bis zu diesem Datum eingereicht haben, können die Bauarbeiten stillgelegt werden. Zudem kann die Behörde ein Bußgeldverfahren gegen Sie einleiten.*

## Satzbau
#### Kurze Sätze / Satzlänge
Teilen Sie lange Sätze in mehrere kurze Sätze auf. Schreiben Sie nicht mehr als 20 Wörter pro Satz. Eher 12 bis 15.
#### Wichtiges in Hauptsätzen
Sagen Sie alles Wichtige in Hauptsätzen. Schreiben Sie je Kernaussage einen eigenen Satz.

**Beispiel**: statt: *„Die Gültigkeitsdauer des Internationalen Führerscheins, die 3 Jahre beträgt, kann nicht verlängert werden.“* besser: *„Der Internationale Führerschein ist 3 Jahre gültig. Die Gültigkeit kann nicht verlängert werden.“*

Platzieren Sie das Verb so früh wie möglich im Satz.

#### Aktiv statt Passiv
Schreiben Sie im Aktiv. 

**Beispiel**: statt: *„Der Zuschuss wird ab dem Monat, in dem der Antrag bei der Behörde eingegangen ist, gezahlt.“* besser: *„Sie erhalten den Zuschuss ab dem Monat, in dem Ihr Antrag eingegangen ist.“*
#### Verben im ersten Satzteil
Verwenden Sie aussagekräftige Verben. Wenn sie das Verb in den ersten Satzteil stellen, ist der Inhalt leichter zu verstehen.

**Beispiel**: statt: *„in Kenntnis setzen“* besser: *mitteilen“*
#### lebendige Verben und Adjektive
Kein Nominalstil. Vermeiden Sie substantivierte Verben und Adjektive (Diese enden auf: -ung; -keit; -heit).

**Beispiel**: statt: *„Änder**ung** durchführen“ und „Zuständigkeit“* besser: *„ändern“* und *„zuständig“*

Vermeiden Sie zu viele Substantive. Gehen Sie sparsam mit Adjektiven/Attributen und Partizipien um. Diese blähen die Texte unnötig auf und verwirren. 

**Beispiel**: statt: *positive Entwicklung* besser: *Verbesserung* 
statt: *konkreter Einzelfall* besser: *Einzelfall*
## Wortwahl
#### gleiches gleich bezeichne
Wiederholen Sie dieselben Wörter, wenn Sie dasselbe meinen.

**Beispiel**: statt: *„Das **Formular 2001-10** können Sie verwenden. Füllen Sie den **Vordruck** vollständig aus und unterschreiben Sie das **Formblatt.** Geben Sie den **Antrag** dann persönlich ab.“* besser: *„Füllen Sie das **Formular 2001-10** vollständig aus und unterschreiben Sie es. Geben Sie das **Formular 2001-10** bitte persönlich ab.*
#### klare Aussagen
Vermeiden Sie missverständliche Wörter (wie „grundsätzlich“ oder „relativ“).

**Beispiel**: statt: *„Für alle Menschen besteht grundsätzlich ein Anspruch auf Sozialhilfe“* besser: *„Sie können Sozialhilfe erhalten, wenn Sie […]“*
#### Fremdwörter meiden
Vermeiden Sie Fremdwörter, wenn es ein treffendes einfaches deutsches Wort gibt. Fremdwörter, die geläufiger sind als deutsche Begriffe, können Sie verwenden.

**Beispiele**: statt: *„essentiell“* und *„transferieren“* besser: *„notwendig“* und *„übertragen“* 
statt: *„Abschrift“* und *„Rechner“* besser: *„Kopie“* und *„Computer“*
#### Lange Wörter aufteilen
Lange Wörter, Zusammensetzungen aus mehr als drei Wortgliedern, sind schwer lesbar. Teilen Sie lange Wörter daher auf. Auch Bindestriche sind erlaubt

**Beispiel**: statt: *„Hinterbliebenenleistungsempfänger“* und *„Bewohnerparkausweis“* besser: *„Empfänger von Leistungen an Hinterbliebene“* und *„Bewohner-Parkausweis“*
#### Füllwörter und Redundanzen weglassen
 Lassen Sie Wörter und Wortteile weg, die im Zusammenhang keine zusätzlichen Informationen tragen.
 
 **Beispiele**: statt: *„getroffene Vereinbarung“* und *„überprüfen“* besser: *„Vereinbarung“* und *„prüfen“* 
 
 Folgende Wörter können im Normalfall weggelassen werden: *absolut, ausdrücklich, eigentlich, gegebenenfalls, auch, aber.* 
 
 Lassen Sie auch einleitende Wendungen weg wie *„Es wird darauf hingewiesen, dass …“.*
## Geschlechterneutrale Sprache
Schreiben Sie geschlechtergerecht. Damit wird der Grundsatz der Gleichberechtigung auch in der Sprachform gewahrt. 

Die Qualitätssicherungskriterien des Bausteins Leistungen orientieren sich an den Empfehlungen des [Rates für deutsche Rechtschreibung]([[FIM-Baustein_Leistungen-Qualitaetssicherungskriterien_V1.00.pdf#page=14&annotation=241R|FIM-Baustein_Leistungen-Qualitaetssicherungskriterien_V1.00, page 14]]); [Zitat]([[FIM-Baustein_Leistungen-Qualitaetssicherungskriterien_V1.00.pdf#page=14&annotation=242R|FIM-Baustein_Leistungen-Qualitaetssicherungskriterien_V1.00, page 14]]): 

„Das Amtliche Regelwerk gilt für Schulen sowie für Verwaltung und Rechtspflege. Der Rat hat vor diesem Hintergrund die Aufnahme von Asterisk („Gender-Stern“), Unterstrich („Gender-Gap“), Doppelpunkt oder anderen verkürzten Formen zur Kennzeichnung mehrgeschlechtlicher Bezeichnungen im Wortinnern in das Amtliche Regelwerk der deutschen Rechtschreibung zu diesem Zeitpunkt nicht empfohlen."*

Berücksichtigen Sie bei Personenbezeichnungen alle Geschlechter. Dazu haben Sie mehrere Möglichkeiten: 
- direkte Ansprache: statt *„der Antragsteller“* besser *„Sie“*
- geschlechtsneutrale Bezeichnungen wie *„minderjährige Person“*, *„vorsitzendes Mitglied“*, *„Elternteil“*, *„wer in Berlin wohnt“*
- Mehrzahl: statt *„die oder der Beschäftigte“* besser *„die Beschäftigten“* 
Verwenden Sie nur dann Paarformen, wenn keine geschlechterneutrale Formulierung möglich ist :
- Bürgerinnen und Bürger
- Antragstellerin oder Antragsteller

Verkürzen Sie Doppelnennungen nicht. Schreiben Sie die männliche und die weibliche Form voll aus

**Beispiele**: statt: *„Bürger(innen)“*, *„Bürger/innen“*, *„Bürger_innen“* oder *„BürgerInnen“*. besser *„Bürgerinnen und Bürger“*

Wenn Sie nicht wissen, wie die geschlechtsneutrale Personenbezeichnung lautet, informieren Sie sich auf folgenden Internetseiten: 
- Aktualisierte Fassung des amtlichen Regelwerks entsprechend den Empfehlungen des Rats für deutsche Rechtschreibung 2016. 
Link: [Regeln und Wörterverzeichnis]([[FIM-Baustein_Leistungen-Qualitaetssicherungskriterien_V1.00.pdf#page=14&annotation=240R|FIM-Baustein_Leistungen-Qualitaetssicherungskriterien_V1.00, page 14]])
## Fließtexte strukturieren
Strukturieren Sie lange Fließtexte durch Absätze, Zwischenüberschriften und Aufzählungen.
#### sinnvolle Absätze
Bilden Sie Absätze nach Sinneinheiten.

Grundsatz: Ein Gedanke je Absatz.
#### Zwischenüberschriften
Fügen Sie Zwischenüberschriften ein, im Idealfall aber nicht mehr als 2 Textebenen.
#### Ergänzungsstriche
Setzen Sie Ergänzungsstriche nur, wenn Sie zwei Wörter mit derselben Endung verwenden, und kein Adjektiv eingeschoben ist.
Beispiel: statt: *Basiskranken- und gesetzlicher Pflegeversicherung* besser: *Dienst- und Handwerkerleistungen*
#### Klammerzusätze
Klammerzusätze im Fließtext sollten Sie vermeiden, weil sie den Lesefluss hemmen.

**Beispiel**: statt: *…. (§ 1 Einkommensteuergesetz (EStG))* besser: *…. (§ 1 Einkommensteuergesetz)* 

In - in Klammern gefassten - Gesetzeszitaten verwenden Sie bitte keine doppelten Klammern.

Verweisen Sie im Modul „Volltext“ oder anderen Textmodulen nicht auf die Rechtsvorschrift in der der Sachverhalt geregelt ist. Es sollte nur unter „Handlungsgrundlage(n)“ auf die Rechtsvorschrift verwiesen werden.
#### Geschützte Leerzeichen
Achten Sie darauf, dass mit einem Wort oder (Sonder-)Zeichen inhaltlich eng verbundene Zahlen am Zeilenende nicht getrennt werden. Verwenden Sie in solchen Fällen das „geschützte“ Leerzeichen " 

**Beispiel**: statt: *reichen Sie die Unterlagen bis spätestens 31. Dezember 2020 ein.* besser: *reichen Sie die Unterlagen bis spätestens 31. Dezember 2020 ein.*
## Aufzählungen
#### Listen nutzen
Formatieren Sie Aufzählungen als Liste mit Aufzählungszeichen (unsortierte Liste). Wenn eine Reihenfolge zwingend ist, können Sie die Liste auch nummerieren (sortierte Liste).

**Beispiele**: 
unsortierte Liste:
- Punkt 1
- Punkt 2 
sortierte Liste: 
1. Punkt 1 
2. Punkt 2
#### Verwenden Sie „und“ oder „oder
 Aufzählungen im Fließtext trennen Sie bitte nicht durch Schrägstrich „/“ oder „bzw.“. Verwenden Sie stattdessen die Worte „und“ oder „oder“. 
 
 Eine Aufzählung muss mehr als einen Unterpunkt enthalten. Wenn es nur einen Unterpunkt gibt, dann darf kein Aufzählungszeichen davor gesetzt werden. 
 
 In folgenden Modulen sind zwingend Aufzählungen zu verwenden:
 - Erforderliche Unterlagen
 - Kurztext 
 
 In folgenden Modulen dürfen keine Aufzählungen verwendet werden:
 - Bezeichnung I und II
 - Teaser
## Links (Verlinkungen)
#### Keine Links im Text
Vermeiden Sie, Links im Fließtext zu erfassen.

Nutzen Sie für Links die entsprechenden Felder des „Musterformulars Leistungsbeschreibungen“

Erfassen Sie Links nur, wenn diese das Präfix „https“ tragen.
#### Linktexte nutzen
 Verwenden Sie Linktexte, die einen Hinweis darauf geben, wohin der Link führt. Linktexte sollen aus mindestens zwei Wörtern bestehen.
 
 **Beispiel**: statt: *„Ein Merkblatt der XYZ-Behörde finden Sie hier“* besser: *„Merkblatt der XYZ- Behörde“*
 
 Bei Links auf Dokumente können Sie Dateityp, Größe und Seitenzahl in Klammern anfügen. Trennen Sie die Informationen mit Semikolons. Runden Sie die Größe in Megabyte auf eine Nachkommastelle. Kürzen Sie Dateiformat und Megabyte ab.
 
 **Beispiele**: 
 - Fotomustertafel der Bundesdruckerei 
 - Fotomustertafel der Bundesdruckerei (PDF; 2,1 MB; 8 Seiten)
## Schreibweise: Rechtschreibung, Abkürzungen, Zahlen, Maßeinheiten, Sonderzeichen, Zitieren
#### amtliches Regelwerk beachten
Halten Sie sich an die aktualisierte Fassung des amtlichen Regelwerks entsprechend den Empfehlungen des Rats für deutsche Rechtschreibung.
Wenn mehrere Schreibweisen möglich sind, wählen Sie die vom Duden empfohlene.
#### nicht abkürzen
Kürzen Sie nicht ab. Verwenden Sie auch keine vermeintlich gängigen Abkürzungen wie *„Abs.“* oder *„ggf.“*, *„z.B.“*.
Verwenden Sie Abkürzungen nur dann, wenn diese bekannter sind als die ausgeschriebene Fassung und die Abkürzung auch gesprochen wird („ARD“).
Dies entspricht auch dem gängigen Vorgehen zur Barrierefreiheit, vgl. [[https://www.bitvtest.de/infothek/artikel/lesen/abkuerzungen.html]] 

Wenn Sie Abkürzungen verwenden wollen, schreiben Sie den Begriff beim ersten Mal aus und fügen die Abkürzung in Klammern an. Danach können Sie die Abkürzung verwenden.

**Beispiel**: *„Europäische Union (EU)“*
#### Zahlen als Ziffer
Schreiben Sie Zahlen als Ziffern, aber den unbestimmten Artikel „ein/eine“ als Wort.

Kürzen Sie auch Maßeinheiten im Normalfall nicht ab („Jahr“, „Meter“). In technischen Zusammenhängen können Sie die Einheiten abkürzen („cm³“, „kW“). 

**Beispiel**: Statt: *„Gebäude über 5m Firsthöhe […]“* und *„Fahrzeuge mit weniger als 1.500 Kubikzentimetern Hubraum.“* Besser: *„Gebäude über 5 Meter Firsthöhe […]“* und *„Fahrzeuge mit weniger als 1.500 cm³ Hubraum.“*
#### Zitieren von Handlungsgrundlagen
Erfassen Sie Handlungsgrundlagen nur im Modul „Handlungsgrundlagen(n)“.

Sofern dies doch – und mehrfach - erforderlich ist, verwenden Sie folgenden Zitier-Stil 

Orientieren Sie sich an den Ausführungen im Abschnitt 2.2.9 – Modul „Handlungsgrundlage(n)“.

Zitieren von Handlungsgrundlagen Hinweise auf Rechtsgrundlagen sind im Volltext und anderen Textmodulen in der Regel entbehrlich. Stattdessen können Sie den Rechtsbereich nennen. Und die genaue Norm/Fundstelle im Modul „Handlungsgrundlage(n)“ angeben. 

**Beispiel**: statt: *„Nach § 91 TKG benötigen Sie dafür eine Frequenzzuteilung.“* besser: *„Nach den Regelungen des Telekommunikationsrechts benötigen Sie dafür eine Frequenzzuteilung.“*

## Einzelne Formate: Datum, Anschrift, Telefon, Fax, E-Mail, Öffnungszeiten
#### TT.MM.JJJJ
Datumsformat: TT.MM.JJJJ TT.MM.JJJJ

**Beispiel**: *02.11.2015*

Format für Anschriften in Deutschland: 

Organisationsname
\[falls erforderlich: Abteilung/Ressort\]
Straße Hausnummer \[falls erforderlich: Adresszusatz\]
PLZ Ort 

**Beispiel**: 
*Ministerium für Infrastruktur und Digitales des Landes Sachsen-Anhalt
Geschäfts- und Koordinierungsstelle FIM / Baustein Leistungen
Turmschanzenstraße 30
39114 Magdeburg* 

Verwenden Sie für Anschriften im Ausland den Standard des jeweiligen Staates.

Schreiben Sie darunter den Staat auf Deutsch.

Sie können eine Anschrift bei Bedarf mit zusätzlichen Informationen versehen, zum Beispiel:
- über einen barrierefreien Zugang,
- über Haltestellen von öffentlichen Verkehrsmitteln in der Nähe oder – über Parkmöglichkeiten in der Nähe.
#### internationale Telefonnummern
Format für Telefon- und Faxnummern: \[Länderkennung\]\[Ortsnetzkennzahl\]\[Leerzeichen\]\[Teilnehmerrufnummer\]\[Bindestich\]\[Durchwahl\]

Wenn Sie mehrere Telefonnummern mit unterschiedlichen Funktionen angeben, können Sie die Funktionen in Klammern hinter den Nummern schreiben. 

Lange Ziffernfolgen können Sie mit Leerzeichen in Vierer-Blöcke gliedern.

**Beispiel**: 
Tel.: +49 391 895-9995 (Urkundenstelle)
Tel.: +49 391 895-9999 (Auskunft)
Tel.: +49 800 1000 4800 (Hotline)
Mobil: +49 163 1815 234
Fax: +49 40 1527-9090
#### Erreichbarkeit von Hotlines angeben
Geben Sie bei der strukturierten Erfassung von Telefon- und Faxnummern zusätzlichen Informationen an, wenn Ihnen diese bekannt sind. Dies sind beispielsweise Erreichbarkeiten und Angaben zu Kosten.

**Beispiel**:
Tel.: +49 391 895-0 (Hotline)

Anrufzeiten:
Montag 09:00 – 18:00 Uhr
Dienstag 09:00 – 18:00 Uhr
Mittwoch 09:00 – 18:00 Uhr
Donnerstag 09:00 – 18:00 Uhr
Freitag 09:00 – 18:00 Uhr 

Aus dem deutschen Festnetz: 0,14 EURO pro Minute, Mobilfunk: bis zu 0,42 EURO pro

Schreiben Sie E-Mail-Adressen als Linktext.

**Beispiel**: E-Mail: info@kontakt.de
#### Öffnungszeiten
Listen Sie die Tage Montag bis Freitag auf jeden Fall auf. Falls an diesen Tagen nicht geöffnet ist, schreiben Sie dahinter „geschlossen“.

Listen Sie Samstag und Sonntag nur auf, falls an diesen Tagen geöffnet ist.
Kürzen Sie die Wochentage mit einem Punkt ab (Montag, Dienstag, Mittwoch, Donnerstag, Freitag, Samstag, Sonntag).

Format für Zeitangaben: „**HH**:mm Uhr“

Format für Zeiträume: „**HH**:mm – **HH**:mm Uhr“ (Leerzeichen vor und nach dem Gedankenstrich). Schreiben Sie „und“ zwischen mehreren Zeiträumen. 

Öffnungszeiten am Vor- und Nachmittag oder dazwischen werden jeweils als separater Zeitraum angegeben.

Falls es Besonderheiten zu den einzelnen Tagen gibt, schreiben Sie diese zuletzt. Setzen Sie ein Komma zwischen Uhrzeiten und Besonderheiten.

Verlinken Sie auf aktuelle Wartezeitenangaben, wenn dies möglich ist.

**Beispiele**: 
Montag 07:30 – 12:30 Uhr
Dienstag 00:00 – 24:00 Uhr
Mittwoch geschlossen
Donnerstag 07:30 – 12:30 Uhr und 14:00 – 16:00 Uhr
Freitag nur mit Termin
Samstag 07:30 – 12:30 Uhr, nur jeden ersten Samstag im Monat