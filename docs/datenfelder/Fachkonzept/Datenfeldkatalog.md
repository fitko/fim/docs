# Dokumentsteckbriefe

## Grundlegendes über den Datenfeldkatalog

Ein Dokumentsteckbrief unterstützt die Katalogisierung von Formularen und Dokumenten, indem er
eine übersichtliche Menge an Metadaten als Steckbrief bereitstellt. Folglich bildet die Menge der
Dokumentsteckbriefe den Datenfeldkatalog.
Mit Hilfe von Dokumentsteckbriefen kann im ersten Schritt ein Dokument (mit seinen Metadaten) im
Datenfeldkatalog erfasst werden, ohne gleichzeitig oder direkt anschließend die Struktur des
Dokuments (d.h. das zugehörige Datenschema) erstellen zu müssen. Folglich können beispielsweise
die mitzubringenden Unterlagen innerhalb einer Leistung katalogisiert und referenziert werden, ohne
dass sie strukturell erfasst worden sind. So enthält ein Dokumentsteckbrief keine Angaben zu den
Datenfeldern und -gruppen, die die Struktur des Dokuments repräsentieren sollen. Diese werden zu
einem späteren Zeitpunkt mit dem zugehörigen Datenschema abgebildet.
Dokumentsteckbriefe werden im Datenfeldkatalog des Bausteins Datenfelder gesammelt und auf diese
Weise dem Nutzer zur Verfügung gestellt.

## Metadaten

Die Metadaten geben Aufschluss über die Inhalte des durch
den Steckbrief beschriebenen Dokuments. Die folgende Tabelle listet die Metadaten für
Dokumentsteckbriefe auf.

### Identifikationsangaben

|Metadatum|Definition|
|-----|-----|
|<u>**Identifizierungsangabe (ID)***</u> (1)|Eine eindeutige ID, siehe Identifikatoren.|
|<u>**Versionsangabe***</u> (1 für versionierte Dokumentsteckbriefe, 0 sonst)|Die Versionsnummer wird verwaltet, um die Existenz unterschiedlicher Versionen von Elementen zu ermöglichen.|

### Definitionsangaben

|Metadatum|Definition|
|-----|-----|
|**Name** (1)|FIM-interner Name des Datenfelds, der sichtbar ist für alle Rollen in FIM, die auf der Ebene der abstrakten Formulare tätig sind (fachliche Ersteller, Methodenexperten, Informationsmanager, FIM-Koordinierungsstelle, FIM-Nutzer).|
|*Definition* (0…1)|Fasst den Inhalt eines Datenfelds aus redaktioneller Sicht zusammen, d. h. aus Sicht der Rollen fachlicher Ersteller, Methodenexperte, Informationsmanager, FIM-Koordinierungsstelle, FIM-Nutzer.|
|*Bezug zur Handlungsgrundlage* (0…n)|Dieses Attribut ermöglicht es, Bezüge zu einschlägigen Rechtsnormen sowie zu in anderen Vorhaben wie den XÖV-Vorhaben (z. B. XWaffe) oder Vorhaben wie P23R gleich oder ähnlich definierten Datencontainer zu erstellen und diese Verbindung zu dokumentieren. Hierdurch lässt sich die Ausgestaltung eines Stammdatenschemas gemäß einschlägigen Rechtsnormen oder Standardisierungsverfahren belegen. Es besteht die Möglichkeit zu jeder Handlungsgrundlage eine URI zu hinterlegen.|
|*Beschreibung* (0…1)|Zusätzliche Beschreibung und Erläuterung eines Datenfelds für FIM-Nutzer.|
|*Stichwörter (Tags)* (0…n)|Dieses Metadatum beinhaltet eine Liste an vorgegebenen und frei wählbaren Stichwörtern, die es dem FIM-Nutzer ermöglichen, eine spezielle Semantik zu kennzeichnen oder eine Suche innerhalb eines Redaktionssystem zu erleichtern.|
|*Relation* (0…n)|Relationen dokumentieren Beziehungen der Elemente untereinander, wie z.B. "ist abgeleitet von", "ersetzt" oder "ist äquivalent zu". Eine Relation wird durch die Unterangaben Prädikat und Objekt vollständig beschrieben.|
|*Versionshinweis* (0…1)|Ein Hinweis, der bei der Versionierung eines Elements erstellt wird. Es wird beispielsweise beschrieben, was sich an einem Element geändert hat.|
|**istAbstrakt** (1)|Dieses Attribut ermöglicht die Unterscheidung zwischen konkreten und abstrakten Dokumentsteckbriefen. Es kann den Wert true oder false annehmen, wobei true für abstrakte Dokumentsteckbriefe gewählt wird und false für konkrete Dokumentsteckbriefe.|
|**Dokumentart** (1)|Die Dokumentart gibt an, um was für eine Art von Dokument es sich handelt. Dafür ist ein Wert aus der Codeliste XXX auszuwählen.|

### Angaben für Formular-Nutzer

|Metadatum|Definition|
|-----|-----|
|**Bezeichnung**  (1)|Bezeichnung des Dokumentsteckbriefs, welche für den Bürger/das Unternehmen auf Input-Formularen sichtbar ist.|
|*Hilfetext* (0…1)|Erläuternder Hilfetext für den Bürger/das Unternehmen eines Input-Dokuments.|

### Zustandsangaben

|Metadatum|Definition|
|-----|-----|
|**Fachlicher Ersteller** (0…1)|Ersteller oder erstellende Institution des Elements. Der fachliche Ersteller entspricht der Freigabeinstanz für eine Silber- oder Goldfreigabe.|
|<u>**Erstelldatum**</u>|Datum der Erstellung des Datenfelds :bomb:|
|<u>**Letztes Änderungsdatum**</u> (1)|Datum der letzten Änderung des Datenfelds.|
|<u>**Letzter Bearbeiter**</u>|Der letzte Bearbeiter des Datenfelds. :bomb:|
|<u>**Status**</u> (1)|Der Status gibt Aufschluss darüber, ob und wie das Element im Rahmen eines Editors zu verwenden ist. Mögliche Ausprägungen dieses Metadatums sind durch die Codeliste urn:xoev-de:xprozess:codeliste:status gegeben. Näheres dazu finden Sie im Kapitel Status, Freigabe und Veröffentlichung. Der Defaultwert für den Status ist „in Bearbeitung“, alle anderen Status müssen durch den Nutzer aktiv gesetzt werden.|
|*Gültig ab* (0…1)|Sofern es eine Beschränkung der Gültigkeit gibt, ist der Gültigkeitszeitraum zu erfassen. Mit diesem Metadatum wird der Beginn der Gültigkeit erfasst.|
|*Gültig bis* (0…1)|Sofern es eine Beschränkung der Gültigkeit gibt, ist der Gültigkeitszeitraum zu erfassen. Mit diesem Metadatum wird das Ende der Gültigkeit erfasst.|
|<u>**Freigabedatum**</u> (0…1)|Datum der Freigabe durch die Freigabeinstanz dieses Datenfelds.|
|<u>**Veröffentlichungsdatum**</u> (0…1)|Datum der Veröffentlichung durch die Veröffentlichungsinstanz dieses Datenfelds.|

## Repräsentation im Standard XDatenfelder 3
### konkrete Dokumentsteckbrief

~~~
<xdf3:dokumentsteckbrief>
    <xdf3:identifikation>
        <xdf3:id>D82000000064</xdf3:id>
        <xdf3:version>1.0.0</xdf3:version>
    </xdf3:identifikation>
    <xdf3:name>"normaler" Dokumentsteckbrief</xdf3:name>
    <xdf3:definition>Dies ist eine Beispiel-Definition.</xdf3:definition>
    <xdf3:bezug link="">§ 123</xdf3:bezug>
    <xdf3:freigabestatus listURI="urn:xoev-de:xprozess:codeliste:status" listVersionID="2020-03-19">
        <code>3</code>
    </xdf3:freigabestatus>
    <xdf3:statusGesetztAm>2023-05-15</xdf3:statusGesetztAm>
    <xdf3:statusGesetztDurch>Jessica Gettkandt</xdf3:statusGesetztDurch>
    <xdf3:versionshinweis>Version erstellt ohne Angabe eines Versionshinweises.</xdf3:versionshinweis>
    <xdf3:veroeffentlichungsdatum>2023-05-15</xdf3:veroeffentlichungsdatum>
    <xdf3:letzteAenderung>2023-05-15T16:27:59+02:00</xdf3:letzteAenderung>
    <xdf3:istAbstrakt>false</xdf3:istAbstrakt>
    <xdf3:dokumentart listURI="urn:xoev-de:fim-datenfelder:codeliste:dokumentart" listVersionID="2022-01-01">
        <code>001</code>
    </xdf3:dokumentart>
    <xdf3:bezeichnung>normaler Dokumentsteckbrief</xdf3:bezeichnung>
</xdf3:dokumentsteckbrief>
~~~

### abstrakter Dokumentsteckbrief

~~~
<xdf3:dokumentsteckbrief>
    <xdf3:identifikation>
        <xdf3:id>D82000000065</xdf3:id>
        <xdf3:version>2.1.0</xdf3:version>
    </xdf3:identifikation>
    <xdf3:name>abstrakter Dokumentsteckbrief</xdf3:name>
    ...
    <xdf3:relation>
        <xdf3:praedikat listURI="urn:xoev-de:fim-datenfelder:codeliste:relation" listVersionID="1.1">
            <code>VKN</code>
        </xdf3:praedikat>
        <xdf3:objekt>
            <xdf3:id>D82000000066</xdf3:id>
        </xdf3:objekt>
    </xdf3:relation>
    <xdf3:relation>
        <xdf3:praedikat listURI="urn:xoev-de:fim-datenfelder:codeliste:relation" listVersionID="1.1">
            <code>VKN</code>
        </xdf3:praedikat>
        <xdf3:objekt>
            <xdf3:id>D82000000067</xdf3:id>
        </xdf3:objekt>
    </xdf3:relation>
    <xdf3:istAbstrakt>true</xdf3:istAbstrakt>
    <xdf3:dokumentart listURI="urn:xoev-de:fim-datenfelder:codeliste:dokumentart" listVersionID="2022-01-01">
        <code>999</code>
    </xdf3:dokumentart>
    <xdf3:bezeichnung>abstrakter Dokumentsteckbrief</xdf3:bezeichnung>
</xdf3:dokumentsteckbrief>
~~~