# Grundsätzliches über Datenfeldgruppen

## Was ist eine Datenfeldgruppe?

Eine Datenfeldgruppe (kurz Gruppe) ist ein Element des Datenfeldbaukastens, welches Datenfelder und wiederum Datenfeldgruppen bündelt. Die durch die Datenfeldgruppe gebündelten bzw. enthaltenen Elemente sind die **Unterelemente** dieser Datenfeldgruppe; diese sollten thematisch zusammenpassen.

Die Unterelemente einer Datenfeldgruppe haben eine definierte Reihenfolge und eine [**Multiplizität**](EA_Multiplizitäten). Durch die Multiplizität wird angegeben, wie oft ein Element in einer Gruppe verwendet werden muss.

Dadurch, dass Datenfeldgruppen selbst auch Datenfeldgruppen enthalten, können diese Strukturen sehr verschachtelt sein.

Die Verhältnisse zwischen den Unterelementen einer Gruppe können zusätzlich mit Regeln beschrieben werden.

## Wann werden Datenfeldgruppen verwendet?

Datenfeldgruppen werden verwendet, um **inhaltlich** eng **zusammenhängende Datenfelder** oder **Datenfeldgruppen** zu **bündeln**. <br/> D. h. wenn in einem Datenschema mehrere Elemente modelliert wurden, die sich unter einer gemeinsamen Klammer zusammenfassen lassen, sind Datenfeldgruppen zu bilden.

Das Modellieren von Datenfeldgruppen erfordert ein bisschen Geschick, ist aber für eine saubere Modellierung unerlässlich.

## Was ist das Metadatum Strukturelementart?

Datenfeldgruppen und Datenfelder werden in Datenschemata und Datenfeldgruppen eingebunden, sie sind damit Teil einer **Struktur**. In diesem Kontext spricht man also davon, dass eine Datenfeldgruppe, genauso wie ein Datenfeld, ein **Strukturelement** ist.

Strukturelemente haben ein Metadatum, durch welches definiert ist, auf welche Weise sie zu verwenden sind, das ist die **Strukturelementart**. Es gibt folgende Strukturelemente Arten, die durch den Standard XDatenfelder 3 definiert sind: rechtsnormgebunden, harmonisiert und abstrakt.

|Strukturelementart|Element|
|------------------|-------|
|rechtsnormgebunden|Datenfelder, Datenfeldgruppen|
|harmonisiert|Datenfelder, Datenfeldgruppen|
|abstrakt|Datenfeldgruppen|

## Was sind rechtsnormgebundene Datenfelder und -gruppen?

Rechtsnormgebunden ist der Defaultwert für die Strukturelementart, der gewählt wird wenn eine Gruppe oder ein Feld neu angelegt werden. Rechtsnormgebunden heißt, dieses Element ist im Bezug und auf eine, in den Metadaten hinterlegte, Handlungsgrundlage erstellt und modelliert worden.

:::info

:warning: **Auch rechtsnormgebundene Datenfelder- und gruppen sollten nachgenutzt werden.** Es ist nicht zielführend zwei oder mehrere identische Elemente zu erstellen, nur weil die Handlungsgrundlage unterschiedlich ist. Im Kapitel über [BOB](./EA_BOB.md) erfahren Sie, wie Sie diese Elemente nachnutzen.

:::

## Was sind harmonisierte Datenfelder und -gruppen?

Die Strukturelementart harmonisiert signalisiert, dass es sich hier um eine Gruppe oder ein Feld handelt, welches im Sinne der Nachnutzung erstellt wurde. Sie sollten in Ihrem Datenschema möglichst viele harmonisierte Elemente nutzen, wenn Sie können. 

Harmonisierte Elemente sollten auf einer Handlungsgrundlage basieren und damit einen Bezug zur Handlungsgrundlage haben, aber das ist nicht zwingend notwendig.

:::note

Auch ein rechtsnormgebundenes Element kann im Nachhinein harmonisiert werden.

:::

## Was sind abstrakte Datenfeldgruppen?

Für den Fall, dass die Strukturelementart den Wert **abstrakt** annimmt, spricht man von abstrakten Gruppen. Diese dienen der Nachnutzbarkeit, indem sie umfassend und übergreifend sämtliche Datenfelder und Datenfeldgruppen zu einer Sachlage enthalten. Abstrakte Datenfeldgruppen sind nicht direkt zu verwenden, d. h. man soll sie keinesfalls direkt in ein Datenschema oder eine andere Datenfeldgruppe einbinden.

Abstrakte Datenfeldgruppen sind dazu da, dass von ihnen eine **spezialisierte Datenfeldgruppe abgeleitet** wird. Diese hat danach auch eine andere Strukturelementart (i. d. R. **rechtsnormgebunden**) und kann dann direkt verwendet werden.

## Was ist das neue Metadatum Gruppenart? :new:

Im Standard XDatenfelder 3 wurde das neue Metadatum Gruppenart eingeführt. Damit kann man angeben, ob es sich bei einer Datenfeldgruppe um eine Gruppe im klassischen Sinne handelt oder ob es sich um eine **Auswahlgruppe** handelt. In der Regel wird es sich jedoch um eine klassische Gruppe, wie man sie aus XDatenfelder 2 kennt, handeln. Auswahlgruppen impliziert eine gewisse Logik, die besagt, dass ein Unterelement dieser Datenfeldgruppe zwingend durch den Antragstellenden befüllt werden muss, wobei alle anderen nicht befüllt werden dürfen.
Im Abschnitt [Tipps für die praktische Modellierung](./DFG_Tipps.md#wie-verwende-ich-auswahlgruppen) wird das praktische Vorgehen erklärt.

## Zusammenfassung: Was hat sich beim Wechsel des Standards explizit für Datenfeldgruppen geändert?

:::info

- Auswahlgruppen wurden ergänzt.
- Eine Datenfeldgruppe benötigt mindestens ein Unterelement, um konform zum Standard XDatenfelder 3 zu sein.

:::
