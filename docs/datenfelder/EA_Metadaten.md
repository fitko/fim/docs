# Qualitätskriterien für Standard-Metadaten

Die Metadaten, die in diesem Abschnitt beschrieben werden, betreffen jeweils die meisten, wenn nicht sogar alle Elemente im FIM-Baustein Datenfelder. In den Tabellen dieses Abschnitts gibt es eine zusätzliche Zeile **betroffene Elemente**, die genau spezifiziert, für welche Elemente ein spezifisches Metadatum relevant ist. Wenn in diesem Eintrag der Wert **alle** steht, sind immer Dokumentsteckbriefe, Datenschemata, Datenfeldgruppen, Datenfelder und Regeln gemeint.

## Identifizierungsangaben :sparkles:

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Eine eindeutige ID, die den Anforderungen an eine ID im FIM-Baustein Datenfelder genügt. Jede ID beginnt mit dem Buchstaben, der ihrem Elementtyp entspricht.|
| Zielgruppe      | Modellierende, Dienstleister nachnutzender Systeme |
| Pflichtfeld | Ja |
| Elementspezifisches Qualitätskriterium |Der Aufbau einer ID ist der unten folgenden [Grafik](./EA_Metadaten.md#aufbau-einer-id) zu entnehmen. <br/> Sie besteht aus einem Buchstaben, der den **Typ des Elements** kennzeichnet; gefolgt von zwei Stellen für den **Hauptnummernkreis** und drei Stellen für den **Nebennummernkreis**; die letzten sechs Stellen der ID sind die **nummerische ID**; danach wird bei Versionen noch die **Versionskennung** angehängt.|
| Empfehlung für die maximale Länge |Eine XDatenfelder 3 konforme ID hat zwölf Stellen.|
| Umfang der methodischen Prüfung | nicht |
| Verweis auf den QS-Bericht      | E1038, E1039 |
| betroffene Elemente | alle |
| Beispiel      | F00 000 000 001 |
| mögliche Abweichungen im Referenzkontext| nein |

### Aufbau einer ID

Die ID muss eindeutig sein; sie ist fortlaufend und wird in der Regel automatisch vergeben. Sie setzt sich wie folgt zusammen:

![Identifier](./images/identifier.png)

Der erste Buchstabe gibt den Typ des Elements an. Die möglichen Werte sind:

|Code|Elementart|
|:----------:|:---------------------------|
|D|Dokumentsteckbrief|
|S|Datenschema|
|F|Datenfeld|
|G|Datenfeldgruppe|
|R|Regel|

Der Hauptnummernkreis ist abhängig vom Repository, der Bereich von 00 bis 16 ist den Produktivsystemen der einzelnen Landesredaktionen vorbehalten; diese haben jeweils den Unternummernkreis 000.

Am Hauptnummernkreis kann man schon erkennen, aus welchem Bundesland ein Element stammt, 00 steht für die Bundesredaktion und 01 bis 16 bezieht sich auf die aktuellen Länderschlüssel (siehe Tabelle).

Der Baukasten optimierter Bausteine (kurz **BOB**) hat den Nummernkreis 60 und der zentrale Katalog **KATE** hat den Nummernkreis 99. Außerdem sind die Nummernkreise zwischen 80 und 89 für Test- und Schulungssystemen vorgesehen.

|#|Land|
|:----------:|:---------------------------|
|00|Bundesredaktion|
|01|[Schleswig-Holstein](https://de.wikipedia.org/wiki/Schleswig-Holstein)|
|02|[Freie und Hansestadt Hamburg](https://de.wikipedia.org/wiki/Hamburg)|
|03|[Niedersachsen](https://de.wikipedia.org/wiki/Niedersachsen)|
|04|[Freie Hansestadt Bremen](https://de.wikipedia.org/wiki/Freie_Hansestadt_Bremen)|
|05|[Nordrhein-Westfalen](https://de.wikipedia.org/wiki/Nordrhein-Westfalen)|
|06|[Hessen](https://de.wikipedia.org/wiki/Hessen)|
|07|[Rheinland-Pfalz](https://de.wikipedia.org/wiki/Rheinland-Pfalz)|
|08|[Baden-Württemberg](https://de.wikipedia.org/wiki/Baden-Württemberg)|
|09|[Freistaat Bayern](https://de.wikipedia.org/wiki/Bayern)|
|10|[Saarland](https://de.wikipedia.org/wiki/Saarland)|
|11|[Berlin](https://de.wikipedia.org/wiki/Berlin)|
|12|[Brandenburg](https://de.wikipedia.org/wiki/Brandenburg)|
|13|[Mecklenburg-Vorpommern](https://de.wikipedia.org/wiki/Meckelnburg-Vorpommern)|
|14|[Freistaat Sachsen](https://de.wikipedia.org/wiki/Sachsen)|
|15|[Sachsen-Anhalt](https://de.wikipedia.org/wiki/Sachsen-Anhalt)|
|16|[Thüringen](https://de.wikipedia.org/wiki/Thüringen)|

Der Teil, der als numerische ID bezeichnet wird, wird auf sechs Stellen von führenden Nullen aufgefüllt.

Falls das Element versioniert wurde, erfolgt eine Versionsangabe im Anhang an die eigentliche ID.

## Versionskennung :sparkles:

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Versionen von Elementen erhalten zusätzlich zur ID eine Versionskennung. Zu einer ID können mehrere verschieden Versionen existieren, die jeweils eine andere Versionskennung haben.|
| Zielgruppe      | Modellierende, Dienstleister nachnutzender Systeme |
| Pflichtfeld | Ja, für Versionen von Elementen. |
| Elementspezifisches Qualitätskriterium |Der Aufbau einer Versionskennung ist der [Grafik](./EA_Metadaten.md#aufbau-einer-id) oben zu entnehmen. <br/> Eine Versionskennung besteht aus dem vorgestellten **V**; einer Zahl, die der **Major Version** entspricht, gefolgt von einem Punkt; einer Zahl, die der **Minor Version** entspricht, gefolgt von einem Punkt; einer Zahl für die **Micro Version**. <br/> Nach Möglichkeit sollte bei der Modellierung immer die aktuelle Version eines Elements verwendet werden.|
| Empfehlung für die maximale Länge |Die maximale Länge einer Versionskennung ergibt sich aus der XDatenfelder 3 Spezifikation. Sowohl Major, Minor, als auch Micro-Version können jeweils zwei Stellen haben. Das heißt, eine Versionskennung besteht maximal aus acht Zeichen.|
| Umfang der methodischen Prüfung|nicht |
| Verweis auf den QS-Bericht      | x |
| betroffene Elemente | alle |
| Beispiel      | 1.1.0 |
|mögliche Abweichungen im Referenzkontext| nein |

## Versionshinweis :sparkles:

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Der Versionshinweis wird manuell vergeben, um dem Modellierenden die Möglichkeit zu geben, die Unterschiede zwischen den verschiedenen Versionen zu verdeutlichen. Er kann bei der Erstellung einer neuen Version auch leer gelassen werden, dies sollte aber nicht die Option erster Wahl sein.|
| Zielgruppe      | Modellierende, Dienstleister nachnutzender Systeme |
| Pflichtfeld | Nein |
| Elementspezifisches Qualitätskriterium |Der Versionshinweis soll die Unterschiede zur vorherigen Version deutlich machen. Dies kann Stichwort-artig erfolgen.|
| Empfehlung für die maximale Länge |255 Zeichen|
| Umfang der methodischen Prüfung | nicht |
| Verweis auf den QS-Bericht      | nicht vorhanden |
| betroffene Elemente | alle |
| Beispiel      | "Initiale Version des Datenschemas"; <br/> "Verbesserung kleiner Schreibfehler"; <br/>"Element G60 000 000 085 wurde durch Element G60 000 000 183 ersetzt." |
|mögliche Abweichungen im Referenzkontext| nein |

## Fachlicher Ersteller

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Ersteller oder erstellende Institution des Elements. Der fachliche Ersteller entspricht der Freigabeinstanz für eine Silber- oder Goldfreigabe.|
| Zielgruppe      | Modellierende |
| Pflichtfeld | Ja |
| Elementspezifisches Qualitätskriterium |:warning: Der Fachliche Ersteller ist keine natürliche Person und sollte damit auch nicht mit dem Namen des Modellierenden befüllt werden.|
| Empfehlung für die maximale Länge |255 Zeichen|
| Umfang der methodischen Prüfung | Wird exemplarisch manuell geprüft. |
| Verweis auf den QS-Bericht      | stichprobenartig |
| betroffene Elemente | alle |
| Beispiel      | :x: Max Müller <br/>:heavy_check_mark: BMI KM 5 |
|mögliche Abweichungen im Referenzkontext| nein |

## :exclamation: Erstellungsdatum :exclamation:

:warning: Im Vergleich zur vorherigen Version gestrichen, da es sich nicht um ein XDatenfelder Attribut handelt.

## Letztes Änderungsdatum

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Datum der letzten Änderung eines bestimmten Elements. Es gibt Aufschluss darüber, wann ein Element das letzte mal geändert wurde.|
| Zielgruppe      | Modellierende |
| Pflichtfeld | Ja, da es sich um ein sehr wichtiges Metadatum handelt, sollte es automatisch gepflegt werden. |
| Elementspezifisches Qualitätskriterium |Das letzte Änderungsdatum ist ein Angabe, die sowohl ein Datum, als auch eine Zeit enthält.|
| Empfehlung für die maximale Länge |Die maximale Länge ist durch das Format des Metadatums eingeschränkt.|
| Umfang der methodischen Prüfung| Wird nicht geprüft.|
| Verweis auf den QS-Bericht      | nicht vorhanden |
| betroffene Elemente | alle |
| Beispiel      | 04.10.2023 15:34 |
|mögliche Abweichungen im Referenzkontext| nein |

## :exclamation: Letzter Bearbeiter :exclamation:

:warning: Im Vergleich zur vorherigen Version gestrichen, da es sich nicht um ein XDatenfelder Attribut handelt.

## Status :sparkles:

Die Status werden im Baustein Datenfelder laut folgender Codeliste (https://www.xrepository.de/details/urn:xoev-de:fim:codeliste:status) vergeben. Die Status 1 und 8 finden momentan noch keine Anwendung.

|Code|Status|
|:----:|:----|
|1|in Planung|
|2|in Bearbeitung|
|3|Entwurf|
|4|methodisch freigegeben|
|5|fachlich freigegeben (silber)|
|6|fachlich freigegeben (gold)|
|7|inaktiv|
|8|vorgesehen zum Löschen|

<br/>

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Der Status gibt Aufschluss darüber, in welchem Reifegrad sich ein Element befindet.|
| Zielgruppe      | Modellierende, Fachlichkeit, Dienstleister nachnutzender Systeme |
| Pflichtfeld | Ja. Default Wert **in Bearbeitung** |
| Elementspezifisches Qualitätskriterium |Neu erstellte Arbeitskopien von Elemente, ohne jegliche Freigabe oder Veröffentlichung, erhalten zunächst den Status **in Bearbeitung**.<br/>Nur für versionierte Elemente kann der Status je nach Reifegrad angepasst werden auf die Status 3 bis 6.<br/><ul><li> **Entwurf** - dieses Element hat einen guten Reifegrad, aber hat noch keine offizielle methodische oder fachliche Freigabe erhalten.</li><li>**methodisch freigegeben** - dieses Element wurde methodisch freigeben</li><li>**fachlich freigegeben (Silber oder Gold)** - dieses Element wurde durch die Freigabeinstanz auf Kommunal-, Landes- oder Bundesebene freigegeben</li><li>Elemente, die nach Möglichkeit nicht mehr verwendet werden sollen, werden durch den Status **Inaktiv** gekennzeichnet. In diesem Falle ist es oft hilfreich auf eine aktuellere Version des Elements zu verweisen.</li></ul>|
| Empfehlung für die maximale Länge |nicht relevant|
| Umfang der methodischen Prüfung| Sollte automatisiert abgefragt werden |
| Verweis auf den QS-Bericht      | E1032 |
| betroffene Elemente | Dokumentsteckbriefe, Datenschemata, Datenfeldgruppen und Datenfelder |
| Beispiel      | Inaktiv |
|mögliche Abweichungen im Referenzkontext|:bomb: ja|

## Gültig ab

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Sofern es eine Beschränkung der Gültigkeit gibt, ist der Gültigkeitszeitraum zu erfassen. Mit diesem Metadatum wird der Beginn der Gültigkeit erfasst.|
| Zielgruppe      | Modellierende, Dienstleister nachnutzender Systeme |
| Pflichtfeld | Nein |
| Elementspezifisches Qualitätskriterium |<ul><li>Die Gültigkeit ist ein Metadatum, welches aus der Angabe eines Datums besteht.</li><li>Das Metadatum muss zeitlich vor dem Metadatum **Gültig bis** liegen.</li><li>Es kann auch nur eine der beiden Angaben *Gültig ab / Gültig bis* getroffen werden.</li></ul>|
| Empfehlung für die maximale Länge |Die maximale Länge ist durch das Format des Metadatums eingeschränkt.|
| Umfang der methodischen Prüfung| Wird automatisiert geprüft, falls das Feld befüllt wird.|
| Verweis auf den QS-Bericht      | x |
| betroffene Elemente | Dokumentsteckbriefe, Datenschemata, Datenfeldgruppen und Datenfelder |
| Beispiel      | 01.01.2023 |
|mögliche Abweichungen im Referenzkontext| nein |

## Gültig bis

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Sofern es eine Beschränkung der Gültigkeit gibt, ist der Gültigkeitszeitraum zu erfassen. Mit diesem Metadatum wird das Ende der Gültigkeit erfasst.|
| Zielgruppe      | Modellierende, Dienstleister nachnutzender Systeme |
| Pflichtfeld | Nein |
| Elementspezifisches Qualitätskriterium |<ul><li>Die Gültigkeit ist ein Metadatum, welches aus der Angabe eines Datums besteht.</li><li>Das Metadatum muss zeitlich nach dem Metadatum **Gültig ab** liegen.</li><li>Es kann auch nur eine der beiden Angaben *Gültig ab / Gültig bis* getroffen werden.</li></ul>|
| Empfehlung für die maximale Länge |Die maximale Länge ist durch das Format des Metadatums eingeschränkt.|
| Umfang der methodischen Prüfung| Wird automatisiert geprüft, falls das Feld befüllt wird. |
| Verweis auf den QS-Bericht      | x |
| betroffene Elemente | Dokumentsteckbriefe, Datenschemata, Datenfeldgruppen und Datenfelder |
| Beispiel      |31.12.2023 |
|mögliche Abweichungen im Referenzkontext| nein |

## Freigabedatum

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Datum der Freigabe durch die Freigabeinstanz. Das Freigabedatum sollte automatisch durch ein Redaktionssystem ermittelt und gesetzt werden. Regeln haben kein Freigabedatum.|
| Zielgruppe      | Modellierende |
| Pflichtfeld | Ja, bei Elementen in den Status 3 bis 6. |
| Elementspezifisches Qualitätskriterium |Das Freigabedatum ist ein Metadatum, welches aus der Angabe eines Datums besteht. <br/> Es zählt das Datum als Freigabedatum, zu dem erstmalig der Status eines Elements in einen Freigabestatus angehoben wurde. Es betrifft also die Status: Entwurf, methodisch freigegeben, fachlich freigegeben (Gold und Silber). Das heißt auch, dass das Freigabedatum eines Elements in diesem Status immer gesetzt sein muss. <br/> Ein Freigabedatum kann nur bei einem versionierten Element gesetzt werden.|
| Empfehlung für die maximale Länge |Die maximale Länge ist durch das Format des Metadatums eingeschränkt.|
| Umfang der methodischen Prüfung| Wird nicht geprüft. |
| Verweis auf den QS-Bericht      | x |
| betroffene Elemente | Dokumentsteckbriefe, Datenschemata, Datenfeldgruppen und Datenfelder |
| Beispiel      | 01.05.2023 |
|mögliche Abweichungen im Referenzkontext| nein |

## Veröffentlichungsdatum

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Datum der Veröffentlichung durch die Veröffentlichungsinstanz. Das Veröffentlichungsdatum sollte automatisch durch ein Redaktionssystem ermittelt und gesetzt werden. Regeln haben kein Veröffentlichungsdatum.|
| Zielgruppe      | Modellierende |
| Pflichtfeld | Nein|
| Elementspezifisches Qualitätskriterium |Das Veröffentlichungsdatum ist ein Metadatum, welches aus der Angabe eines Datums besteht. Ein Veröffentlichungsdatum kann nur gesetzt werden, wenn das Element bereits ein Freigabedatum hat.|
| Empfehlung für die maximale Länge |Die maximale Länge ist durch das Format des Metadatums eingeschränkt.|
| Umfang der methodischen Prüfung| Wird automatisch gesetzt. |
| Verweis auf den QS-Bericht      | x |
| betroffene Elemente | Dokumentsteckbriefe, Datenschemata, Datenfeldgruppen und Datenfelder |
| Beispiel      | 01.07.2023 |
|mögliche Abweichungen im Referenzkontext| nein |

## Stichwörter :new:

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | FIM-internes Stichwort, sichtbar für alle Anwender eines Redaktionssystems des Bausteins Datenfelder (fachliche Ersteller, Methodenexperten, Informationsmanager, FIM-Koordinierungsstelle, FIM-Nutzer). Ein Stichwort wird vergeben, um einem Element ein zusätzliches Klassifizierungsattribut zu übergeben, welches nicht durch die Metadaten abgedeckt werden kann.|
| Zielgruppe      | Modellierende, Dienstleister nachnutzender Systeme |
| Pflichtfeld | Nein|
| Elementspezifisches Qualitätskriterium |<ul><li>Das Stichwort soll prägnant sein.</li><li>Doppelbenennungen sind zu vermeiden.</li><li>Ein Stichwort aus mehreren Wörtern ist zu vermeiden.</li><li>Es können auch mehr als ein Stichwort pro Element vergeben werden.</li><li>Es sind entweder die vordefinierten Stichworte zu vergeben oder es kann ein Freitext Stichwort vergeben werden.</li></ul>|
| Empfehlung für die maximale Länge |500 Zeichen für alle einem Element zugeordneten Stichwörter|
| Umfang der methodischen Prüfung| nicht |
| Verweis auf den QS-Bericht      | x |
| betroffene Elemente |alle |
| Beispiel      |<ul><li>Das Feld F60 000 000227 Familienname aus BOB erhält das Stichwort ‘personenbezogene Daten’.</li><li>Das Feld F60 000 000246 Postleitzahl aus BOB erhält das zusätzliche Stichwort ‘BOB’.</li></ul> |
|mögliche Abweichungen im Referenzkontext| nein |
