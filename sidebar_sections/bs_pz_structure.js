const pz_structure = [
	{
		'type': 'category',
		link: {
			type: 'generated-index',
			description: 'Hier finden Sie einen Überblick der behandelten Themen. Für einen schnellen Einstieg empfehlen wir die Einleitung zu lesen.'
		},
		'label': 'Baustein Prozesse',
		'items': [
			'prozesse/einleitung'
		],
	},
]

    // export the structure to enable use in other files
module.exports = pz_structure;