# Grundsätzliches über Regeln

## Was ist eine Regel?

Eine Regel beschreibt logische Zusammenhänge zwischen den Elementen eines Datenschemas.

## Regeln und Sprache

Da es momentan im FIM-Baustein Datenfelder keine gültige Regelsprache gibt, genügt es vollkommen die Regeln nur textuell zu beschreiben. Diese sprachliche Ausformulierung der Regel sollte möglichst den gesamten Funktionsumfang abdecken, der durch die Regel gesteuert werden soll.  