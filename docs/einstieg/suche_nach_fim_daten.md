---
sidebar_label: Suche nach FIM-Daten
---

# Suche nach FIM-Daten

Das FIM-Portal ermöglicht Suchen für sehr verschiedene Anwendungsszenarien:

Baustein Leistungen:
- [Leistungssuche mit dem Feld "Leistungsbeschreibung I"](../ressources/leistungssuche_fuehrerschein_mit_leistungsbeschreibung_i.mp4) 🎞️
- [Leistungssuche mit (einem Fragment des) Leistungsschlüssels](../ressources/leistungssuche_fuehrerschein_mit_leistungsschluessel_fragment.mp4) 🎞️
- [Leistungssuche in einem einzelnen Bundesland für ein einzelnes Ressort](../ressources/leistungssuche_in_hessen_mit_hmwk.mp4) 🎞️