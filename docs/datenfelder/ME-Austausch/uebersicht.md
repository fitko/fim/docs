# ME-Austausch Übersichtsseite

:::info

Die Seiten befinden sich noch im Aufbau, d. h. die verwendete Struktur und die Inhalte sind noch nicht final.

:::

Herzlich willkommen zur Informationsseite des ME-Austauschs! Hier erfahren Sie, welche Themen in den letzten Runden diskutiert wurden und welche in den nächsten Runden diskutiert werden sollen. Auch Begleitmaterial zu den Terminen wird Ihnen hier zur Verfügung gestellt.

## Was ist der ME- Austausch?

Der ME-Austausch ist ein Austauschformat, indem Interessierte sich über aktuelle Themen, die im Kontext des FIM-Bausteins Datenfelder relevant sind, austauschen können.
Zudem diskutieren wir Fragen zur Methodik, aber auch konkrete Modellierungsfragen sind uns stets willkommen.

Die Termine finden in der Regel alle vier Wochen Dienstags zwischen 9:00 und 10:30 statt. Beachten Sie dazu unsere Agenda Seite, um stets im Blick zu haben, wann der nächste Termin stattfindet und welche Themen eingeplant sind.

## Wer kann teilnehmen?

Alle Interessierte sind eingeladen an unserem Austauschformat teilzunehmen. Wir führen einen Verteiler, in den Sie sich gerne eintragen lassen können, um die Termineinladungen zu erhalten. Schreiben Sie hierfür an: [ticket@fimportal.de](mailto:ticket@fimportal.de).

## Wie kann ich mitwirken?

Kommen Sie gerne vorab auf uns zu und melden uns Ihre Themenwünsche und Fragen.
Wenn Sie einen Vortrag über ein spannendes Thema halten oder Ihre Fachexpertise mit uns teilen möchten, sind Sie dazu auch jederzeit herzlich eingeladen. Sagen Sie uns am Besten vorab Bescheid, dann können wir einen Zeitslot für Ihr Thema blocken.
Haben Sie bitte Verständnis dafür, dass wir, abhängig von der Agenda, größere Themenblöcke nicht spontan aufnehmen können. 
Für spontane Fragestellung ist aber in der Regel immer Zeit.