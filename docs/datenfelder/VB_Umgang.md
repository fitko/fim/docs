# Umgang mit diesem Dokument

## Allgemeines

In dieser Sektion soll kurz vorgestellt werden, in welchen Teilen dieser Dokumentation Sie welche Informationen finden.
Die Dokumentation untergliedert sich in Informationen zu den Bereichen des FIM-Bausteins Datenfelder: dem [Datenfeldkatalog](DSB_Allgemeines), der [Datenschemabibliothek](DS_Allgemeines), sowie dem [Datenfeldbaukasten](DFG_Allgemeines). Informationen, die sich zu keiner dieser drei Kategorien zuordnen lassen, da sie z. B. sowohl den Datenfeldkatalog, als auch den Datenfeldbaukasten betreffen, finden sich unter [Elementunabhängige Angaben](EA_Metadaten).
<br/>
Die Informationen zu den Bereichen sind wiederum in drei Unterkategorien unterteilt:

-   Allgemeine Informationen zu einem Thema finden sich unter der Sektion **Grundsätzliches über...**
-   Informationen und Tipps zur konkreten Modellierung finden Sie unter der Sektion **Tipps für die praktische Modellierung**
-   Unter **Informationen zu den Metadaten** werden alle Metadaten eines Elements gelistet. An dieser Stelle erfahren Sie alles, was Sie zu einem bestimmten Metadatum wissen wollen. Als Darstellungsform wurden hier Tabellen gewählt, folgend wird auch erklärt, wie diese Tabellen zu lesen sind.
-   Sie haben einen Modellierungstipp, möchten diesen Teilen und finden ihn in diesem Dokument nicht wieder? 
-   ODER: Sie haben eine Frage und möchten, dass diese in diesem Dokument adressiert wird? In beiden Fällen schreiben Sie uns unter: ticket@fimportal.de 

## Lesehilfe zu den Tabellen "Informationen zu den Metadaten"

Zu jedem Element im Baustein Datenfelder gibt es Metadaten, die durch den Modellierenden zu befüllen sind. Die Tabellen spezifizieren klar, um was für ein Metadatum es sich handelt und was für dieses Metadatum zu beachten ist, d. h. welche Qualitätskriterien für ein bestimmtes Metadatum gelten.

Dieser Abschnitt soll den Aufbau einer solchen Tabelle erklären.

:warning: Im Vergleich zur Vorgängerversion dieses Dokuments sind vier neue Einträge hinzugekommen: **Relevanz**, **Empfehlung für die maximale Länge**, **Verweis auf den QS-Bericht** und **Empfehlung für referenzbasierte Datenschemata**.

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Wie ist dieses Metadatum definiert? Hier wird versucht in Kürze zu erklären, um was es bei diesem Metadatum geht.|
| Zielgruppe :sparkles: | Es findet sich ein knapper Hinweis darauf, für wen dieses Metadatum relevant ist. Mögliche Ausprägungen dafür sind: **Modellierende** / **Fachlichkeit** / **Dienstleister nachnutzender Systeme** / **Antragstellende** |
| Pflichtfeld | Gibt an, ob diese Angabe bei der Modellierung zwingend erforderlich ist oder nicht. Mögliche Ausprägungen dafür sind: AAA, BBB und CCC.|
| Elementspezifisches Qualitätskriterium | Die Elementspezifischen Qualitätskriterien listen die Punkte, die erfüllt sein müssen, damit dieses Element den methodischen Anforderungen, die an ein Stammdatenschema gestellt werden, genügt.|
| Empfehlung für die maximale Länge :sparkles:|Aus methodischer Sicht macht es oft Sinn, getroffene Angaben mit einer maximalen Längenangabe zu begrenzen.|
| Im Umfang der methodischen QS? | Gibt an, in welchem Umfang dieses spezielle Metadatum Teil der methodischen Qualitätssicherung ist. Mögliche Ausprägungen sind: **nicht** / **stichprobenartig** / **vollständig** |
| Verweis auf den QS-Bericht :sparkles:| Der QS-Bericht ist in der Lage viele Modellierungsfehler automatisiert zu erkennen. Dieser Punkt listet zugehörige Fehler oder Warnungen. Mehr zu diesem Thema finden Sie unter [Umgang mit dem QS-Bericht](A_QS).|
| Beispiel      |An dieser Stelle werden Beispiele gelistet, an denen die Theorie illustriert werden sollen. |
|mögliche Abweichungen im Referenzkontext :sparkles:| Klärt, ob es zu diesem Metadatum im Referenzkontext Abweichungen geben kann. Dafür gibt es zwei Ausprägungen: **nein** und **ja**. <br/> **Nein** heißt in diesem Zusammenhang, dass dieses Metadatum im Referenzkontext analog umzusetzen ist. <br/> :bomb: Bei **ja** kann es im Referenzkontext zu Abweichungen kommen. Auf genauere Hilfestellung hierzu wird verwiesen.|

## Verwendung von Symbolen

:warning: Aufgepasst, diese Information ist besonders wichtig.

:sparkles: Hierbei handelt es sich um eine neue Information, die in dieser Form im Altdokument nicht enthalten war.

:heavy_check_mark: Wird bei Beispielen verwendet und kennzeichnet die positive Variante eines Beispiels.

:x: Wird bei Beispielen verwendet und kennzeichnet die negative Variante eines Beispiels.

:new: Dadurch wird ein in XDatenfelder 3 neu eingeführtes Metadatum, bzw. eine Ergänzung der Methodik durch den neuen Standard, gekennzeichnet.

:bomb: Kennzeichnet ein Metadatum, welches im Referenzkontext abweichen kann.
