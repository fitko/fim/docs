# Bundesredaktion

## Wer ist die Bundesredaktion und welche Aufgaben übernimmt sie?
Das **redaktionelle Betriebsmodell** der FIM-Methodik zur Umsetzung der redaktionellen Aufgaben sieht Redaktionen auf allen Verwaltungsebenen vor:

**Bundesredaktion:** Unterstützung der Bundesressorts bei der Umsetzung der FIM-Methodik und Bereitstellung von FIM-Informationen zu Verwaltungsleistungen der Typen 1, 2, 3, 6 und 7. Sie besteht aus der **zentralen Bundesredaktion** (angesiedelt im Bundesministerium des Innern und für Heimat, zuständig für Bedarfskoordinierung und methodische Expertise) und den **Ressortansprechpersonen (RAP)** („Bundesredaktion im [Ressort]“ - Kontaktstelle in den Ressorts für FIM, zuständig für die Koordinierung der Bedarfe in den verschiedenen Häusern). 

**Neben der Bundesredaktion existieren Landesredaktionen und konzeptionell kommunale Redaktionen:** Diese ergänzen die vom Bund bereitgestellten FIM-Stamminformationen (Typ 2, Typ 3) um lokale Spezifika (sog. Lokalinformationen) und erstellen FIM-Informationen auf landes- und kommunaler Ebene (bspw. Typ 4, Typ 5)


## Wie kontaktiere ich die zentrale Bundesredaktion?
Für die nachgelagerte operative Abstimmung und zu spezifischen Fragen die Bundesredaktion betreffend, kann die zentrale Bundesredaktion unter bundesredaktion@bmi.bund.de erreicht werden. Einreichungen zu FIM-Informationen sollen nicht über das Funktionspostfach erfolgen, da in diesen Fällen keine Ticketnummer für Ihren Bedarf generiert werden kann (s.u.).
 
**Allgemeine Fragen zu FIM:** ticket@fimportal.de<br/>
Hier erreichen Sie die FITKO, in der das operative Produktmanagement beheimatet ist sowie die drei Bausteinbetreiber Leistungen, Prozesse & Datenfelder. Fragen zur (Weiterentwicklung der) Methodik, der FIM-Infrastruktur, der Pflege der Kataloge und Baukästen oder der FIM-Rollen und Organisationseinheiten im Redaktionsprozess sind hier an der richtigen Stelle.
 
**Die Einreichung von Bedarfen** erfolgt über das übergreifende Ticketsystem (ticket@fimportal.de). Über das Ticketsystem werden Bedarfe an die zuständige Redaktion vermittelt. Die Bundesredaktion erreichen auf diesem Wege **Typ 1, 2, 3, 6, 7 - Leistungen.**
 
**Unterstützung bei der Arbeit mit FIM-Informationen**<br/>
Die zentrale Bundesredaktion unterstützt die Fachbereiche auf Bundesebene bei der Erstellung und fachlichen Prüfung von FIM-Informationen (Leistungszuschnitte, Leistungsbeschreibungen, Prozesse, Datenfelder). Bei Fragen kann die Bundesredaktion direkt kontaktiert werden. Einen ersten Einstieg in die Thematik bietet die [Handreichung der zentralen Bundesredaktion](../ressources/handreichung_der_zentralen_bundesredaktion.pdf).
 
Für die Fachseite finden Sie [hier](http://fimportal.de/download/d90d36746bd8693ecac92c210f92d304) ein unterstützendes Video zur fachlichen Prüfung von mit Leistungszuschnitten.


## Was ist eine Ressortansprechperson (RAP) und welche Aufgaben nimmt sie wahr?
Ressortansprechpersonen (Ressortansprechpartner/in und Ressortansprechperson werden synonym verwendet) sind **Teil der Bundesredaktion.** Sie sind die Kontaktstelle in den Ressorts für die zentrale Bundesredaktion, sind innerhalb Ihres Hauses die zentrale Anlaufstelle für FIM-Fragen und koordinieren alle Anliegen zu FIM innerhalb ihres Ressorts und sollten auch Schnittstellen zu OZG und SDG-Thematiken erfassen. Das Bundesressort ist für die fachliche Freigabe von allen FIM-Informationen (Leistungszuschnitte, Leistungsbeschreibungen, Prozesse, Datenfelder) mit Grundlage im Bundesrecht als rechtsetzende Instanz zuständig.