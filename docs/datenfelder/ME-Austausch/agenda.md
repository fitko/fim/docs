# Agenda der kommenden Termine

# 11.02.2025

freie Agenda

# 11.03.2025

- Vorstellung und Fragen zum Thema FIT-Connect; hierfür steht eine [Umfrage](https://forms.office.com/e/wDxdZG7A9T) bereit.

# 08.04.2025

- Wiederverwendung von Baukastenelementen
- Rollen (Antragstellende, Anzeigende, natürliche Personen, gesetzliche Vertreter, ...)

# virtuelle Zettelbox

Zur Planung der nächsten Termine gibt es eine Umfrage, über die Sie uns Fragen, Themenwünsche und weitere Anmerkungen zukommen lassen können: [Virtuelle Zettelbox](https://forms.office.com/e/hCW4TJT0qX)