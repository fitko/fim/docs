---
sidebar_label: Was ist FIM?
---

# Was ist FIM?

Das Föderale Informationsmanagement ist ein Produkt des [IT-Planungsrats](https://www.it-planungsrat.de/), um den Vollzug von Verwaltungsleistungen zu harmonisieren.
Dazu werden Informationen auf der Grundlage von Rechtsvorschriften standardisiert erfasst.
Diese Informationen werden als FIM-Informationen oder (FIM-) Stamminformationen bezeichnet und unterteilen sich in:

- **FIM-Leistungsbeschreibungen** erklären in einer bürgerfreundlichen Sprache den Gegenstand einer Leistung
- **FIM-Datenschemata** geben an, welche Daten die zuständige Behörde erfassen und verarbeiten muss
- **FIM-Prozesse** zeigen abstrakt den behördeninternen Bearbeitungsprozess.

## TODO

Mehr Einsteigsinformationen
