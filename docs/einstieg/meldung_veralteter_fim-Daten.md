---
sidebar_label: Meldung veralteter FIM-Daten
---

# Meldung veralteter FIM-Daten

Um veraltete FIM-Daten zu melden, schreiben Sie bitte eine E-Mail an ticket@fimportal.de

:::tip

Sollten Sie finden, dass für das FIM-Portal eine andere Möglichkeit zur Meldung veralteter Daten besser geeignet ist, schreiben Sie dies bitte mit Ihren Anforderungen an ticket@fimportal.de mit dem Betreff "FIM-Portal - Anforderung an Feedback".

:::
