# Links

Wichtige Links zum Weiterlesen:

- [FIM-Portal](https://fimportal.de/)
- [XRepository](https://www.xrepository.de/)
- [Informationen zum Standard XDatenfelder](https://www.xrepository.de/details/urn:xoev-de:fim:standard:xdatenfelder)

Bezugsort wichtiger Tools:

- [QS-Bericht und Visualisierung](https://github.com/vs5000/FIM-XSLT)
- neuer Link
