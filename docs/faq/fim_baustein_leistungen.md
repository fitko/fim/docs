# FIM Baustein Leistungen

## Was muss ich tun, um eine FIM-Leistung zu beantragen, ändern oder zu löschen?
Die Beantragung einer FIM-Leistung dient der strukturierten Erfassung einer Verwaltungsleistung im Leistungskatalog. Ein solcher Katalogeintrag kennzeichnet sich durch einen 14-stelligen Leistungsschlüssel (ugs. auch 99er-Schlüssel oder Leika-Schlüssel, fälschlich teilweise aus Leika-ID) aus und wird nach konkreten Regeln bestimmt. Ihn kann man als eindeutige Kennung verstehen, der eine Leistung klar von einer anderen Leistung im Katalog abgrenzt.

Alle Bedarfe, die den Katalogeintrag (auch „Leistungssteckbrief“) betreffen, müssen über ein eigenes Formular abgestimmt werden (**Excel-Dokument**).
Vier Aktionen sind in diesem Formular möglich:
- Neuanlage
- Löschen = FIM-Leistung falsch zugeschnitten oder fachlich falsch
- Außer Kraft setzen = Rechtsgrundlage nicht mehr gültig
- Änderung

Das **Musterformular Leistungszuschnitt** muss vollständig ausgefüllt an ticket@fimportal.de  gesendet werden. Alle Bedarfe müssen fachlich begründet sein. Die Anträge werden durch Redaktionen und Bausteine methodisch und im Anschluss durch die Fachseite fachlich überprüft. Bei Klärungsbedarf gehen die Redaktionen auf die Bedarfsmeldenden zu.

**Achtung:** Diese Bedarfe sind zu unterscheiden von Bedarfen an Leistungsbeschreibungen, welche den sog. Bibliothekseintrag (vereinfacht: tiefergehende beschreibende Texte und Informationen zu Verwaltungsleistungen) können nicht über das „Musterformular LZ“ beantragt werden – siehe: „Wie kann eine Leistungsbeschreibung geändert werden?“


## Wie kann ich eine FIM-Leistungsbeschreibung ändern?
Hierzu müssen sie die **alte Leistungsbeschreibung in das Musterformular Leistungsbeschreibungen übertragen** und Aktualisierungen im Änderungsmodus (ganz wichtig!) kenntlich machen. Die alte Leistungsbeschreibung kann im FIM-Portal abgerufen werden: Die Suche erfolgt entweder über den
- FIM-Leistungsschlüssel (99er-Nummer) oder
- per Textsuche

auf dem [fimportal.de](https://fimportal.de). Für das Herunterladen ist eine Anmeldung auf dem FIM-Portal notwendig. In den Suchergebnissen die gewünschte Leistung anklicken und das Dokument herunterladen (üblicherweise: (de)). Die Dokumente können derzeit nur als .pdf heruntergeladen werden.

**Tipp:** Jede neuere Version von Microsoft Word kann .pdfs problemlos in eine editierbare Version überführen. Hierzu muss lediglich die .pdf in Microsoft Word geladen werden und kann dann als .docx abgespeichert werden.
Das vollständig ausgefüllte Formular wird anschließend an [ticket@fimportal.de](mailto:ticket@fimportal.de) gesendet. Alle Bedarfe müssen fachlich begründet sein.


## Wie gehe ich mit dem Musterformular Leistungsbeschreibungen um?
Das Musterformular Leistungsbeschreibungen nutzen Sie, wenn Sie eine Leistungsbeschreibung abstimmen bzw. erstellen möchten. Bitte füllen Sie es so vollständig wie möglich aus – berücksichtigen Sie hierbei die für den jeweiligen Leistungstyp relevanten Felder.

**Tipp:** So werden beispielsweise bei Typ 2 oder 3 Leistungen keine regionalen Inhalte im Stammtext erfasst, sondern nur solche, die bundesweit einheitlich gültig sind.
Folgende Hinweise erleichtern den Umgang:

- Wenn Sie nicht wissen, ob Sie **ein Feld ausfüllen müssen** – per Kommentarfunktion kennzeichnen/nachfragen.
- Wenn bei einem Auswahlfeld eine gewünschte Auswahlmöglichkeit nicht vorhanden ist: Als Freitext unter die Auswahlmöglichkeiten einpflegen und so melden, was gewünscht ist.

Wenn **keine Information möglich** ist: Feld leer lassen und entsprechend kennzeichnen, aber das Feld nicht löschen.


## Wie kommen Stammtexte zu Typ 2 & 3 Leistungen in den LeiKa?
Derzeit werden FIM-Stammtexte auch für Typ 2 & 3 Leistungen initial durch die Länder erstellt. Die Landesredaktionen sind daher befugt im Redaktionssystem des Bausteins Leistungen Stammtexte zu diesen Leistungen im Silberstatus zu veröffentlichen.

Die Pflegeverantwortung für die Leistungen geht erst dann auf die zentrale Bundesredaktion über, wenn diese über die Veröffentlichung informiert wurde. Dies wird gleichzeitig zum Anlass genommen die Stammtexte in den Goldstatus zu überführen.


## Was bedeutet „Typisierung“?
Mit der Typisierung wird eindeutig widergespiegelt, durch welche Ebene die Leistung rechtlich geregelt (**Regelungszuständigkeiten**) und durch welche Ebenen sie vollzogen wird (**Vollzugszuständigkeiten**). Diese Information ist auf technischer Ebene sowie für den Redaktionsprozess relevant. Sie zeigt an welche Redaktionen und Systeme für die Bereitstellung der jeweiligen Informationen, insbesondere Lokalinformationen, zuständig sind.

Die aktuelle Codeliste zur Typisierung finden Sie auf www.xRepository.de – weiterführende Informationen finden Sie im Glossar des FIM-Portals. Die Typisierung wird kontinuierlich weiterentwickelt um auch komplexere Regelungs- und Vollzugskonstellationen korrekt abbilden zu können. Besondere Fallkonstellationen werden häufig in Verbindung mit der Leistungskategorie abgebildet.


## Was bedeutet „Leistungskategorie“?
Die Leistungskategorie ist ein „Modul“ einer Leistung. Sie kann eine Aussage über Eigenschaften einer Leistung treffen. Diese Information ist, im Zusammenspiel mit der Typisierung an vielen Stellen für eine technisch korrekte Verarbeitung und Ausspielung notwendig.

Beispiel: Die Leistungskategorie „Leistung im gemeinsamen Vollzug“ trifft eine Aussage darüber, dass eine Leistung durch eine Stelle für andere, eigentlich zuständige Stellen, übernommen wird. Dies kann z.B. der Fall sein, wenn eine Behörde (bspw. aus Effizienzgründen) in einem Land eine Leistung in einem bestimmten Kontext unabhängig vom Ort der Beantragung für alle oder einige Länder übernimmt. Diese Information ist von der Typisierung zu unterscheiden, welche lediglich die Ebene von Rechtsetzung und Vollzug angibt.