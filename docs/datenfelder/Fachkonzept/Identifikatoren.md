# Identifikatoren

## Identifikation (FIM-ID)
Alle Elemente im Baustein Datenfelder bekommen eine Identifikation nach folgendem Schema:

![Identifier](./images/identifier.png)

### Typ des Elements
Der erste Buchstabe gibt den Typ des Elements an. Die möglichen Werte sind:

|Code|Elementart|
|:----------:|:---------------------------|
|D|Dokumentsteckbrief|
|S|Datenschema|
|F|Datenfeld|
|G|Datenfeldgruppe|
|R|Regel|

### Nummernkreis
Die Summe aus Hauptnummernkreis und Unternummernkreis bezeichnet den Nummernkreis; der Nummernkreis ist stets eindeutig, d. h. eine bereits vorhandene Kombination aus Haupt- und Unternummernkreis kann kein zweites mal vergeben werden.

#### Hauptnummernkreis
Damit die Landesredaktionen unabhängig von der Bundesredaktion neue IDs für ihre Elemente
vergeben können folgen nach dem Elementtyp die zwei Ziffern des Hauptnummernkreises.

Die Hauptnummernkreise sind aktuell wie folgt festgelegt:

|Hauptnummernkreis|Beschreibung|
|:----------|:---------------------------|
|<u>00-19</u>|<u>Produktivsysteme</u>|
|00|Bundesredaktion|
|*01-16*|*Landesredaktionen gemäß Regionalschlüssel*|
|17|Temporäres FIM-System im Rahmen der OZG-Umsetzung - “Phönix”|
|<u>20-59</u>|<u>Reserviert</u>|
|<u>60-69</u>|<u>Systeme mit spezieller Bedeutung</u>|
|60|BOB - Baukasten optimierter Bausteine|
|<u>70-79</u>|<u>Reserviert</u>|
|<u>80-89</u>|<u>Testsysteme</u>|
|80|Schulungssystem|
|81|OZG-Referenzsystem “Atlantis”|
|82|Testrepository|
|<u>90-99</u>|<u>teilweise reserviert</u>|
|99|KATE - Zentraler Katalog|

#### Unternummernkreis
Seit dem Standard XDatenfelder 3 ist es möglich zu jedem Hauptnummernkreis 1000 Unternummernkreise zu vergeben.

Eine Landesredaktion erhält automatisch den Unternummernkreis 000.

Die Unternummernkreise 001 bis 999 können innerhalb eines Bundeslandes zusätzlich an beispielsweise Kommunen vergeben werden.

### Nummerische ID
Pro Elementtyp und Nummernkreis werden fortlaufende Nummern vergeben. Diese werden auf sechs
Stellen mit führenden Nullen aufgefüllt.

In einem Nummernkreis darf eine ID nicht mehrfach vergeben werden; sie muss stets eindeutig sein.

### Versionskennung
Versionierte Elemente erhalten einen Zusatz, der aus dem Buchstaben *V*, der Hauptversion, einem
Punkt, der Nebenversion, einem Punkt und der Microversion besteht. Die änderbaren Arbeitskopien tragen keinen Versions-Zusatz.

Beispiel: *S 00 000 000 001 V1.1.1*

## Repräsentation im Standard XDatenfelder 3

~~~
<xdf:identifikation>  
  <xdf:id>S17000000671</xdf:id>
  <xdf:version>1.0.1</xdf:version>
</xdf:identifikation>
~~~

Identifikation für ein versioniertes Datenschema; für die Arbeitskopie würde die Versionskennung entfallen.

## Umgang mit Identifikatoren

### Nummernkreise und Bearbeitungsrechte
Jedem Repository ist ein eindeutiger Nummernkreis zugeordnet, der nachträglich nicht mehr geändert werden kann. In diesem Repository liegen die bekannten Elemente des Bausteins in Form von Arbeitskopien und Versionen vor.

Ein Identifikator in einem Repository hat eindeutig zu sein; d. h. der gleiche Identifikator kann nicht mehrfach vergeben werden.

Ein Repository kann Inhalte verschiedener anderer Repositorys in Form von Kopien beheimaten, jedoch dürfen Fremdinhalte dort nicht mehr editiert werden. Das einzige, was innerhalb eines Repositorys bearbeitbar ist, sind Arbeitskopien von Elementen des eigenen Nummernkreises.

### Übertragung eines Elements in ein anderes Repository
#### Übertragung eines Datenschemas
![Umgang mit Identifikatoren 1](./images/ID%20Umgang1.png)

Aus einem Repository A heraus soll ein Datenschema in ein anderes Repository B übertragen werden. Im in der Grafik gezeigten Fall nutzt man dafür ein Datenschema, welches bereits versioniert ist. Die Arbeitskopie zu diesem Datenschema liegt in Repository A auch vor, d. h. Repository A ist das Ursprungs-Repository dieses Datenschemas. 

Im ersten Schritt wird das Wunschelement *S00 000 000 001 V1.0.0* in das Ziel-Repository kopiert, dort erhält es die gleiche ID wie im Ursprungs-Repository, auch alle anderen Metadaten des Elements sind gleich; das kopierte Datenschema kann im neuen Repository nicht weiterbearbeitet bzw. editiert werden.

Will man mit dem Datenschema weiterarbeiten, wird zunächst eine neue Arbeitskopie erstellt. Diese hat eine andere Identifikation als die der zugrundeliegende Version des Datenschemas - alle andere Metadaten des Datenschemas und die gesamte Struktur bleiben erhalten.

#### Baumansicht des übertragenen Datenschemas
![Umgang mit Identifikatoren 2](./images/ID%20Umgang2.png) 

In diesem Abschnitt soll betrachtet werden, welche Einfluss die Übertragung eines Datenschemas auf die Struktur der daraus entstandenen Arbeitskopie hat. Wir betrachten die Arbeitskopie *S15 000 000 005* aus obigem Beispiel, diese enthält sowohl Elemente aus dem eigenen Nummernkreis, als auch aus anderen Nummernkreisen. Hier tauchen als Nummernkreisfremde Elemente nur Elemente des Hauptnummernkreises 00 und 60 auf. Das die 60 hier auftaucht, legt nahe, dass bereits im ursprünglichen Datenschema mit der ID *S00 0 000 001* Elemente aus dem 60er Hauptnummernkreis, also aus BOB, verwendet wurden.

Die Darstellung ist stark vereinfacht und die orangenen Kästchen mit den Punkten sollen eine weitere Verzweigung der Struktur andeuten.