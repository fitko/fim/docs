# Dokumentation für den Baustein Prozesse

Die vollständige Dokumentation für den Baustein Prozesse findet sich in den folgenden Dokumenten. Die Dokumentation wird sukzessive hierhin überführt.

[Download Fachkonzept](20200731_Prozesse_Fachkonzept_V1.01.pdf)

[Download Qualitätssicherungs-Kriterien (QS-Kriterien)](20200731_Prozess_QS-Kriterien_V1.00.pdf)

[Download FIM-Prozesskatalog (Excel)](FIM_Prozesskatalog.xlsx) Stand: 06.02.2025 