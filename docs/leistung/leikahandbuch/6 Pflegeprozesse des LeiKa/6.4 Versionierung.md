# Versionierung

Der LeiKa wird fortlaufend aktualisiert. Die jeweils tagesaktuelle Fassung ist im LeiKa-plus Portal (siehe 7.4) abrufbar.