# Tipps für die praktische Modellierung

## Wie kann ich eine Regel nachnutzen?

Da Regeln keine selbstständigen Elemente sind, können Sie nur über die Elemente, an denen sie hängen nachgenutzt werden. D. h. zur Nachnutzung einer Regel kann es sinnvoll sein, die entsprechenden Elemente nachzunutzen. Falls Sie nur eine Formulierung nachnutzen wollen, kopieren Sie diese bitte direkt aus der Regel raus.

## An welcher Stelle muss meine Regel hängen?

Eine Regel muss immer alle Elemente "überblicken" können, die durch sie gesteuert werden. Beispielsweise muss eine Regel, die Elemente innerhalb einer Gruppe steuern soll, auf Ebene dieser Gruppe verortet werden.
Wenn eine Regel Elemente aus mehreren Gruppen steuern soll, dann muss die Regel an einem Element verortet werden, welches alle diese Gruppen beinhaltet. In manchen Fällen kann dies das Datenschema selbst sein. Sie sollten sich jedoch bemühen die Regel an dem kleinsten gemeinsame Element zu verorten.
Dadurch können in einem nachnutzenden System präzisere Fehlermeldung geschaltet werden.

## Kleine Tipps & Tricks

:::tip

- Es kann sinnvoller sein, eine Regel in mehrere kleinere Regeln aufzusplitten, falls diese sonst zum umfangreich wird.
- Regeln hängen eher selten an Datenfeldern.

:::

## Vorlagen mit Beispielen

### Vorlage für Code- oder Wertelisten

:::info
Wenn das **Feld (ID des Feldes) ("Name des Feldes")** = **(Eintrag der Code- oder Werteliste mit Schlüssel und Wert)**, dann muss das **Element (ID des Elements) ("Name des Elements")** befüllt sein. Wenn das **Feld (ID des Feldes) ("Name des Feldes")** /= **(Eintrag der Code- oder Werteliste mit Schlüssel und Wert)**, dann darf das **Element (ID des Elements) ("Name des Elements")** nicht befüllt sein.
:::

Dies ist für jeden Eintrag der Code- oder Werteliste zu wiederholen.
Falls mehrere Elemente durch die Wahl eines Werts betroffen sind, müssen alle betroffenen Elemente genannt werden.

### konkretes Beispiel

Wenn das **Feld F60 000 000263 "Abfrage Anschrift Inland oder Ausland" = 001 "in Deutschland"**, dann muss die **Datenfeldgruppe  G60 000 000088 "Anschrift Inland"** befüllt sein. Wenn das **Feld F60 000 000263 "Abfrage Anschrift Inland oder Ausland" /= 001 "in Deutschland"**, dann darf die **Datenfeldgruppe  G60 000 000088 "Anschrift Inland"** nicht befüllt sein.

Wenn das **Feld F60 000 000263 "Abfrage Anschrift Inland oder Ausland" = 002 "außerhalb von Deutschland"**, dann muss die **Datenfeldgruppe  G60 000 000091 "Anschrift Ausland"** befüllt sein. Wenn das **Feld F60 000 000263 "Abfrage Anschrift Inland oder Ausland" /= 002 "außerhalb von Deutschland"**, dann darf die **Datenfeldgruppe  G60 000 000091 "Anschrift Ausland"** nicht befüllt sein.

### Vorlage für Wahrheitswerte

:::info
Wenn das **Feld (ID des Feldes) ("Name des Feldes")** = **WAHR**, dann muss das **Element (ID des Elements) ("Name des Elements")** befüllt sein. Wenn das **Feld (ID des Feldes) ("Name des Feldes")** = **FALSCH**, dann darf das **Element (ID des Elements) ("Name des Elements")** nicht befüllt sein.
:::

Es kann auch sein, dass durch die Wahrheitswertabfrage mehrere Elemente gesteuert werden.

### konkretes Beispiel

Wenn das **Feld (ID des Feldes) "Besitz weiterer Staatsangehörigkeiten"** = **WAHR**, dann muss das **Element (ID des Elements) "Staatsangehörigkeit"** befüllt sein. Wenn das **Feld (ID des Feldes) "Besitz weiterer Staatsangehörigkeiten"** = **FALSCH**, dann darf das **Element (ID des Elements) "Staatsangehörigkeit"** nicht befüllt sein.
