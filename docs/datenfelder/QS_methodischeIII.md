# Hilfestellung zum QS-Bericht III (WIP)

In diesem Abschnitt werden weitere Fehlercodes des QS-Berichts näher beleuchtet.

## 1080: Die obere Wertgrenze muss ein Zeitpunkt (Datum und Uhrzeit) sein.

#### Warum ist das wichtig?

asdf

#### Wie löse ich den Fehler auf?

asdf

## 1081: Die untere Wertgrenze muss eine Uhrzeit sein.

#### Warum ist das wichtig?

asdf

#### Wie löse ich den Fehler auf?

asdf

## 1082: Die obere Wertgrenze muss eine Uhrzeit sein.

#### Warum ist das wichtig?

asdf

#### Wie löse ich den Fehler auf?

asdf

## 1083: Der Inhalt muss ein Zeitpunkt (Datum und Uhrzeit) sein.

#### Warum ist das wichtig?

asdf

#### Wie löse ich den Fehler auf?

asdf

## 1084: Der Inhalt muss eine Uhrzeit sein.

#### Warum ist das wichtig?

asdf

#### Wie löse ich den Fehler auf?

asdf

## 1085: Der Inhalt muss ein Code-Wert der Werteliste sein.

#### Warum ist das wichtig?

asdf

#### Wie löse ich den Fehler auf?

asdf

## 1087: Relation zu einem unzulässigen Objekt.

#### Warum ist das wichtig?

asdf

#### Wie löse ich den Fehler auf?

asdf

## 1088: Diese Relation darf in diesem Objekt nicht verwendet werden.

#### Warum ist das wichtig?

asdf

#### Wie löse ich den Fehler auf?

asdf

## 1101: Der Bezug zur Handlungsgrundlage sollte bei Elementen mit der Strukturelementart 'harmonisiert' möglichst nicht leer sein.

#### Warum ist das wichtig?

asdf

#### Wie löse ich den Fehler auf?

asdf

## 1102: Bei Eingabedatenfeldern mit dem Datentyp 'Text' oder 'String.Latin+' sollten, wenn möglich, die minimale und maximale Feldlänge angegeben werden.

#### Warum ist das wichtig?

asdf

#### Wie löse ich den Fehler auf?

asdf

## 1103: Bei Datenfeldern mit dem Datentyp ‘Nummer', 'Ganzzahl', 'Geldbetrag' oder 'Datum’ sollte, wenn möglich, ein Wertebereich angegeben werden.

#### Warum ist das wichtig?

asdf

#### Wie löse ich den Fehler auf?

asdf

## 1104: Zu einer Regel ist kein Script definiert - wird vorübergehend abgestellt

#### Warum ist das wichtig?

asdf

#### Wie löse ich den Fehler auf?

asdf

## 1105: Die Version einer Codeliste sollte im Datumsformat in folgender Form angegeben werden, z. B. 2018-03-01.

#### Warum ist das wichtig?

asdf

#### Wie löse ich den Fehler auf?

asdf

## 1106: Eine Datenfeldgruppe sollte mehr als ein Unterelement haben.

#### Warum ist das wichtig?

asdf

#### Wie löse ich den Fehler auf?

asdf

## 1107: Der Hilfetext eines Dokumentsteckbriefs sollte befüllt werden.

#### Warum ist das wichtig?

asdf

#### Wie löse ich den Fehler auf?

asdf

## 1108: Wenn ein Datenfeld die Feldart 'Auswahl' hat, sollte der Datentyp i. d. R. vom Typ 'Text' sein.

#### Warum ist das wichtig?

asdf

#### Wie löse ich den Fehler auf?

asdf

## 1109: Bei einem Feld mit nur lesendem Zugriff, der Feldart 'Statisch' wird i. d. R. der Inhalt mit einem Text befüllt.

#### Warum ist das wichtig?

asdf

#### Wie löse ich den Fehler auf?

asdf

## 1110: In Codelisten sollte der Bezug zur Handlungsgrundlage nicht leer sein.

#### Warum ist das wichtig?

asdf

#### Wie löse ich den Fehler auf?

asdf

## 1111: Codeliste wird nicht verwendet.

#### Warum ist das wichtig?

asdf

#### Wie löse ich den Fehler auf?

asdf

## 1112: Versionierte Baukastenelemente, die nicht verwendet werden, sollten gelöscht werden.

#### Warum ist das wichtig?

asdf

#### Wie löse ich den Fehler auf?

asdf

## 1113: Eine Arbeitsversion, die nicht verwendet wird und keine Versionen hat, sollte gelöscht werden.

#### Warum ist das wichtig?

asdf

#### Wie löse ich den Fehler auf?

asdf

## 1114: Element wird nicht verwendet.

#### Warum ist das wichtig?

asdf

#### Wie löse ich den Fehler auf?

asdf

## 1115: Auf die Codeliste kann nicht im XRepository zugegriffen werden. Dies könnte auf einen Fehler in der URN oder der Version der Codeliste zurückzuführen sein.

#### Warum ist das wichtig?

asdf

#### Wie löse ich den Fehler auf?

asdf

## 1116: Baukastenelemente im Status 'inaktiv' sollten nur in Ausnahmefällen verwendet werden.

#### Warum ist das wichtig?

asdf

#### Wie löse ich den Fehler auf?

asdf

## 1117: Das Feld 'Fachlicher Ersteller' darf zur Versionierung oder Veröffentlichung nicht leer sein.

<a name="1117"/>

#### Warum ist das wichtig?

asdf

#### Wie löse ich den Fehler auf?

asdf