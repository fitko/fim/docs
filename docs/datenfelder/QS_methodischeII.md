# Hilfestellung zum QS-Bericht II (WIP)

In diesem Abschnitt werden weitere Fehlercodes des QS-Berichts näher beleuchtet.

## 1041: Baukastenelemente, die in Regeln referenziert werden, verwenden noch die alte, verkürzte Element-ID.

#### Warum ist das wichtig?

Die verkürzten Element-IDs entsprechen dem Standard XDatenfelder 2, jedoch sind diese IDs mit der Umstellung auf XDatenfelder 3 verlängert worden. Um zu verhindern, dass die in Ihren Regeln referenzierten Elemente nicht eindeutig referenziert werden, müssen die Element-IDs angepasst werden.

#### Wie löse ich den Fehler auf?

Prüfen Sie sorgfältig, wie die neuen Element-IDs lauten. Oft wird es sich hier bei um die alten Element-IDs handeln, die um drei Nullen erweitert wurden, die Ziffern des Unternummernkreises.

## 1042: Unbekannte Gruppenart.

#### Warum ist das wichtig?

Die Gruppenart wird durch eine Codeliste im Standard XDatenfelder 3 vorgegeben. Werte, die nicht in dieser Codeliste vorhanden sind, dürfen nicht gewählt werden.

#### Wie löse ich den Fehler auf?

Passen Sie Ihre Gruppenart auf einen, im Standard definierten, Wert an.

## 1043: Eine Auswahlgruppe muss mindestens zwei Unterelemente haben.

#### Warum ist das wichtig?

Ein Auswahlgruppe impliziert, dass eine Auswahl zwischen den Unterelementen der Gruppe zu erfolgen hat. Wird nur ein Unterelement genutzt, kann keine Auswahl erfolgen.

#### Wie löse ich den Fehler auf?

-	Ergänzen Sie Unterelemente an Ihrer Auswalgruppe.
-	ODER: Prüfen Sie, ob die Nutzung einer Auswahlgruppe an dieser Stelle sinnvoll ist.

## 1044: Eine Werteliste muss mindestens zwei Einträge haben.

#### Warum ist das wichtig?

Durch Wertelisten soll dem Endnutzer die Möglichkeit gegeben werden, aus einer gewissen Anzahl von Werten den passenden zu wählen. Bei nur einem Wert besteht keine Wahlmöglichkeit.

#### Wie löse ich den Fehler auf?

Ergänzen Sie weitere Einträge in Ihrer Werteliste.

## 1045: Innerhalb einer Werteliste dürfen Codes (Schlüssel) nicht doppelt verwendet werden.

#### Warum ist das wichtig?

Falls ein Schlüssel doppelt gewählt wurde, ist danach keine Eindeutigkeit mehr gegeben.

#### Wie löse ich den Fehler auf?

Benennen Sie den Schlüssel um, so dass alle Codes einzigartig sind.

## 1046: Die Multiplizität entspricht nicht den Formatvorgaben.

#### Warum ist das wichtig?

Durch den Standard ist vorgegeben, in welchem Format die Multiplizität zu erfassen ist.

#### Wie löse ich den Fehler auf?

Ändern Sie das Format Ihre Multiplizität ab. Prüfen Sie im Zweifel noch einmal den Standard, um sicherzugehen, dass Ihr Format valide ist.

## 1047: In der Multiplizität muss die minimale Anzahl kleiner oder gleich der maximalen Anzahl sein.

#### Warum ist das wichtig?

Minimale Werte dürfen nicht größer als maximale Werte sein bzw. maximale Werte dürfen nicht kleiner als minimale Werte sein.

#### Wie löse ich den Fehler auf?

Passen Sie die Multiplizität entsprechend an.

## 1048: Die URN-Kennung einer Codeliste entspricht nicht den Formatvorgaben, z. B. urn🇩🇪fim:codeliste:dokumenttyp (Umlaute sind nicht erlaubt!).

#### Warum ist das wichtig?

Das XRepository ist die Bezugsquelle für Codelisten und über die Kennung sind Codelisten definiert. Diese Kennung wird in Form einer URN notiert, d. h., wenn man sich nicht an diese Notation hält, kann keine Codeliste referenziert werden.

#### Wie löse ich den Fehler auf?

Überprüfen Sie Ihre Eingabe. Im Zweifelsfall rufen Sie direkt die Seite des XRepositorys aus und kopieren dort die Kennung Ihrer Codeliste in die Zwischenablage, um Sie so in Ihr Redaktionssystem zu übertragen.

## 1049: Die Definition eines Dokumentsteckbriefs muss befüllt sein.

#### Warum ist das wichtig?

Die Definition des Dokumentsteckbriefs ist wichtig, um diesen eindeutig zu definieren.

#### Wie löse ich den Fehler auf?

Ergänzen Sie die Definition.

## 1050: Die Bezeichnung muss befüllt sein.

#### Warum ist das wichtig?

Die Bezeichnung von Dokumentsteckbriefen und Datenschemata ist ein Pflichtattribut in XDatenfelder 3, deswegen muss die Bezeichnung stets gepflegt werden.

#### Wie löse ich den Fehler auf?

Pflegen Sie das Attribut Bezeichnung am gekennzeichneten Element nach.

## 1051: Der Bezug zur Handlungsgrundlage darf nicht leer sein.

#### Warum ist das wichtig?

Für eine saubere Modellierung ist es notwendig, dass die Elemente auf Handlungsgrundlagen basieren.

#### Wie löse ich den Fehler auf?

Prüfen Sie, welche Handlungsgrundlage für dieses Element an dieser Stelle passend ist. Holen Sie sich im Zweifel Unterstützung bei Ihrer Fachlichkeit.

## 1052: Der Bezug zur Handlungsgrundlage darf nur bei abstrakten Dokumentsteckbriefen leer sein.

#### Warum ist das wichtig?

Abstrakte Dokumentsteckbriefe selbst basieren in der Regel nicht auf Handlungsgrundlagen. Die konkreten, zugeordneten nicht abstrakten Dokumentsteckbriefe wiederum basieren auf Handlungsgrundlagen.

#### Wie löse ich den Fehler auf?

Prüfen Sie, ob die Handlungsgrundlage in Ihrem Fall leer gelassen werden kann.

## 1053: Zu einem konkreten Dokumentsteckbrief dürfen keine Dokumentsteckbriefe zugeordnet werden.

#### Warum ist das wichtig?

Die Verknüpfung zwischen Dokumentsteckbriefen erfolgt nur zwischen abstrakten und nicht abstrakten / konkreten Dokumentsteckbriefen. Folglich kann einem konkreten Dokumentsteckbrief kein Dokumentsteckbrief zugeordnet sein.

#### Wie löse ich den Fehler auf?

-	Lösen Sie die Verknüpfung zwischen den Dokumentsteckbriefen wieder auf.
-	ODER: Prüfen Sie, ob es sich bei einem der Steckbriefe nicht doch um einen abstrakten Dokumentsteckbrief hätte handeln sollen.

## 1054: Zu einem abstrakten Dokumentsteckbrief müssen mindestens zwei Dokumentsteckbriefe zugeordnet werden.

#### Warum ist das wichtig?

Einen abstrakter Dokumentsteckbrief kann man sich auch als eine Art Platzhalter für konkrete Dokumentsteckbriefe vorstellen. Falls einem abstrakten Dokumentsteckbrief nur ein konkreter Dokumentsteckbrief zugeordnet wird, besteht keine Notwendigkeit für einen Platzhalter.

#### Wie löse ich den Fehler auf?

Arbeiten Sie Ihren abstrakten Dokumentsteckbrief weiter aus und verknüpfen Sie weitere konkrete Dokumentsteckbriefe.

## 1055: Unbekannte Dokumentart

#### Warum ist das wichtig?

Die Dokumentart wird durch eine Codeliste vorgegeben. Werte, die nicht in dieser Codeliste vorhanden sind, dürfen nicht gewählt werden.

#### Wie löse ich den Fehler auf?

Passen Sie Ihre Dokumentart auf einen, im Standard definierten, Wert an.

## 1056: Die Dateigröße darf nur befüllt sein, wenn der Datentyp des Feldes den Wert 'Anlage (Datei)' hat.

#### Warum ist das wichtig?

Präzisierungsangaben für Datenfelder sind abhängig von der Feldart und dem Datentyp. Nur für Anlagefelder darf eine Dateigröße gepflegt werden.

#### Wie löse ich den Fehler auf?

Entfernen Sie die Dateigröße, falls es sich nicht um ein Anlagefeld hätte handeln sollen.

## 1057: Erlaubte Datentypen dürfen nur angegeben werden, wenn der Datentyp des Feldes den Wert 'Anlage (Datei)' hat.

#### Warum ist das wichtig?

Präzisierungsangaben für Datenfelder sind abhängig von der Feldart und dem Datentyp. Nur für Anlagefelder dürfen die erlaubten Datentypen gepflegt werden.

#### Wie löse ich den Fehler auf?

Entfernen Sie die erlaubten Datentypen, falls es sich nicht um ein Anlagefeld hätte handeln sollen.

## 1058: Spaltendefinitionen dürfen nur angegeben werden, wenn im Datenfeld eine referenzierte Codeliste enthalten ist.

#### Warum ist das wichtig?

asdf

#### Wie löse ich den Fehler auf?

asdf

## 1059: Die minimale Feldlänge muss eine ganze Zahl größer oder gleich Null sein.

#### Warum ist das wichtig?

Präzisierungsangaben für Datenfelder sind abhängig von der Feldart und dem Datentyp. Für Eingabefelder (Text oder String.Latin+) ist weder ein negativer Wert, noch die Null sinnvoll.

#### Wie löse ich den Fehler auf?

Wählen Sie eine sinnvole Zeichenbegrenzung, die größer als Null ist.

## 1060: Die maximale Feldlänge muss eine ganze Zahl größer Null sein.

#### Warum ist das wichtig?

Präzisierungsangaben für Datenfelder sind abhängig von der Feldart und dem Datentyp. Für Eingabefelder (Text oder String.Latin+) ist weder ein negativer Wert, noch die Null sinnvoll.

#### Wie löse ich den Fehler auf?

Wählen Sie eine sinnvole Zeichenbegrenzung, die größer als Null ist.

## 1061: Die minimale Feldlänge darf nicht größer sein als die maximale Feldlänge.

#### Warum ist das wichtig?

Die minimale Feldlänge darf nicht größer als die maximale Feldlänge sein. Die maximale Feldlänge darf nicht kleiner als die minimale Länge sein. Sinnvolle Angaben sind hier wichtig, davon profitiert auch das nachnutzende System.

#### Wie löse ich den Fehler auf?

Passen Sie die minimale und die maximale Feldlänge entsprechend an.

## 1062: Die untere Wertgrenze muss eine Zahl sein.

#### Warum ist das wichtig?

Präzisierungsangaben für Datenfelder sind abhängig von der Feldart und dem Datentyp. Für Eingabefelder (Dezimalzahl oder Geldbetrag) muss die untere Grenze auch in Form eines numerischen Werts (Ganz- oder Dezimalzahl) erfasst werden.

#### Wie löse ich den Fehler auf?

Prüfen Sie, ob die untere Wertgrenze in Form einer Zahl ist, z. B. 3 oder -12,55.

## 1063: Die obere Wertgrenze muss eine Zahl sein.

#### Warum ist das wichtig?

Präzisierungsangaben für Datenfelder sind abhängig von der Feldart und dem Datentyp. Für Eingabefelder (Dezimalzahl oder Geldbetrag) muss die obere Grenze auch in Form eines numerischen Werts (Ganz- oder Dezimalzahl) erfasst werden.

#### Wie löse ich den Fehler auf?

Prüfen Sie, ob die obere Wertgrenze in Form einer Zahl ist, z. B. 3 oder -12,55.

## 1064: Die untere Wertgrenze darf nicht größer sein als die obere Wertgrenze.

#### Warum ist das wichtig?

Die untere Wertgrenze darf nicht größer als die obere Wertgrenze sein. Die obere Wertgrenze darf nicht kleiner als die untere Wertgrenze sein. Sinnvolle Angaben sind hier wichtig, davon profitiert auch das nachnutzende System.

#### Wie löse ich den Fehler auf?

Passen Sie die untere und die obere Wertgrenze entsprechend an.

## 1065: Die untere Wertgrenze muss eine ganze Zahl sein.

#### Warum ist das wichtig?

Präzisierungsangaben für Datenfelder sind abhängig von der Feldart und dem Datentyp. Für Eingabefelder (Ganzzahl) muss die untere Grenze auch in Form einer Ganzzahl erfasst werden.

#### Wie löse ich den Fehler auf?

Ergänzen Sie die untere Wertgrenze in Form einer Zahl, z. B. 3 oder -12.

## 1066: Die obere Wertgrenze muss eine ganze Zahl sein.

#### Warum ist das wichtig?

Präzisierungsangaben für Datenfelder sind abhängig von der Feldart und dem Datentyp. Für Eingabefelder (Ganzzahl) muss die obere Grenze auch in Form einer Ganzzahl erfasst werden.

#### Wie löse ich den Fehler auf?

Ergänzen Sie die obere Wertgrenze in Form einer Zahl, z. B. 8 oder 402.

## 1067: Die untere Wertgrenze muss ein Datum sein.

#### Warum ist das wichtig?

Präzisierungsangaben für Datenfelder sind abhängig von der Feldart und dem Datentyp. Für Eingabefelder (Datum) muss die untere Grenze auch in Form eines Datums erfasst werden.

#### Wie löse ich den Fehler auf?

Ergänzen Sie die untere Wertgrenze in Form eines Datums, z. B. 01.01.2024. Prüfen Sie im Zweifel das Format Ihres Datums.

## 1068: Die obere Wertgrenze muss ein Datum sein.

#### Warum ist das wichtig?

Präzisierungsangaben für Datenfelder sind abhängig von der Feldart und dem Datentyp. Für Eingabefelder (Datum) muss die obere Grenze auch in Form eines Datums erfasst werden.

#### Wie löse ich den Fehler auf?

Ergänzen Sie die obere Wertgrenze in Form eines Datums, z. B. 01.01.2024. Prüfen Sie im Zweifel das Format Ihres Datums.

## 1069: Der Inhalt muss eine Zahl sein.

#### Warum ist das wichtig?

Präzisierungsangaben für Datenfelder sind abhängig von der Feldart und dem Datentyp. Der Inhalt gibt in diesem Fall einen Defaultwert für die Eingabe des Antragstellenden vor. Dieser muss zur Feldart und zum Datentyp des Datentfeldes passen.

#### Wie löse ich den Fehler auf?

Ergänzen Sie den Inhalt in Form einer Zahl, z. B. 5 oder 312,99.

## 1070: Der Inhalt muss eine ganze Zahl sein.

#### Warum ist das wichtig?

Präzisierungsangaben für Datenfelder sind abhängig von der Feldart und dem Datentyp. Der Inhalt gibt in diesem Fall einen Defaultwert für die Eingabe des Antragstellenden vor. Dieser muss zur Feldart und zum Datentyp des Datentfeldes passen.

#### Wie löse ich den Fehler auf?

Ergänzen Sie den Inhalt in Form einer Ganzzahl, z. B. 5 oder 312.

## 1071: Der Inhalt muss ein Datum sein.

#### Warum ist das wichtig?

Präzisierungsangaben für Datenfelder sind abhängig von der Feldart und dem Datentyp. Der Inhalt gibt in diesem Fall einen Defaultwert für die Eingabe des Antragstellenden vor. Dieser muss zur Feldart und zum Datentyp des Datentfeldes passen.

#### Wie löse ich den Fehler auf?

Ergänzen Sie den Inhalt in Form eines Datums, z. B. 01.01.2024. Prüfen Sie im Zweifel das Format Ihres Datums.

## 1072: Der Inhalt unterschreitet die untere Wertgrenze.

#### Warum ist das wichtig?

Präzisierungsangaben für Datenfelder sind abhängig von der Feldart und dem Datentyp. Der Inhalt gibt in diesem Fall einen Defaultwert für die Eingabe des Antragstellenden vor. Dieser muss innerhalb des, durch die Wertegrenzen definierten Wertebereichs, liegen.

#### Wie löse ich den Fehler auf?

Der Inhalt muss innerhalb des, durch die untere und obere Grenze definierten, Wertebereichs liegen. Prüfen Sie diesen Wertebereich und passen Sie Ihren Defaultwert entsprechend an.

## 1073: Der Inhalt überschreitet die obere Wertgrenze.

#### Warum ist das wichtig?

Präzisierungsangaben für Datenfelder sind abhängig von der Feldart und dem Datentyp. Der Inhalt gibt in diesem Fall einen Defaultwert für die Eingabe des Antragstellenden vor. Dieser muss innerhalb des, durch die Wertegrenzen definierten Wertebereichs, liegen.

#### Wie löse ich den Fehler auf?

Der Inhalt muss innerhalb des, durch die untere und obere Grenze definierten, Wertebereichs liegen. Prüfen Sie diesen Wertebereich und passen Sie Ihren Defaultwert entsprechend an.

## 1074: Der Inhalt unterschreitet die Minimallänge.

#### Warum ist das wichtig?

Präzisierungsangaben für Datenfelder sind abhängig von der Feldart und dem Datentyp. Der Inhalt gibt in diesem Fall einen Defaultwert für die Eingabe des Antragstellenden vor. Dieser muss innerhalb des, durch die Wertegrenzen definierten Wertebereichs, liegen.

#### Wie löse ich den Fehler auf?

Der Inhalt muss innerhalb des, durch die untere und obere Grenze definierten, Wertebereichs liegen. Prüfen Sie diesen Wertebereich und passen Sie Ihren Defaultwert entsprechend an.

## 1075: Der Inhalt überschreitet die Maximallänge.

#### Warum ist das wichtig?

Präzisierungsangaben für Datenfelder sind abhängig von der Feldart und dem Datentyp. Der Inhalt gibt in diesem Fall einen Defaultwert für die Eingabe des Antragstellenden vor. Dieser muss innerhalb des, durch die Wertegrenzen definierten Wertebereichs, liegen.

#### Wie löse ich den Fehler auf?

Der Inhalt muss innerhalb des, durch die untere und obere Grenze definierten, Wertebereichs liegen. Prüfen Sie diesen Wertebereich und passen Sie Ihren Defaultwert entsprechend an.

## 1076: Der Inhalt stimmt nicht mit der zugeordneten Codeliste überein.

#### Warum ist das wichtig?

asdf

#### Wie löse ich den Fehler auf?

asdf

## 1077: Der Inhalt muss ein Wahrheitswert sein (true oder false).

#### Warum ist das wichtig?

asdf

#### Wie löse ich den Fehler auf?

asdf

## 1078: Ein Datenschema muss mindestens ein Unterlement enthalten.

#### Warum ist das wichtig?

asdf

#### Wie löse ich den Fehler auf?

asdf

## 1079: Die untere Wertgrenze muss ein Zeitpunkt (Datum und Uhrzeit) sein.

#### Warum ist das wichtig?

asdf

#### Wie löse ich den Fehler auf?

asdf

## 1080: Die obere Wertgrenze muss ein Zeitpunkt (Datum und Uhrzeit) sein.

#### Warum ist das wichtig?

asdf

#### Wie löse ich den Fehler auf?

asdf