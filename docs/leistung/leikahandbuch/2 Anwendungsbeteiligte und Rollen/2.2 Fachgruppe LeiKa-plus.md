---
sidebar_label: 2.2 Fachgruppe Föderales Informationsmanagement (FG FIM)
---

# 2.2 Fachgruppe Föderales Informationsmanagement (FG FIM)

Die Fachgruppe LeiKa-plus ist das zentrale Entscheidungsgremium der an der Anwendung „LeiKa-plus“ beteiligten Nutzer. Aufgaben und Handlungskontexte der Fachgruppe LeiKa-plus sind in der Geschäftsordnung der Fachgruppe LeiKa-plus geregelt (Anlage 1).