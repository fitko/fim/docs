# Grundsätzliches über Datenfelder

## Was ist ein Datenfeld?
Datenfelder (kurz Felder) sind die kleinste elementare Einheit im FIM-Baustein Datenfelder und damit Bestandteil von Datenfeldgruppen und Datenschemata.
Datenfelder bilden zusammen mit den Datenfeldgruppen den Baukasten im FIM-Baustein Datenfelder.

## Wann werden Datenfelder verwendet?
Datenfelder werden als Unterelement von Datenschemata und Datenfeldgruppen verwendet, wenn eine einzelne Eingabe oder Auswahl vom Bürger erwartet wird. Auch größere Hinweistexte können über Datenfelder abgebildet werden. Der genaue Zweck eines Datenfeldes ist über die Metadaten “Feldart” und “Datentyp” eindeutig definiert.

## Was sind die Metadaten Feldart und Datentyp? :sparkles:
Die beiden Metadaten beschreiben das Naturell eines Datenfelds vollständig. Die “Feldart” gibt an, welche Art von Bürgerinteraktion hier zu erwarten ist und der “Datentyp” gibt an, von welchem Typ die Information ist, die in diesem Datenfeld erfasst wird. Im Folgenden ist ein Tabelle abgebildet, aus der die erlaubten Kombinationen von Feldart und Datentyp hervorgehen. Danach werden die validen Möglichkeiten kurz beschrieben.

![Übersicht Feldart und Datenttypen](./images/Feldart-Datenttyp-XDF3.png)

:::note 

Die Einträge in der Tabelle, bei denen ein :heavy_check_mark: steht, stellen valide Kombinationen aus Feldart und Datentyp dar. Die Einträge, bei denen ein :x: steht, stellen keine valide Kombination dar und dürfen nicht verwendet werden.

:::

:::note

 Die Kombination aus Feldart und Datentyp hat Einfluss auf die [Präzisierungsangaben](./DF_Allgemeines.md#präzisierung-von-datenfeldern-) eines Datenfelds. Damit sind beispielsweise Beschränkungen des Wertebereichs einer Eingabe gemeint.

:::

### Kombinationen mit der Feldart Eingabe
#### Datentyp Text
Ein Datenfeld, welches in einem aus dem XDatenfelder Format entwickelten Formular/Onlinedienst eine Texteingabe vom Bürger erwartet.

*__Beispiel:__* Ein Datenfeld ‘Ort’, welches die manuelle Eingabe eines Ortsnamen erwartet.

#### Datentypen Datum, Dezimalzahl, Ganzzahl und Geldbetrag
Ein Datenfeld, welches in einem aus dem XDatenfelder Format entwickelten Formular/Onlinedienst eine Eingabe in Form eines Datums, einer Dezimal- bzw. Ganzzahl oder eines Geldbetrags durch den Bürger erwartet.

*__Beispiele:__* 
- Ein Datenfeld vom Typ Ganzzahl: ‘Anzahl der Kinder’, welches die Eingabe der Anzahl der Kinder in Form einer Ganzzahl erwartet.
- Ein Datenfeld vom Typ Dezimalzahl: ‘Höhe in Metern’, welches die Eingabe der Höhe inklusive Komma und Nachkommastellen erlaubt, wie z. B. 2,45.
- Ein Datenfeld vom Typ Geldbetrag: ‘Gebühr pro Urkunde’, welches die Eingabe der Gebührenhöhe für eine Urkunde erwartet, wie z. B. 15,72.

#### Datentyp Wahrheitswert
Ein Datenfeld, welches in einem aus dem XDatenfelder Format entwickelten Formular/Onlinedienst eine ja oder nein Frage an den Bürger stellt.

*__Beispiel:__* Haben Sie die Hinweise zur Datenschutzgrundverordnung gelesen? Onlinedienst: Radiobutton mit ja/nein Auswahl - Papierformular: eckige Kästchen mit ja und nein, die angehakt werden.

#### Datentyp Anlage
Ein Datenfeld, welches in einem aus dem XDatenfelder Format entwickelten Formular/Onlinedienst eine Nachweiserbringung /einen Dateiupload vom Bürger erwartet.

#### Datentyp Zeit und Zeitpunkt :sparkles:
Ein Datenfeld, welches in einem aus dem XDatenfelder Format entwickelten Formular/Onlinedienst eine Eingabe einer Uhrzeit (Zeit) oder eine Eingabe einer Uhrzeit, samt Datumsangabe (Zeitpunkt) erwartet.

#### Datentyp String.Latin+ :new:
Ein Datenfeld, welches in einem aus dem XDatenfelder Format entwickelten Formular/Onlinedienst eine Texteingabe vom Bürger erwartet, welche explizit den Anforderungen an String.Latin+ 1.2 (DIN Spec 91379) Datentyp C genügen muss. Dies betrifft in der Regel Datenfelder, die die Eingabe eines Namens erwarten. Siehe auch: [Was macht die neue Feldart String.Latin+ und wann wird diese benötigt?](./DF_Allgemeines.md#was-macht-der-neue-datentyp-stringlatin-und-wann-wird-dieser-benötigt-)

### Kombinationen mit der Feldart Auswahl
Ein Datenfeld, welchem eine Code- oder Werteliste zugeordnet wird. Aus den Einträgen der zugeordneten Code- oder Werteliste kann eine Auswahl durch den Bürger vorgenommen werden.

*__Beispiel:__* Ein Datenfeld ‘Staatsangehörigkeit’, welches es dem Bürger ermöglicht aus einer Liste von möglichen Staatsangehörigkeiten die passende auszuwählen.

### Kombinationen mit der Feldart Statisch
Datentyp Text
Ein Datenfeld, welches die Möglichkeit bietet, dem Bürger einen längeren Text auszugeben. In diesem Kontext findet keine Eingabe statt.

*__Beispiel:__* Hinweise zur Datenschutzgrundverordnung.

#### Datentyp Objekt
Ein Datenfeld, welches die Möglichkeit bietet an dieser Stelle beispielsweise innerhalb einer digitalen Umsetzung eines Datenschemas, etwas einzubinden. Für das nachnutzende System sollte hier zusätzlich über das Metadatum “Beschreibung” Informationen gepflegt werden, damit die Intention dieses Datenfeldes klar vermittelt werden kann.

*__Beispiel:__* In einem Onlinedienst soll ein Video eingebunden werden.

### Kombinationen mit der Feldart versteckt :new:
Mit der Feldart versteckt können alle Datentypen kombiniert werden.

### Kombinationen mit der Feldart gesperrt :new:
Mit der Feldart gesperrt können alle Datentypen kombiniert werden.

## Präzisierung von Datenfeldern :sparkles:

Die Art der Präzisierung ist in erster Linie abhängig vom **Datentyp**.

![Übersicht Präzisierung von Datenfeldern](./images/Präzisierung-Datenfelder-XDF3.png)

Die Einträge in der Tabelle, bei denen ein :heavy_check_mark: steht, klassifizieren die gültigen Präzisierungsangaben für eine Datenfeld dieses Datentyps; die Einträge mit einem :x: klassifizieren eine Kombination aus Präzisierungsangaben und Datenfeld, die so nicht zu verwenden ist. Voraussetzung hierfür ist immer, dass eine gültige Kombination aus Datentyp und Feldart gewählt wurde (siehe [oben](./DF_Allgemeines#was-sind-die-metadaten-feldart-und-datentyp-)).

### Präzisierung durch Pattern
Ein regulärer Ausdruck zur Prüfung der Eingabe entsprechend dem HTML-Attribut "pattern". Die Bürgereingabe wird gegen diesen Ausdruck geprüft.

Siehe auch: HTML attribute: pattern - HTML: HyperText Markup Language | MDN 

Beispiel: In einem Datenfeld ‘Postleitzahl’ wird ein Pattern hinterlegt, welches prüft, ob die Bürgereingabe einer validen deutschen Postleitzahl entspricht.

### Präzisierung durch Angabe einer minimalen / maximalen Länge
Es kann auch jeweils nur einer der beiden Werte angegeben werden. Durch die Angabe der Länge wird bestimmt, wie viele Zeichen eine textuelle Bürgereingabe haben muss.

Beispiel: Ein Datenfeld ‘Strafhergang’, welches es dem Bürger ermöglicht den Strafhergang schriftlich wiederzugeben. Es gibt eine Begrenzung der minimalen Länge auf 500 Zeichen.

### Präzisierung durch Angabe eines minimalen / maximalen Werts
Es kann auch jeweils nur einer der beiden Werte angegeben werden. Durch Angabe des Wertes wird bestimmt, in welchen Grenzen eine numerische bzw. zeitliche Bürgereingabe sich bewegt.

Beispiel: Ein Datenfeld ‘Monat (numerisch)’, welches es dem Bürger ermöglicht einen oder mehrere Monate auszuwählen. Es gibt eine Begrenzung des minimalen Werts auf 1 und eine Begrenzung des maximalen Werts auf 12.

### Präzisierung durch Angabe eines Dateityps und der maximalen Dateigröße :new:
Es kann auch jeweils nur einer der beiden Werte angegeben werden. Durch Angabe dieser Angaben werden die Eigenschaften gültiger Anlagen näher spezifiziert.

Beispiel: Ein Datenfeld ‘Geburtsurkunde’, welches es ermöglicht an dieser Stelle eine Geburtsurkunde zu hinterlegen. Es gibt eine Begrenzung des Dateityps auf PDF und eine Begrenzung der maximalen Dateigröße auf 1048576 Bytes (= 1MB).

## Was sind versteckte Felder und wie wird damit umgegangen?  :new:
Versteckte Felder werden in der Regel in einem referenzbasierten Kontext verwendet.

Ein verstecktes Feld, ist ein Datenfeld, welches dem Endnutzer nicht angezeigt wird; d. h. diese werden nicht an den Bürger bzw. an das Nutzer-Frontend ausgegeben.

Diese Felder werden in erster Linie zur (internen) Berechnung oder zum Transport von Daten benötigt. Mögliche Beispiele für die Berechnung sind Kosten- oder Gebührenberechnungen.

## Was sind gesperrte Felder und wie wird damit umgegangen?  :new:
Gesperrte Felder werden in der Regel in einem referenzbasierten Kontext verwendet.

Es sind Felder, die dem Endnutzer nur lesend angezeigt werden, d. h. die Felder können durch den Bürger nicht weiterbearbeitet werden. Sie werden bereits im Vorfeld oder direkt während der Eingabe eines Formulars ermittelt bzw. berechnet.

Mögliche Anwendungsfälle sind Berechnungsergebnisse, die im Nutzer-Frontend mit angezeigt werden. Ein weiterer referenzbasierter Anwendungsfall ist die Datenübernahme; beispielsweise könnten Nutzerdaten als gesperrte Felder angezeigt werden.

## Was macht der neue Datentyp String.Latin+ und wann wird dieser benötigt? :new:

:::info

**String.Latin+** definiert, welche Zeichen in Unicode für die elektronische Verarbeitung von Namen und den Datenaustausch in Europa zu verwenden sind.

:warning: Hierbei handelt es sich jedoch um eine Einschränkung zum Datentyp **Text**.

Das heißt, dass durch die Verwendung von String.Latin+ die Verwendung mancher Zeichen nicht mehr zulässig ist. Beispielsweise sind Texte mit griechischen oder kyrillischen Buchstaben oder mit erweiterten (nicht-normativen) Nicht-Buchstaben in diesem Datentyp unzulässig.

Der Datentyp **String.Latin+** ist dann zu wählen, wenn in der Eingabe eines Datenfelds Namen im erweiterten Sinne gemäß der Zeichensatzvorgabe von String.Latin+ gespeichert bzw. verarbeitet werden können müssen. Dies liefert eine wertvolle Information an die Dienstleister nachnutzender Systemen.

:::

## Was sind die Hintergründe zum Datentyp String.Latin+?

:::note

Die Bezeichnung String.Latin+ ist verkürzt, um genau zu sein, geht es hier um **String.Latin+ 1.2 Datentyp C**, basierend auf der **DIN Spec 91379**.

In einem Beschluss des IT-Planungsrats vom 27.01.2012 hieß es: *„Softwaresysteme, die dem Länder übergreifenden Austausch von Personen- und Adressdaten dienen, (müssen) einen definierten Zeichenvorrat handhaben können.“* Die DIN SPEC 91379:2019-03 ist eine Weiterentwicklung zu diesem Beschluss, sie *“legt die Zeichen fest, die für Namen im weiteren Sinne benötigt werden.”*

*“Namen im weiteren Sinne sind Namen natürlicher Personen gemäß der Vorgaben des Personenstandsrechts ebenso wie Namen juristischer Personen, Namen von Produkten, Orten und Straßen sowie Titel von Dokumenten oder Gesetzen. Namen im weiteren Sinne können konkrete Objekte bezeichnen, aber auch virtuelle Konstrukte wie Produktgruppen oder Musikstile.“*

Weitere Informationen zu diesem Thema finden Sie unter: https://www.xoev.de/downloads-2316#StringLatin

Maßgeblich in der Entscheidung für den Datentyp C war, dass dieser von allen Datentypen der DIN SPEC 91379 den größten normativen Zeichensatz hat.

:::

## Wie funktioniert der Umgang mit Codelisten ab XDatenfelder 3? :new:

Im Standard XDatenfelder 2 gab es sowohl interne als auch externe Codelisten, diese werden in XDatenfelder 3 von Wertelisten und Codelisten abgelöst.

**Interne Codelisten** wurden direkt in den jeweiligen Redaktionssystemen erstellt und konnten in mehrere Datenfelder eingebunden werden. Diese werden im Standard XDatenfelder 3 mit Wertelisten bezeichnet. Wertelisten werden direkt an einem Datenfeld gepflegt.

**Externe Codelisten** wurden in den jeweiligen Redaktionssystem hochgeladen und stammen in der Regel aus dem [XRepository](https://www.xrepository.de/); diese werden im Standard XDatenfelder 3 als Codelisten bezeichnet.

|XDatenfelder 2|XDatenfelder 3|
|--------------|--------------|
|interne Codeliste|Werteliste|
|externe Codeliste|Codeliste|

## Sind Codelisten mit oder ohne Versionskennung einzubinden? :new:

Ob Codelisten mit oder ohne Versionskennung einzubinden sind, hängt in erster Linie von der entsprechenden Nutzergruppe (Fachlichkeit) ab.

Das heißt, innerhalb der Nutzergruppe sollte vereinbart werden, wie mit Codeliste umgegangen wird. Dabei muss festgelegt werden, was es bedeutet, wenn eine Codeliste ohne Kennung eingebunden wird. Informationen dazu müssen in der Beschreibung des jeweiligen Felds ergänzt werden.

Falls keine Festlegung getroffen wurde, gilt folgendes:

Ist eine Codeliste ohne Versionskennung eingebunden, dann ist immer die aktuelle Version dieser Codeliste gemeint.

## Exkurs: Wann ändert ein Datenfeld seinen Wert?

:::info

Um das **Zusammenspiel** zwischen den neuen Feldarten **versteckt** und **gesperrt**, sowie dem Metadatum **Vorbefüllung** besser zu verstehen, sollte bekannt sein, an welchem Punkten ein Datenfeld seinen Befüllung ändern kann.

1. Ein Datenfeld wird in einem Redaktionssystem initial erstellt bzw. editiert: Hierbei kann der Modellierende dem Feld einen Defaultwert über das Inhaltsfeld mitgeben. Siehe: [**Erstellung von Defaultwerten**](./DF_Tipps#wie-kann-einem-datenfeld-ein-defaultwert-zugewiesen-werden).

2. Ein Antragsverfahren einer Behörde: Ein importiertes Datenschema wird genutzt, damit eine Behörde in ihrem Antragsportal beispielsweise ein elektronisches Antragsformular bereitstellen kann. In diesem Schritt kann oder muss die Behörde manche Datenfelder mit Werten vorbelegen.

3. Der Bürger bearbeitet den Onlinedienst / das Formular: Hierbei kann der Formularausfüller den Inhalt aller editierbaren Felder verändern.

:::

## Was ist das neue Metadatum Vorbefüllung und wie geht man damit um? :new:

Der Wert des Metadatums Vorbefüllung legt fest, ob die zuständige Behörde Defaultwerte für die gekennzeichneten Datenfelder angeben oder überschreiben kann.

Hierfür gibt es genau drei Optionen:

- **Keine** - die Behörde darf diesen Wert **nicht** ändern
- **Optional** - die Behörde **kann** diesen Wert ändern
- **Verpflichtend** - die Behörde **muss** diesen Wert ändern

Ob ein Feld einer Vorbefüllung bedarf oder nicht, lässt sich aus den Handlungsgrundlagen entnehmen. Ein Beispiel für Vorbefüllung findet sich in der Gewerbeanzeige.

Der Defaultwert eines Datenfelds wird bei der Modellierung, wie aus dem Vorgängerstandard bekannt, über das Metadatum Inhalt gepflegt.

Unten folgt eine Tabelle, in der alle Fälle, die durch das Metadatum Vorbefüllung auftreten können, erläutert werden.

Die entscheidenden Parameter hierbei sind:

- Vorhandensein eines Default Werts durch den Modellierenden - d. h. hat dieser bei der Modellierung einen Defaultwert unter Inhalt gepflegt?
- Die Antwort auf die Frage, ob die zuständige Behörde an diesem Feld eine Vorbefüllung vornehmen muss - d. h. keine, optional oder verpflichtend.
- Die Feldart

Aus diesen drei Parametern ergibt sich die Bedeutung des Datenfelds im Kontext der Vorbefüllung. Es gilt auch hier wieder die Einschränkung, dass nur gültige Kombinationen aus Feldart und Datentyp zu verwenden sind.

Die Abkürzung ZS steht für zuständige Stelle.

|Default Wert durch Modellierenden hinterlegt?|Muss die zuständige Behörde eine Vorbefüllung vornehmen? (Wert den “Vorbefüllung” annimmt)|Feldart des entsprechenden Datenfelds|Semantische Bedeutung, die sich daraus ergibt|
|-----|-----|-----|-----|
|ja|keine|Eingabe|Normales Formularfeld mit Vorgabewert des Modellierenden|
|ja|keine|Auswahl|Normales Auswahlfeld mit Vorgabewert des Modellierenden|
|ja|keine|statisch|Statischer Text mit Wert des Modellierenden|
|ja|keine|versteckt|Versteckter Wert des Modellierenden|
|ja|keine|gesperrt|Gesperrtes Formularfeld mit Wert des Modellierenden|
|ja|optional|Eingabe|Normales Formularfeld mit Vorgabewert des Modellierenden oder der ZS|
|ja|optional|Auswahl|Normales Auswahlfeld mit Vorgabewert des Modellierenden oder der ZS|
|ja|optional|statisch|Statischer Text mit Wert des Modellierenden oder der ZS|
|ja|optional|versteckt|Versteckter Wert des Modellierenden oder der ZS|
|ja|optional|gesperrt|Gesperrtes Formularfeld mit Wert des Modellierenden oder der ZS|
|ja|verpflichtend|(alle)|(Sinnlose Variante: Ein Vorgabewert des Modellierenden macht keinen Sinn, wenn die ZS den Wert zwingend überschreiben muss)|
|nein|keine|Eingabe|Normales Formularfeld|
|nein|keine|Auswahl|Normales Auswahlfeld|
|nein|keine|statisch|Dieser statische Text kann nur durch eine Regel befüllt werden. :warning:|
|nein|keine|versteckt|Dieser versteckte Wert kann nur durch eine Regel befüllt werden|
|nein|keine|gesperrt|Dieses gesperrte Formularfeld kann nur durch eine Regel befüllt werden|
|nein|optional|Eingabe|Normales Formularfeld ggf. mit Vorgabewert der ZS|
|nein|optional|Auswahl|Normales Auswahlfeld ggf. mit Vorgabewert der ZS|
|nein|optional|statisch|Statischer Text, der leer ist oder von der ZS befüllt wurde|
|nein|optional|versteckt|Versteckter Text, der durch seinen Wert Einfluss auf Regeln nehmen kann|
|nein|optional|gesperrt|Gesperrtes Formularfeld, das leer ist oder von der ZS befüllt wurde|
|nein|verpflichtend|Eingabe|Normales Formularfeld mit Vorgabewert der ZS|
|nein|verpflichtend|Auswahl|Normales Auswahlfeld mit Vorgabewert der ZS|
|nein|verpflichtend|statisch|Statischer Text der ZS|
|nein|verpflichtend|versteckt|Versteckter Wert der ZS|
|nein|verpflichtend|gesperrt|Gesperrtes Formularfeld mit Wert der ZS|

## Zusammenfassung: Was hat sich beim Wechsel des Standards explizit für Datenfelder geändert?

:::info

- Erweiterung der Metadaten Feldart und Datentyp
    - Datentyp: String.Latin+, Zeit und Zeitpunkt
    - Feldart: gesperrt und versteckt
- Vorbefüllung wurde ergänzt
- Erweiterung der Präzisierungsangaben um Dateityp und maximale Dateigröße
- veränderter Umgang mit Codelisten
    - Codelisten wurden zur Code- und Wertelisten
    - zusätzliche Parameter für Codelisten: Code Key, Name Key und Help Key

:::
