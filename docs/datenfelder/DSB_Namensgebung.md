# Namensgebung von Dokumentsteckbriefen

:::info

Bitte versuchen Sie die folgenden Regeln so gut wie möglich umzusetzen. Falls Sie an der ein oder anderen Stelle daneben liegen sollten, machen Sie sich keine Sorgen; da Dokumentsteckbriefe alle zentral gepflegt werden sollten, wird ein Dokumentsteckbriefe bei der Überführung in den zentralen Katalog KATE zwangsläufig noch ein weiteres Mal qualitätsgesichert.

:::

<!-- 

## Allgemeine Hinweise zur Namensgebung

- Im Kontext geläufige Abkürzungen dürfen verwendet werden.
- Füllwörter sind (wenn möglich) zu streichen.

## Spezielle Hinweise zur Namensgebung

Allgemeine Hinweise gelten immer, danach "Dokument ist durch originäre Handlungsgrundlage definiert", nur wenn das nicht zutrifft, wird "Dokument ist durch die Verwendung definiert" aktiv. Die gelisteten Punkte der einzelnen Unterkapitel sind jeweils nacheinander abzuarbeiten. Zuerst der erste Punkt, wenn das nicht zutrifft, dann der zweite, usw.

Bei der Benamung ist es wichtig die Dokumentart und die Herkunft des Dokuments zu kennen.
(...)
Danach geht man nach den darauf folgenden Regeln vor; falls die erste Regel nicht erfüllbar ist, versucht man die zweite Regel anzuwenden und so weiter.

### Dokument ist durch die origiänre Handlungsgrundlage definiert

Dabei handelt es sich um Dokumentsteckbriefe, die durch Ihre Handlungsgrundlage definiert werden, das sind z. B. **Auslöser, Bescheide, Registeranfragen, Registerantworten**.
Wichtig ist, dass hierbei die Handlungsgrundlage betrachtet wird, in dem das Dokument originär erzeugt wird; nicht jedoch die Handlunsgrundlage, die das Dokument verwendet.
Es gilt wie folgt:

- Wenn die Handlungsgrundlage einen eindeutigen Namen für das Dokument bereitstellt, dann ist dieser zu verwenden.
- Falls es sich bei dem Dokument um  einen Bescheid handelt und das Ergebnis der Entscheidung nicht in der Handlungsgrundlage steht, dann ist dieses zu ergänzen. (Bsp. Ablehnung, Bewilligung, ...)
- Ansonsten: Falls die Handlungsgrundlage keinen Anhaltspunkt liefert, ist folgende Konstruktion zu verwenden: <br/> **Dokumentart + Leistungsobjekt + Verrichtung (ggf. + Verrichtungsdetail)**.

### Dokument ist durch die Verwendung definiert

Typische Dokumentsteckbriefe, die durch ihre Verwendung definiert werden sind **konkrete oder abstrakte Nachweise**.
Diese Nachweise können inner- oder außerhalb der öffentlichen Verwaltung entstehen.
Gilt auch für **Anträge, Anzeigen und Meldungen**.

- Wenn das (einen Antrag, eine Anzeige, eine Meldung oder einen Nachweis) Auslösende Dokument das Ergebnis eines anderen Prozesses ist, dann muss der in der Handlungsgrundlage dieses Prozesses spezifizierte Name verwendet werden.
- Wenn in der (das Dokument verwendenden) Handlungsrundlage bereits das Dokument benannt ist, muss dieser Name verwendet werden. Oft erkennt man dies daran, dass die Dokumentart bereits im Namen enthalten ist, z. B. Geburtsurkunde, Führerschein
- Ansonsten: analog zu oben ist folgende Konstruktion zu verwenden: <br/> **Dokumentart + Leistungsobjekt + Verrichtung (ggf. + Verrichtungsdetail)**.

....


- Wird derselbe Dokumentname in unterschiedlichen rechtlichen Kontexten verwendet, muss der Kontext im Namen des Dokumentsteckbriefs in Klammern hinzugesetzt werden.

Wie verhindern, dass der letzte Satz falsch verstanden wird?


-------------------------------------------------------------------------------

- Benamung von DSB abhängig von der Dokumentart

- für die Benamung ist wichtig, woher das Dokument (Welche Quelle liegt zugrunde) kommt, d. h. welche Handlungsgrundlage das originär erstellt und nicht, wo das Dokument verwendet wird

- typische DSB, die durch ihre Handlungsgrundlage definiert werden: Auslöser, Bescheide, Registeranfrage, Registerantwort
- typische DSB, die durch ihre Verwendung definiert werden: Nachweise (konkrete und abstrakte)
- AUSNAHMEN: Dokumente, die außerhalb der öffentlichen Verwaltung entstehen, hier muss die Benamung an die verwendende Handlungsgrundlage angepasst werden

- ACHTUNG: Führerzeugnisbeispiel - hier ist ein großer unterschied zwischen der definierten handlungsgrundlage und der definierenden handlungsgrundlage. die definierende sticht

- Grafik am besten passend zur Codeliste Dokumentart, falls gewünscht
- Reihenfolge für die Dokumentation:
    - zuerst "Bescheide" 5 in der PowerPoint und 4 lt. Codeliste

### allgemeine Hinweise

- Es ist zuerst zu prüfen, ob Regel 1 angewendet werden kann, falls nicht dann sukzessive die folgenden Regeln
- Wird derselbe Dokumentname in unterschiedlichen rechtlichen Kontexten verwendet, muss der Kontext in Klammern hinzugesetzt werden
- Geläufige Abkürzungen können verwendet werden  
- Füllwörter (wenn möglich) rausnehmen

### spezielle Hinweise


falls möglich gilt folgendes:

ABCDEF

dann Standardregeln listen

falls dies nicht möglich ist, gilt für Dokumentart XYZ folgende Ausnahemregelung

## last but not least

### abstrakte DSB

werden (fast) immer durch Verwendung definiert

### OZG DSB

Komib-XYZ


## Beispiele

- z. B. kommt die Gewerbeanzeige aus der Gewerbeordnung
- Wohngeld Erstantrag Mietzuschuss kommt aus dem Wohngeldgesetz
- alle Beispiele übernehmen mit Ergänzung um die Dokumentart ggf. alle auffindbar in KATE Tabelle

## Handlungsgrundlagen von DSB

- i. d. erzeugende Handlungsgrundlage, aber kein Anspruch auf Vollständigkeit bei Typ 4 und 5 Leistungen und abstrakten DSB
- d. h. konkret, Sie müssen den DSB dann nicht mehr abändern, wenn eine weitere Handlungsgrundlage dazu kommt
- ohne zeitlichen Bezug
- bei OZG-Kombi Handlungsgrundlage ist Summe aus einzelnen DSB (kann großzügig zusammengefasst werden)
- abstrakte DSB: kann Summe aus zugeordneten DSB sein, kann aber auch leer sein, fallabhängig

## weiterführende Informationen

Foliensatz

-->