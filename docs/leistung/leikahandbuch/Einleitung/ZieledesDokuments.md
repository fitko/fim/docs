# Ziele des Dokuments

Ziel dieses Dokuments ist es, einen Handlungsrahmen zum Aufbau und zur Pflege des LeiKa sowie zum Umgang mit diesem zu beschreiben. Es werden Aufgaben beschrieben sowie Rollen und Prozesse zu deren Erledigung definiert. Des Weiteren skizziert es den Gesamtzusammenhang, in dem der LeiKa und dessen Struktur stehen.