# Freigabe

Stammtexte werden durch die zuständige Bundesbehörde freigegeben. Das Freigabedatum wird als Stamminformation im Modul 20 (fachlich freigegeben am) und das freigebende Ressort als Stamminformation im Modul 19 (fachlich freigegeben durch) vermerkt. 

Werden Stamminformationen durch die jeweils nachgeordnet zuständigen, gesetzesausführenden Behörden auf Landes- und Kommunalebene ersetzt, entfällt der Freigabevermerk. Werden Stamminformationen ergänzt, so ist dies kenntlich zu machen.