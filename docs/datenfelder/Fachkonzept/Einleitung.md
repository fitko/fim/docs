# Einleitung

## Der Baustein Datenfelder im FIM-Kontext (ALT)

Das Gesamtsystem FIM besteht aus unterschiedlichen Einzelsystemen, genannt Bausteine. Neben den
fachlichen Systemen für die Bausteine Leistungen, Datenfelder und Prozesse ist auch ein
gemeinsames Portalsystem vorhanden, über welches nach Baustein-Inhalten recherchiert und über
das Baustein-Inhalte abgerufen werden können. Die Systemlandschaft des gesamten Projektes FIM
gliedert sich analog zu der organisatorischen Aufteilung in vier Bereiche: Leistungen, Datenfelder,
Prozesse und Portal.
Die Software-Systeme der Bausteine Leistungen, Datenfelder und Prozesse werden als eigenständige
Systeme betrachtet. Mittels des FIM-Portals sind die FIM-Inhalte einheitlich über die drei Bausteine
hinweg erschlossen, d.h. die in der FIM-Logik spezifizierten Querbezüge zwischen Leistungen,
Datenfeldern und Prozessen sind technisch umgesetzt. Führend ist dabei der Leistungskatalog (LeiKa)
mit Bezügen in den Leistungsstammtexten und der dabei zugeordneten Leistungs-ID, die sich in den
Bausteinen Prozesse und Formular widerspiegelt.
Das FIM-Portal selbst hält keine eigenen FIM-Inhalte vor. Die Bausteine sind mit einer
Serviceschnittstelle angeschlossen und liefern über eine Exportschnittstelle Inhalte zu.
Es sind hierfür drei XÖV-konforme Austauschstandards zum Transport der bausteinspezifischen FIMInhalte festgelegt worden. Das System des Bausteins Leistungen nutzt den Standard XZufi.
Prozessinformationen werden im Baustein des Systems Prozesse über den Standard XProzess
ausgetauscht. Für den Baustein Datenfelder soll der neue XÖV-Standard XDatenfelder genutzt werden.
Die XÖV-Standards sind entsprechend spezifiziert und teilweise zertifiziert.

## Zielgruppe des Fachkonzept

Das Fachkonzept richtet sich an Interessierte und Hersteller von Werkezugen, das sind Redaktionssysteme im FIM-Baustein Datenfelder.

BLUB