# Metamodell

## Was ist das und welche Rolle spielt es?

Das Metamodell zeigt die Zusammenhänge der einzelnen Elemente des FIM-Bausteins Datenfelder auf einer abstrakt fachlichen Ebene auf. Hierbei gibt es verschiedene Granularitätsstufen, die durch die verschiedenen Level des Metamodells abgebildet werden. Auf Level 0 beispielsweise erkennt man nur die Struktur des Bausteins und dessen Schnittstellen zu den anderen zwei Bausteilen. Je höher das Level des Metamodells ist, desto feingranularer ist die Beschreibung.

Die einzelnen Elemente des Bausteins werden hierbei in ihrem Zusammenhang aufgezeigt.

Für Hilfestellung zur Notation der Grafiken, beachten Sie auch die entsprechende Wikipedia Seite zur [UML Notation](https://de.wikipedia.org/wiki/Unified_Modeling_Language).

## Level 0 - die Struktur des Bausteins und die Verbindung zu den anderen Bausteinen

Betrachtet man zunächst nur den linken Teil der Grafik, der in den Farben des Bausteins (rotbraun) gehalten ist, erkennt man den Aufbau des Bausteins, der in dieser Form dem Aufbau der anderen Bausteine gleicht.

Ein Repository im Baustein, besteht aus drei Verzeichnissen, diese sind:

- der Datenfeldkatalog, welcher beliebig viele Dokumentsteckbriefe enthalten kann
- die Datenschemabibliothek, welche beliebig viele Datenschemata enthalten kann
- der Datenfeldbaukasten, welcher beliebig viele Baukastenelemente enthalten kann

Baukastenelemente werden in Datenschemata verwendet, welche selbst mindestens eines bis beliebig viele Baukastenelemente enthält.
Ein Datenschema ist genau einem Dokumentsteckbrief zugeordnet; wobei einem Dokumentsteckbrief selbst keine oder beliebig viele Datenschemata zugeordnet sind.

![Metamodell L0](./images/Metamodell_L0.png)

Die Schnittstelle zu den beiden anderen *FIM-Bausteinen* *Prozesse* (grün) und *Leistungen* (blaugrau) ist der Dokumentseckbrief. Dokumentsteckbriefe werden im FIM-Baustein Prozesse in Nachrichten- und Sequenzflüssen referenziert. Im FIM-Baustein Leistungen werden Dokumentsteckbriefe als aus- und eingehende Dokumente referenziert oder in Formularen.

## Level 1 - die Elemente des Bausteins

In Level 1 werden die einzelnen Elemente des FIM-Bausteins Datenfelder und dessen Verhältnisse untereinander näher betrachtet.

Im linke Teil der Grafik sieht man den gleichen Verzeichnisaufbau wie in Level 0 des Metamodells, mit einem Unterschied. Der Aufbau des Datenfeldbaukastens wird näher spezifiziert: er besteht aus Datenfeldern und Datenfeldgruppen, dies sind die sogenannten Baukastenelemente. In Datenschemata verwendet werden sie auch als Strukturelement bezeichnet.

![Metamodell L1](./images/Metamodell_L1.png)

Auf Level 1 gibt es Elemente. Diese Elemente sind hierarchisch aufgebaut. 

Auf oberster Ebene steht der Dokumentsteckbrief. Einem Dokumentsteckbrief können Stammdatenschemata zugeordnet sein. Ein Stammdatenschema enthält Baukastenelemente wie Datenfeldgruppen und Datenfelder. 

Baukastenelementen sind Regeln zugeordnet, die Sonderfälle und den Umgang damit beschreiben.

Für Informationen bzgl. der jeweils enthaltenen Daten empfiehlt sich der Blick in die XDatenfelder-Spezifikation. 