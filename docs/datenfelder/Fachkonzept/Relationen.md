# Relationen

Zwischen den Elementen im Baustein Datenfelder gibt es verschiedene Beziehungen, welche über Relationen abgebildet werden. Die Codeliste der möglichen Relationen, die vom Standard XDatenfelder 3 genutzt wird, findet sich hier: [urn:xoev-de:fim-datenfelder:codeliste:relation](https://www.xrepository.de/details/urn:xoev-de:fim-datenfelder:codeliste:relation).

Hierbei wird die Codeliste nach Nutzungstyp 3 des Codelistenhandbuchs der KoSIT verwendet, d. h. diese Codeliste wird vom Standard XDatenfelder 3 direkt referenziert und sie kann sich unabhängig vom Standard ändert; es wird im Standard selbst nicht angegeben, welche Version der Codeliste zu verwenden ist. Im Zweifelsfall ist es sinnvoll die aktuellste Version der Codeliste zu verwenden.

## Relationen im Standard XDatenfelder 3
Die Werte der Relations-Codeliste lauten seit dem 20.04.2023 wie folgt:

|Code|Beschreibung|
|:----------:|:---------------------------|
|ABL|ist abgeleitet von|
|ERS|ersetzt|
|EQU|ist äquivalent zu|
|VKN|ist verknüpft mit|

### ABL - ist abgeleitet von

Die Relationsart **ist abgeleitet von** stellt eine Beziehung zwischen Elementen gleichen Typs dar. Zum Beispiel wird eine Datenfeldgruppe von einer anderen Datenfeldgruppe abgeleitet.

Diese Relation ist bei einem Kopiervorgang zu erstellen, sie kann aber auch manuell gesetzt werden.

### ERS - ersetzt

Die Relationsart **ersetzt** stellt eine Beziehung zwischen Baukastenelementen dar, z. B. eine Datenfeldgruppe ersetzt eine andere Datenfeldgruppe.

Diese Relation wird in einem System manuell gesetzt.

### EQU - ist äquivalent zu

Die Relationsart **ist äquivalent zu** stellt u. a. eine Beziehung zwischen Baukastenelementen dar, z. B. ein Datenfeld ist äquivalent zu einer Datenfeldgruppe.

Diese Relation wird in einem System manuell gesetzt.

### VKN - ist verknüpft mit

Die Relationsart **ist verknüpft mit** stellt eine Beziehung zwischen abstrakten und nicht-abstrakten Dokumentsteckbriefen dar.

Diese Relation wird in einem System manuell gesetzt.

## Repräsentation im Standard XDatenfelder 3

~~~
<xdf3:stammdatenschema>
        <xdf3:identifikation>
            <xdf3:id>S00000000092</xdf3:id>
            <xdf3:version>1.0.0</xdf3:version>
        </xdf3:identifikation>
        <xdf3:name>Antrag Selbstauskunft Verstoßdatei Seefischereigesetz</xdf3:name>
...
        <xdf3:relation>
            <xdf3:praedikat listURI="urn:xoev-de:fim-datenfelder:codeliste:relation" listVersionID="1.0">
                <code>ABL</code>
            </xdf3:praedikat>
            <xdf3:objekt>
                <xdf3:id>S00000000094</xdf3:id>
            </xdf3:objekt>
        </xdf3:relation>
....
</xdf3:stammdatenschema>
~~~

## Erstellen von Relationen
Beim Erstellen von Relationen muss darauf geachtet werden, dass Relationen nicht wahllos zwischen unterschiedlichen Elementen erstellt werden können. Die Tabelle gibt Aufschluss darüber, zwischen welchen Elementen welche Relationen bestehen können.

||Dokumentsteckbrief|Datenschema|Datenfeldgruppe|Datenfeld|
|:----------:|:----------:|:----------:|:----------:|:----------:|
|**Dokumentsteckbrief**|VKN, ABL, ERS|-|-|-|
|**Datenschema**|-|ABL, ERS, EQU|-|-|
|**Datenfeldgruppe**|-|-|ABL, ERS, EQU|ERS, EQU|
|**Datenfeld**|-|-|ERS, EQU|ABL, ERS, EQU|

## Vererbung von Relationen
Die Vererbung von Relationen ist vorerst nicht angedacht; d. h. bei einem Kopiervorgang erbt das kopierte Element die Relationen seines Ursprungselements vorerst nicht.

## SPO Notation
Die Relationen stellen die Beziehungen der Objekte im FIM-Baustein Datenfelder in einer Subjekt - Prädikat - Objekt Notation dar.

Die Bedeutung der Relation entspricht hierbei der sprachlichen Bedeutung; das heißt, dass die Richtung in der Relationen abgelegt werden sollen für die Bedeutung der Relation entscheidend ist.

### Beispiele
- Kindelement “ist abgeleitet von” Elternelement
- neues Element “ersetzt” altes Element

## Relationen im Standard XDatenfelder 2
In der vorherigen Version des Fachkonzepts waren die Relationen noch abweichend definiert; anbei eine Übersicht der alten Relationen und der Entsprechungen im neuen Standard. Diese veralteten Relationen waren kein Teil des XDatenfelder 2 Exports.

|alte Relation aus XDatenfelder 2|Definition|Entsprechung im Standard XDatenfelder 3|
|:----:|:----:|:----:|
|besteht aus|Repräsentiert (hierarchische) Beziehungen zwischen Elementen des FIM-Datenfelderbaukastens sowie der FIM-Datenfelderbibliothek (z. B. ein Stammdatenschema besteht aus Datenfeldgruppen und Datenfeldern, eine Datenfeldgruppe besteht aus Datenfeldgruppen und Datenfeldern).|Diese Relation ist gestrichen worden. Die hierarchische Beziehung zwischen beispielsweise Datenschemata und dessen zugeordneten Elementen wird nicht über Relationen dargestellt.|
|ist abgeleitet von|siehe oben|1 zu 1 übernommen - siehe: ABL - ist abgeleitet von|
|ist ersetzt durch|siehe oben|1 zu 1 übernommen - siehe: ERS - ersetzt|
|ist strukturiert durch|Hierdurch wurde die Beziehung zwischen einem Dokumentsteckbrief und seinem zugehörigen Datenschema gekennzeichnet.|Diese Relation ist gestrichen worden.Datenschemata haben dafür in XDatenfelder 3 ein weiteres Metadatum erhalten, das Metadatum “dokumentsteckbrief”.|