# Qualitätskriterien

Für die Bereitstellung und Pflege von Stammtexten wurden Qualitätskriterien definiert. Es wird empfohlen, dass sich alle Bereitsteller und Nutzer von Informationen an diesen Kriterien orientieren. Die Qualitätskriterien sind dem Handbuch als Anlage 2 beigefügt.