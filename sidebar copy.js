module.exports = {
    defaultSidebar: [
        'einstieg/intro',
		
        {
        	'type': 'category',
        	'label': 'Baustein Datenfelder',
        	'items': [
				{
					'type': 'category',
					'label': 'FIM Schema-Repository',
					'items': [
						'schema-repository/about-this-schemarepository',
						'schema-repository/about-this-doc',
						'schema-repository/context',
						'schema-repository/schemarepository-for-group-1',
						'schema-repository/schemarepository-for-group-2',
						'schema-repository/schemarepository-for-group-4',
						'schema-repository/schema-repository-api',
						'schema-repository/roadmap',
						'schema-repository/glossary',
						'schema-repository/changelog',
					]
				},
				{
					'type': 'category',
					'label': 'Fachkonzept (in Arbeit)',
					'items': [
						// 'datenfelder/Fachkonzept/Einleitung',
						'datenfelder/Fachkonzept/Lesehilfe',
						'datenfelder/Fachkonzept/Identifikatoren',
						'datenfelder/Fachkonzept/Datenfelder',
						'datenfelder/Fachkonzept/Datenfeldgruppen',
						// 'datenfelder/Fachkonzept/Regeln',
						'datenfelder/Fachkonzept/Datenschemabibliothek',
						'datenfelder/Fachkonzept/Datenfeldkatalog',
						'datenfelder/Fachkonzept/Metamodell',
						'datenfelder/Fachkonzept/Relationen',
						'datenfelder/Fachkonzept/Status',
						// 'datenfelder/Fachkonzept/Schnittstellen',
						'datenfelder/Fachkonzept/zentrale_Repositorys',
					]
				},
				{
					'type': 'category',
					'label': 'QS-Kriterien',
					'items': [
					{
					  type: 'category',
					  label: 'Überblick',
					  collapsed: true,
					  items: [
						'datenfelder/VB_Einleitung',
						'datenfelder/VB_Umgang',
						'datenfelder/VB_Releasenotes',
					  ],
					},
					{
					  type: 'category',
					  label: 'Elementunabhängige Angaben',
					  collapsed: true,
					  items: [
						'datenfelder/EA_Sprache',
						'datenfelder/EA_Handlungsgrundlagen',
						'datenfelder/EA_Metadaten',
						'datenfelder/EA_Relationen',
						'datenfelder/EA_Multiplizitäten',
						'datenfelder/EA_BOB',
						'datenfelder/EA_KATE',
					  ],
					},
					{
					  'type': 'category',
					  'label': 'Datenfeldbaukasten',
					  items: [
							{
							  type: 'category',
							  label: 'Datenfelder',
							  collapsed: true,
							  items: [
								'datenfelder/DF_Allgemeines',
								'datenfelder/DF_Tipps',
								'datenfelder/DF_Metadaten',
							  ],
							},
							{
							  type: 'category',
							  label: 'Datenfeldgruppen',
							  collapsed: true,
							  items: [
								'datenfelder/DFG_Allgemeines',
								'datenfelder/DFG_Tipps',
								'datenfelder/DFG_Metadaten',
							  ],
							},
							{
							  type: 'category',
							  label: 'Regeln',
							  collapsed: true,
							  items: [
								'datenfelder/R_Allgemeines',
								'datenfelder/R_Tipps',
								'datenfelder/R_Metadaten',
							  ],
							}
						  ]
					},
					{
					  'type': 'category',
					  'label': 'Datenschemabibliothek',
					  'items': [
							'datenfelder/DS_Allgemeines',
							'datenfelder/DS_Tipps',
							'datenfelder/DS_Metadaten',
					  ]
					},
					{
					  'type': 'category',
					  'label': 'Datenfeldkatalog',
					  'items': [
							'datenfelder/DSB_Allgemeines',
							'datenfelder/DSB_Tipps',
							'datenfelder/DSB_Namensgebung',
							'datenfelder/DSB_Metadaten',
					  ]
					},
					{
					  'type': 'category',
					  'label': 'Qualitätssicherung',
					  'items': [
									'datenfelder/QS_Bericht',
									'datenfelder/QS_new',
									'datenfelder/QS_new2',
									'datenfelder/QS_new3',
									'datenfelder/QS_new4',
									'datenfelder/QS_new5',
									  ]
					},
					{
					  'type': 'category',
					  'label': 'Anhang',
					  'items': [
									'datenfelder/A_Versionierung',
									'datenfelder/A_Links',
					  ]
					},
				  ]
				},
				{
					'type': 'category',
					'label': 'ME-Austausch (in Arbeit)',
					'items': [
						'datenfelder/ME-Austausch/uebersicht',
						'datenfelder/ME-Austausch/agenda',
						'datenfelder/ME-Austausch/informationen',
						'datenfelder/ME-Austausch/archiv',
					]
				},
        	]
        },
        {
        	'type': 'category',
        	'label': 'Baustein Leistungen',
        	'items': [
        		{
        		  'type': 'category',
        		  'label': 'Leistung QS Kriterien (in Arbeit)',
        		  'items': [
        		    'leistung/leistungqskriterien/Einleitung',
        		    'leistung/leistungqskriterien/Qualitätssicherungskriterien',
        		    'leistung/leistungqskriterien/Module zur Leistung',
        		  ]
        		},
        		{
        		  'type': 'category',
        		  'label': 'LeiKa Handbuch (in Arbeit)',
        		  'items': [
        		    {
        		      type: 'category',
        		      label: 'Einleitung',
        		      collapsed: true,
        		      items: [
        		        'leistung/leikahandbuch/Einleitung/GrundsaetzlicheZielsetzung',
        		        'leistung/leikahandbuch/Einleitung/Handlungsgrundlagen',
        		        'leistung/leikahandbuch/Einleitung/ZieledesDokuments',
        		      ],
        		    },
        		    {
        		      type: 'category',
        		      label: 'Anwendungsbeteiligte und Rollen',
        		      collapsed: true,
        		      items: [
        		        'leistung/leikahandbuch/2 Anwendungsbeteiligte und Rollen/2.1 Geschäftsstelle des IT-Planungsrats GS-ITPLR',
        		        'leistung/leikahandbuch/2 Anwendungsbeteiligte und Rollen/2.2 Fachgruppe LeiKa-plus',
        		        'leistung/leikahandbuch/2 Anwendungsbeteiligte und Rollen/2.3 Geschäfts- und Koordinierungsstelle LeiKa GK LeiKa',
        		        'leistung/leikahandbuch/2 Anwendungsbeteiligte und Rollen/2.4 Geschäfts- und Koordinierungsstelle 115',
        		        'leistung/leikahandbuch/2 Anwendungsbeteiligte und Rollen/2.5 Bundesbehörden Soll-Stand',
        		          ],
        		    },
        		    {
        		      type: 'category',
        		      label: 'Definition einer Leistung',
        		      collapsed: true,
        		      items: [
        		        'leistung/leikahandbuch/3 Definition einer Leistung/3',
        		          ],
        		    },
        		    {
        		      type: 'category',
        		      label: 'LeiKa-Struktur',
        		      collapsed: true,
        		      items: [
        		        'leistung/leikahandbuch/4 LeiKa-Struktur/4',
        		          ],
        		    },
        		    {
        		      type: 'category',
        		      label: 'Inhalt des LeiKa, Stammtext- und Ergänzungsmodell',
        		      collapsed: true,
        		      items: [
        		        'leistung/leikahandbuch/5 Inhalt des LeiKa, Stammtext- und Ergänzungsmodell/5',
        		        {
        		          type: 'category',
        		          label: 'Begriffsdefinitionen',
        		          collapsed: true,
        		          items: [
        		            'leistung/leikahandbuch/5 Inhalt des LeiKa, Stammtext- und Ergänzungsmodell/5.1 Begriffsdefinitionen/5.1.1 Leistungsbericht',
        		            'leistung/leikahandbuch/5 Inhalt des LeiKa, Stammtext- und Ergänzungsmodell/5.1 Begriffsdefinitionen/5.1.2 Modul',
        		            'leistung/leikahandbuch/5 Inhalt des LeiKa, Stammtext- und Ergänzungsmodell/5.1 Begriffsdefinitionen/5.1.3 Befüllung von Modulen gemäß Typisierung der Leistungen',
        		              ],
        		        },
        		        'leistung/leikahandbuch/5 Inhalt des LeiKa, Stammtext- und Ergänzungsmodell/5.2 Bereitstellung von Inhalten über den LeiKa',
        		        'leistung/leikahandbuch/5 Inhalt des LeiKa, Stammtext- und Ergänzungsmodell/5.3 Qualitätskriterien',
        		          ],
        		    },
        		    {
        		      type: 'category',
        		      label: 'Pflegeprozesse des LeiKa',
        		      collapsed: true,
        		      items: [
        		        'leistung/leikahandbuch/6 Pflegeprozesse des LeiKa/6.1 Ersterstellung, Änderung, Außerkrafttreten, Monitoring',
        		        'leistung/leikahandbuch/6 Pflegeprozesse des LeiKa/6.2 Freigabe',
        		        'leistung/leikahandbuch/6 Pflegeprozesse des LeiKa/6.3 Benachrichtigung der Nutzer über erfolgte Änderungen',
        		        'leistung/leikahandbuch/6 Pflegeprozesse des LeiKa/6.4 Versionierung',
        		          ],
        		    },
        		    {
        		      type: 'category',
        		      label: 'Technische Bereitstellung und Visualisierung des LeiKa',
        		      collapsed: true,
        		      items: [
        		        'leistung/leikahandbuch/7 Technische Bereitstellung und Visualisierung des LeiKa/7.1 Datenimport',
        		        'leistung/leikahandbuch/7 Technische Bereitstellung und Visualisierung des LeiKa/7.2 Webservice',
        		        'leistung/leikahandbuch/7 Technische Bereitstellung und Visualisierung des LeiKa/7.3 Leistungsviewer',
        		        'leistung/leikahandbuch/7 Technische Bereitstellung und Visualisierung des LeiKa/7.4 LeiKa-plus Portal',
        		          ],
        		    }
        		  ]
        		}, 
			]
		},
        {
        	'type': 'category',
        	'label': 'Baustein Prozesse',
        	'items': [
				'prozess/about-this-doc'
			],
		},
		{
			type: 'category', 
			link: {
				type: 'generated-index',
				description: 'Hier finden Sie einen Überblick der behandelten Themen. Für einen schnelle Einstieg empfehlen wir die Einleitung zu lesen.'
			},
			label: 'Bundesredaktion', items: [
				'zentrale-bundesredaktion/einleitung',
				  {type: 'category', label: 'Aufträge & Bedarfsmeldungen', items: 
					[
					'zentrale-bundesredaktion/bedarfsmeldungen/allgemeines',
					'zentrale-bundesredaktion/bedarfsmeldungen/redaktionsfenster',
					
					]},
				  {type: 'category', label: 'Leistungszuschnitt', items: 
					['zentrale-bundesredaktion/stream_lz/Einleitung',
						{type: 'category', label: 'Redaktionsprozess', items:
							[
							'zentrale-bundesredaktion/stream_lz/redaktionsprozess/Einreichung',
							'zentrale-bundesredaktion/stream_lz/redaktionsprozess/Vorpruefung',
							'zentrale-bundesredaktion/stream_lz/redaktionsprozess/Abstimmung',
							'zentrale-bundesredaktion/stream_lz/redaktionsprozess/Veroeffentlichung',
							]},
						'zentrale-bundesredaktion/stream_lz/Hilfestellungen',

					]},
				
			] 
		  },
        {
        	'type': 'category',
        	'label': 'Querschnittliches',
        	'items': [
				'quer/fim_portal_overview',
				'quer/fim_portal_sources',
				'quer/fim_portal_differences',
			],
		},
        'glossar',
        {
        	'type': 'category',
        	'label': 'FAQ',
        	'items': [
                'faq/allgemeine_fragen_zu_fim',
                'faq/bausteinuebergreifende_fragen',
                'faq/fim_baustein_leistungen',
                'faq/fim_baustein_prozesse',
                'faq/bundesredaktion'
        	]
        } 
    ]
}
