---
slug: /
sidebar_label: Willkommen
---

# Einführung
Herzlich Willkommen in der FIM-Dokumentation. Auf diesen Seiten finden Sie gesammelte Informationen zum föderalen Informationsmanagement - FIM.

Sie finden hier umfangreiche Informationen für verschiedene Zielgruppen in unterschiedlichen Detailtiefe. 

- Wenn Sie noch nie was über "FIM" gehört haben, starten Sie am besten in der Einführung ["Über FIM"](einstieg/fim_einleitung)
- Sie suchen relevante Informationen für Ihre Zielgruppe? Starten Sie mit der [zielgruppenspezifischen Informationssammlung](einstieg/schnelleinstieg)


# Was ist die FIM-Dokumentation?
Diese Dokumentation soll die verschiedenen Zielgruppen dabei unterstützen, FIM für Ihre jeweiligen Zwecke nutzbar zu machen; Redaktionsmitarbeitende finden Hilfen, ihren redaktionellen Aufwand bei der Übersetuzng von Rechtssprache in eine bürgerinnen- und unternehmensfreundliche Sprache zu reduzieren.

:::note 

Die Dokumentation ist noch in Entwicklung.

:::

