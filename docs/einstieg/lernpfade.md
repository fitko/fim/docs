---
sidebar_label: Lernpfade
---

# Lernpfade

Der Zugang kann entlang fünf verschiedener Lernpfade erfolgen, zugeschnitten auf unterschiedliche Wissensbedarfe:

| Lernpfad | Inhalte | Mögliche Zielgruppe |
|----------|---------|---------------------|
|[Einführung](./lernpfade_einfuehrung.md)|Kurzüberblick zu FIM|Für Personen, die eine möglichst kurze Einführung zu FIM interessiert  |
|FIM verstehen |Grundlagen zu FIM: FIM-Baustein Leistungen, FIM-Baustein Prozesse, FIM-Baustein Datenfelder|Für Personen, die nach dem Kurz-überblick vertiefter in FIM und die FIM-Bausteine einsteigen möchten|
|IT & Beratung|Anwendungsgebiete von FIM: FIM und das Zusammenspiel mit Online-Leistungen, Grenzen von FIM|Für Personen, die den Leistungs-schlüssel oder FIM-Stamm-informationen in ihrer Arbeit verwenden (z. B. IT-Dienstleister)|
|Erstellung Stamminformationen|Baustein Prozesse im Detail mit Erstellung von Stammprozessen, Baustein Datenfelder im Detail mit Erstellung von Datenschemata, Baustein Leistungen im Detail mit Erstellung von Stammtexten|Für Personen, die ein allgemeines Verständnis für die Methodik der Stamminformationserstellung wollen|
|Fachlichkeit|Freigabeprozess und fachliche Prüfung von Leistungszuschnitten, Freigabeprozess und fachliche Prüfung von Stammtexten, Freigabeprozess und fachliche Prüfung von Stammprozessen, Freigabeprozess und fachliche Prüfung von Stammdatenschemata|Für Personen, die FIM-Stamminformationen im Rahmen ihrer fachlichen Arbeit freigeben (Fachlichkeit aller Ebenen)|


