# Informationen zu den Metadaten

## Name

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition |FIM-interner Name der Datenfeldgruppe, sichtbar für alle Anwender eines Redaktionssystems des Bausteins Datenfelder (fachliche Ersteller, Methodenexperten, Informationsmanager, FIM-Koordinierungsstelle, FIM-Nutzer).|
| Zielgruppe      | Modellierende |
| Pflichtfeld | Ja |
| Elementspezifisches Qualitätskriterium |<ol><li>Der Name sollte nach Möglichkeit eindeutig sein. Datenfeldgruppen mit gleichem Namen sind zu vermeiden, da dadurch die Suche nach dem passenden Baukastenelementen erschwert wird. Zusätzlich widerspricht dies dem Gedanken der Nachnutzbarkeit von Baukastenelementen.</li><li>Der Name soll zweckorientiert sein, damit die Datenfeldgruppe den Kontext ihrer Unterelemente eindeutig vorgeben kann, z. B. durch Hinzufügen der Fachlichkeit. Falls dies nicht möglich sein sollte, z. B. wenn die Datenfeldgruppe in mehreren Fachkonzepten verwendet wird, kann eine mit/ohne Konstruktion verwendet werden.</li><li>Der Name sollte sprechend sein, so dass sich Datenfeldgruppen mit ähnlichen Themen gut voneinander unterscheiden lassen (-> 3. Beispiel).</li><li>Der Name kann geläufige Abkürzungen enthalten (z. B. HWK für Handwerkskammer).</li></ol>|
| Empfehlung für die maximale Länge |255 Zeichen|
| Art der Prüfung     | Exemplarisch manuell |
| Verweis auf den QS-Bericht      | x |
| Beispiel      |<ol><li>Eine übergreifende Datenfeldgruppe ist die „Natürliche Person (abstrakt, umfassend)“, die daraus abgeleitete spezialisierte Datenfeldgruppe wäre dann z. B. „Antragsteller Hundesteuer“, „Antragsteller Fahrerlaubnis“.</li><li>Eine weitere übergreifende Datenfeldgruppe ist die Gruppe „Adressen (abstrakt)“, diese enthält mehrere Untergruppen mit verschiedenen Varianten der Inlands- oder Auslandsanschrift.</li><li>Es existiert eine Datenfeldgruppe „Angaben zur Bankverbindung“ und eine weitere „Angaben zur Bankverbindung nur IBAN und BIC“.</li></ol>|
|mögliche Abweichungen im Referenzkontext| nein |

## Definition

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Fasst den Inhalt einer Datenfeldgruppe aus redaktioneller Sicht zusammen, d.h. aus Sicht der Rollen Ersteller, Methodenexperte, Informationsmanager, FIM- Koordinierungsstelle, FIM-Nutzer. Eine Definition muss einen Mehrwert bieten, ansonsten kann sie weggelassen werden. Bei harmonisierten Datenfeldgruppen kann eine Definition sinnvoll sein.|
| Zielgruppe      | Modellierende, Fachlichkeit |
| Pflichtfeld | Nein|
| Elementspezifisches Qualitätskriterium |<ol><li>Datenfeldgruppen sollen möglichst (kontext-)spezifisch definiert sein.</li><li>Falls vorhanden, sollen die Definition aus einer Rechtsnorm oder aus einem Standardisierungsverfahren (wie z.B. XÖV) übernommen bzw. als Grundlage genutzt werden. Abgeleitete Definitionen sollen wörtlich übernommen werden. Dies sollte in der Regel nur harmonisierte Datenfeldgruppen betreffen.</li><li>Nichtssagende Floskeln und allgemein gültige Aussagen, die einen Sachverhalt doppelt wiedergeben (Tautologien) sind zu vermeiden.</li><li>Kurze, präzise und möglichst eindeutige Erklärung der Datenfeldgruppe.</li><li>Definition an verwaltungsinterne Nutzer/Formularentwickler richten, entsprechende Fachsprache verwenden.</li><li>Bei komplexen Datenfeldgruppen (d.h. eine Verschachtelung mehrerer Datenfelder und -gruppen) ist auf eine zweckorientierte Definition abzuzielen, anstatt den Inhalt wiederzugeben.</li></ol>|
| Empfehlung für die maximale Länge |5000 Zeichen|
| Art der Prüfung     | exemplarisch manuell |
| Verweis auf den QS-Bericht      | x |
| Beispiel      |Datenfeldgruppe „Organisationseinheit“: Die Organisationseinheit fasst Angaben zur Darstellung der internen hierarchischen Struktur einer Organisation zusammen, z.B. zur Darstellung von Abteilungen oder Referaten. |
|mögliche Abweichungen im Referenzkontext| nein |

## Bezug zur Handlungsgrundlage

:::info
Die Namensgebung zu diesem Metadatum hat sich geändert, in XDatenfelder 2 hieß dieses Metadatum noch **Bezug zu Rechtsnorm oder Standardisierungsvorhaben**.
:::

### Bezug zur Handlungsgrundlage

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Dieses Attribut ermöglicht es, Bezüge zu einschlägigen Rechtsnormen, Verordnungen oder Richtlinien zu dokumentieren. Auch ein Bezug zu XÖV-Vorhaben (oder ähnlichem) und den entsprechenden Datencontainern kann hier gesetzt werden, sowie die Nutzung dieses Standards rechtlich nachweisbar ist. Aus dem Bezug zur Rechtsnorm ergibt sich die Gestaltung der Datenfeldgruppe.|
| Zielgruppe      | Modellierende, Fachlichkeit |
| Pflichtfeld | **Ja** / Nein|
| Elementspezifisches Qualitätskriterium |<ol><li>Bei Datenfeldgruppen, deren Strukturelementart **rechtsnormgebunden** ist, ist dies ein Pflichtfeld, bei **harmonisierten** Datenfeldgruppen ist es keines.</li><li>Falls die Bezugsquelle der Rechtsnorm oder des Standardisierungsvorhabens eine eigene Notation vorsieht, dann ist diese zu befolgen.</li><li>Bei Angaben zu einem Bezug sollen diese möglichst konkret sein.</li><li>Die [Regeln zur Erfassung von Handlungsgrundlagen](EA_Handlungsgrundlagen) sind zu beachten.</li><li>Rechtsnormen sind insbesondere dann anzugeben, wenn diese die rechtliche Grundlage für die Erstellung und die Definition der Datenfeldgruppe darstellen.</li><li>Eine Gruppe muss alle Rechtsbezüge ihrer fachspezifischen Unterelemente listen.</li><li>Der Rechtsbezug sollte nach Möglichkeit bei harmonisierten Datenfeldgruppen mit einer Version versehen werden, damit man einen sicheren (zeitlichen) Bezugspunkt für die Gültigkeit der Rechtsgrundlage hat.</li><li>Bei mehreren Handlungsgrundlagen an einer Datenfeldgruppe, sollte man diese mit Strichpunkt von einander trennen.</li></ol>|
| Empfehlung für die maximale Länge |255 Zeichen pro Bezug; ein Element kann davon mehrere haben.|
| Art der Prüfung     | Exemplarisch manuell |
| Verweis auf den QS-Bericht      | x |
| Beispiel      |<ol><li>Datenfeldgruppe „Sterbedatum (teilbekannt)“: XPersonenstand.portal2StA.Sterbefall.084030.sterbedatum Version 1.7.5</li><li>Datenfeldgruppe „Angaben zum Unternehmen“: XUnternehmen.Kerndatenmodell Version 1.0</li><li>Datenfeldgruppe „Signatur“: § 126 BGB; § 126a BGB; § 3a (2) VwVfG</li></ol>|
|mögliche Abweichungen im Referenzkontext| nein |

### Link :new:

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Dieses Attribut ermöglicht es, zu jedem Bezug einen entsprechenden Link anzugeben.|
| Zielgruppe      | Modellierende, Fachlichkeit |
| Pflichtfeld | Nein|
| Elementspezifisches Qualitätskriterium |<ol><li>Es ist immer ein gültiger Link zu erfassen.</li><li>Eine gute Quelle für Links zu Rechtsgrundlagen finden sich unter: [Gesetze im Internet](https://www.gesetze-im-internet.de/)</li><li>Eine gute Quelle für Links zu XÖV-Standards findet sich unter: [XRepository 3.0](https://www.xrepository.de/)</li><li>Es sind keine unpassenden Links zu ergänzen.</li></ol>|
| Empfehlung für die maximale Länge | 255 Zeichen |
| Art der Prüfung     | Exemplarisch manuell |
| Verweis auf den QS-Bericht      |  |
| Beispiel      | GRUPPEN BEISPIELE ERGÄNZEN <ol><li>Datenfeld “meldepflichtige Krankheiten” basierend auf § 6 IfSG - Link: § 6 IfSG - Einzelnorm </li><li>Datenfeld “Ort der wirtschaftlichen Tätigkeit” Xunternehmen.Kerndatenmodell.Ort der wirtschaftlichen Tätigkeit.Art Version 1.0; basierend auf Codeliste urn:xoev-de:xunternehmen:codeliste:artortwirtschaftlichetaetigkeit_1 Version 1 Link: XRepository 3.0</li></ol> |
|mögliche Abweichungen im Referenzkontext| nein |

## Strukturelementart

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Mit Hilfe einer Klassifizierung nach Arten soll es möglich sein, zwischen harmonisierten und nicht harmonisierten Elementen bzw. nach geeigneten und ungeeigneten Elementen für die Zusammensetzung von Referenz- und Stammdatenschemata zu unterscheiden.|
| Zielgruppe      | Modellierende |
| Pflichtfeld | Ja, ein geeigneter Defaultwert ist: "rechtsnormgebunden"|
| Elementspezifisches Qualitätskriterium |<ol><li>Es ist immer eine der vordefinierten Strukturelementarten auszuwählen.</li><li>Es existieren drei Arten von Gruppen: **abstrakt** (möglichst umfassend und fachübergreifend), **harmonisiert** (fachübergreifend oder fachspezifisch) oder **rechtsnormgebunden** (nicht harmonisierbar).</li><li>Rechtsnormgebundene Elemente können im Nachhinein noch harmonisiert werden. Aus hierarchischer Sicht sind rechtsnormgebundene Gruppen den harmonisierten Gruppen untergeordnet, welche wiederum den abstrakten Gruppen untergeordnet sind. Dabei ist die Ableitung und somit die Herstellung einer hierarchischen Relation nicht zwingend.</li></ol>|
| Empfehlung für die maximale Länge |nicht relevant|
| Art der Prüfung     | keine |
| Verweis auf den QS-Bericht      | [1031](./QS_methodische.md#1031-unbekannte-strukturelementart) |
| Beispiel      |siehe Tabelle |
|mögliche Abweichungen im Referenzkontext| nein |

## Gruppenart :new:

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Über die Gruppenart wird angegeben, ob es sich bei der vorliegenden Gruppe um eine normale Datenfeldgruppe oder um eine Auswahlgruppe handelt.|
| Zielgruppe      | Modellierende |
| Pflichtfeld | Ja, als Defaultwert wird davon ausgegangen, dass es sich um eine normale Datenfeldgruppe handelt.|
| Elementspezifisches Qualitätskriterium |<ol><li>Es ist immer einer der vordefinierten Werte auszuwählen.</li><li>Es gibt nur zwei Optionen:<ol><li>normale Datenfeldgruppe</li><li>Auswahlgruppe</li></ol></li><li>Eine Gruppe muss nicht als Auswahlgruppe modelliert werden, selbst wenn sie sich dafür eignet, kann es Sinn machen die Gruppe wie in XDatenfelder 2 zu modellieren.</li></ol>|
| Empfehlung für die maximale Länge |nicht relevant|
| Art der Prüfung     | Wird nicht geprüft. |
| Verweis auf den QS-Bericht      | [1042](./QS_methodischeII.md#1042-unbekannte-gruppenart) |
| Beispiel      |<ol><li>Die Gruppe “Anschrift Inland oder Ausland” mit den Untergruppen “Anschrift Inland” und “Anschrift Ausland” kann als Auswahlgruppe modelliert werden.</li><li>Die Gruppe “Anschrift Inland” mit den Untergruppen “Anschrift Inland Straßenanschrift” und “Anschrift Inland Postfachanschrift” kann als Auswahlgruppe modelliert werden.</li></ol>|
|mögliche Abweichungen im Referenzkontext| nein |

## Beschreibung

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Zusätzliche Beschreibung und Erläuterungen einer Datenfeldgruppe für FIM- Nutzer.|
| Zielgruppe| Modellierende, Dienstleister nachnutzender Systeme |
| Pflichtfeld | Nein|
| Elementspezifisches Qualitätskriterium |Über die Beschreibung können Umsetzungshinweise zu einer Datenfeldgruppe ergänzt werden. Außerdem können hier Hinweise an andere Modellierende hinterlegt werden. <br/>Vorstellbar sind Hinweise zu: <ul><li>Codelisten und Quellen</li><li>Möglichkeit der Abkürzung</li><li>Zeichenbeschränkung</li><li>Notwendigkeit von verschiedenen Zeichensätzen</li><li>etc.</li></ul>|
| Empfehlung für die maximale Länge |5000 Zeichen|
| Art der Prüfung     | Wird nicht geprüft. |
| Verweis auf den QS-Bericht      | x |
| Beispiel      |<ol><li>„Die folgenden Informationen sind aufgrund §12 (1) GastG bereitzustellen.“</li><li>„Falls die Informationen zur Scheidung, direkt aus den Meldedaten abgerufen werden können, sollte das Feld "Nachweis" nicht eingefordert werden und das Feld "Datum (seit wann)" nicht befüllt werden.“</li><li>„In dieser Datenfeldgruppe wird per Regel gefordert, dass mindestens eine Kommunikationsmöglichkeit angegeben werden muss. Falls es gewünscht ist, dass keine Kommunikationsmöglichkeit angegeben werden braucht, muss die ganze Feldgruppe bei der Verwendung auf optional gesetzt werden.“</li><li>„Wenn technisch möglich sollten hier nur Haushaltsmitglieder referenziert werden können, die unter Feldgruppe „Abschnitt Persönliche Angaben (Haushaltsmitglied Wohngeld)„ eingegeben wurden.“</li></ol>|
|mögliche Abweichungen im Referenzkontext| nein |

## Bezeichnung Eingabe

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Bezeichnung der Datenfeldgruppe, welche für den Bürger/das Unternehmen bei der Eingabe eines Formulars sichtbar ist; sie ist wie eine Art Überschrift zu betrachten.|
| Zielgruppe      | Modellierende, Fachlichkeit, Dienstleister nachnutzender Systeme, Antragstellende |
| Pflichtfeld | Ja|
| Elementspezifisches Qualitätskriterium |<ol><li>Die Bezeichnung soll kurz und prägnant sein.</li><li>Die Bezeichnung soll keine expliziten Bezüge zum Kontext herstellen, wenn dies nicht zwingend notwendig ist.</li><li>Die Bezeichnung soll nicht als Frage formuliert sein.</li></ol>|
| Empfehlung für die maximale Länge |255 Zeichen|
| Art der Prüfung     | Komplett manuell |
| Verweis auf den QS-Bericht | [1009](./QS_methodische.md#1009-die-bezeichnung-eingabe-muss-befüllt-werden) |
| Beispiel      |<ol><li>Positiv: Angaben zur antragstellenden Person</li><li>Negativ: Angaben zur antragstellenden Person (weitere Wohnungen)</li></ol> |
|mögliche Abweichungen im Referenzkontext| nein |

## Bezeichnung Ausgabe

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Bezeichnung der Datenfeldgruppe, welche für den Bürger/das Unternehmen bei der Ausgabe sichtbar ist; sie ist wie eine Art Überschrift zu betrachten.|
| Zielgruppe      | Modellierende, Fachlichkeit, Dienstleister nachnutzender Systeme, Antragstellende |
| Pflichtfeld | Ja|
| Elementspezifisches Qualitätskriterium |<ol><li>Ist in der Regel identisch mit der Bezeichnung Eingabe.</li><li>Die Bezeichnung soll kurz und prägnant sein.</li><li>Die Bezeichnung soll keine expliziten Bezüge zum Kontext herstellen, wenn dies nicht zwingend notwendig ist.</li><li>Die Bezeichnung soll nicht als Frage formuliert sein.</li></ol>|
| Empfehlung für die maximale Länge |255 Zeichen|
| Art der Prüfung     | Komplett manuell |
| Verweis auf den QS-Bericht|[1010](./QS_methodische.md#1010-die-bezeichnung-ausgabe-muss-befüllt-werden)|
| Beispiel      |<ol><li>Positiv: Angaben zur antragstellenden Person</li><li>Negativ: Angaben zur antragstellenden Person (weitere Wohnungen)</li></ol> |
|mögliche Abweichungen im Referenzkontext| nein |

## Hilfetext Eingabe

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Erläuternder Hilfetext für den Formularanwender eines Auslöser-Formulars, um zu verdeutlichen, welche Inhalte in einem Datenfeld einzugeben sind bzw. welche Aktion vorzunehmen ist.|
| Zielgruppe | Modellierende, Fachlichkeit, Dienstleister nachnutzender Systeme, Antragstellende |
| Pflichtfeld | Nein|
| Elementspezifisches Qualitätskriterium |<ol><li>Beschreibung der Bedeutung des Feldes für den Endnutzer auf Inputformularen (Bürger/Unternehmen).</li><li>Hilfetext sollte Ausfüllhilfe für das Feld/die Feldgruppe geben, aktive Ansprache des Bürgers („Geben Sie … an“)</li><li>Es sollten sinnvolle Beispiele für die Befüllung des Feldes und Synonyme gegeben werden.</li><li>Fremdwörter sollten vermieden werden</li></ol>|
| Empfehlung für die maximale Länge |500 Zeichen|
| Art der Prüfung     | Komplett manuell |
| Verweis auf den QS-Bericht      | |
| Beispiel      |<ol><li>Datenfeldgruppe „Organisationseinheit“: Geben Sie Informationen zur internen hierarchischen Struktur der Organisation an.</li><li>Datenfeldgruppe „Unterhaltszahlungen der letzten vier Monate“: Geben Sie Datum und die Höhe der letzten vier Unterhaltszahlungen an.</li><li>Datenfeldgruppe „Geburtsdatum (teilbekannt)“: Geben Sie das Geburtsdatum so an, wie auf den Ausweisdokumenten hinterlegt. Nur falls der Tag oder der Monat der Geburt unbekannt sind, dürfen diese weggelassen werden.</li><li>Datenfeldgruppe „Vorauszahlung Unterhalt“: Geben Sie das Datum und die Höhe der Vorauszahlung an und für welchen Zeitraum die Vorauszahlung gedacht ist.</li></ol>|
|mögliche Abweichungen im Referenzkontext| nein |

## Hilfetext Ausgabe

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Erläuternder Hilfetext für den Formularanwender (Bürger/Unternehmen) eines Ergebnis-Formulars.|
| Zielgruppe |Modellierende, Fachlichkeit, Dienstleister nachnutzender Systeme, Antragstellende  |
| Pflichtfeld | Nein|
| Elementspezifisches Qualitätskriterium |<ol><li>Beschreibung der Bedeutung des Feldes für den Endnutzer auf Output- Formularen (Bürger/Unternehmen).</li><li>Hilfetext sollte Erläuterung zu dem Inhalt des Felds geben („Dieses Feld enthält …“)</li><li>Ergibt sich aus Anforderungen an die Barrierefreiheit.</li></ol>|
| Empfehlung für die maximale Länge |500 Zeichen|
| Art der Prüfung     | Komplett manuell |
| Verweis auf den QS-Bericht      | |
| Beispiel      |<ol><li>Positiv: Datenfeldgruppe „Geburtsdatum (teilbekannt)“: Falls der Tag oder der Monat der Geburt unbekannt sind, können diese leer sein.</li><li>Negativ: Diese Gruppe enthält…</li><li>Für den Bürger/ für ein Unternehmen ist das Konzept einer Gruppe bzw. einer Datenfeldgruppe nicht unbedingt ersichtlich.</li></ol> |
|mögliche Abweichungen im Referenzkontext| nein |

## Strukturelement :sparkles:

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Die von einer Datenfeldgruppe verwendeten Unterelemente (Datenfelder und Datenfeldgruppen) bilden die Struktur der Gruppe und werden damit als Strukturelemente der Gruppe bezeichnet.|
| Zielgruppe      | Modellierende |
| Pflichtfeld | **Ja**, eine Datenfeldgruppe muss Unterelemente enthalten, um konform zum XDatenfelder 3 Standard zu sein.|
| Elementspezifisches Qualitätskriterium |<ul><li>Eine Datenfeldgruppe muss mehr als ein Unterelement enthalten.</li><li>Eine Gruppe kann mehrmals das gleiche Element enthalten, dies muss aber durch die Multiplizität gekennzeichnet werden.</li></ul>|
| Empfehlung für die maximale Länge |nicht relevant|
| Art der Prüfung     | teilweise manuell |
| Verweis auf den QS-Bericht      | x |
| Beispiel      |Die harmonisierte Datenfeldgruppe G60 000 000 091 *Anschrift Ausland* besteht aus den Strukturelementen F60 000 000 261 *Staat*, mit der Multiplizität 1:1 und der Gruppe G60 000 000 092 *Anschriftzone*, mit der Multiplizität 1:1. |
|mögliche Abweichungen im Referenzkontext| nein |

## Bezug aus Struktur :star: ACHTUNG FEHLERHAFT

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Bezeichnet die Gesamtheit an Informationen, die beschreiben wie oft und warum ein Unterelement einer Datenfeldgruppe (Datenfeldgruppen oder Datenfelder) an einer bestimmten Stelle in der Struktur der Gruppe eingefügt wurde. Die hierbei relevanten Informationen sind die Multiplizität und der Rechtsbezug. <ul><li>Die Multiplizität steuert (siehe oben) das Vorkommen des Unterelements in einer Gruppe.</li><li>Der Bezug zur Handlungsgrundlage gibt an, aus welchem Grund ein harmonisiertes Unterelement oder ein Unterelement aus einem fremden Rechtsgebiet an der gewählten Position innerhalb der Gruppe eingefügt wurde.</li></ul>|
| Zielgruppe      | Modellierende, Fachlichkeit |
| Pflichtfeld | Ja, für harmonisierte Unterelemente. / Nein, für rechtsnormgebundene Unterelemente.|
| Elementspezifisches Qualitätskriterium |<ol><li>Die Angaben zur Multiplizität ergeben sich aus dem Rechtskontext der Datenfeldgruppe.</li><li>Die Angabe zu Rechtsnorm oder Standardisierungsvorhaben muss erfolgen, wenn ein Element in eine Struktur eingefügt wird, die auf einer anderen Handlungsgrundlage basiert als das Element selbst.</li><li>Die Angabe ist dann nicht erforderlich, wenn die Handlungsgrundlage aus dem gleichen Kontext stammt wie das eingefügte Element selbst.</li><li>Falls die Bezugsquelle der Rechtsnorm oder des Standardisierungsvorhabens eine eigene Notation vorsieht, dann ist diese zu befolgen.</li><li>Bei Angaben zu einem Bezug sollen diese möglichst konkret sein.</li><li>Die Regeln zur Erfassung von Rechtsbezügen sind zu beachten.</li></ol>|
| Empfehlung für die maximale Länge |255 Zeichen pro Bezug; ein Element kann davon mehrere haben.|
| Art der Prüfung     | automatisiert |
| Verweis auf den QS-Bericht      |  |
| Beispiel |<ol><li>Datenfeldgruppe „Früherer Bezug von Unterhaltsvorschuss“: die Gruppe besteht aus drei Feldern (einer Abfrage zum früheren Bezug von Unterhaltsleistungen, der Bezeichnung der Behörde/ des Jugendamtes und einer Angabe zum letzten Bezug der Leistung), bei den letzten zwei Feldern handelt es sich um harmonisierte Felder, die auch in einem anderen Kontext genutzt werden könnten. An dieser Stelle in der Datenfeldgruppe können sie gemäß RL Nr. 9.7.4. UhVorschG; RL Nr. 9.7.5. UhVorschG; RL Nr. 9.8. UhVorschG genutzt werden.</li><li>Datenfeldgruppe „Angaben zum Arbeitgeber (Asbest)“ aus dem Stammdatenschema „Anzeige Tätigkeiten mit Asbest“: es wird u. a. der Name des Unternehmens abgefragt, bezugnehmend auf § 23 (1) ArbSchG.</li></ol> |
|mögliche Abweichungen im Referenzkontext| nein |

## Verweis auf Regeln

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Die von einer Datenfeldgruppe verwendeten Regeln sind direkt an einer Datenfeldgruppe verortet, d. h. die Datenfeldgruppe referenziert die IDs dieser Regeln.|
| Zielgruppe      |Modellierende, Fachlichkeit|
| Pflichtfeld | Nein|
| Elementspezifisches Qualitätskriterium |<ol><li>Es dürfen nur gültige Regel Identifikatoren referenziert werden.</li><li>Es können mehrere Regeln referenziert werden.</li></ol>|
| Empfehlung für die maximale Länge |nicht relevant|
| Art der Prüfung     | Wird nicht geprüft. |
| Verweis auf den QS-Bericht      | x |
| Beispiel      |R00 000 001 490 |
|mögliche Abweichungen im Referenzkontext| nein |
