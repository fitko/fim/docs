# Installation

1. First clone the project with:
    ```bash
    git clone git@gitlab.opencode.de:fitko/fim/docs.git
    ```
2. After that install the dependencies with:
    ```bash
    yarn install
    ```

3. To run the documentation on your local machine, you need first to build it:
    ```bash 
    yarn build
    ```
4. After that you can serve it locally with:
    ```bash
    yarn serve
    ```
   or with:
    ```bash
    npm run serve
    ```
