# Lesehilfe

## Darstellung der Metadaten

Um die Metadaten übersichtlich zu listen, werden diese textuell unterschiedlich hervorgehoben, abhängig davon, ob es sich um Pflichtattribute, optionale Attribute oder Attribute handelt, die von einem Redaktionssystem automatisiert gepflegt werden sollten.

### Pflichtattribute und optionale Attribute

- Pflichtattribute werden **fett** hervorgehoben, optionale Attribute werden *kursiv* hervorgehoben.

- Informationen bzgl. der Identifikatoren werden zusätzlich mit einem Stern* versehen.

- <u>Unterstrichene Attribute</u> sollten von einem Redaktionssystem nach Möglichkeit automatisiert gepflegt werden.
