# Datenfelder (Prototyp 2)

import Tabs from '@theme/Tabs';

import TabItem from '@theme/TabItem';

:::info

<Tabs>
  <TabItem value="kurz" label="Kurz und Knapp"> Ein Datenfeld ist die kleinste singuläre Einheit im FIM-Baustein Datenfelder. Es wird definiert durch seine entsprechenden Metaangabe, wie z. B. Feldart, Datentyp, Bezeichnung Eingabe, ... . Datenfelder können Bestandteil von Datenfeldgruppen und Datenschemata sein. </TabItem>
  <TabItem value="pfad" label="Datenfelder technisch verstehen"> Sie sind hier richtig, wenn Sie das Thema aus einer technischen Perspektive verstehen wollen. Zu diesem Thema gibt es einen <b>Lernpfad</b> "Datenfelder technisch verstehen". Dies ist Teil 2 von 30 des entsprechenden Lernpfades; es geht hier weiter zu [Teil 3](LINK), zurück zu [Teil 1](LINK) </TabItem>
   <TabItem value="pfad2" label="Datenfelder methodisch verstehen"> Wenn Sie sich eher für die Methodik aus Sicht eines Modellierenden interessieren, schauen sich auch unseren <b>Lernpfad</b> "Datenfelder methodisch verstehen"(LINK) an. </TabItem>
</Tabs>

:::

## Grundlegendes über Datenfelder

Das Datenfeld bildet die kleinste Einheit des FIM-Baukastens aus Sicht des Bausteins „Datenfelder“. Es besteht in der Regel aus einer Bezeichnung und der Möglichkeit zur Eingabe von Informationen (z. B. in Form eines Freitexts, einer Liste oder einer Checkbox). Datenfelder, die keine Möglichkeit zur Eingabe von Informationen (z. B. reine Informationstexte) bieten , werden ebenfalls als Datenfelder im Baukasten geführt, verfügen jedoch über einen entsprechenden Datentyp.

Zu jedem Datenfeld im Baukasten werden Metadaten erfasst und gepflegt, die Informationen zu dessen Inhalt sowie strukturellen, redaktionellen und technischen Eigenschaften bereitstellen. Die Metadaten dienen im Rahmen der Befüllung und Nutzung des FIM-Baukastens zur besseren Strukturierung und Klassifizierung von Datenfeldern, zur strukturierten Ablage, zur besseren Auffindbarkeit im FIM-Baukasten sowie zur Einbindung der Felder in den FIM-Gesamtkontext. Auch im späteren Verlauf der FIM-Nutzung durch FMS-Anbieter – bzw. allgemeiner durch die FIM-Nutzer – können diese Metadaten genutzt werden. Ein Formularmanagementsystem wird dabei lediglich eine Untermenge der gelisteten Metadaten benötigen. Die hier definierte Menge an Metadaten sollte beständig sein und nur in Ausnahmefällen geändert werden. Falls dennoch eine Änderung durchgeführt werden muss, so stellt dies eine Anpassung der FIM-Logik dar, welche den dafür vorgesehenen Redaktionsprozess zu durchlaufen hat.

## Metadaten

Im Folgenden finden Sie eine Auflistung der Metadaten für Datenfelder inklusive Erläuterungen. Die Metadaten lehnen sich an verschiedene Standards für Metadaten, insbesondere aus dem Bereich der öffentlichen Verwaltung wie der deutschen Open Government Data-Metadaten-Struktur, an.

### Identifikationsangaben

- [**Identifizierungsangabe (ID)**\* (1)](/docs/datenfelder/Fachkonzept/metadaten.md)
- [**Versionsangabe*** (1 für versionierte Datenfelder, 0 sonst)](/docs/datenfelder/Fachkonzept/metadaten.md)

### Definitionsangaben

- [**Name**](/docs/datenfelder/Fachkonzept/metadaten.md)
- [*Definition* (0…1)](/docs/datenfelder/Fachkonzept/metadaten.md)
- [**Bezug zur Handlungsgrundlage** (0…n)](/docs/datenfelder/Fachkonzept/metadaten.md)
- [*Beschreibung* (0…1)](/docs/datenfelder/Fachkonzept/metadaten.md)
- *Stichwörter (Tags)* (0…n)
- *Relation* (0…n)
- *Versionshinweis* (0…1)
- *Regel* (0…n)
- **Strukturelementart** (1)
- **Feldart** (1)
- **Datentyp** (1)
- *Präzisierung* (0…1)
- *max Size* (0…1)
- *media Type* (0…*)
- *Inhalt* (0…1)
- *Verweis auf Codeliste* (0…1)
- *code Key* (0…1)
- *name Key* (0…1)
- *help Key* (0…1)
- *Werte* (0…1)
- **Vorbefüllung** (1)

### Angaben für Formular-Nutzer

...

### Zustandsangaben

...


## Repräsentation im Standard XDatenfelder 3

### Feldart Eingabe - Datentyp Text

~~~
<xdf3:datenfeld>
    <xdf3:identifikation>
        <xdf3:id>F82000000015</xdf3:id>
    </xdf3:identifikation>
    <xdf3:name>Standard Datenfeld für Texteingaben</xdf3:name>
    <xdf3:bezug link="">§ 123 AbcG</xdf3:bezug>
    <xdf3:freigabestatus listURI="urn:xoev-de:xprozess:codeliste:status" listVersionID="2020-03-19">
        <code>2</code>
    </xdf3:freigabestatus>
    <xdf3:letzteAenderung>2023-05-10T13:51:41+02:00</xdf3:letzteAenderung>
    <xdf3:bezeichnungEingabe>Hier steht eine passende Eingabeaufforderung</xdf3:bezeichnungEingabe>
    <xdf3:bezeichnungAusgabe>Passender Ausgabetext zur Eingabe</xdf3:bezeichnungAusgabe>
    <xdf3:schemaelementart listURI="urn:xoev-de:fim:codeliste:xdatenfelder.schemaelementart" listVersionID="1.0">
        <code>RNG</code>
    </xdf3:schemaelementart>
    <xdf3:hilfetextEingabe>Erläuternde Hilfestellungen können wertvolle Hinweise liefern.</xdf3:hilfetextEingabe>
    <xdf3:feldart listURI="urn:xoev-de:fim:codeliste:xdatenfelder.feldart" listVersionID="2.0">
        <code>input</code>
    </xdf3:feldart>
    <xdf3:datentyp listURI="urn:xoev-de:fim:codeliste:xdatenfelder.datentyp" listVersionID="2.0">
        <code>text</code>
    </xdf3:datentyp>
    <xdf3:praezisierung pattern=""/>
    <xdf3:vorbefuellung listURI="urn:xoev-de:fim:codeliste:xdatenfelder.vorbefuellung" listVersionID="1.0">
        <code>keine</code>
    </xdf3:vorbefuellung>
</xdf3:datenfeld>
~~~

### Feldart Eingabe - Datentyp Wahrheitswert

~~~
<xdf3:datenfeld>
    <xdf3:identifikation>
        <xdf3:id>F82000000016</xdf3:id>
    </xdf3:identifikation>
    <xdf3:name>Standard Datenfeld für Wahrheitswertabfrage</xdf3:name>
    ...
    <xdf3:bezeichnungEingabe>Stimmen Sie der Aussage zu?</xdf3:bezeichnungEingabe>
    ...
    <xdf3:feldart listURI="urn:xoev-de:fim:codeliste:xdatenfelder.feldart" listVersionID="2.0">
        <code>input</code>
    </xdf3:feldart>
    <xdf3:datentyp listURI="urn:xoev-de:fim:codeliste:xdatenfelder.datentyp" listVersionID="2.0">
        <code>bool</code>
    </xdf3:datentyp>
</xdf3:datenfeld>
~~~

### Feldart Statisch - Datentyp Text

~~~
<xdf3:datenfeld>
    <xdf3:identifikation>
        <xdf3:id>F82000000017</xdf3:id>
    </xdf3:identifikation>
    <xdf3:name>Datenfeld für statische Texte</xdf3:name>
    ...
    <xdf3:bezeichnungEingabe>Hat einen Überschriftscharakter</xdf3:bezeichnungEingabe>
    ...
    <xdf3:feldart listURI="urn:xoev-de:fim:codeliste:xdatenfelder.feldart" listVersionID="2.0">
        <code>label</code>
    </xdf3:feldart>
    <xdf3:datentyp listURI="urn:xoev-de:fim:codeliste:xdatenfelder.datentyp" listVersionID="2.0">
        <code>text</code>
    </xdf3:datentyp>
    <xdf3:inhalt>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy
    eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
    At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren,
    no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet,
    consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore
    magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et
    ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit
    amet.</xdf3:inhalt>
    ....
 </xdf3:datenfeld>
~~~

### Feldart Eingabe - Datentyp Anlage (mit Link zur Handlungsgrundlage)

~~~
<xdf3:datenfeld>
    <xdf3:identifikation>
        <xdf3:id>F05000000003</xdf3:id>
    </xdf3:identifikation>
    <xdf3:name>Anlagefeld</xdf3:name>
    <xdf3:bezug link="https://de.wikipedia.org/wiki/Portable_Document_Format">§ 12 BspG</xdf3:bezug>
    <xdf3:freigabestatus listURI="urn:xoev-de:xprozess:codeliste:status" listVersionID="2020-03-19">
        <code>2</code>
    </xdf3:freigabestatus>
    <xdf3:statusGesetztDurch>FIM Baustein Datenfelder</xdf3:statusGesetztDurch>
    <xdf3:letzteAenderung>2024-04-16T11:05:20+00:00</xdf3:letzteAenderung>
    <xdf3:bezeichnungEingabe>PDF Anlage</xdf3:bezeichnungEingabe>
    <xdf3:bezeichnungAusgabe>PDF Anlage</xdf3:bezeichnungAusgabe>
    <xdf3:schemaelementart listURI="urn:xoev-de:fim:codeliste:xdatenfelder.schemaelementart" listVersionID="1.0">
        <code>RNG</code>
    </xdf3:schemaelementart>
    <xdf3:feldart listURI="urn:xoev-de:fim:codeliste:xdatenfelder.feldart" listVersionID="2.0">
        <code>input</code>
    </xdf3:feldart>
    <xdf3:datentyp listURI="urn:xoev-de:fim:codeliste:xdatenfelder.datentyp" listVersionID="2.0">
        <code>file</code>
    </xdf3:datentyp>
    <xdf3:vorbefuellung listURI="urn:xoev-de:fim:codeliste:xdatenfelder.vorbefuellung" listVersionID="1.0">
        <code>keine</code>
    </xdf3:vorbefuellung>
    <xdf3:maxSize>1500000</xdf3:maxSize>
    <xdf3:mediaType>application/pdf</xdf3:mediaType>
</xdf3:datenfeld>
~~~

### Präzisierung eines Datenfelds (mit optionaler Vorbefüllung)

~~~
<xdf3:datenfeld>
    <xdf3:identifikation>
        <xdf3:id>F05000000004</xdf3:id>
    </xdf3:identifikation>
    <xdf3:name>Ganzzahl</xdf3:name>
    ...
    <xdf3:feldart listURI="urn:xoev-de:fim:codeliste:xdatenfelder.feldart" listVersionID="2.0">
        <code>input</code>
    </xdf3:feldart>
    <xdf3:datentyp listURI="urn:xoev-de:fim:codeliste:xdatenfelder.datentyp" listVersionID="2.0">
        <code>num_int</code>
    </xdf3:datentyp>
    <xdf3:praezisierung maxValue="99" minValue="1" pattern=""/>
    <xdf3:inhalt>55</xdf3:inhalt>
    <xdf3:vorbefuellung listURI="urn:xoev-de:fim:codeliste:xdatenfelder.vorbefuellung" listVersionID="1.0">
        <code>optional</code>
    </xdf3:vorbefuellung>
</xdf3:datenfeld>
~~~

## Umgang mit Code- und Wertelisten

Der Umgang mit Code- und Wertelisten hat sich im Vergleich zum Standard XDatenfelder 2 geändert.
...
Hinweis darauf, dass auf die Nutzungstypen des Codelistenhandbuchs der KoSIT verwiesen wird.
Aber, dass hier bei Nutzungstyp 3 klar definiert ist, dass immer die aktuellste Version einer Codeliste gemeint ist, wenn keine Version zusätzlich zur Kennung angegeben wird.

### Werteliste

~~~
<xdf3:werte>
	<xdf3:wert>
		<xdf3:code>001</xdf3:code>
		<xdf3:name>Arbeitslosengeld II („Hartz 4", SGB II)</xdf3:name>
	</xdf3:wert>
	<xdf3:wert>
		<xdf3:code>002</xdf3:code>
		<xdf3:name>Grundsicherung im Alter/bei Erwerbsminderung oder Hilfe zum Lebensunterhalt ("Sozialhilfe", SGB XII)</xdf3:name>
	</xdf3:wert>
	<xdf3:wert>
		<xdf3:code>003</xdf3:code>
		<xdf3:name>Leistungen der Kinder- und Jugendhilfe (SGB VIII)</xdf3:name>
	</xdf3:wert>
…
</xdf3:werte>
~~~

### Codeliste

~~~
<xdf3:codelisteReferenz>
	<xdf3:canonicalIdentification>urn:de:bund:destatis:bevoelkerungsstatistik:schluessel:staatsangehoerigkeit</xdf3:canonicalIdentification>
	<xdf3:version>2021-02-19</xdf3:version>
	<xdf3:canonicalVersionUri>urn:de:bund:destatis:bevoelkerungsstatistik:schluessel:staatsangehoerigkeit_2021-02-19</xdf3:canonicalVersionUri>
</xdf3:codelisteReferenz>
~~~

## Weiterlesen

:::tip

Wollen Sie Ihr Wissen vertiefen? Dann können wir Ihnen folgende Seiten empfehlen:

- [Metamodell Datenfelder](/docs/datenfelder/Fachkonzept/Metamodell.md)
- [Datenfelder aus methodischer Sicht](/docs/datenfelder/DF_Allgemeines.md)
- [Datenfeldgruppen](/docs/datenfelder/DFG_Allgemeines.md)

:::