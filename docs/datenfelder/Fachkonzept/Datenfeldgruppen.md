# Datenfeldgruppen

## Grundlegendes über Datenfeldgruppen

Eine Datenfeldgruppe besteht aus einer Sammlung von Datenfeldern und/oder Datenfeldgruppen und
weiteren datenfeldgruppenspezifischen Informationen. Datenfeldgruppen können selbst Teil von
anderen Datenfeldgruppen sein. Dies ermöglicht eine maximale Flexibilität bei der modularen
Zusammenstellung von Stammdatenschemata in der Datenfeldbibliothek.
Bei der Konstruktion einer Datenfeldgruppe werden die verwendeten Felder und Feldgruppen nicht
verändert, sondern nur referenziert, d. h. identisch mit allen Metadaten, Regeln und ggf. beinhalteten
Unterelementen wiederverwendet. Sollen Felder oder Feldgruppen in veränderter Form genutzt
werden, so sind diese zuerst zu kopieren und die erstellten Kopien anzupassen. Die konkreten
Mechanismen zur Verwendung der Elemente des Datenfeldbaukastens werden im Redaktionskonzept
beschrieben.
Analog zu Datenfeldern ist auch für Datenfeldgruppen die Verwendung von Metadaten notwendig und
sinnvoll. Die Metadaten gleichen in ihrer Struktur im Wesentlichen den Metadaten der Datenfelder.
Lediglich einige Metadaten sind auf Feldgruppenebene obsolet (z.B. „Datentyp“ und „Inhalt“). Auch der
Metadatensatz der Datenfeldgruppen muss, analog zu dem Metadatensatz für Datenfelder, beständig
sein und darf nur in wenigen Ausnahmefällen durch Anpassung der FIM-Logik und nach Durchlaufen
der entsprechenden Redaktionsprozesse geändert werden.

## Metadaten

Metadaten geben Aufschluss über die Natur einer Datenfeldgruppe; in den folgenden Tabellen werden die Metadaten für Datenfeldgruppen im Standard XDatenfelder 3 gelistet.

### Identifikationsangaben

|Metadatum|Definition|
|-----|-----|
|<u>**Identifizierungsangabe (ID)***</u> (1)|Eine eindeutige ID, siehe Identifikatoren.|
|<u>**Versionsangabe***</u> (1 für versionierte Datenfeldgruppen, 0 sonst)|Die Versionsnummer wird verwaltet, um die Existenz unterschiedlicher Versionen von Elementen zu ermöglichen.|

### Definitionsangaben

|Metadatum|Definition|
|-----|-----|
|**Name** (1)|FIM-interner Name des Datenfelds, der sichtbar ist für alle Rollen in FIM, die auf der Ebene der abstrakten Formulare tätig sind (fachliche Ersteller, Methodenexperten, Informationsmanager, FIM-Koordinierungsstelle, FIM-Nutzer).|
|*Definition* (0…1)|Fasst den Inhalt eines Datenfelds aus redaktioneller Sicht zusammen, d. h. aus Sicht der Rollen fachlicher Ersteller, Methodenexperte, Informationsmanager, FIM-Koordinierungsstelle, FIM-Nutzer.|
|*Bezug zur Handlungsgrundlage* (0…n)|Dieses Attribut ermöglicht es, Bezüge zu einschlägigen Rechtsnormen sowie zu in anderen Vorhaben wie den XÖV-Vorhaben (z. B. XWaffe) oder Vorhaben wie P23R gleich oder ähnlich definierten Datencontainer zu erstellen und diese Verbindung zu dokumentieren. Hierdurch lässt sich die Ausgestaltung eines Stammdatenschemas gemäß einschlägigen Rechtsnormen oder Standardisierungsverfahren belegen. Es besteht die Möglichkeit zu jeder Handlungsgrundlage eine URI zu hinterlegen.|
|*Beschreibung* (0…1)|Zusätzliche Beschreibung und Erläuterung eines Datenfelds für FIM-Nutzer.|
|*Stichwörter (Tags)* (0…n)|Dieses Metadatum beinhaltet eine Liste an vorgegebenen und frei wählbaren Stichwörtern, die es dem FIM-Nutzer ermöglichen, eine spezielle Semantik zu kennzeichnen oder eine Suche innerhalb eines Redaktionssystem zu erleichtern.|
|*Relation* (0…n)|Relationen dokumentieren Beziehungen der Elemente untereinander, wie z.B. "ist abgeleitet von", "ersetzt" oder "ist äquivalent zu". Eine Relation wird durch die Unterangaben Prädikat und Objekt vollständig beschrieben.|
|*Versionshinweis* (0…1)|Ein Hinweis, der bei der Versionierung eines Elements erstellt wird. Es wird beispielsweise beschrieben, was sich an einem Element geändert hat.|
|**istAbstrakt** (1)|Dieses Attribut ermöglicht die Unterscheidung zwischen konkreten und abstrakten Dokumentsteckbriefen. Es kann den Wert true oder false annehmen, wobei true für abstrakte Dokumentsteckbriefe gewählt wird und false für konkrete Dokumentsteckbriefe.|
|**Dokumentart** (1)|Die Dokumentart gibt an, um was für eine Art von Dokument es sich handelt. Dafür ist ein Wert aus der Codeliste XXX auszuwählen.|

### Angaben für Formular-Nutzer

|Metadatum|Definition|
|-----|-----|
|**Bezeichnung Eingabe** (1)|Bezeichnung der Datenfeldgruppe, welche für den Bürger/das Unternehmen auf Input-Formularen sichtbar ist.|
|**Bezeichnung Ausgabe** (0…1)|Bezeichnung der Datenfeldgruppe, welche für den Bürger/das Unternehmen auf Output-Formularen sichtbar ist.|
|*Hilfetext Eingabe* (0…1)|Erläuternder Hilfetext für den Formularanwender (Bürger/Unternehmen) eines Input-Dokuments, um zu verdeutlichen, welche Inhalte in einer Datenfeldgruppe einzugeben sind bzw. welche Aktion vorzunehmen ist.|
|*Hilfetext Ausgabe* (0…1)|Erläuternder Hilfetext für den Formularanwender (Bürger/Unternehmen) eines Output-Dokuments.|

### Zustandsangaben

|Metadatum|Definition|
|-----|-----|
|**Fachlicher Ersteller** (0…1)|Ersteller oder erstellende Institution des Elements. Der fachliche Ersteller entspricht der Freigabeinstanz für eine Silber- oder Goldfreigabe.|
|<u>**Erstelldatum**</u> :bomb:|Datum der Erstellung des Datenfelds|
|<u>**Letztes Änderungsdatum**</u> (1)|Datum der letzten Änderung des Datenfelds.|
|<u>**Letzter Bearbeiter**</u> :bomb:|Der letzte Bearbeiter des Datenfelds.|
|<u>**Status**</u> (1)|Der Status gibt Aufschluss darüber, ob und wie das Element im Rahmen eines Editors zu verwenden ist. Mögliche Ausprägungen dieses Metadatums sind durch die Codeliste urn:xoev-de:xprozess:codeliste:status gegeben. Näheres dazu finden Sie im Kapitel Status, Freigabe und Veröffentlichung. Der Defaultwert für den Status ist „in Bearbeitung“, alle anderen Status müssen durch den Nutzer aktiv gesetzt werden.|
|*Gültig ab* (0…1)|Sofern es eine Beschränkung der Gültigkeit gibt, ist der Gültigkeitszeitraum zu erfassen. Mit diesem Metadatum wird der Beginn der Gültigkeit erfasst.|
|*Gültig bis* (0…1)|Sofern es eine Beschränkung der Gültigkeit gibt, ist der Gültigkeitszeitraum zu erfassen. Mit diesem Metadatum wird das Ende der Gültigkeit erfasst.|
|<u>**Freigabedatum**</u> (0…1)|Datum der Freigabe durch die Freigabeinstanz dieses Datenfelds.|
|<u>**Veröffentlichungsdatum**</u> (0…1)|Datum der Veröffentlichung durch die Veröffentlichungsinstanz dieses Datenfelds.|

## Repräsentation im Standard XDatenfelder 3
### Auswahlgruppen

~~~
<xdf3:datenfeldgruppe>
  <xdf3:identifikation>
    <xdf3:id>G82000000020</xdf3:id>
  </xdf3:identifikation>
  <xdf3:name>Anschrift Inland oder Ausland</xdf3:name>
  ...
  <xdf3:art listURI="urn:xoev-de:fim:codeliste:datenfelder.gruppenart" listVersionID="1.0">
    <code>X</code>
  </xdf3:art>
  ...
  <xdf3:struktur>
    <xdf3:anzahl>0:1</xdf3:anzahl>
    <xdf3:enthaelt>
      <xdf3:datenfeldgruppe>
        <xdf3:identifikation>
          <xdf3:id>G82000000021</xdf3:id>
        </xdf3:identifikation>
        <xdf3:name>Anschrift Inland</xdf3:name>
        ...
  </xdf3:struktur>
  <xdf3:struktur>
    <xdf3:anzahl>0:1</xdf3:anzahl>
    <xdf3:enthaelt>
      <xdf3:datenfeldgruppe>
        <xdf3:identifikation>
          <xdf3:id>G82000000022</xdf3:id>
        </xdf3:identifikation>
        <xdf3:name>Anschrift Ausland</xdf3:name>
        ...
  </xdf3:struktur>
</xdf3:datenfeldgruppe>
~~~