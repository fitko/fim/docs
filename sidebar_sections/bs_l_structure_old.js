// 
const bsl_structure_old = [
	{
		'type': 'category',
		'label': 'Baustein Leistungen',
		'items': [
			{
			  'type': 'category',
			  'label': 'Leistung QS Kriterien (in Arbeit)',
			  'items': [
				'leistung/leistungqskriterien/Einleitung',
				'leistung/leistungqskriterien/Qualitätssicherungskriterien',
				'leistung/leistungqskriterien/Module zur Leistung',
			  ]
			},
			{
			  'type': 'category',
			  'label': 'LeiKa Handbuch (in Arbeit)',
			  'items': [
				{
				  type: 'category',
				  label: 'Einleitung',
				  collapsed: true,
				  items: [
					'leistung/leikahandbuch/Einleitung/GrundsaetzlicheZielsetzung',
					'leistung/leikahandbuch/Einleitung/Handlungsgrundlagen',
					'leistung/leikahandbuch/Einleitung/ZieledesDokuments',
				  ],
				},
				{
				  type: 'category',
				  label: 'Anwendungsbeteiligte und Rollen',
				  collapsed: true,
				  items: [
					'leistung/leikahandbuch/2 Anwendungsbeteiligte und Rollen/2.1 Geschäftsstelle des IT-Planungsrats GS-ITPLR',
					'leistung/leikahandbuch/2 Anwendungsbeteiligte und Rollen/2.2 Fachgruppe LeiKa-plus',
					'leistung/leikahandbuch/2 Anwendungsbeteiligte und Rollen/2.3 Geschäfts- und Koordinierungsstelle LeiKa GK LeiKa',
					'leistung/leikahandbuch/2 Anwendungsbeteiligte und Rollen/2.4 Geschäfts- und Koordinierungsstelle 115',
					'leistung/leikahandbuch/2 Anwendungsbeteiligte und Rollen/2.5 Bundesbehörden Soll-Stand',
					  ],
				},
				{
				  type: 'category',
				  label: 'Definition einer Leistung',
				  collapsed: true,
				  items: [
					'leistung/leikahandbuch/3 Definition einer Leistung/3',
					  ],
				},
				{
				  type: 'category',
				  label: 'LeiKa-Struktur',
				  collapsed: true,
				  items: [
					'leistung/leikahandbuch/4 LeiKa-Struktur/4',
					  ],
				},
				{
				  type: 'category',
				  label: 'Inhalt des LeiKa, Stammtext- und Ergänzungsmodell',
				  collapsed: true,
				  items: [
					'leistung/leikahandbuch/5 Inhalt des LeiKa, Stammtext- und Ergänzungsmodell/5',
					{
					  type: 'category',
					  label: 'Begriffsdefinitionen',
					  collapsed: true,
					  items: [
						'leistung/leikahandbuch/5 Inhalt des LeiKa, Stammtext- und Ergänzungsmodell/5.1 Begriffsdefinitionen/5.1.1 Leistungsbericht',
						'leistung/leikahandbuch/5 Inhalt des LeiKa, Stammtext- und Ergänzungsmodell/5.1 Begriffsdefinitionen/5.1.2 Modul',
						'leistung/leikahandbuch/5 Inhalt des LeiKa, Stammtext- und Ergänzungsmodell/5.1 Begriffsdefinitionen/5.1.3 Befüllung von Modulen gemäß Typisierung der Leistungen',
						  ],
					},
					'leistung/leikahandbuch/5 Inhalt des LeiKa, Stammtext- und Ergänzungsmodell/5.2 Bereitstellung von Inhalten über den LeiKa',
					'leistung/leikahandbuch/5 Inhalt des LeiKa, Stammtext- und Ergänzungsmodell/5.3 Qualitätskriterien',
					  ],
				},
				{
				  type: 'category',
				  label: 'Pflegeprozesse des LeiKa',
				  collapsed: true,
				  items: [
					'leistung/leikahandbuch/6 Pflegeprozesse des LeiKa/6.1 Ersterstellung, Änderung, Außerkrafttreten, Monitoring',
					'leistung/leikahandbuch/6 Pflegeprozesse des LeiKa/6.2 Freigabe',
					'leistung/leikahandbuch/6 Pflegeprozesse des LeiKa/6.3 Benachrichtigung der Nutzer über erfolgte Änderungen',
					'leistung/leikahandbuch/6 Pflegeprozesse des LeiKa/6.4 Versionierung',
					  ],
				},
				{
				  type: 'category',
				  label: 'Technische Bereitstellung und Visualisierung des LeiKa',
				  collapsed: true,
				  items: [
					'leistung/leikahandbuch/7 Technische Bereitstellung und Visualisierung des LeiKa/7.1 Datenimport',
					'leistung/leikahandbuch/7 Technische Bereitstellung und Visualisierung des LeiKa/7.2 Webservice',
					'leistung/leikahandbuch/7 Technische Bereitstellung und Visualisierung des LeiKa/7.3 Leistungsviewer',
					'leistung/leikahandbuch/7 Technische Bereitstellung und Visualisierung des LeiKa/7.4 LeiKa-plus Portal',
					  ],
				}
			  ]
			}, 
		]
	},
]

    // export the structure to enable use in other files
module.exports = bsl_structure_old;