# Aufbau dieser Dokumentation

Verschiedene Zielgruppen haben unterschiedliche Bedarfe, entsprechend ist diese Dokumentation strukturiert und wendet sich an

- **„FIM-Verstehende“:** Personen, die FIM verstehen müssen: Behördenleitungen, Führungskräfte, politisch Entscheidende sowie Lernende des eGovCampus
- **„FIM-Anwendende“:** Personen, die FIM-Stamminformationen erstellen
- **„FIM-Verwendende“:** Personen, die FIM-Stamminformationen und darauf aufbauende Referenzinformationen in ihrer Arbeit verwenden müssen (Referent:innen im Gesetzgebungsprozess, Verwaltungsdigitalisierende, IT-Dienstleistende)
- **"Entwickler:innen"**: Personen, die dafür sorgen, dass die API des Sammelrepository für den jeweiligen technischen und fachlichen Kontext angebunden wird.