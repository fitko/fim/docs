# Über das FIM Schema-Repository

Das zentrale Daten Repository erfüllt die Funktion des Sammelrepository im FIM-Baustein Datenfelder. Im Folgenden wird es kurz Sammelrepository genannt.

Es ist die zentrale Ablage für Dokumentsteckbriefe und Datenschemata. Es unterstützt Upload, Download und die Suche nach Inhalten. Darüber hinaus stellt es einige nützliche Dienste u.a. zur Konvertierung bereit. 