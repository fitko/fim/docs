# Bausteinübergreifende Fragen

## Was ist der Unterschied zwischen Stamm- u. Referenzinformationen?

**FIM-Stamminformationen** werden von dem für die Rechtsgrundlage federführenden Ministerium oder einer vergleichbaren Behörde auf Bundes-, Landes- oder kommunaler Ebene ausschließlich auf Basis der Handlungsgrundlagen erstellt. FIM-Stamminformationen sind **FIM-Stammtexte, FIM-Stammprozesse und FIM-Stammdatenschemata.** Sie bilden die Grundlage, die i.d.R. von der nächsten Ebene konkretisiert werden muss. Hat die für den Vollzug zuständige Behörde die letzte Detaillierung vorgenommen, bezeichnet man das Ergebnis als Lokalinformationen.

**OZG-Referenzinformationen** sind Referenzinformationen, die auf FIM-Stamminformationen basieren und eine nutzerfreundliche Zielvision für die digitale Inanspruchnahme einer Leistung darstellen. Das heißt, OZG-Referenzinformationen zu einer OZG-Leistung verdeutlichen, welche Anpassungen vorgenommen werden müssen, um nutzerfreundliche Verwaltungsleistungen für Bürgerinnen und Bürger und/oder Unternehmen online zur Verfügung zu stellen. Sie können den geltenden Rechtsrahmen bewusst überschreiten. Sollten Rechtsänderungen erforderlich sein, sind sie möglichst präzise zu benennen, z.B. direkt an einzelnen Prozessschritten oder Datenfeldern. **OZG-Referenzinformationen sind OZG-Referenzprozesse und OZG-Referenzdatenschemata** (vgl. auch [hier](https://www.onlinezugangsgesetz.de/Webs/OZG/DE/service/faq/faq-node.html)).



## Wie verläuft der gesamte FIM-Freigabeprozess (DF u. PZ) bis hin zur Veröffentlichung?

Der Freigabeprozess besteht aus den **vier unten gelisteten Schritten**. Dabei unterscheiden sich der **Gold- und Silberstatus** lediglich darin, dass für den Goldstatus die rechtsetzende Instanz selbst und im anderen Fall die nächstuntergeordnete Fachlichkeit (Silberstatus) die inhaltliche Freigabe erteilt. Für Stammdatenschemata u. Prozesse für Typ 2 bzw. 3 Leistungen, die im OZG-Kontext erstellt werden, reicht der Silberstatus aus und ist konzeptionell eigentlich auch vorgesehen.

- **fachliche Vorprüfung** auf Landesebene (Silberstatus)
- **methodische Freigabe** durch Bausteinbetreiber über ticket@fimportal.de, eröffnet durch die entsprechende Landesredaktion und bestenfalls mit dem Betreff: „Methodische Prüfung Stammprozess XYZ“ oder „Methodische Prüfung Stammdatenschema XYZ“. Anzuhängen sind das Schema oder der Prozess im XDatenfelder oder XProzess-Format, alternativ auch Excel oder PDF bzw. ein Link auf das entsprechende Repository.
- **fachliche Prüfung u. Freigabe** auf Landesebene (Silberstatus)
- **Veröffentlichung / Übergabe der Leistung an die Bundesredaktion.** Ab dem Zeitpunkt ist sie dann für die Überführung in den Goldstatus und die weitere Pflege verantwortlich.

Ein tiefergehendes Verständnis der genannten Prüf- und Freigabeprozesse zur Erteilung von Silber- und Goldstatus kann durch Sichtung des Dokumentes „FIM/OZG Informationsunterlagen“ (S.13) auf dem FIM-Portal erlangt werden.



## Was bedeutet „Status“?

Der Status ist ein übergreifend genutztes Merkmal um den Bearbeitungsstand der Inhalte von FIM-Informationen widerzuspiegeln. Der Status kommt in allen Bausteinen zum Einsatz.