# Unterlagen & Notizen zu den letzten Terminen

:::info

Auf dieser Seite finden Sie alle Informationen zu den Terminen, die zuletzt stattgefunden haben. Wenn Sie auf dieser Seite Informationen vermissen, sprechen Sie uns gerne direkt an.

:::

## 29.08.2024

Der Termin vom 29.08.2024 stand ganz unter dem Thema der **Vertreterregelung**. Den zum Termin präsentierten Foliensatz finden Sie hier:

[Folien zur Vertreterregelung](./assets/240827_ME-Austausch-Vertreterregelung-in-AnnexPA.pdf)

## 24.09.2024

In diesem Termin wurde über [Decision Model and Notation](https://de.wikipedia.org/wiki/Decision_Model_and_Notation) und dessen mögliche Anwendungen im Kontext des Bausteins gesprochen.

## 19.11.2024

Im heutigen Termin wurden die Vorüberlegungen zum neuen Regelwerk geteilt. Dazu gab es auch eine [Präsentation](./assets/20241119ME-Austausch_Regelwerk_2.pdf). Interessenten, die sich weiter über das Thema austauschen und mitwirken wollen, sind eingeladen sich direkt unter [fim@fitko.de](mailto:fim@fitko.de) zu melden.

## 17.12.2024

Die Unterlagen zum heutigen Termin: [Datenschutz auf Basis von Stamminformationen](./assets/Datenschutz_aus_Stamminformationen.pdf).