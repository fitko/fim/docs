# BOB & die Nachnutzung von Baukastenelementen

## Was ist BOB?

BOB ist der zentrale harmonisierte Datenfeldbaukasten des FIM-Bausteins Datenfelder; die Abkürzung BOB steht für **Baukasten optimierter Bausteinelemente**. <br/> In BOB sind Baukastenelemente, um genau zu sein harmonisierte Datenfelder, sowie harmonisierte und abstrakte Datenfeldgruppen, enthalten, welche mit dem Hintergedanken der **größtmöglichen Nachnutzbarkeit** modelliert wurden. Bei diesen Baukastenelementen hat man sich (wo möglich) an Standards orientiert, wie unter anderem dem Personalausweisgesetz und XUnternehmen.

## Muss ich BOB verwenden?

**Ja**, Elemente aus BOB müssen bei der Modellierung verwendet werden. Beachten Sie hierzu auch den Abschnitt unten: [Wie werden Elemente aus BOB nachgenutzt?](#wie-werden-elemente-aus-bob-nachgenutzt).

## Was zeichnet Elemente aus BOB aus? Woran kann ich sie erkennen?

:::tip

- Ein Element aus BOB liegt als veröffentlichte Version vor und ist im Status **fachlich freigegeben (Gold)**.
- Es ist in der Regel **harmonisiert**, aber es liegen auch **abstrakte** Datenfeldgruppen vor.
- Der **Nummernkreis** dieses Elements ist die **60**.

:::

## Kann man Elemente aus BOB direkt verwenden?

- Harmonisierte Elemente aus BOB können direkt genutzt werden, sowie eine [Verwendungsbegründung](#gestaltungs--und-verwendungsbegründung) gepflegt wird.
- Abstrakte Elemente dürfen nicht direkt verwendet werden.

## Ich würde gerne ein Element aus BOB verwenden, aber es hat nicht die richtige Handlungsgrundlage, was kann ich tun?

Das ist kein Problem, in der Regel wird das Element, dass Sie nachnutzen wollen, nicht die richtige Handlungsgrundlage haben. <p/> Bei der Nachnutzung von BOB Elementen müssen Sie für jedes Element aus BOB festhalten, warum (d. h. aufgrund welcher Handlungsgrundlage) Sie es an dieser Stelle der Struktur in Ihrem Datenschema bzw. Ihrer Datenfeldgruppe verwenden. Man spricht in diesem Zusammenhang von der [**Verwendungsbegründung**](#gestaltungs--und-verwendungsbegründung) eines Elements.

## Ich würde gerne ein Element aus BOB verwenden, aber die Bezeichnung / der Hilfetext passen nicht, was kann ich tun?

Wenn Ihre gewünschten Bezeichnungen bzw. Hilfetexte nicht den angegebenen Werten des BOB Elements entsprechen, überlegen Sie bitte im ersten Schritt genau, ob hierfür eine Abweichung von BOB notwendig ist. Danach ziehen Sie bitte folgende Optionen in betracht:
### Kontext durch übergeordnete Struktur schaffen
Sie wollen z. B. klar machen, dass es sich bei der verwendeten Datenfeldgruppe nicht um irgendeine Anschrift handelt, sondern um die Anschrift des Antragstellers.
Dafür müssen Sie die Datenfeldgruppe aus BOB *Anschrift Inland* nicht umbenennen. Der Kontext wird hier durch eine übergeordnete Gruppe *Antragsteller* geschaffen; wird die Gruppe *Anschrift Inland* in der Gruppe *Antragsteller* verwendet, ist durch die Struktur eindeutig klar, dass an dieser Stelle Inlandsanschrift des Antragstellers gefordert ist.
### Abweichungen von BOB auf Mehrwert prüfen
Bei Hilfetexten, aber auch generell bei abgewandelten Bezeichnungen, prüfen Sie bitte genau, ob Ihre Änderung einen sprachlichen Mehrwert schafft. Falls nicht, dürfen Sie nicht von BOB abweichen; verwenden Sie die Baukastenelemente so, wie Sie diese in BOB vorgefunden haben.
### Änderungen an Bezeichnung und Hilfetext in ein statisches Feld auslagern
Hierfür ist es notwendig die gewünschten BOB Elemente als Teil einer größeren (von Ihnen erstellten) Gruppe nachzunutzen. Ergänzen Sie in dieser Gruppe ein statisches Feld, welches nähere Ausfüllhinweise zu den darauf folgenden BOB Elementen enthält.
### Keine dieser Optionen ist umsetzbar
Erst nachdem Sie alle diese Optionen sorgsam geprüft haben, können Sie die Möglichkeit in Betracht ziehen, von BOB abzuweichen. Erstellen Sie hierbei eine Kopie des BOB Elements und nehmen Sie die entsprechenden Änderungen vor.

## Gestaltungs- und Verwendungsbegründung

:::info

- Eine **Gestaltsungsbegründung** wird an einem Element direkt unter dem Metadatum *Bezug zur Handlungsgrundlage* gepflegt. Aus Ihr geht hervor, warum ein Element genau so modelliert wurde; d. h. warum es so gestaltet ist.
- Eine **Verwendungsbegründung** wird nicht am Element direkt gepflegt, sondern an der Struktur des nachnutzenden Elements. Ein alternativer Name hierfür ist Strukturbezug. Die Verwendungsbegründung gibt an, warum an dieser Stelle in der Struktur eines Elements ein bereits bestehendes Element nutzen.
<p/>

### Beispiel

Die Datenfeldgruppe *Geburtsdatum (teilbekannt)* in BOB wurde basierend auf der Handlungsgrundlage § 5 (2) PAuswG; Anhang 3 Abschnitt 1 PAuswV; Tabelle 9 BSI TR-03123 Version 1.5.1 modelliert; dies ist die **Gestaltungsbegründung** dieser harmonisierten Datenfeldgruppe.

In einem Stammdatenschema *Antrag auf Ausstellung einer Lebenspartnerschaftsurkunde* wird das Geburtsdatum in zwei verschiedenen Kontexten erfasst:
- Zweimal als Geburtsdatum der beiden Lebenspartner, nach § 55 (1) Nr. 3 i.V.m. § 58 (1) PStG; Anlage 1 Nr. 3140 PStV.
- Einmal als Geburtsdatum der antragberechtigten Person, nach § 62 (1) PStG.

Dies sind die **Verwendungsbegründungen** der Datenfeldgruppe *Geburtsdatum (teilbekannt)*.

:::

## Muss ich für alle meine nachgenutzten Elemente Verwendungsbegründungen angeben?

**Ja**, das ist wahrscheinlich notwendig, denn in der Regel sind Gestaltungsbegründung und Verwendungsbegründung nicht gleich. Das heißt, die Verwendungsbegründung muss in folgenden Nachnutzungsfällen hinterlegt werden:
- Sie verwenden ein BOB Element in Ihrem Datenschema / Ihrer Datenfeldgruppe.
- Sie nutzen ein harmonisiertes Element aus Ihrem lokalen Baukasten nach.
- Sie verwenden ein Element aus einem anderen rechtlichen Kontext in Ihrem Datenschema.

Bei der Nachnutzung harmonisierter Datenfeldgruppen, wird die Verwendungsbegründung nur für die verwendete Gruppe angegeben.

## Wie werden Elemente aus BOB nachgenutzt?

### Nachnutzung von harmonisierten & abstrakten Elementen

- **Direktverwendung**: Bauen Sie das entsprechende Element aus BOB direkt so in Ihre Struktur ein, wie es ist. Vergessen Sie nicht eine Verwendungsbegründung zu hinterlegen. Dies gilt natürlich für Datenfeldgruppen und Datenfelder gleichermaßen.
- **Mix**: Sie können sich aus den Elementen von BOB auch Ihre eigenen Datenfeldgruppen von Grund auf neuzusammenstellen und diese (falls nötig) um eigene Elemente erweitern.
- **Klonen und Ableiten**: Dies betrifft besonders abstrakte Datenfeldgruppen, kann jedoch auch auf harmonisierte Elemente zutreffen. Erstellen Sie eine neue Arbeitskopie aufgrund der Vorlage eines BOB-Elements. Editieren Sie die Kopie entsprechend Ihrer Anforderungen.

### Besonderheiten der Nachnutzung bei abstrakten Elementen

Eine abstraktes Datenfeldgruppe enthält oft alle relevanten, die es zu einem Thema gibt, z. B. enthält die Gruppe **natürliche Person (abstrakt, umfassend)** alle relevanten Informationen, die es zum Thema *natürliche Person* gibt. Dies Gruppe, sowie andere abstrakte Gruppen, darf nicht direkt verwendet werden. Wenn Sie davon ableiten sollten, nutzen Sie in der Regel nicht alle Unterelemente dieser Gruppe. Entefernen Sie nicht benötigte Unterelemente und behalten nur die benötigten. Vergessen Sie auch nicht die Handlungsgrundlage Ihrer Gruppe anzupassen und den Namen abzuändern.


## Wie sollen Elemente aus BOB nicht nachgenutzt werden?

:::warning

### Kopieren und nur den Namen und ggf. noch die Bezeichnung abändern

Dieses Vorgehen ist aus mehreren Gründen stark kritisch zu sehen:
- Indem Sie ein Baukastenelement erzeugen, welches identisch zu einem BOB-Element ist, blähen Sie Ihren lokalen Baukasten nur unnötig auf. Es wird schwieriger den lokalen Baukasten zu durchsuchen und das richtige Element zu finden.
- Wenn das ursprüngliche BOB-Element aktualisiert oder auf inaktiv gesetzt wird, bekommen Sie darüber keinen Hinweis. Ihr Element bleibt stets auf einem älteren Stand und Aktualisierungen müssen manuell vorgenommen werden.
- Die Handlungsgrundlage Ihres Elements ist falsch, diese muss explizit von Ihnen angepasst werden. Es ist extrem unwahrscheinlich, dass Ihr Datenschema genau die gleiche Handlungsgrundlage verlangt, wie diese, die beim BOB-Element hinterlegt wäre.

:::


## Was ist ein lokaler harmonisierter Baukasten?

Es kann sein, dass Ihr Bundesland, analog zu BOB, einen lokalen harmonisierten Baukasten besitzt. D. h. dieser enthält Gruppen und Felder, die in Ihrem Themenfeld mit dem Hintergedanken der größtmöglichen Nachnutzbarkeit modelliert wurden. Diese Elemente können Sie an der Strukturelementart erkennen (*harmonisiert* und **abstrakt**).

## In meinem Bundesland nutzen wir einen lokalen harmonisierten Baukasten, welche Regeln für die Nachnutzung gelten?

Es gelten die gleichen Regeln für die Nachnutzung, wie für BOB.

## Und wie sieht das mit beliebigen Elementen aus? Kann ich diese auch nachnutzen?

Ja, das ist möglich. Auch hier geltern die gleichen Regeln. Stellen Sie vor der Nachnutzung sicher, dass das Element, welches Sie nachnutzen möchten bereits fertig modelliert wurde. Führen Sie bei der Nachnutzung keine Änderungen am Element selbst durch.

