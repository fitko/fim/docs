# Glossar

#### Ableitungsmodifikation {#ableitungsmodifikation}

In den Detailansichten des FIM-Baustein Datenfelder wird mit den zwei Einträgen Repräsentation und Struktur die Möglichkeit der Modifikation eines abgeleiteten Elements bzw. die Möglichkeit der Veränderungen an der Struktur von abgeleiteten Elementen beschrieben. (Quelle: XDatenfelder-Spezifikation 3.0.0)

#### Ansprechpunkt {#ansprechpunkt}
Ansprechpunkte sind die Organisationseinheiten, die einen Antrag entgegen nehmen, diesen aber nicht bearbeiten und nicht den Verwaltungsakt erlassen; z. B. Servicecenter / Bürgerbüro.

#### Anwendungsgebiet {#anwendungsgebiet}

Im Kontext der Beschreibung eines Prozesses kann mit der verwaltungspolitischen Kodierung das Anwendungsgebiet des Prozesses beschrieben. 

Handelt es sich um Prozesse der Bundeseigenverwaltung (Leistungstyp 1), darf die Verwaltungs-
politische Kodierung nicht ausgefüllt werden. Die Angabe der Verwaltungspolitischen Kodierung bei Referenz- oder Lokalprozessen in länderspezifischen Systemen kann zusätzlich zum Prozessschlüssel und zur Versionsnummer als weite-
res Schlüsselattribut verwendet werden. (Quelle: QS-Kriterien des Baustein Prozesse)

#### Baukasten {#baukasten}

Strukturierte Sammlung von standardisierten FIM-Elementen (z. B. [Datenfeldern](#datenfeld), LeiKa-Modulen oder [Referenzaktivitätengruppen](#referenzaktivitaetengruppe)) aus denen [Stamminformationen](#fim-stamminformation) zusammengestellt werden können. Er dient der redaktionellen Standardisierung von [Lokalinformationen](#lokalinformation). Die FIM-Elemente sind durch eindeutige Schlüsselnummern identifizierbar.
  

#### Baustein {#baustein}

FIM basiert auf drei **Bausteinen:** Prozesse, Leistungen, Datenfelder. Sie bilden das Fundament der [FIM-Methodik](#fim-methodik).  

#### Bausteinbetreiber {#bausteinbetreiber}

- Bereitstellung der technischen Infrastruktur für die [FIM-Bausteine](#baustein) (u. a. zentrale [FIM-Repositorys](#repository)).
- Pflege der [Kataloge](#katalog) und [Baukästen](#baukasten).
- Ansprechpartner:in und fachliche Unterstützung für die FIM-Rollen und Organisationseinheiten im Redaktionsprozess   

#### Bearbeitungsdauer {#bearbeitungsdauer}
In der Beschreibung einer Leistung wird hier detailliert, wie lange die Bearbeitung des Antrags in der Regel in der Behörde dauert und ob es eine gesetzliche Bearbeitungsfrist gibt.

#### Bedarfsmelder:in {#bedarfsmelder}

Rolle, die einen Erstellungs- oder Änderungsbedarf zu [Stamminformationen](#stamminformation) meldet. Dies muss nicht zwingend eine FIM-Rolle sein (im OZG zum Beispiel Leistungsverantwortliche). 

#### Bezeichnung {#bezeichnung}

In der Detailansicht eines Dokumentsteckbriefs oder Datenschemas die Bezeichnung, welche für den Bürger/das Unternehmen auf Input-Formularen sichtbar ist. (Quelle: XDatenfelder-Spezifikation 3.0.0)

#### Bibliothek {#bibliothek}

Strukturierte Sammlung von [Stamminformationen](#stamminformation). Die Stamminformationen werden eindeutig durch Schlüsselnummern identifiziert. Bezüge zwischen den Stamminformationen werden durch den FIM-Ordnungsrahmen abgebildet und durch die Systematik der Schlüsselnummern nachvollziehbar.     

#### BOB - Baukasten optimierter Bausteinelemente {#bob}

**BOB** ist der zentrale Baukasten des FIM-Bausteins Datenfelder. Er stellt harmonisierte Baukastenelemente, insbesondere Datenfeldgruppen und Datenfelder, zur Verfügung. 

Diese Baukastenelemente können in unterschiedlichsten Datenschemata verwendet werden, unabhängig vom Fachrechtsbezug des einzelnen Schemas. Sie haben größtmöglichen Wiederverwendungswert und basieren zumeist auf Standards oder auf einer hohen Bandbreite der Einsatzmöglichkeit, die im Rahmen der allgemeinen Methodenexpertise über Redaktionen ermittelt wurde.

Die Verwendung der Baukastenelemente aus BOB trägt somit einerseits zur einer harmonischen und nutzerfreundlichen Gestaltung von Formularen und Dokumenten bei und bildet andererseits die Grundlage für medienbruchfreien, fachübergreifenden Datenaustausch.    

#### Bundesredaktion {#bundesredaktion}

s. Rolle „[Redaktion: Verwaltungsebenenunabhängig](#redaktion)“

Verantwortungsbereich: Verwaltungsleistungen mit rechtssetzender Stelle Bund      

#### Codeliste {#codeliste}

„Eine Codeliste ist eine Liste von Codes und der Beschreibung ihrer jeweiligen Bedeutung. Die Bedeutung von Codes kann dabei beispielsweise in Form von Namen (Augsburg, Bremen, München, etc.), Begrifflichkeiten (ledig, verheiratet, geschieden, etc.) oder Statusbeschreibungen (Antrag übermittelt, Antrag empfangen, Antrag unvollständig, etc.) vorliegen.“ (Quelle: Handbuch zur Entwicklung XÖV-konformer Standards Version 2.3.1)

#### Datenfeld {#datenfeld}

Unter einem (Daten-)Feld wird grundsätzlich die kleinste logische Einheit eines Datensatzes oder eines Formulars zur Datenerfassung verstanden (Stahlknecht, Hasenkamp, 2002).

Im Kontext des FIM-Bausteins Datenfelder bezeichnen Datenfelder die elementaren Bestandteile zur Beschreibung von Stammdatenschemata und stellen einen Typ von Stamminformationen dar. Bei der Definition Datenfeldern werden durch die regulatorisch zuständigen Stellen Name, Definition und ein Identifikationsschlüssel einheitlich vorgegeben. Wertebereiche, Hilfetexte und sonstige Merkmale können auf Vollzugsebene angepasst werden.

Typische Kombinationen von Feldern werden zu [Datenfeldgruppen](#datenfeldgruppe) zusammengefasst. Kombinationsmöglichkeiten von Feldern und Feldgruppen werden durch Feldregeln abgebildet. Dabei kann ein Feld bzw. eine Feldgruppe in mehreren [Stammdatenschemata](#fim-stammdatenschema) verwendet werden. Somit kann ein Feld bzw. die damit erhobenen Daten auch in unterschiedlichen Prozessen bzw. Leistungen verwendet werden.  

#### Datenfeldgruppe {#datenfeldgruppe}

Datenfeldgruppen dienen dazu, Datenfelder zu logisch in Beziehung stehenden Gruppen zusammen zu fassen. Datenfeldgruppen beinhalten folglich Datenfelder und ermöglichen gleichzeitig die strukturierte Darstellung von [Datenschemata](#fim-stammdatenschema).  

#### Datentyp {#datentyp}

Im FIM-Baustein Datenfelder ist dies der Typ des Datenfelds wie z.B. Text, Zeit, Geldbetrag. Die möglichen Werte finden sich in der [XDatenfelder-Spezifikation](https://www.xrepository.de/details/urn:xoev-de:fim:standard:xdatenfelder). (Quelle: XDatenfelder-Spezifikation 3.0.0)

#### Definition {#definition}

In den Detailansichten des FIM-Baustein Datenfelder fasst die Definition den Inhalt des Elements aus redaktioneller Sicht zusammen, d.h. aus Sicht der Rollen fachlicher Ersteller/-in, Methodenexperte/-in, Informationsmanager/-in, FIM-Koordinierungsstelle, FIM-Nutzer/-in. (Quelle: XDatenfelder-Spezifikation 3.0.0)

#### Detaillierungsstufe {#detaillierungsstufe}

Die Detaillierungsstufe den Grad der Detailgenauigkeit eines [Prozesses](#prozess) sowie dessen Bezug zur föderalen Ebene. Es gibt drei Haupttypen:

1. Stammprozesse: Die abstrakteste Ebene, basierend auf rechtlichen Vorgaben.
2. Referenzprozesse: Detaillierter, mit organisatorischen Anpassungen für mehrere Behörden.
3. Lokalprozesse: Die detaillierteste Ebene, spezifisch für lokale Behörden und deren IT-Strukturen.

Diese Stufen ermöglichen eine strukturierte Prozessmodellierung je nach Anwendungsfall.

Quelle: Fachkonzept Prozess

#### Dokumentart {#dokumentart}

In der Detailansicht eines Dokumentsteckbriefs gibt Dokumentart an, ob es sich um einen Antrag, Anzeige, Bericht, Bescheid oder einen anderen Eintrag aus urn:xoev-de:fim-datenfelder:codeliste:dokumentart handelt. (Quelle: XDatenfelder-Spezifikation 3.0.0)

#### Dokumentsteckbrief {#dokumentsteckbrief}

Im FIM-Baustein Datenfelder beschreiben Dokumentsteckbriefe den Einsatzzweck eines Dokumentes der öffentlichen Verwaltung (z.B. für einen Antrag, Meldung, Bescheid).

Dokumentsteckbriefe sind die [Katalogeinträge](#katalog), welche den Datenfeldkatalog bilden.

Einem [Datenschemata](#fim-stammdatenschema) ist immer ein Dokumentsteckbrief zugeordnet. Ein Datenschemata beschreibt die Struktur.  

#### Downloads {#downloads}

In den Detailansichten für FIM-Elemente der drei Bausteine finden sich hier herunterladbare Dateien. XDatenfelder-, XZuFi- und XProzess-Dateien sind ursprünglich aus externen Systemen und werden unverändert angeboten, andere Dateien sind das Ergebnis einer FIM-Portal-internen Konvertierung.

#### Einheitlicher Ansprechpartner {#einheitlicher-ansprechpartner}

Der Einheitliche Ansprechpartner (EA) ist eine zentrale Servicestelle, die im Rahmen der EU-Verordnung zum Single Digital Gateway (SDG) eingerichtet wurde. Er unterstützt Bürger, Unternehmen und Selbstständige bei Verwaltungs- und Genehmigungsverfahren, insbesondere zur Aufnahme und Ausübung von Dienstleistungen. Ziel ist die Vereinfachung und Digitalisierung dieser Prozesse, oft durch eine einzige Kontaktstelle, die die Kommunikation mit anderen Behörden koordiniert. Dies erleichtert den Zugang zum EU-Binnenmarkt und fördert grenzüberschreitende Verwaltungsprozesse.

Im Kontext eines Leistungs-Stammtexts markiert dieses Merkmal, dass eineLeistung über eine einheitliche Stelle (§ 71a-e Verwaltungsverfahrensgesetz) abgewickelt werden kann. Die einheitliche Stelle (eS) nimmt Anzeigen, Anträge, Willenserklärungen und Unterlagen entgegen und leitet sie unverzüglich an die für die Erbringung der Leistung zuständigen Behörden weiter.

Fehlt ein Eintrag in der Datenpflege des Felds, so ist von einem "nein" auszugehen.

Quelle: [SDG Webseite Rheinland-Pfalz](https://sgdsued.rlp.de/einheitlicher-ansprechpartner), Musterformular Leistungsbeschreibung Version 2.0

#### Erforderliche Unterlagen {#erforderliche-unterlagen}
In der Beschreibung einer Leistung sind hier	Art und Format der zu erbringenden Nachweise detailliert.

#### Fachexpert:innen {#fachexperte}

- Verantwortlich für die fachliche Richtigkeit, die Gültigkeit und Aktualität der FIM-Stamminformationen zu den Leistungen
- Fachliche Unterstützung bei der Erstellung/Modellierung der FIM-Stamminformationen
- Fachliche Prüfung/Freigabe der FIM-Stamminformationen
          
#### Feldart {#feldart}

Im FIM-Baustein Datenfelder ist dies die Art des Datenfelds wie z.B. Eingabefeld, statischer Text. Die möglichen Werte finden sich in der [XDatenfelder-Spezifikation](https://www.xrepository.de/details/urn:xoev-de:fim:standard:xdatenfelder). (Quelle: XDatenfelder-Spezifikation 3.0.0)

#### FIM-Dezentral {#fim-dezentral}

Alle Tätigkeiten und damit verbundene Strukturen auf Bundes-, Landes- und Kommunalebene. Entsprechend dem Grundverständnis von FIM liegt dieses dezentrale Tätigkeitsspektrum in der alleinigen Zuständigkeit von Bund und Ländern.  
               

#### FIM-Föderal {#fim-foederal}

Funktionen zur Sicherstellung der Anwendung der [FIM-Methodik](#fim-methodik), insbesondere Bereithalten der FIM-Infrastruktur und Koordination und Moderation der Zusammenarbeit zwischen Bund und den Ländern im Rahmen des FIM-Redaktionsmodells. FIM-Föderal wird organisatorisch von einer [Geschäfts- und Koordinierungsstelle FIM (GK FIM)](#gk-fim) ausgeübt.  
                

#### FIM-Inhalte {#fim-inhalte}

FIM-Inhalte sind Elemente des [Baukastens](#baukasten) und der [Bibliothek](#bibliothek).     

#### FIM-Leistung {#fim-leistung}

Mit dem Begriff Leistungen wird das Handeln der öffentlichen Verwaltung (unmittelbaren und mittelbaren) sowie jenes der gesetzgebenden und rechtsprechenden Gewalten (Legislative, Judikative) bezeichnet. In Betracht kommen sowohl verwaltungsinterne Leistungen als auch solche mit Beteiligung von Bürgerinnen und Bürgern, Unternehmen oder sonstigen Dritten.

#### FIM-Methodik {#fim-methodik}

Standardisierte Teile von [Lokalinformationen](#lokalinformation) wie z.&nbsp;B. [Datenfeldgruppen](#datenfeldgruppe) oder [Referenzaktivitätengruppen](#referenzaktivitaetengruppe), die zur Erstellung von [Stamminformationen](#fim-stamminformation) genutzt werden. FIM-Elemente können auch direkt für die Erstellung von [Lokalinformationen](#lokalinformation) genutzt werden, insbesondere wenn nicht die Bereitstellung einer [Stamminformation](#fim-stamminformationen) als Ziel angestrebt wird. Beispielfälle dafür sind die Erstellung von Formularen für individuelle kommunale Leistungen wie z.&nbsp;B. Familienrabattkarten für reduzierte Eintrittspreise in öffentlichen Einrichtungen.

#### FIM-Stammdatenschemata {#fim-stammdatenschema}

siehe [FIM-Stamminformationen](#fim-stamminformation)

#### FIM-Stamminformationen {#fim-stamminformation}

FIM-Stamminformationen sind FIM-Stammtexte, FIM-Stammprozesse und FIM-Stammdatenschemata.
Sie werden von dem für die Rechtsgrundlage federführenden Ministerium oder einer vergleichbaren Behörde auf Bundes-, Landes- oder kommunaler Ebene ausschließlich auf Basis der [Handlungsgrundlagen](#handlungsgrundlage) erstellt.
Sie bilden die Grundlage, die i.d.R. von der nächsten Ebene konkretisiert werden muss. Hat die für den Vollzug zuständige Behörde die letzte Detaillierung vorgenommen, bezeichnet man das Ergebnis als [Lokalinformationen](#lokalinformation).

#### FIM-Stammprozesse {#fim-stammprozess}

siehe [FIM-Stamminformationen](#fim-stamminformation)

#### FIM-Stammtexte {#fim-stammtext}

siehe [FIM-Stamminformationen](#fim-stamminformation)

#### Föderales Informationsmanagement (FIM) {#fim}

FIM liefert nach dem [Baukastenprinzip](#baukasten) standardisierte Informationen für Verwaltungsleistungen (z.&nbsp;B. Antrags- und Anzeigeverfahren). Für Behörden auf allen Verwaltungsebenen sind diese Informationen direkt nutzbar. Der Vorteil: Die mühsame Erhebung dieser Informationen entfällt. Zudem können die einzelnen Behörden sicher sein, dass die Informationen korrekt sind und juristisch geprüft wurden. Ein Redaktionssystem sorgt dafür, dass die Informationen immer auf dem aktuellsten Stand sind.

#### Formular {#formular}
In der Beschreibung einer Leistung werden hier die Formulare genannt, die nötig sind, um die Leistung in Anspruch zu nehmen.

#### Frist {#frist}
In der Beschreibung einer Leistung sind hier die Fristen, die Antragstellende einhalten oder beachten müssen, um die Leistung in Anspruch nehmen zu können, detailliert.

#### Gebiet {#gebiet}

„Gebiet repräsentiert eine räumlich (meist) zusammenhängende Fläche oder ein Areal. Für Zuständigkeitsfinder spielen insbesondere verwaltungspolitsche Gebiete eine Rolle. Zuständigkeiten werden für Gebiete definiert.“ (Quelle: Spezifikation XZuFi 2.2.0 vom 07.06.2019)

Das PVOG-Sammlerdienstkonzept fordert einen 12stelligen Schlüssel auf Basis der verwaltungspolitischen Kodierung von DESTATIS, bei dem fehlende Stellen rechtsbündig durch Nullen aufgefüllt werden (z.B. 095750000000 für die Kreisebene). (Quelle: PVOG – Konzept Sammlerdienst - XZUFI 2.2.0 - Version 4.6.10)

#### Geschäfts- und Koordinierungsstelle FIM (GK FIM) {#gk-fim}

Der fachlichen Weisung des Vorsitzes der Fachgruppe FIM unterliegendes Gremium, das die strategischen und Grundsatzfragen bearbeitet sowie die Qualitätssicherung der [Bibliotheken](#bibliothek), den Betrieb, die Entwicklung und die Pflege der [Baukästen](#baukasten) und [Kataloge](#katalog) einschließlich der übrigen Inhalte von FIM im Kontext von [FIM-Föderal](#fim-foederal) verantwortet. Die Mitglieder der GK FIM sind die 3 [Bausteinbetreiber](#bausteinbetreiber) (Leistungen, Prozesse, Datenfelder) sowie der FIM-Querschnitt (FITKO).

#### Gültig ab/Gültig bis {#gueltig_ab_bis}

In den Detailansichten des FIM-Baustein Datenfelder: Sofern es eine Beschränkung der Gültigkeit gibt, ist der Gültigkeitszeitraum zu erfassen. Mit diesem Metadatum wird der Beginn bzw. das Ende der Gültigkeit erfasst. (Quelle: XDatenfelder-Spezifikation 3.0.0)

#### Handlungsform {#handlungsform}

Ein Merkmal bei der Beschreibung einer [Prozessklasse](#prozessklasse). Ermöglicht es z. B. aus Sicht des Verwaltungsrechts, die jeweilige Handlungsform, die der Verwaltung zur Umsetzung des verfolgten operativen Ziels zur Verfügung steht, anzugeben. Die Handlungsform gibt den Rahmen für den Verwaltungsablauf vor und kann durch die Angabe der entsprechenden Verfahrensart untersetzt werden.

Quelle: [XRepository](https://www.xrepository.de/details/urn:xoev-de:fim:codeliste:handlungsform)

#### Handlungsgrundlage {#handlungsgrundlage}

Es gibt unterschiedliche Handlungsgrundlagen, die für die Erstellung von FIM-Informationen relevant sein können wie z.&nbsp;B. Gesetze, Rechtsverordnungen, Verwaltungsvorschriften, Beschlüsse oder Normen/Standards. Eine Übersicht der Arten liefert die Codeliste [Handlungsgrundlagenart](https://www.xrepository.de/details/urn:xoev-de:mv:em:codeliste:xprozess:handlungsgrundlagenart), die Sie auf www.xRepository.de finden.

#### Herausgeber {#herausgeber}

„Ein Herausgeber ist verantwortlich für die Herausgabe von Inhalten. Dies beinhaltet die Erstellung, Ergänzung oder Zusammenstellung von Daten. Der Herausgeber kann - muss aber nicht - der Urheber der Inhalte sein.“ (Quelle: Spezifikation XZuFi 2.2.0 vom 07.06.2019)

Das PVOG-Sammlerdienstkonzept impliziert, dass zur Identifikation des Herausgebers optimaler Weise die 115-Teilnehmernummer verwendet werden soll. (Quelle: PVOG – Konzept Sammlerdienst - XZUFI 2.2.0 - Version 4.6.10)

#### Hilfetext {#hilfetext}

In der Detailansicht eines Dokumentsteckbriefs oder Datenschemas der Hilfetext für dieses Element. (Quelle: XDatenfelder-Spezifikation 3.0.0)

#### Hinweise und Besonderheiten {#hinweise-besonderheiten}
In der Beschreibung einer Leistung können hier Hinweise im Zusammenhang mit der Leistung gegeben werden. Dies kann zusätzliche Informationen, die in anderen Modulen nicht erwähnt wurden, umfassen, so zum Beispiel Folgen, wenn im Antrag falsche Angaben gemacht werden.


#### Information über und Benachrichtigung zu Veränderungen in den FIM-Repositorys {#abo}

Über neue, veränderte und gelöschte Verwaltungsleistungen wird über das FIM-Portal informiert (reaktiv mit Hilfe von abonierbaren RSS-Feeds). Plant ein Vorhaben die Beschreibung einer Verwaltungsleistung, werden alle Teilnehmer benachrichtigt. Die Benachrichtigung verhindert Doppelarbeiten und Redundanzen.

#### Informationsmanager:in {#info}

Koordinierung der Arbeiten im Rahmen der FIM-Stamminformationsmodellierung mit den Fachbereichen:
- Kommunikationsschnittstelle zwischen Fachexpert:innen aus den Fachressorts und der Redaktion/FIM-Methodenexpert:innen
- Unterstützung des FIM-Redaktionsprozesses und Zuordnung von Änderungs- und Erweiterungsbedarfen entsprechend des Zuständigkeitsbereichs

#### Inhalt {#inhalt}

In den Detailansichten eines Datenfelds dient diese Angabe dazu, den Inhalt eines Formularfeldes zu spezifizieren, d.h. ein vordefinierter (Default-)Wert für ein Textfeld. (Quelle: XDatenfelder-Spezifikation 3.0.0)

#### Katalog {#katalog}

Der Katalog ist ein thematisch strukturiertes Verzeichnis, das redaktionell gepflegt wird. Die Katalogeinträge sind mit Metadaten hinterlegt, die die [Bibliotheksinhalte](#bibliothek) eindeutig strukturieren.
Die Einträge im [Leistungskatalog](#leika) heißen Leistungssteckbriefe, die Einträge im Prozesskatalog heißen Prozessklassen und die Einträge im Datenfeldkatalog heißen [Dokumentsteckbriefe](#dokumentsteckbrief).

#### Klassifizierung {#klassifizierung}
Ein Schlüssel aus der XZuFi-Spezifikation. Die Klassifizierung dient der Zuordnung von eigenen/benutzerdefinierten Codes für die Klassifizierung der Leistung. Dies sind Codelisten, welche nicht in XZuFi selbst definiert oder referenziert werden und auch nicht durch Leistungskategorien abgebildet werden. Beispiel: KGSt-Katalog

#### Kosten {#kosten}
In der Beschreibung einer Leistung sind hier die Gebühren oder andere Kosten detailliert, die Antragstellende tragen müssen.

#### Kurztext {#kurztext}
In der Beschreibung einer Leistung können hier Informationen für telefonische Auskünfte detailliert werden. Dieser Kurztext wird nicht in Verwaltungsportalen veröffentlicht und dient nur den Hotline-Mitarbeitenden.

#### Lagen Portalverbund {#lagenportalverbund}

Die Unterarbeitsgruppe Lagen (UAG Lagen) der KG Portalverbund hat mit dem Portalverbund-Lagenmodell (PV-Lagenmodell) ein einheitliches Modell für Lebens- und Geschäftslagen geschaffen, dass zur Harmonisierung der Navigations- und Such-Strukturen der Verwaltungsportale in Deutschland verwendet werden kann.

Übersichtlichkeit und Nutzerfreundlichkeit werden für Nutzerinnen und Nutzer des Portalverbundes erhöht, Verwaltungsleistungen können leichter gefunden werden, der Wiedererkennungswert steigt unabhängig vom konkreten Verwaltungsportal, das genutzt wird.

Die zugehörige Codeliste findet sich im [XRepository](https://www.xrepository.de/details/urn:xoev-de:fim:codeliste:pvlagen).

#### Leistung {#leistung}

siehe [FIM-Leistung](#fim-leistung)

#### Leistungen der Länder und Kommunen {#leistungen-der-laender-und-kommunen}

Neben den auf Bundesrecht beruhenden Verwaltungsleistungen werden auch die auf Landes- und kommunalem Satzungsrecht beruhenden Verwaltungsleistungen in FIM beschrieben und zur Nachnutzung in öffentlich zugänglichen Verzeichnissen (Repositorys) veröffentlicht bzw. ausgetauscht.

#### Leistungen im gemeinsamen Vollzug {#leistungen-im-gemeinsamen-vollzug}

Eine **Leistung im gemeinsamen Vollzug** ist eine Leistung, bei der mindestens zwei für den Vollzug verantwortliche Behörden einer organisatorischen Einheit (innerhalb oder außerhalb der öffentlichen Verwaltung) die Zuständigkeit für den Vollzug übertragen. Dies beinhaltet auch den Fall, dass eine der verantwortlichen Behörden die Zuständigkeit übernehmen kann.

Beispiel: Bei Leistungen der Jobcentern entscheiden sich viele Länder/Kommunen die Erbringung der Leistung der Bundesagentur für Arbeit zu übertragen. Es können sich Kommunen aber auch dafür entscheiden, diese Leistungen in kommunalen Jobcentern selbst anzubieten.

Der Leistungsnehmer kann nicht entscheiden, bei wem er die Leistung in Anspruch nimmt – dies hängt vom Gebiet ab.

#### Leistungsbeschreibungen {#leistungsbeschreibungen}

siehe [Lokalinformationen](#lokalinformation)

#### Leistungsbezeichnung I {#leistungsbezeichnung1}
Bezeichnung der Leistung gemäß FIM-Systematik (Leistungsgruppierung/Leistungskennung/Verrichtungskennung/Verrichtungsdetail).

#### Leistungsbezeichnung II {#leistungsbezeichnung2}
Bürgerorientierte Leistungsbezeichnung.

#### Leistungsgruppierung {#leistungsgruppierung}
Die Leistungsgruppierung erfüllt eine Systematisierungsfunktion innerhalb des Leistungskatalogs und ist aus der
vorgegebenen Liste von Gruppierungen auszuwählen: https://www.fimportal.de → Kataloge → Katalog Download Leistungen → Interner Bereich →
Downloads im csv-Format Leistungsobjekte LeiKa-Schluessel Bezeichnung.csv

#### Leistungskatalog (LeiKa) {#leika}

Mit dem Leistungskatalog (siehe auch [Katalog](#katalog)) wird in Deutschland erstmalig ein einheitliches und umfassendes Verzeichnis der Verwaltungsleistungen des Bundes, der Länder und Kommunen aufgebaut. Ziel ist es, eine zentrale Informationsbasis in Form eines Stammtext-Managements zur Verfügung zu stellen, die von allen Verwaltungsbereichen anwendungs- und vorhabenübergreifend für alle Informations- und Kommunikationskanäle genutzt wird.

#### Leistungskennung {#leistungskennung}
Die Leistungskennung stellt den Regelungsgegenstand einer Leistung dar und ist individuell zu benennen.

#### Leistungsklärung {#leistungsklaerung}

Eine Leistungsklärung beschreibt den Vorgang, mittels einer [Normenanalyse](#normenanalyse) unter Anwendung der Zuschnittsindikatoren die jeweiligen FIM-Leistungen zu identifizieren. Das Ergebnis ist der Leistungszuschnitt.

#### Leistungsmodule {#leistungsmodule}
Texte, die u.a. zur Erstellung von [Leistungssteckbriefen](#leistungssteckbrief) und [Stammtexten](#stammtext) benutzt werden. Informationen werden nach einem vorgegebenen Raster geordnet (ein Textmodul für jede Information). Textmodule bilden im Baustein Leistungen den Leistungsbaukasten. Beispiele sind „Gebühren“
oder „Fristen“. 


#### Leistungsobjekt {#leistungsobjekt}
Das Leistungsobjekt bezieht sich unmittelbar auf einen rechtlichen Regelungsgegenstand und setzt sich zusammen aus [Leistungsgruppierung](#leistungsgruppierung) und [Leistungskennung]{#leistungskennung}.

#### Leistungsschlüssel (LSL) {#leistungsschlüssel-lsl}

Zentrales Referenzierungsmerkmal jeder erfassten [FIM-Leistung](#fim-leistung)

#### Leistungsstammtext {#leistungsstammtext}
siehe [Stammtext](#stammtext)

#### Leistungssteckbrief {#leistungssteckbrief}
Der Leistungssteckbrief beschreibt die wichtigsten Merkmale zur Identifizierung der Leistung und ist Teil des [Leistungskatalogs](#leika). Bei den [Leistungstypen](#leistungstyp) 1 bis 3 sind diese Angaben deutschlandweit einheitlich. Nur bei den Leistungstypen 4 und 5 kann es hinsichtlich der Typisierung und Rechtsgrundlage Abweichungen geben.

#### Leistungstyp {#leistungstyp}
Der Leistungstyp beschreibt den Detaillierungsgrad der Leistung. Es gibt [Leistungsobjekt](#leistungsobjekt) LO, Leistungsobjekt mit Verrichtung (eigentlich [Verrichtungskennung](#verrichtungskennung) LOV und Leistungsobjekt mit Verrichtung und Detail LOVD (eigentlich [Verrichtungsdetail](#verrichtungsdetail))

#### Leistungszuschnitt {#leistungszuschnitt}

Ein Leistungszuschnitt ist die Menge aller im Rahmen der [Leistungsklärung](#leistungsklaerung) identifizierten [FIM-Leistungen](#fim-leistung)

#### Letzte Aktualisierung im FIM Portal {#letzte_aktualisierung_im_fim_portal}

In den Detailansichten des FIM-Baustein Datenfelder bezeichnet dies das Datum des letzten Imports in das vom FIM-Portal genutzte zentrale Sammelrepository des Baustein Datenfelder.

#### Letzte Änderung (Redaktion) {#letzte_aenderung_redaktion}

In den Detailansichten des FIM-Baustein Datenfelder bezeichnet dies das Datum der letzten Änderung und wird vom Redaktionssystem automatisch gepflegt. (Quelle: XDatenfelder-Spezifikation 3.0.0)

#### Lokaldatenschemata {#lokaldatenschema}

siehe [Lokalinformationen](#lokalinformation)

#### Lokalinformationen (ehemals Vollzugsinformationen) {#lokalinformation}

Werden Stamminformationen vollumfänglich um die vor Ort geltenden Ausführungsregelungen im Vollzug einer Verwaltungsleistung ergänzt und detailliert, entstehen die sogenannten **Lokalinformationen**. Sie geben konkrete Auskunft zum Beispiel über Verfahrensabläufe, Voraussetzungen und Bedingungen sowie Angaben zur zuständigen Stelle im Rahmen der Ausführung, z.&nbsp;B. mittels Angabe von Organisationseinheiten und Kontaktdaten.
Lokalinformationen sind Leistungsbeschreibungen, Lokalprozesse und Lokaldatenschemata.

#### Lokalprozesse {#lokalprozess}

siehe [Lokalinformationen](#lokalinformation)

#### Methodenexpert:in {#methodenexperte}

- Modellierung/Erstellung der methodenkonformen FIM-Stamminformationen zu Verwaltungsleistungen auf Basis der zu dem Zeitpunkt gültigen Rechtsgrundlage und den Qualitätssicherungskriterien der Bausteine
- Eigenständiger Austausch mit Rechts- und Fachexpert:innen, Recherche und Analyse zu den Rechtsgrundlagen einer Leistung
- Pflege der FIM-Stamminformationen bei Änderungsbedarfen
- Durchführung der methodischen Prüfung modellierter FIM-Stamminformationen auf Basis der Qualitätssicherungskriterien der Bausteine (die Freigabe erfolgt durch die Fachressorts, s.u.)
- Aktive Kommunikation bei der Identifizierung von Fehlern in FIM-Informationen
- Teilnahme an Austauschformaten und Unterarbeitsgruppen für FIM-Methodenexpert:innen

#### Mischleistungen {#mischleistung}

**Mischleistungen** sind Leistungen, die von mehreren Ebenen im Vollzug angeboten werden.
Beispiel: Führungszeugnis ist eine Typ-1-Leistung, die auf Bundesebene und kommunaler Ebene vollzogen wird. Der Leistungsnehmer kann entscheiden bei welcher Ebene er die Leistung in Anspruch nimmt.

#### Musterdatenschemata {#musterdatenschema}

siehe [Musterinformationen](#musterinformation)

#### Musterinformationen {#musterinformation}

**Musterinformationen** dienen als Orientierungshilfen oder Unterstützung bei der Erstellung von FIM-Stamm- und Referenzinformationen.

Musterinformationen werden beispielsweise für Querschnittsleistungen erstellt.

Musterinformationen sind Mustertexte, Musterprozesse und Musterdatenschemata.
 
Musterinformationen sollen von den Landesredaktionen oder der zentralen Bundesredaktion zur Verfügung gestellt werden.

#### Musterprozesse {#musterprozess}

siehe [Musterinformationen](#musterinformation)


#### Mustertexte {#mustertext}

siehe [Musterinformationen](#musterinformation)

#### Nummernkreis {#nummernkreis}
Im FIM-Baustein Datenfelder ist jedem Repository ein eindeutiger Nummernkreis zugeordnet, der nachträglich nicht mehr geändert werden kann. In diesem Repository liegen die bekannten Elemente des Bausteins in Form von Arbeitskopien und Versionen vor. Details finden sich [hier](https://docs.fitko.de/fim/docs/datenfelder/EA_Metadaten#aufbau-einer-id).

#### Normenanalyse {#normenanalyse}

Eine Normenanalyse beschreibt im Kontext von FIM die Analyse von relevanten [Handlungsgrundlagen](#handlungsgrundlage) zur
- Identifizierung von [FIM-Leistungen](#fim-leistung) im Rahmen der [Leistungsklärung](#leistungsklaerung) sowie
- zur Erstellung von [FIM-Stamminformationen](#fim-stamminformation).

#### Nutzer:in {#nutzer}

- Im Sinne der Kaskade: nächste Ebene der Nachnutzung der FIM-Stamminformationen für spezifische Anpassungen
- Nachnutzung im Rahmen der Digitalisierung von Verwaltungsprozessen (zum Beispiel IT-Dienstleister, die auf Basis der FIM-Datenschemata Formulare generieren)

#### Online-Dienst {#onlinedienst}

Als Online-Dienst wird eine elektronisch angebotene bzw. digital verfügbare Verwaltungsleistung verstanden. Berücksichtigt werden in dieser Übersicht zukünftig geplante, aktuell in der Entwicklung befindliche oder bereits fertige Dienste. Diese Services entsprechen mindestens Reifegrad 2 der Online-Verfügbarkeit von Verwaltungsleistungen: Das heißt, eine Online-Beantragung ist grundsätzlich möglich. Online Services, die aus dem OZG-Programm heraus entwickelt wurden und für eine Nachnutzung durch andere Länder und Kommunen nach dem Modell „Einer für alle“ bereitstehen, können zusätzlich auf dem Marktplatz der Nachnutzung aufgeführt werden.
 
Quelle: OZG Leitfaden, Begriffe (früher https://leitfaden.ozg-umsetzung.de/terms/all, am 27.07.2024 nicht erreichbar)

#### Operatives Ziel {#operatives-ziel}
Ein Merkmal bei der Beschreibung einer [Prozessklasse](#prozessklasse). Beschreibt das unmittelbare Ziel des Prozesses. Die Auswahl des operativen Ziels schränkt die möglichen Handlungsformen ein.

Quelle: [XRepository](https://www.xrepository.de/details/urn:xoev-de:fim:codeliste:operativesziel)

#### Organisationseinheiten {#organisationseinheit}

Organisationseinheiten sind im föderalen Informationsmanagement Akteure, die im Kontext einer Leistung oder eines Online-Dienstes eine Rolle einnehmen.

Organisationseinheiten können in diesem Sinne somit

- Rechtspersonen, also
  - Natürliche Personen (z.B. Öffentlich bestellte Vermessungsingenieure)
  - Juristische Person des öffentlichen Rechts (z.B. Gebietskörperschaften)
  - Juristische Person des privaten Rechts (z.B. TÜV Rheinland)
- Personengesellschaften oder deren Organe/Teile, wie Behörden, Abteilungen oder Funktion (z.B. Beauftragter für Arbeitssicherheit) sein.

Die Liste der Rollen, die eine Organisationseinheit zu einer Leistung/einem Online-Dienst innerhalb des FIM-Bausteins Leistungen einnehmen kann und welche Aufgaben/Charakteristika damit verbunden sind, wird in den FIM-Qualitätssicherungskriterien definiert und kontinuierlich weiterentwickelt.

#### OZG-Referenzinformationen {#ozg-referenzinformation}

**OZG-Referenzinformationen** sind Referenzinformationen, die auf FIM-Stamminformationen basieren und eine nutzerfreundliche Zielvision für die digitale Inanspruchnahme einer Leistung darstellen. Das heißt, OZG-Referenzinformationen zu einer OZG-Leistung verdeutlichen, welche Anpassungen vorgenommen werden müssen, um nutzerfreundliche Verwaltungsleistungen für Bürgerinnen und Bürger und/oder Unternehmen online zur Verfügung zu stellen.

Sie können den geltenden Rechtsrahmen bewusst überschreiten. Sollten Rechtsänderungen erforderlich sein, sind sie möglichst präzise zu benennen, z.B. direkt an einzelnen Prozessschritten oder Datenfeldern
OZG-Referenzinformationen sind OZG-Referenzprozesse und OZG-Referenzdatenschemata.

Vgl. [https://www.onlinezugangsgesetz.de/Webs/OZG/DE/service/faq/faq-node.html](https://www.digitale-verwaltung.de/Webs/DV/DE/aktuelles-service/faq-lexikon/faq-ozg/faq-node.html)

#### Präzisierung {#praezisierung}

In den Detailansichten eines Datenfelds kann damit der Datentyp eines Datenfeldes weiter eingeschränkt werden. Es sind Beschränkungen der Länge und des Wertebereichs möglich. (Quelle: XDatenfelder-Spezifikation 3.0.0)

#### Prozess {#prozess}

Prozesse sind Verwaltungsabläufe, die von bestimmten Personen(gruppen) in einer sich wiederholenden Reihenfolge unter bestimmten Vorgaben (z.&nbsp;B. Rechts- und Verwaltungsvorschriften etc.) und unter Nutzung von Hilfsmitteln (z.&nbsp;B. IT-Unterstützung, Formulare etc.) bearbeitet werden. In Betracht kommen sowohl verwaltungsinterne Prozesse als auch solche mit Beteiligung von Bürgerinnen und Bürgern oder Unternehmen. Der Begriff des Verwaltungsablaufs umfasst zugleich auch Verwaltungsverfahren gemäß § 9 VwVfG. (Vgl. Minikommentar zum Gesetz zur Förderung der elektronischen Verwaltung sowie zur Änderung weiterer Vorschriften, S. 28)

Prozesse bilden zusammen mit Leistungen und Datenfelder die drei FIM-Bausteine.

#### Prozesskette {#prozesskette}

Eine Prozesskette ist eine konkrete Vorgänger-Nachfolger-Beziehung zwischen [Prozessklassen](#prozessklasse) innerhalb von Prozessnetzen. Die jeweilige [Handlungsform](#handlungsform) der Prozessklasse kann Einfluss auf die möglichen nachfolgenden Prozessklassen haben.

#### Prozessklasse {#prozessklasse}

Eine Prozessklasse bezeichnet eine Kategorie von Prozessen, die spezifische Merkmale und [Handlungsformen](#handlungsform) aufweisen. Diese Klassen sind Teil des FIM-Prozesskatalogs und dienen dazu, Prozesse eindeutig zu charakterisieren, ohne auf deren konkrete Ausgestaltung einzugehen. Prozessklassen beeinflussen die möglichen nachfolgenden Prozessklassen innerhalb eines [Prozessnetzes](#prozessnetz), das die Beziehungen und Abhängigkeiten zwischen ihnen darstellt. Sie werden durch Metadaten wie Name, Bezeichnung und Handlungsgrundlage beschrieben.

Quelle: Prozess-Fachkonzept

#### Prozessnetz {#prozessnetz}

Ein Prozessnetz stellt die Wechselbeziehung und Abhängigkeiten zwischen [Prozessklassen](#prozessklasse) innerhalb eines Leistungsbündels dar (FIM-Prozesskatalog-Ebene 3). Es können mehrere Prozessnetze in einem Leistungsbündel vorkommen. Im FIM-Kontext ist die Modellierung von Prozessnetzen optional. Ein Prozessnetz kann mit der BPMN 2.0 Modellierungssprache erstellt werden und über das BPMN 2.0 Austauschformat zwischen den Prozessmodellierungstools ausgetauscht werden. Alternativ kann das Prozessnetz auch z.B. in Form einer Prozesslandkarte dargestellt werden.

#### Qualitätssicherungskriterien {#qualitaetssicherungskriterium}

Durch die Einhaltung der **Qualitätssicherungskriterien** wird sichergestellt, dass FIM-Informationen in einer zielgruppenadäquaten Fassung zur Verfügung gestellt werden.

Die Überprüfung, ob die FIM-Qualitätssicherungskriterien eingehalten werden, erfolgt im Rahmen der Erstellung und Bearbeitung von FIM-Elementen. Die Prüfung der Einhaltung der FIM-Qualitätssicherungskriterien ist Teil der methodischen Prüfung und somit Teil des Freigabeprozesses.

#### Quellredaktion {#quellredaktion}
Die Redaktion, die die Texte für die Leistung in den Portalverbund veröffentlicht hat.

#### Querschnittsleistungen {#querschnittsleistung}

**Querschnittsleistungen** sind Leistungen, die in mehreren (u. U. allen) Behörden vorkommen. In der Regel laufen sie in gleicher oder ähnlicher Form ab. 

Querschnittsleistungen sind z.&nbsp;B.
- Auskunftsleistungen nach Datenschutz-, Informationszugangs-/Informationsfreiheits- oder Transparenzgesetz,
- Widerspruchsverfahren,
- Aufsichtsbeschwerde oder
- [Verwaltungsakt](#prozess) nach VwVfG Erlass.
Für eine Querschnittsleistung wird ein Eintrag im Leistungskatalog mit der Typisierung 8 angelegt. Sie erhält dadurch einen individuellen Leistungsschlüssel.

#### Rechtsbehelf {#rechtsbehelf}
In der Beschreibung einer Leistung werden hier Möglichkeiten der Antragstellenden, um gegen die Entscheidung der Behörde vorzugehen, detailliert. Dies kann Angaben der möglichen Rechtsbehelfe inklusive Hinweis auf Klagemöglichkeit im Fall rechtlich vorgesehener Genehmigungsfiktion umfassen.


#### Rechtsgrundlagen {#rechtsgrundlagen}
vgl. [Handlungsgrundlagen](#handlungsgrundlage)

#### Redaktion:  Verwaltungsebenenunabhängig {#redaktion}

Verantwortlich für die Bereit-/Erstellung von FIM-Stamminformationen (Stammprozesse, Stammdatenschemata, Stammtexte) Fachressort-übergreifend entsprechend dem Verantwortungsbereich.

Pflege der FIM-Stamminformationen.

Koordinierung und Sicherstellung der methodischen und fachlichen Freigaben der Modellierungen. 

Sicherstellung der methodischen und fachlichen Qualität der FIM-Stamminformationen.

Gesamtkoordination aller FIM-Aktivitäten auf der jeweiligen Verwaltungsebene sowie aller Beteiligten (FIM-Informationsmanager:innen und Methodenexpert:innen).

Beteiligung an der Entwicklung und Umsetzung von Einführungskonzepten in Abhängigkeit von der Typisierung.

Ansprechpartner:in für alle Verwaltungsebenen zu FIM-Themen (Kontext-abhängig).

Ansprechpartner:in und ggf. Unterstützung bei Digitalisierungsvorhaben.

Teilnahme an Austauschformaten und (Unter-)Arbeitsgruppen für Redaktionen.

Sicherstellung der Aktualität der zum Einsatz kommenden methodischen und technischen Vorgaben sowie Werkzeuge.

Ggf. Sicherstellung des Betriebs und der Administration der eigenen FIM-Systeme.

#### Redaktionskonzept {#redaktionskonzept}

Das FIM-Redaktionskonzept definiert Rollen, die bei der Erstellung und Pflege der FIM-Stamminformationen für verschiedene Tätigkeiten zuständig sind. Das FIM-Redaktionskonzept beschreibt zudem in sogenannten Redaktionsmodellen (Prozessen) das konkrete Zusammenspiel dieser Rollen und Tätigkeiten.

#### Redaktionssystem {#redaktionssystem}

Das bausteinspezifische FIM-Redaktionssystem besteht aus einem Editor zur Erstellung der bausteinspezifischen Inhalte und einem [Repository](#repository) zur zentralen Ablage und (Versions-) Verwaltung der bausteinspezifischen Inhalte.

#### Referenzaktivitätengruppe {#referenzaktivitaetengruppe}

FIM-Stammprozesse bestehen aus Aktivitätengruppen, die sich konsequent aus den Rechts- und Verwaltungsvorschriften ableiten lassen und – in eine Reihenfolge gebracht – den Ablauf der Leistungserstellung widerspiegeln. Die Aktivitätengruppen haben, wenngleich sie thematisch zusammengehörige Prozess-Elemente (z.&nbsp;B. feingliedrigere Aufgaben) bündeln können, einen einheitlichen Detaillierungsgrad.

Die zur Modellierung von Stammprozessen verwendeten Aktivitätengruppen basieren auf vordefinierte Referenzaktivitätengruppen (RAG). Referenzaktivitätengruppen werden sowohl durch einheitlich, übergreifend definierte Kern-Metadaten als auch durch Referenzaktivitäten­gruppen spezifische Metadaten charakterisiert.

#### Referenzinformationen {#referenzinformation}

Im Gegensatz zu FIM-Stamminformationen basieren **Referenzinformationen** nicht nur auf Handlungsgrundlagen, sondern beziehen auch Best Practice Erfahrungen mit ein und können Informationen von Verwaltungskunden (Bürgern, Unternehmen) beinhalten.
Sie umfassen Vorgaben, die für mindestens zwei Organisationen anwendbar sind. Die Fülle der Rahmenbedingungen beeinflusst die Aussagekraft der Referenzinformationen und legt die Reichweite fest.

Referenzinformationen können entweder einen IST-Zustand oder einen SOLL-Zustand darstellen.

Referenzinformationen sind Referenztexte und Referenzprozesse.
Eine besondere Form der Referenzinformationen sind die [OZG-Referenzinformationen](#ozg-referenzinformation).

#### Referenzprozesse {#referenzprozess}

siehe [Referenzinformationen](#referenzinformation)

#### Referenztexte {#referenztext}

siehe [Referenzinformationen](#referenzinformation)

#### Regelwerk {#regelwerk}

Im Baustein Datenfelder wird ein **Regelwerk** eingesetzt, welches die Datenqualität in späteren Anwendungen sicherstellen sollen.
Dazu gehören Regeln zur Nutzersteuerung, Plausibilitätskontrollen und Berechnungen.

#### Repository {#repository}

Ein FIM-bausteinspezifisches Datenbanksystem, in dem die FIM-Informationen des jeweiligen [Bausteins](#baustein) verwaltet werden. Die Inhalte gliedern sich in einen [Katalog](#katalog), eine [Bibliothek](#bibliothek) und einen [Baukasten](#baukasten).

#### Schemaelementart {#schemaelementart}

In den Detailansichten eines Datenfelds oder Datenfeldgruppe bezeichnet dies die Art des Schemaelements, die abstrakt, harmonisiert oder rechtsnormgebunden ist. (Quelle: XDatenfelder-Spezifikation 3.0.0)

#### SDG {#sdg}
Die Abkürzung SDG steht für das Single Digital Gateway der Europäischen Union (EU). Nach der Verordnung der Europäischen Union soll ein einheitlicher digitaler Zugang von den Mitgliedstaaten zu bestimmten Verwaltungsleistungen für alle Bürgerinnen und Bürger sowie Unternehmen innerhalb der Europäischen Union eingerichtet werden. 

Der Begriff SDG1 bezieht sich auf den Anhang 1 der Verordnung und umfasst alle Leistungen, zu denen Informationen zu Rechten, Pflichten und Vorschriften aus dem Unionsrecht und dem nationalen Recht online zur Verfügung gestellt werden müssen.

Unter SDG2 sind Leistungen aus dem Anhang 2 aufgeführt, die von Bürgern und Bürgerinnen vollständig online abgewickelt werden können müssen. Weiterführende Informationen rund um das Thema Single Digital Gateway finden Sie auf den Seiten des Bundesministerium des Innern und für Heimat (BMI) zur digitalen Verwaltung unter: [www.digitale-verwaltung.de](https://www.digitale-verwaltung.de/Webs/DV/DE/onlinezugangsgesetz/info-sdg/info-sdg-node.html) und des IT-Planungsrates unter: [www.it-planungsrat.de](https://www.it-planungsrat.de/DE/ITPlanungsrat/OZG-Umsetzung/Portalverbund/04_SDG/SDG_node.html) sowie der Präsentation des BMI [„Single Digital Gateway der EU“](https://www.it-planungsrat.de/fileadmin/it-planungsrat/der-it-planungsrat/fachkongress/fachkongress_2018/Tag2_RadB_SDG.pdf). Quelle: FAQ der OZG-IP

#### SDG Informationsbereiche {#sdg-informationsbereich}
Zuordnung der SDG Informationsbereiche, die für Bürger und Unternehmen relevant sind, die ihre Binnenmarktrechte ausüben. Eine Codeliste findet sich im [XRepository](https://www.xrepository.de/details/urn%253Axoev-de%253Afim%253Acodeliste%253Asdginformationsbereich).

#### Stamminformationen {#stamminformation}

siehe [FIM-Stamminformationen](#fim-stamminformation)

#### Stammtext {#stammtext}
Der Stammtext erklärt in einer bürgerfreundlichen Sprache den Gegenstand einer Leistung, besteht aus einer festen Anzahl inhaltlicher Module und ist Grundlage für eine Leistungsbeschreibung.

#### Status {#status}

Der Status einer FIM-Information gibt an, in welcher Phase des Lebenszyklus sich die FIM-Information befindet und welchen Reifegrad die FIM-Information damit hat. Nutzer der FIM-Informationen können so bewerten, ob diese ihren Anforderungen entspricht und von Ihnen genutzt werden kann. Eine FIM-Information kann einzelne Status überspringen, z.B. direkte Veröffentlichung einer FIM-Information im "fachlich freigegeben (gold)".
 
**Aktuelle Status-Werte:**

- **in Planung:** Es ist geplant, die Erstellung einer FIM-Information zu beginnen.
- in Bearbeitung: Die Bearbeitung einer neuen FIM-Information ist noch nicht abgeschlossen.
- **Entwurf:** Eine FIM-Information liegt im Entwurf vor. Entwürfe dienen der Vorabinformationen, können aber bis zu den fachlich freigegebenen Versionen substantiell geändert werden.
- methodisch freigegeben: FIM-Informationen, die methodisch freigegeben sind, entsprechen den strukturellen Vorgaben der FIM-Methodik und den Qualitätssicherungskriterien. Eine abschließende fachliche Prüfung ist nicht erfolgt.
- **fachlich freigegeben (silber):** Liegen FIM-Informationen im "Silber-Status" vor, wurde die fachliche Korrektheit stellvertretend von einem Ministerium oder einer vergleichbaren Behörde auf Landes- oder kommunaler Ebene festgestellt, welche unterhalb der für die Rechtsgrundlage federführenden Behörde liegt. Oft wird eine fachliche Freigabe im "Silber-Status" durch ein Landesministerium stellvertretend für ein Bundesministerium erteilt.
[Verdeutlichung des Verfahrens zum Silber/Gold-Status bei Typ 2/3 Stammdatenschemata und Stammprozessen](./ressources/20221013_Informationsunterlage_Uebergang_PZ_u_DF_Bundesredaktion_Silberstatus_v1.1.pdf)
- **fachlich freigegeben (gold):** Liegen FIM-Informationen im "Gold-Status" vor, wurde die fachliche Korrektheit von dem für die Rechtsgrundlage federführenden Ministerium oder einer vergleichbaren Behörde auf Bundes-, Landes- oder kommunaler Ebene festgestellt.
- **inaktiv:** Sollte eine FIM-Information nicht mehr verwendet werden, z.&nbsp;B. weil diese nicht mehr gültig ist oder eine fehlerkorrigierte Version vorliegt, wird dies durch den Status „inaktiv“ verdeutlicht.
- **vorgesehen zum Löschen:** Der Betreiber des Bausteins Leistungen setzt diesen Status, wenn ein Leistungssteckbrief gelöscht werden soll. Damit verbunden startet eine 6-wöchige Löschfrist. Innerhalb dieser haben die zentrale Bundesredaktion und die Landesredaktionen die Möglichkeit, Einwände zurückzumelden. In diesem Fall wird die Löschfrist ausgesetzt. Nach Ablauf der Löschfrist wird der Status des Leistungssteckbriefs auf „inaktiv“ gesetzt.

#### Status gesetzt am {#status_gesetzt_am}

In den Detailansichten des FIM-Baustein Datenfelder bezeichnet dies das Datum der Freigabe durch die Freigabeinstanz. (Quelle: XDatenfelder-Spezifikation 3.0.0)

#### Status gesetzt durch {#status_gesetzt_durch}

In den Detailansichten des FIM-Baustein Datenfelder bezeichnet dies die Person oder erstellende Institution, die den Status gesetzt hat. (Quelle: XDatenfelder-Spezifikation 3.0.0)

#### Stichwörter {#stichwoerter}

In der Detailansicht eines Dokumentsteckbriefs oder Datenschemas zumeist Kategorisierungen wie z.B. Anwendungsgebiet (dann mit einem zugeordneten Bundesland) oder Detaillierungsstufe (dann mit einem Wert wie "Referenzinformation"). In selteneren Fällen sind die Begriffe frei gewählt (z.B. "DSGVO" mit Wert "keine personenbezogenen Daten"). (Quelle: u. a. XDatenfelder-Spezifikation 3.0.0)

#### Teaser {#teaser}

In der Beschreibung einer Leistung dient der Teaser-Text dazu, die Leistung in Suchergebnislisten kurz zu erläutern.

#### Technische Beschreibung {#technische_beschreibung}

In der Detailansicht eines Dokumentsteckbriefs oder Datenschemas eine zusätzliche Beschreibung und Erläuterung des Elements für FIM-Nutzer. (Quelle: XDatenfelder-Spezifikation 3.0.0)

#### Typisierung {#typisierung}

Mit der Typisierung

- wird eindeutig geregelt, durch welche Ebene die Stamminformation erstellt (Regelungszuständigkeiten) und durch welche Ebenen die Stamminformation zu Vollzugsinformationen detailliert wird (Vollzugszuständigkeiten). Ist der Vollzug einer Leistung in den Ländern verschieden geregelt, so ist eine abschließende Typisierung im Stammtext nicht möglich. In diesem Fall ist die Vollzugszuständigkeit (a oder b) als Ergänzung zu vermerken (Typisierungen „1“ bis „5“ und „10“) bzw.
- allgemeine Hinweise mit informativem Charakter, die keine bestimmte Leistung(serbringung) betreffen; konkrete Hinweise zu Service- und Sonderrufnummern oder zu thematisch zusammenhängenden Leistungen gegeben (Typisierungswerte „6“, „7“ und „8“)

Die aktuelle Codeliste zur Typisierung finden Sie auf [www.xRepository.de](https://www.xrepository.de/details/urn:de:fim:leika:typisierung)

#### Ursprungsportal {#ursprungsportal}
In Deutschland stellt der sogenannte Portalverbund sicher, dass die Verwaltungsportale von Bund und Ländern miteinander verknüpft werden. Leistungsbeschreibungen können im Portalverbund allen angeschlossenen Portalen zur Verfügung gestellt werden. Damit ist es Bürgerinnen / Bürgern und Unternehmen möglich, über jedes an den Portalverbund angeschossene Verwaltungsportal die jeweils benötigte Verwaltungsleistung unabhängig vom eigenen Standort finden und ggf. in Anspruch nehmen zu können. Die Herkunft (Quelle) einer im Portalverbund verfügbaren Leistungsbeschreibung wird zum Zweck der Identifizier- bzw. Rückverfolgbarkeit im Modul „Ursprungsportal“ dokumentiert. (Quelle: Baustein Datenfelder)


#### Verfahrensablauf {#verfahrensablauf}
In der Beschreibung einer Leistung ist hier Schritt für Schritt erklärt, was Antragstellende zu tun haben, z.B. Prüfung des Antrags, Bescheid. Nicht gemeint sind verwaltungsinterne Vorgänge. Schriftliche Verfahren und Onlineverfahren können getrennt beschrieben sein.

#### Verfahrensart {#verfahrensart}

Ein Merkmal bei der Beschreibung einer [Prozessklasse](#prozessklasse). Eine Liste der Arten von Verfahren zu einem Prozess.

Quelle: [XRepository](https://www.xrepository.de/details/urn:xoev-de:fim:codeliste:verfahrensart)

#### Verfügbare Versionen {#verfuegbare_versionen}

In den Detailansichten des FIM-Baustein Datenfelder sind dies alle bisher veröffentlichten und noch nicht zum Löschen vorgesehenen oder inaktive Versionen eines Dokumentsteckbriefs, Datenschemas, Datenfeldgruppe oder Datenfelds.

#### Veröffentlicht am {#veroeffentlicht_am}

In den Detailansichten des FIM-Baustein Datenfelder ist dies das Datum, an dem das Element veröffentlicht wurde. (Quelle: XDatenfelder-Spezifikation 3.0.0)

#### Verrichtungskennung {#verrichtungskennung}

Die Verrichtungskennung (VK) beschreibt das Verwaltungshandeln in Bezug auf das Leistungsobjekt aus der Perspektive der Verwaltung. Sie wird aus einer [XRepository-Codeliste](https://www.xrepository.de/details/urn:xoev-de:fim:codeliste:xzufi.verrichtungskennung) ausgewählt.

#### Verrichtungsdetail {#verrichtungsdetail}

Das Verrichtungsdetail (VD) spezifiziert die Verrichtungskennung, v. a. in Bezug auf verschiedene Verfahrensabläufe, Zielgruppen oder Ausnahmen innerhalb einer Leistung

#### Versionshinweis {#versionshinweis}

In den Detailansichten des FIM-Baustein Datenfelder, der Prozessklasse oder des Prozess ein kurzer Text zur Beschreibung der jeweiligen Version, beispielsweise mit Änderungen zur Vorgängerversion.

#### Verwendet in Datenschemata {#verwendet_in_datenschemata}

In der Detailansicht einer Datenfeldgruppe oder eines Datenfeldes finden sich hier Referenzen auf Datenschemata, in denen dieses Element verwendet wird.

#### Volltext {#volltext}
In der Beschreibung einer Leistung dient der Volltext dazu, die Leistung ausführlich in Bürgersprache zu beschreiben

#### Voraussetzungen {#voraussetzungen}
In der Beschreibung einer Leistung sind hier die Voraussetzungen detailliert, die Antragstellende erfüllen müssen, um die Leistung zu erhalten oder beantragen zu können (Antragsvoraussetzungen).

#### Weiterführende Informationen {#weiterfuehrende-informationen}
In der Beschreibung einer Leistung wird hier detailliert, welche weiterführenden Informationen zur Leistung im Internet bereits veröffentlicht sind. Dies können -	Verweise auf Seiten mit weiterführenden Informationen (zum Beispiel Fachportale, Themenportale, Broschüren) sein.

#### Werkzeug {#werkzeug}

FIM-Werkzeuge sind alle Konzepte, Methoden und Standards, die zur Erstellung, Pflege und Nutzung von FIM-Inhalten genutzt werden. Darunter fallen beispielsweise Dokumentenstandards (z.&nbsp;B. OpenDocument), Metadatenstandards, Datenaustauschformate (z.&nbsp;B. XML) oder die Datenfeld- und Leistungsklassifikation.

#### XDatenfelder {#xdatenfelder}
Ein [XÖV-Standard](https://www.xrepository.de/details/urn:xoev-de:fim:standard:xdatenfelder) zu Übertragung und Veröffentlichung der Inhalte des FIM Bausteins Datenfelder.

#### XDF-Version {#xdfversion}
Die Version des [XDatenfelder](#xdatenfelder)-Standards, die zur Modellierung von Datenfeldern verwendet wird. Geläufig sind 2.0 und 3.0.0.

#### Zugehörige Datenschemata {#zugehoerige_datenschemata}

In der Detailansicht eines Dokumentsteckbriefs finden sich hier Referenzen des Dokumentsteckbriefs auf diejenigen Datenschemata, die bei der Bearbeitung des Dokumentsteckbriefs für diesen erstellt bzw. diesem zugeordnet wurden.

In der Detailansicht eines Datenschemas finden sich hier Referenzen des Datenschemas auf andere Datenschemata, die untereinander in Beziehung ("Relation") stehen, z.B. "ist abgeleitet von" oder "ersetzt". 

#### Zugehörige Dokumentsteckbriefe {#zugehoerige_dokumentsteckbriefe}

In der Detailansicht eines Dokumentsteckbriefs finden sich hier Referenzen des Dokumentsteckbriefs auf andere Dokumentsteckbriefe, die untereinander in Beziehung ("Relation") stehen, z.B. "ist abgeleitet von" oder "ersetzt".

In der Detailansicht eines Datenschemas finden sich hier Referenzen des Dokumentsteckbriefs, denen das Datenschema bei der Bearbeitung des Dokumentsteckbriefs zugeordnet wurde.

#### Zugehörige Felder  {#zugehoerige_felder}

In der Detailansicht eines Felds finden sich hier Referenzen des Felds auf andere Felder, die untereinander in Beziehung ("Relation") stehen, z.B. "ist abgeleitet von" oder "ersetzt".

#### Zugehörige Gruppen {#zugehoerige_gruppen}

In der Detailansicht einer Datenfeldgruppe finden sich hier Referenzen der Gruppe auf andere Gruppen, die untereinander in Beziehung ("Relation") stehen, z.B. "ist abgeleitet von" oder "ersetzt".

#### Zugehörige Prozesse {#zugehoerige_prozesse}

In der Detailansicht eines Dokumentsteckbriefs finden sich hier Referenzen des Dokumentsteckbriefs auf diejenigen Prozesse, bei deren Bearbeitung der Dokumentsteckbrief zugeordnet wurde.

#### Zuschnittsindikator {#zuschnittsindikator}

Ein Zuschnittsindikator in der [FIM-Normenanalyse](#normenanalyse) ist ein Anhaltspunkt, an welcher Stelle ein Prozess/eine Leistungsverrichtung beginnt und an welcher Stelle der Prozess/die Leistungsverrichtung endet. Die konsequente Anwendung festgelegter Zuschnittsindikatoren ermöglicht einen einheitlichen und vergleichbaren [Leistungszuschnitt](#leistungszuschnitt).

Die Zuschnittsindikatoren sind im [„bausteinübergreifenden Leitfaden für die Identifikation von Verwaltungsleistungen und Prozessen im Rahmen der Normenanalyse“](./ressources/20200506_Leistungen_Leitfaden_Leistungszuschnitt_1.0.pdf) erläutert.

#### zuständige Stelle {#zustaendige-stelle}
In der Beschreibung einer Leistung ist dies die örtlich und sachlich zuständige Stelle.