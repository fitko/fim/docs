# Roadmap

Das Sammelrepository für den Baustein Datenfelder wurde in 2023 entwickelt:

Zukünftige Erweiterungen hängen vom Bedarf ab und umfassen - ohne Priorisierung, ohne zeitliche Festlegungen und ohne Umsetzungs-Zusage:

* Anbindung Baustein Leistung
* Anbindung Baustein Prozess
* Abruf einzelner Baukastenelemente (Datenfeldgruppe, Datenfeld, Regel)
* Benutzeroberfläche in Produktionsqualität (in Abgrenzung zur vorhanden [Test-UI](http://schema.fim.fitko.net/))
* Beiträge zu einem für die Nachnutzung geeigneten Regelwerk
* Beiträge zu einer für die Visualisierung von XDatenfeld-Dateien geeigneten Formular-Vorschau
* Beiträge zu einem Mapping zwischen Elementen eines Stammdatenschemas und eines Referenzdatenschemas
* Beiträge zur leichteren Identifikation von semantisch gleichwertigen Baukastenelementen

Die Gestaltung der Roadmap hängt davon ab, dass und welche Bedarfsmeldungen uns erreichen.