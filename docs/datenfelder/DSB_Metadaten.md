# Informationen zu den Metadaten

## Name

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | FIM-interner Name des Dokumentsteckbriefs, sichtbar für alle Anwender eines Redaktionssystems des Bausteins Datenfelder (fachliche Ersteller, Methodenexperten, Informationsmanager, FIM-Koordinierungsstelle, FIM-Nutzer).|
| Zielgruppe      | Modellierende |
| Pflichtfeld | Ja|
| Elementspezifisches Qualitätskriterium |[Konstruktionsregeln für die Namensgebung](DSB_Namensgebung) werden in einem gesonderten Abschnitt betrachtet.|
| Empfehlung für die maximale Länge |255 Zeichen|
| Art der Prüfung     | Komplett manuell |
| Verweis auf den QS-Bericht      | x |
| Beispiel      |Beispiele finden sich auch im [Abschnitt zur Namensgebung](DSB_Namensgebung).|
|mögliche Abweichungen im Referenzkontext| nein |

## Definition

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Fasst den Inhalt eines Dokumentsteckbriefs aus redaktioneller Sicht zusammen, d.h. aus Sicht der Rollen Ersteller, Methodenexperte, Informationsmanager, FIM- Koordinierungsstelle, FIM-Nutzer.|
| Zielgruppe      | Modellierende, Fachlichkeit |
| Pflichtfeld | Ja|
| Elementspezifisches Qualitätskriterium |In dieser Angabe sollte möglichst verständlich und prägnant wiedergegeben werden, was die Bewandtnis dieses Antrags bzw. dieser sonstigen Dokumentart sein soll. Sie schafft den eigentlichen Kontext für die, dem Dokumentsteckbrief zugeordneten, Stammdatenschemata. Entsprechende Angaben sind gemäß von Rechtsgrundlagen oder Standardisierungsvorhaben zu machen.|
| Empfehlung für die maximale Länge |5000 Zeichen|
| Art der Prüfung     | Komplett manuell |
| Verweis auf den QS-Bericht      | x |
| Beispiel      |<ol><li>Aus Dokumentsteckbrief „Antrag Leistungen zur Sicherung des Lebensunterhalts“: <br/> *Leistungen zur Sicherung des Lebensunterhalts umfassen Arbeitslosengeld II, Sozialgeld und Leistungen für Bildung und Teilhabe (§ 19 SGB II)*</li><li>Aus Dokumentsteckbrief „Antrag auf Ausstellung einer Eheurkunde“:<br/>*Eine Eheurkunde dient zum Nachweis einer Ehe und wird vom zuständigen Standesamt aus dem Eheregister ausgestellt. Sie kann von denjenigen Personen beantragt werden, auf die sich der Registereintrag bezieht, sowie deren Ehegatten, Lebenspartnern, Vorfahren und Abkömmlingen. Andere Personen haben ein Recht auf Erteilung von Eheurkunden, wenn sie ein rechtliches oder berechtigtes Interesse glaubhaft machen können.*</li></ol>|
|mögliche Abweichungen im Referenzkontext| nein |

## Bezug zur Handlungsgrundlage

:::info
Die Namensgebung zu diesem Metadatum hat sich geändert, in XDatenfelder 2 hieß dieses Metadatum noch **Bezug zu Rechtsnorm oder Standardisierungsvorhaben**.
:::

### Bezug zur Handlungsgrundlage

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | |
| Zielgruppe      |  Modellierende, Fachlichkeit|
| Pflichtfeld | Ja|
| Elementspezifisches Qualitätskriterium |<ol><li>Falls die Bezugsquelle der Rechtsnorm oder des Standardisierungsvorhabens eine eigene Notation vorsieht, dann ist diese zu befolgen.</li><li>Bei Angaben zu einem Bezug sollen diese möglichst konkret sein.</li><li>Die Regeln zur Erfassung von Rechtsbezügen sind zu beachten.</li></ol>|
| Empfehlung für die maximale Länge |255 Zeichen pro Bezug; ein Element kann davon mehrere haben.|
| Art der Prüfung     | Komplett manuell |
| Verweis auf den QS-Bericht      | x |
| Beispiel      |<ol><li>Aus „Urkunde Waffenbesitzkarte“: § 14 (4) WaffG Abschnitt 1.4 WaffVordruckVwV i.V.m. Anlage 4 WaffVordruckVwV</li><li>Aus „Antrag Berechtigungszertifikat“: § 21 ff. Personalausweisgesetz (PAuswG); § 29 Personalausweisverordnung (PAuswV)</li></ol>|
|mögliche Abweichungen im Referenzkontext| nein |

### Link :new:

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Dieses Attribut ermöglicht es, zu jedem Bezug einen entsprechenden Link anzugeben.|
| Zielgruppe      | Modellierende, Fachlichkeit |
| Pflichtfeld | Nein|
| Elementspezifisches Qualitätskriterium |<ol><li>Es ist immer ein gültiger Link zu erfassen.</li><li>Eine gute Quelle für Links zu Rechtsgrundlagen finden sich unter: [Gesetze im Internet](https://www.gesetze-im-internet.de/)</li><li>Eine gute Quelle für Links zu XÖV-Standards findet sich unter: [XRepository 3.0](https://www.xrepository.de/)</li><li>Es sind keine unpassenden Links zu ergänzen.</li></ol>|
| Empfehlung für die maximale Länge |255 Zeichen|
| Art der Prüfung     | Exemplarisch manuell |
| Verweis auf den QS-Bericht      | x |
| Beispiel      |DOKUMENTSTECKBRIEF BEISPIELE ERGÄNZEN <ol><li>Datenfeld “meldepflichtige Krankheiten” basierend auf § 6 IfSG - Link: § 6 IfSG - Einzelnorm </li><li>Datenfeld “Ort der wirtschaftlichen Tätigkeit” Xunternehmen.Kerndatenmodell.Ort der wirtschaftlichen Tätigkeit.Art Version 1.0; basierend auf Codeliste urn:xoev-de:xunternehmen:codeliste:artortwirtschaftlichetaetigkeit_1 Version 1 Link: XRepository 3.0</li></ol> |
|mögliche Abweichungen im Referenzkontext| nein |

## Bezeichnung

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Bezeichnung des Dokumentsteckbriefs, welche für den Bürger/das Unternehmen über den Baustein Leistungen sichtbar ist.|
| Zielgruppe      | Modellierende, Fachlichkeit, Dienstleister nachnutzender Systeme, Antragstellende |
| Pflichtfeld | Ja|
| Elementspezifisches Qualitätskriterium ||
| Empfehlung für die maximale Länge |255 Zeichen|
| Art der Prüfung     | komplett manuell |
| Verweis auf den QS-Bericht      | x |
| Beispiel      | |
|mögliche Abweichungen im Referenzkontext| nein |

## Dokumentart :new:

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition |Über die Dokumentart wird an einem Dokumentsteckbrief hinterlegt, um welche Art von Dokument es sich bei diesem Steckbrief handelt.|
| Zielgruppe      | Modellierende, Fachlichkeit |
| Pflichtfeld | Ja, ein geeigneter Defaultwert ist **unbestimmt**.|
| Elementspezifisches Qualitätskriterium |<ul><li>Es ist immer eine der im Standard vordefinierten Dokumentarten anzuwenden. Die Liste der Dokumentarten findet sich im XRepository unter: [urn:xoev-de:fim-datenfelder:codeliste:dokumentart](https://www.xrepository.de/details/urn:xoev-de:fim-datenfelder:codeliste:dokumentart)</li><li>Der Defaultwert **unbestimmt** muss bei der konkreten Erstellung eines Dokumentsteckbriefs angepasst werden. Nur wenn in der Liste tatsächlich keine passende Dokumentart vorhanden ist, darf **unbestimmt** als Wert gesetzt werden.</li></ul>|
| Empfehlung für die maximale Länge |nicht relevant|
| Art der Prüfung     | keine |
| Verweis auf den QS-Bericht      | x |
| Beispiel |<ul><li>Für den Dokumentsteckbrief *Antrag Führerschein Ausstellung* lautet die passende Dokumentart *001 - Antrag*</li><li>Für den Dokumentsteckbrief *Führerschein* ist die passende Dokumentart *011 - Urkunde*</li></ul>|
|mögliche Abweichungen im Referenzkontext| nein |

## Ist abstrakt? :new:

|    Aspekt     | Beschreibung |
|:--------------:|-----|
| Definition | Durch dieses Metadaten wird eindeutig beschrieben, ob es sich um einen abstrakten Dokumentsteckbrief handelt.|
| Zielgruppe      | Modellierende, Fachlichkeit |
| Pflichtfeld | Ja, ein geeigneter Defaultwert lautet **nein**|
| Elementspezifisches Qualitätskriterium |<ul><li>Ein Dokumentsteckbrief kann entweder abstrakt sein oder nicht. Weiter Auswahloptionen gibt es nicht.</li><li>Im Rahmen der normalen Modellierung von Datenfeldern werden von den Modellierenden in der Regel keine abstrakten Dokumentsteckbriefe erstellt.</li><li>Mehr Informationen zu abstrakten Dokumentsteckbriefen gibt es HIER!!!!!!</li></ul>|
| Empfehlung für die maximale Länge |nicht relevant|
| Art der Prüfung     | keine |
| Verweis auf den QS-Bericht      | x |
| Beispiel      |<ul><li>Der Dokumentsteckbrief *Antrag Führerschein Ausstellung* ist *nicht-abstrakt*.</li><li>Der Dokumentsteckbrief *Identitätsnachweis* ist *abstrakt*.</li><li>Der Dokumentsteckbrief *Reisepass* ist *nicht-abstrakt*.</li></ul> |
|mögliche Abweichungen im Referenzkontext| nein |
