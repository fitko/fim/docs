Bereitstellung der technischen Infrastruktur für die FIM-Bausteine (u. a. zentrale FIM-Repositorys). <br/>Pflege der Kataloge und Baukästen. <br/>Ansprechpartner:in und fachliche Unterstützung für die FIM-Rollen und Organisationseinheiten im Redaktionsprozess.

