# Versionierung

## Grundsätzliches über Versionierung

Alle Arbeitskopien von eigenständigen Elementen des FIM-Bausteins Datenfelder können versioniert werden. Das betrifft also Datenfelder, Datenfeldgruppen, Datenschemata und Dokumentsteckbriefe. Regeln sind davon nicht betroffen, da eine Regel kein eigenständiges Element ist. Regeln werden immer im Zusammenhang mit den Elementen, an denen sie hängen, versioniert.

## Wann sollte versioniert werden?

Es wird empfohlen Datenschemata im speziellen ab dem Zeitpunkt zu versionieren, ab dem erste Arbeitsergebnisse mit der Fachlichkeit geteilt werden sollen. So stellt man sicher, dass man einen definierten festgehaltenen Arbeitsstand hat, über den man spricht.

Falls dies nicht erfolgt ist, steht spätestens zum Zeitpunkt der Freigabe die erste Versionierung an, da nur Versionen freigegeben und nur freigegebene Elemente veröffentlicht werden dürfen.

Datenfelder oder Datenfeldgruppen können bei Bedarf auch erst im Zuge der Versionierung ihres übergeordneten Datenschemas versioniert werden.

Auch vor einer methodischen Prüfung ist es sinnvoll, eine Version zu erstellen.

## Wer erstellt Versionen?

Versionen können durch die Modellierenden selbst erstellt.

## Was sind Major, Minor und Micro Version und wann werden diese verwendet?

Bei der Versionierung wird zwischen Major, Minor und Micro Version unterschieden.

### Major Version

Die Major Version ist die erste Stelle der Versionskennung, z. B. bei V2.1.6 gibt die 2 die Major Version an.

Major Versionen sollten nur dann erzeugt werden, wenn substantielle Änderungen
vorgenommen wurden, z. B. wenn eine Novellierung der Handlungsgrundlage vorliegt oder wenn in einem Stammdatenschema mehrere Elemente ergänzt bzw. ersetzt worden sind.

### Minor Version

Die Minor Version ist die zweite Stelle der Versionskennung, z. B. bei V2.1.6 gibt die 1 die Minor Version an.

Falls nur einige wenige, mittelgroße Änderungen, wie z. B. das Austauschen weniger Elemente, vorgenommen wurden, eignet sich die Minor Version.

### Micro Version

Die Micro Version ist die dritte Stelle der Versionskennung, z. B. bei V2.1.6 gibt die 6 die Micro Version an.

Eine Micro Versionsänderungen ist beispielsweise dann zu verwenden, wenn Rechtschreibfehler ausgebessert
oder andere kleinere Änderungen an einzelnen Element vorgenommen wurden.

## Löschung von Versionen

Es ist möglich eine Version im Nachhinein zu löschen, sofern sie nicht schon in Benutzung ist,
fachlich freigegeben oder veröffentlicht wurde. Dies sollte aber nur im Ausnahmefall geschehen,
wenn z. B. noch kritische Fehler behoben werden müssen oder Versionsnummern angeglichen werden sollen.

## Veränderliche Parameter einer Version

Ein versioniertes Element ist zeitlich fixiert und damit revisionssicher. Nur wenige Parameter von Versionen können im Nachhinein verändert werden, dazu zählen:
- (Freigabe-) Status
- Veröffentlichungsflag
- Relationen - diese können auch zwischen versionierten und nicht versionierten Elementen existieren
- Zuordnung eines Dokumentsteckbriefs zu einem Datenschema.
