Dies ist eine Datei zur Übung. Sie wird nicht veröffentlicht.


Übungsaufgabe: 
Füge deinen Namen in die u.s. Liste als neuen Bulletpoint ein und "committe" deine Änderung im System.


# Übung durchgeführt:
- Thilo
- Paulina
- Daniel
- Benjamin
- Julia
- Nicole
- David


# Übung 2 (optional) - Tragt einen Smiley hinter eurem Namen ein
- Thilo :)
- Paulina
- Daniel :)
- Benjamin
- Julia
- Nicole
- David :)
