---
sidebar_label: Anleitungen
---

# Anleitungen

Die Anleitungen zielen auf die Lösung praktischer Probleme bei der Anwendung von FIM

- Leistungszuschnitt - wie man für eine Leistung den richtigen Rahmen und Leistungsschlüssel findet
- Bedarfsmeldung - wie man eine Leistung "zur Welt bringt" und wie man sie später verändern kann
- Erstellung eines Datenschemas - Wie man für eine Onlineleistung das zugehörige Datenschema modelliert
- Modellierung eines Prozesses mit Adonis
- Mitarbeit an der FIM-Dokumentation
- Suche nach FIM-Daten
- Meldung veralteter FIM-Daten
